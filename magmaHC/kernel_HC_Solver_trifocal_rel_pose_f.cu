#ifndef kernel_HC_Solver_3view_unknownf_cu
#define kernel_HC_Solver_3view_unknownf_cu
// ============================================================================
// homotopy continuation solver for 3view_unknownf problem
//
// Modifications
//    Chien  21-09-16:   3view_unknownf benchmark problem; originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// cuda included
#include <cuda.h>
#include <cuda_runtime.h>

// magma
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

// -- header --
#include "magmaHC-kernels.h"

// -- device function --
#include "dev-eval-indxing-3view_unknownf.cuh"
#include "dev-cgesv-batched-small.cuh"
#include "dev-get-new-data.cuh"

namespace magmaHCWrapper {

  template<int N, int coefsCount, int max_steps, int max_corr_steps, int predSuccessCount>
  __global__ void
  homotopy_continuation_solver_3view_unknownf(
    magma_int_t ldda,
    magmaFloatComplex** d_startSols_array, magmaFloatComplex** d_Track_array,
    magmaFloatComplex** d_startCoefs_array, magmaFloatComplex** d_targetCoefs_array,
    magmaFloatComplex** d_cgesvA_array, magmaFloatComplex** d_cgesvB_array,
    magmaInt_ptr d_const_mat_Hx_scalar, magmaInt_ptr d_const_matrix_Hx_X1,
    magmaInt_ptr d_const_matrix_Hx_X2, magmaInt_ptr d_const_matrix_Hx_X3,
    magmaInt_ptr d_const_matrix_Hx_X4, magmaInt_ptr d_const_mat_Hx_Y,
    magmaInt_ptr d_const_matrix_Ht_s, magmaInt_ptr d_const_matrix_Ht_X_collection,
    magmaInt_ptr d_const_matrix_Ht_Y, magmaFloatComplex_ptr d_const_cd)
  {
    extern __shared__ magmaFloatComplex zdata[];
    const int tx = threadIdx.x;
    const int batchid = blockIdx.x ;

    magmaFloatComplex* d_startSols = d_startSols_array[batchid];
    magmaFloatComplex* d_track = d_Track_array[batchid];
    magmaFloatComplex* d_startCoefs = d_startCoefs_array[0];
    magmaFloatComplex* d_targetCoefs = d_targetCoefs_array[0];
    magmaFloatComplex* d_cgesvA = d_cgesvA_array[batchid];
    magmaFloatComplex* d_cgesvB = d_cgesvB_array[batchid];
    const magma_int_t* d_Hx_const_mat_s = d_const_mat_Hx_scalar;
    const magma_int_t* d_Hx_const_mat_X1 = d_const_matrix_Hx_X1;
    const magma_int_t* d_Hx_const_mat_X2 = d_const_matrix_Hx_X2;
    const magma_int_t* d_Hx_const_mat_X3 = d_const_matrix_Hx_X3;
    const magma_int_t* d_Hx_const_mat_X4 = d_const_matrix_Hx_X4;
    const magma_int_t* d_Hx_const_mat_Y = d_const_mat_Hx_Y;
    const magma_int_t* d_Ht_const_mat_s = d_const_matrix_Ht_s;
    const magma_int_t* d_Ht_const_mat_X = d_const_matrix_Ht_X_collection;
    const magma_int_t* d_Ht_const_mat_Y = d_const_matrix_Ht_Y;
    const magmaFloatComplex* d_const_vec_cd = d_const_cd;

    // -- registers declarations --
    magmaFloatComplex r_cgesvA[N] = {MAGMA_C_ZERO};
    magmaFloatComplex r_cgesvB = MAGMA_C_ZERO;
    int linfo = 0, rowid = tx;
    float t0 = 0.0, t_step = 0.0, delta_t = 0.05;
    bool inf_failed = 0;
    bool end_zone = 0;

    // -- shared memory declarations --
    magmaFloatComplex *s_startCoefs = (magmaFloatComplex*)(zdata);
    magmaFloatComplex *s_targetCoefs = s_startCoefs + coefsCount;
    magmaFloatComplex *s_cdt = s_targetCoefs + coefsCount;
    magmaFloatComplex *s_sols = s_cdt + coefsCount;
    magmaFloatComplex *s_track = s_sols + (N+1);
    magmaFloatComplex *s_track_last_success = s_track + (N+1);
    magmaFloatComplex *sB = s_track_last_success + (N+1);
    magmaFloatComplex *sx = sB + N;
    float* dsx = (float*)(sx + N);
    int* sipiv = (int*)(dsx + N);
    float *s_sqrt_sols = (float*)(sipiv + N);
    float *s_sqrt_corr = s_sqrt_sols + N;
    float *s_norm = s_sqrt_corr + N;
    bool s_isSuccessful = (bool)(s_norm + 2);
    int s_pred_success_count = (int)(s_isSuccessful + 1);

    // -- initialization: read from gm --
    #pragma unroll
    for(int i = 0; i < N; i++) {
      r_cgesvA[i] = d_cgesvA[ i * ldda + tx ];
    }
    r_cgesvB = d_cgesvB[tx];

    #pragma unroll
    for(int i = 0; i < coefsCount; i++) {
      s_startCoefs[i] = d_startCoefs[i];
      s_targetCoefs[i] = d_targetCoefs[i];
      s_cdt[i] = d_startCoefs[i];
    }
    s_sols[tx] = d_startSols[tx];
    s_track[tx] = d_track[tx];
    s_track_last_success[tx] = s_track[tx];
    s_sqrt_sols[tx] = 0;
    s_sqrt_corr[tx] = 0;
    s_isSuccessful = 0;
    s_pred_success_count = 0;
    if (tx == 0) {
      s_sols[N] = MAGMA_C_MAKE(1.0, 0.0);
      s_track[N] = MAGMA_C_MAKE(1.0, 0.0);
      s_track_last_success[N] = MAGMA_C_MAKE(1.0, 0.0);
    }
    __syncthreads();

    float one_half_delta_t;   // -- 1/2 \Delta t --

    //#pragma unroll
    for (int step = 0; step <= max_steps; step++) {
      if (t0 < 1.0 && (1.0-t0 > 0.001)) {
        t_step = t0;
        one_half_delta_t = 0.5 * delta_t;
        // ===================================================================
        // -- Runge-Kutta Predictor --
        // ===================================================================
        // -- get HxHt for k1 --
        eval_cdt_3view_unknownf<N, coefsCount>( tx, t0, s_cdt, s_startCoefs, s_targetCoefs );
        eval_Jacobian_3view_unknownf<N, coefsCount>(tx, s_track, r_cgesvA, d_Hx_const_mat_s, d_Hx_const_mat_X1, d_Hx_const_mat_X2, d_Hx_const_mat_X3, d_Hx_const_mat_X4, d_Hx_const_mat_Y, s_cdt);
        eval_Ht_3view_unknownf(tx, s_track, r_cgesvB, d_Ht_const_mat_s, d_Ht_const_mat_X, d_Ht_const_mat_Y, d_const_vec_cd);

        // -- solve k1 --
        cgesv_batched_small_device<N>( tx, r_cgesvA, sipiv, r_cgesvB, sB, sx, dsx, rowid, linfo );
        magmablas_syncwarp();

        // -- compute x for the creation of HxHt for k2 --
        create_x_for_k2( tx, t0, delta_t, one_half_delta_t, s_sols, s_track, sB );
        magmablas_syncwarp();

        // -- get HxHt for k2 --
        eval_cdt_3view_unknownf<N, coefsCount>( tx, t0, s_cdt, s_startCoefs, s_targetCoefs );
        eval_Jacobian_3view_unknownf<N, coefsCount>(tx, s_track, r_cgesvA, d_Hx_const_mat_s, d_Hx_const_mat_X1, d_Hx_const_mat_X2, d_Hx_const_mat_X3, d_Hx_const_mat_X4, d_Hx_const_mat_Y, s_cdt);
        eval_Ht_3view_unknownf(tx, s_track, r_cgesvB, d_Ht_const_mat_s, d_Ht_const_mat_X, d_Ht_const_mat_Y, d_const_vec_cd);

        // -- solve k2 --
        cgesv_batched_small_device<N>( tx, r_cgesvA, sipiv, r_cgesvB, sB, sx, dsx, rowid, linfo );
        magmablas_syncwarp();

        // -- compute x for the generation of HxHt for k3 --
        create_x_for_k3( tx, delta_t, one_half_delta_t, s_sols, s_track, s_track_last_success, sB );
        magmablas_syncwarp();

        // -- get HxHt for k3 --
        eval_cdt_3view_unknownf<N, coefsCount>( tx, t0, s_cdt, s_startCoefs, s_targetCoefs );
        eval_Jacobian_3view_unknownf<N, coefsCount>(tx, s_track, r_cgesvA, d_Hx_const_mat_s, d_Hx_const_mat_X1, d_Hx_const_mat_X2, d_Hx_const_mat_X3, d_Hx_const_mat_X4, d_Hx_const_mat_Y, s_cdt);
        eval_Ht_3view_unknownf(tx, s_track, r_cgesvB, d_Ht_const_mat_s, d_Ht_const_mat_X, d_Ht_const_mat_Y, d_const_vec_cd);

        // -- solve k3 --
        cgesv_batched_small_device<N>( tx, r_cgesvA, sipiv, r_cgesvB, sB, sx, dsx, rowid, linfo );
        magmablas_syncwarp();

        // -- compute x for the generation of HxHt for k4 --
        create_x_for_k4( tx, t0, delta_t, one_half_delta_t, s_sols, s_track, s_track_last_success, sB );
        magmablas_syncwarp();

        // -- get HxHt for k4 --
        eval_cdt_3view_unknownf<N, coefsCount>( tx, t0, s_cdt, s_startCoefs, s_targetCoefs );
        eval_Jacobian_3view_unknownf<N, coefsCount>(tx, s_track, r_cgesvA, d_Hx_const_mat_s, d_Hx_const_mat_X1, d_Hx_const_mat_X2, d_Hx_const_mat_X3, d_Hx_const_mat_X4, d_Hx_const_mat_Y, s_cdt);
        eval_Ht_3view_unknownf(tx, s_track, r_cgesvB, d_Ht_const_mat_s, d_Ht_const_mat_X, d_Ht_const_mat_Y, d_const_vec_cd);

        // -- solve k4 --
        cgesv_batched_small_device<N>( tx, r_cgesvA, sipiv, r_cgesvB, sB, sx, dsx, rowid, linfo );
        magmablas_syncwarp();

        // -- make prediction --
        s_sols[tx] += sB[tx] * delta_t * 1.0/6.0;
        s_track[tx] = s_sols[tx];
        __syncthreads();

        // ===================================================================
        // -- Gauss-Newton Corrector --
        // ===================================================================
        //#pragma unroll
        //for(int i = 0; i < 4; i++) {
        for(int i = 0; i < 4; i++) {
          eval_Jacobian_3view_unknownf<N, coefsCount>(tx, s_track, r_cgesvA, d_Hx_const_mat_s, d_Hx_const_mat_X1, d_Hx_const_mat_X2, d_Hx_const_mat_X3, d_Hx_const_mat_X4, d_Hx_const_mat_Y, s_cdt);
          eval_H_3view_unknownf<coefsCount>(tx, s_track, r_cgesvB, d_Ht_const_mat_s, d_Ht_const_mat_X, d_Ht_const_mat_Y, s_cdt);

          // -- G-N corrector first solve --
          cgesv_batched_small_device<N>( tx, r_cgesvA, sipiv, r_cgesvB, sB, sx, dsx, rowid, linfo );
          magmablas_syncwarp();

          // -- correct the sols --
          s_track[tx] -= sB[tx];
          __syncthreads();

          // -- compute the norms; norm[0] is norm(sB), norm[1] is norm(sol) --
          compute_norm2<N>( tx, sB, s_track, s_sqrt_sols, s_sqrt_corr, s_norm );
          __syncthreads();
          
          s_isSuccessful = s_norm[0] < 0.000001 * s_norm[1];
          __syncthreads();

          if (s_isSuccessful)
	           break;
        }

        // -- stop if the values of the solution is too large --
        if (s_norm[1] > 1e14) {
          inf_failed = 1;
          break;
        }

        // ===================================================================
        // -- Decide Track Changes --
        // ===================================================================
        if (!s_isSuccessful) {
          s_pred_success_count = 0;
          delta_t *= 0.5;
          // -- should be the last successful tracked sols --
          s_track[tx] = s_track_last_success[tx];
          s_sols[tx] = s_track_last_success[tx];
          __syncthreads();
          t0 = t_step;
        }
        else {
          s_track_last_success[tx] = s_track[tx];
          s_sols[tx] = s_track[tx];
          __syncthreads();
          s_pred_success_count++;
          if (s_pred_success_count >= predSuccessCount) {
            s_pred_success_count = 0;
            delta_t *= 2;
          }
        }

        // ===================================================================
        // -- Decide delta t at end zone --
        // ===================================================================
        end_zone = (!end_zone && fabs(1 - t0) <= 0.0600001);
        if (end_zone)
            if (delta_t > fabs(1 - t0))
                delta_t = fabs(1 - t0);
        else if (delta_t > fabs(1 - 0.06 - t0))
            delta_t = fabs(1 - 0.06 - t0);

      }
      else {
        break;
      }
    }

    // -- write back to gm --
    /*#pragma unroll
    for(int i = 0; i < N; i++){
        d_cgesvA[ i * ldda + rowid ] = r_cgesvA[i];
    }*/

    // -- d_cgesvB tells whether the track is finished, if not, stores t0 and delta_t --
    d_cgesvB[tx] = (t0 >= 1.0 || (1.0-t0 < 0.001)) ? MAGMA_C_ONE : MAGMA_C_MAKE(t0, delta_t);
    //d_cgesvB[tx] = r_cgesvB;

    // -- d_startSols tells the pred_success_count and inf_failed for unfinished tracks --
    d_startSols[tx] = MAGMA_C_MAKE(s_pred_success_count, inf_failed);

    // -- d_track stores the solutions --
    d_track[tx] = s_track[tx];
  }

  extern "C" real_Double_t
  kernel_HC_Solver_3view_unknownf(
    magma_int_t N, magma_int_t batchCount, magma_int_t coefsCount, magma_int_t ldda,
    magma_queue_t my_queue,
    magmaFloatComplex** d_startSols_array, magmaFloatComplex** d_Track_array,
    magmaFloatComplex** d_startCoefs_array, magmaFloatComplex** d_targetCoefs_array,
    magmaFloatComplex** d_cgesvA_array, magmaFloatComplex** d_cgesvB_array,
    const_mats *cm)
  {
    real_Double_t gpu_time;
    const magma_int_t thread_x = N;
    dim3 threads(thread_x, 1, 1);
    dim3 grid(batchCount, 1, 1);
    cudaError_t e = cudaErrorInvalidValue;

    // -- decalre shared memory --
    magma_int_t shmem  = 0;
    shmem += (N+1) * sizeof(magmaFloatComplex);       // startSols
    shmem += (N+1) * sizeof(magmaFloatComplex);       // track
    shmem += (N+1) * sizeof(magmaFloatComplex);       // track_pred_init
    shmem += coefsCount * sizeof(magmaFloatComplex);  // variable vector cdt
    shmem += coefsCount * sizeof(magmaFloatComplex); // start coefficients
    shmem += coefsCount * sizeof(magmaFloatComplex); // target coefficients
    shmem += N * sizeof(magmaFloatComplex); // sB
    shmem += N * sizeof(magmaFloatComplex); // sx
    shmem += N * sizeof(float);            // dsx
    shmem += N * sizeof(int);               // pivot
    shmem += N * sizeof(float);             // s_sqrt for sol norm-2 in G-N corrector
    shmem += N * sizeof(float);             // s_sqrt for corr norm-2 in G-N corrector
    shmem += 2 * sizeof(float);             // s_norm for norm-2 in G-N corrector
    shmem += 1 * sizeof(bool);              // is_successful 
    shmem += 1 * sizeof(int);               // predictor_success counter

    //void *kernel_args[] = {&ldda, &d_startSols_array, &d_Track_array, &d_startCoefs_array, &d_targetCoefs_array, &d_cgesvA_array, &d_cgesvB_array};

    void *kernel_args[] = {&ldda, &d_startSols_array, &d_Track_array, &d_startCoefs_array, &d_targetCoefs_array, &d_cgesvA_array, &d_cgesvB_array,
                           &cm->d_const_matrix_Hx_s, &cm->d_const_matrix_Hx_X1, &cm->d_const_matrix_Hx_X2, &cm->d_const_matrix_Hx_X3, &cm->d_const_matrix_Hx_X4, 
                           &cm->d_const_matrix_Hx_Y, &cm->d_const_matrix_Ht_s, &cm->d_const_matrix_Ht_X_collection, &cm->d_const_matrix_Ht_Y, &cm->d_const_cd};

    gpu_time = magma_sync_wtime( my_queue );

    // -- <N, numOfCoeffs, max_steps, max_corr_steps, successes_to_incremental_factor> --
    e = cudaLaunchKernel((void*)homotopy_continuation_solver_3view_unknownf< 18, 153, 100, 3, 10>, grid, threads, kernel_args, shmem, my_queue->cuda_stream());

    gpu_time = magma_sync_wtime( my_queue ) - gpu_time;
    if( e != cudaSuccess ) {
        printf("cudaLaunchKernel of homotopy_continuation_solver_3view_unknownf is not successful!\n");
    }

    return gpu_time;
  }

}

#endif

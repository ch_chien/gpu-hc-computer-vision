#ifndef magmaHC_define_const_matrices_h
#define magmaHC_define_const_matrices_h
// ============================================================================
// Calling and defining constant matrices
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../magmaHC-problems.cuh"

// -- 3-view triangulation problem --
// -- 3vTrg Hx --
#include "3vTrg/Hx-3vTrg-const-mat-s.h"
#include "3vTrg/Hx-3vTrg-const-mat-X-collection.h"
#include "3vTrg/Hx-3vTrg-const-mat-Y.h"
// -- 3vTrg Ht --
#include "3vTrg/Ht-3vTrg-const-mat-s.h"
#include "3vTrg/Ht-3vTrg-const-mat-X-collection.h"
#include "3vTrg/Ht-3vTrg-const-mat-Y.h"

// -- 4-view triangulation problem --
// -- 4vTrg Hx --
#include "4vTrg/Hx-4vTrg-const-mat-s.h"
#include "4vTrg/Hx-4vTrg-const-mat-X-collection.h"
#include "4vTrg/Hx-4vTrg-const-mat-Y.h"
// -- 4vTrg Ht --
#include "4vTrg/Ht-4vTrg-const-mat-s.h"
#include "4vTrg/Ht-4vTrg-const-mat-X-collection.h"
#include "4vTrg/Ht-4vTrg-const-mat-Y.h"

// -- Eq17 problem --
// -- Eq17 Hx --
#include "Eq17/Hx-Eq17-const-mat-s.h"
#include "Eq17/Hx-Eq17-const-mat-X-collection.h"
#include "Eq17/Hx-Eq17-const-mat-Y.h"
// -- Eq17 Ht --
#include "Eq17/Ht-Eq17-const-mat-s.h"
#include "Eq17/Ht-Eq17-const-mat-X-collection.h"
#include "Eq17/Ht-Eq17-const-mat-Y.h"

// -- optimal pose 4 points problem --
// -- optpose4pt Hx --
#include "optpose4pt/Hx-optpose4pt-const-mat-s.h"
#include "optpose4pt/Hx-optpose4pt-const-mat-X-collection.h"
#include "optpose4pt/Hx-optpose4pt-const-mat-Y.h"
// -- optpose4pt Ht --
#include "optpose4pt/Ht-optpose4pt-const-mat-s.h"
#include "optpose4pt/Ht-optpose4pt-const-mat-X-collection.h"
#include "optpose4pt/Ht-optpose4pt-const-mat-Y.h"

// -- homo 3 points problem --
// -- homo3pt Hx --
#include "homo3pt/Hx-homo3pt-const-mat-s.h"
#include "homo3pt/Hx-homo3pt-const-mat-X-collection.h"
#include "homo3pt/Hx-homo3pt-const-mat-Y.h"
// -- homo3pt Ht --
#include "homo3pt/Ht-homo3pt-const-mat-s.h"
#include "homo3pt/Ht-homo3pt-const-mat-X-collection.h"
#include "homo3pt/Ht-homo3pt-const-mat-Y.h"

// -- dualTDOA problem --
// -- dualTDOA Hx --
#include "dualTDOA/Hx-dualTDOA-const-mat-s.h"
#include "dualTDOA/Hx-dualTDOA-const-mat-X-collection.h"
#include "dualTDOA/Hx-dualTDOA-const-mat-Y.h"
// -- dualTDOA Ht --
#include "dualTDOA/Ht-dualTDOA-const-mat-s.h"
#include "dualTDOA/Ht-dualTDOA-const-mat-X-collection.h"
#include "dualTDOA/Ht-dualTDOA-const-mat-Y.h"

// -- optimalPnPQ problem --
// -- optimalPnPQ Hx --
#include "optimalPnPQ/Hx-optimalPnPQ-const-mat-s.h"
#include "optimalPnPQ/Hx-optimalPnPQ-const-mat-X-collection.h"
#include "optimalPnPQ/Hx-optimalPnPQ-const-mat-Y.h"
// -- optimalPnPQ Ht --
#include "optimalPnPQ/Ht-optimalPnPQ-const-mat-s.h"
#include "optimalPnPQ/Ht-optimalPnPQ-const-mat-X-collection.h"
#include "optimalPnPQ/Ht-optimalPnPQ-const-mat-Y.h"

// -- 9pt2radial problem --
// -- 9pt2radial Hx --
#include "9pt2radial/Hx-9pt2radial-const-mat-s.h"
#include "9pt2radial/Hx-9pt2radial-const-mat-X-collection.h"
#include "9pt2radial/Hx-9pt2radial-const-mat-Y.h"
// -- 9pt2radial Ht --
#include "9pt2radial/Ht-9pt2radial-const-mat-s.h"
#include "9pt2radial/Ht-9pt2radial-const-mat-X-collection.h"
#include "9pt2radial/Ht-9pt2radial-const-mat-Y.h"

// -- pose_quiver problem --
// -- pose_quiver Hx --
#include "pose_quiver/Hx-pose_quiver-const-mat-s.h"
#include "pose_quiver/Hx-pose_quiver-const-mat-X-collection.h"
#include "pose_quiver/Hx-pose_quiver-const-mat-Y.h"
// -- pose_quiver Ht --
#include "pose_quiver/Ht-pose_quiver-const-mat-s.h"
#include "pose_quiver/Ht-pose_quiver-const-mat-X-collection.h"
#include "pose_quiver/Ht-pose_quiver-const-mat-Y.h"

// -- 5ptRelativePose problem --
// -- 5ptRelativePose Hx --
#include "5ptRelativePose/Hx-5ptRelativePose-const-mat-s.h"
#include "5ptRelativePose/Hx-5ptRelativePose-const-mat-X-collection.h"
#include "5ptRelativePose/Hx-5ptRelativePose-const-mat-Y.h"
// -- 5ptRelativePose Ht --
#include "5ptRelativePose/Ht-5ptRelativePose-const-mat-s.h"
#include "5ptRelativePose/Ht-5ptRelativePose-const-mat-X-collection.h"
#include "5ptRelativePose/Ht-5ptRelativePose-const-mat-Y.h"

// -- 5ptRelativePose version 2 problem --
// -- 5ptRelativePose_v2 Hx --
#include "5ptRelativePose_v2/Hx-5ptRelativePose_v2-const-mat-s.h"
#include "5ptRelativePose_v2/Hx-5ptRelativePose_v2-const-mat-X-collection.h"
#include "5ptRelativePose_v2/Hx-5ptRelativePose_v2-const-mat-Y.h"
// -- 5ptRelativePose_v2 Ht --
#include "5ptRelativePose_v2/Ht-5ptRelativePose_v2-const-mat-s.h"
#include "5ptRelativePose_v2/Ht-5ptRelativePose_v2-const-mat-X-collection.h"
#include "5ptRelativePose_v2/Ht-5ptRelativePose_v2-const-mat-Y.h"

// -- 5ptRelativePose version 4 problem --
// -- 5ptRelativePose_v4 Hx --
#include "5ptRelativePose_v4/Hx-5ptRelativePose_v4-const-mat-s.h"
#include "5ptRelativePose_v4/Hx-5ptRelativePose_v4-const-mat-X-collection.h"
#include "5ptRelativePose_v4/Hx-5ptRelativePose_v4-const-mat-Y.h"
// -- 5ptRelativePose_v4 Ht --
#include "5ptRelativePose_v4/Ht-5ptRelativePose_v4-const-mat-s.h"
#include "5ptRelativePose_v4/Ht-5ptRelativePose_v4-const-mat-X-collection.h"
#include "5ptRelativePose_v4/Ht-5ptRelativePose_v4-const-mat-Y.h"

// -- P3P problem --
// -- P3P Hx --
#include "P3P/Hx-P3P-const-mat-s.h"
#include "P3P/Hx-P3P-const-mat-X-collection.h"
#include "P3P/Hx-P3P-const-mat-Y.h"
// -- P3P Ht --
#include "P3P/Ht-P3P-const-mat-s.h"
#include "P3P/Ht-P3P-const-mat-X-collection.h"
#include "P3P/Ht-P3P-const-mat-Y.h"

// -- P3P v2 problem --
// -- P3P_v2 Hx --
#include "P3P_v2/Hx-P3P_v2-const-mat-s.h"
#include "P3P_v2/Hx-P3P_v2-const-mat-X-collection.h"
#include "P3P_v2/Hx-P3P_v2-const-mat-Y.h"
// -- P3P_v2 Ht --
#include "P3P_v2/Ht-P3P_v2-const-mat-s.h"
#include "P3P_v2/Ht-P3P_v2-const-mat-X-collection.h"
#include "P3P_v2/Ht-P3P_v2-const-mat-Y.h"

// -- 2vTrg_distortion problem --
// -- 2vTrg_distortion Hx --
#include "2vTrg_distortion/Hx-2vTrg_distortion-const-mat-s.h"
#include "2vTrg_distortion/Hx-2vTrg_distortion-const-mat-X-collection.h"
#include "2vTrg_distortion/Hx-2vTrg_distortion-const-mat-Y.h"
// -- 2vTrg_distortion Ht --
#include "2vTrg_distortion/Ht-2vTrg_distortion-const-mat-s.h"
#include "2vTrg_distortion/Ht-2vTrg_distortion-const-mat-X-collection.h"
#include "2vTrg_distortion/Ht-2vTrg_distortion-const-mat-Y.h"

// -- R6P1lin problem --
// -- R6P1lin Hx --
#include "R6P1lin/Hx-R6P1lin-const-mat-s.h"
#include "R6P1lin/Hx-R6P1lin-const-mat-X-collection.h"
#include "R6P1lin/Hx-R6P1lin-const-mat-Y.h"
// -- R6P1lin Ht --
#include "R6P1lin/Ht-R6P1lin-const-mat-s.h"
#include "R6P1lin/Ht-R6P1lin-const-mat-X-collection.h"
#include "R6P1lin/Ht-R6P1lin-const-mat-Y.h"

extern "C" {
namespace magmaHCWrapper {

  // -- homotopy continuation solver - alea6 kernel --
  void const_mats::define_const_matrices(
    magmaFloatComplex *h_startCoefs, magmaFloatComplex *h_targetCoefs, int coefsCount, std::string hc_problem
  )
  {
    // -- cd vector --
    for(int i = 0; i < coefsCount; i++) {
      h_const_cd[i] = h_startCoefs[i] - h_targetCoefs[i];
    }

    if (hc_problem == "3vTrg") {
      // -- \partial H / \partial x --
      Hx_3vTrg_const_matrix_scalar(h_const_Hx_scalar_mat);
      Hx_3vTrg_const_matrix_X_collection(h_const_Hx_X_collection_mat);
      Hx_3vTrg_const_matrix_Y(h_const_Hx_Y_mat);

      // -- \partial H / \partial t --
      Ht_3vTrg_const_matrix_scalar(h_const_Ht_scalar_mat);
      Ht_3vTrg_const_matrix_X_collection(h_const_Ht_X_collection_mat);
      Ht_3vTrg_const_matrix_Y(h_const_Ht_Y_mat);
    }
    else if (hc_problem == "4vTrg") {
      // -- \partial H / \partial x --
      Hx_4vTrg_const_matrix_scalar(h_const_Hx_scalar_mat);
      Hx_4vTrg_const_matrix_X_collection(h_const_Hx_X_collection_mat);
      Hx_4vTrg_const_matrix_Y(h_const_Hx_Y_mat);

      // -- \partial H / \partial t --
      Ht_4vTrg_const_matrix_scalar(h_const_Ht_scalar_mat);
      Ht_4vTrg_const_matrix_X_collection(h_const_Ht_X_collection_mat);
      Ht_4vTrg_const_matrix_Y(h_const_Ht_Y_mat);
    }
    else if (hc_problem == "Eq17") {
      // -- \partial H / \partial x --
      Hx_Eq17_const_matrix_scalar(h_const_Hx_scalar_mat);
      Hx_Eq17_const_matrix_X_collection(h_const_Hx_X_collection_mat);
      Hx_Eq17_const_matrix_Y(h_const_Hx_Y_mat);

      // -- \partial H / \partial t --
      Ht_Eq17_const_matrix_scalar(h_const_Ht_scalar_mat);
      Ht_Eq17_const_matrix_X_collection(h_const_Ht_X_collection_mat);
      Ht_Eq17_const_matrix_Y(h_const_Ht_Y_mat);
    }
    else if (hc_problem == "optpose4pt") {
      // -- \partial H / \partial x --
      Hx_optpose4pt_const_matrix_scalar(h_const_Hx_scalar_mat);
      Hx_optpose4pt_const_matrix_X_collection(h_const_Hx_X_collection_mat);
      Hx_optpose4pt_const_matrix_Y(h_const_Hx_Y_mat);

      // -- \partial H / \partial t --
      Ht_optpose4pt_const_matrix_scalar(h_const_Ht_scalar_mat);
      Ht_optpose4pt_const_matrix_X_collection(h_const_Ht_X_collection_mat);
      Ht_optpose4pt_const_matrix_Y(h_const_Ht_Y_mat);
    }
    else if (hc_problem == "homo3pt") {
      // -- \partial H / \partial x --
      Hx_homo3pt_const_matrix_scalar(h_const_Hx_scalar_mat);
      Hx_homo3pt_const_matrix_X_collection(h_const_Hx_X_collection_mat);
      Hx_homo3pt_const_matrix_Y(h_const_Hx_Y_mat);

      // -- \partial H / \partial t --
      Ht_homo3pt_const_matrix_scalar(h_const_Ht_scalar_mat);
      Ht_homo3pt_const_matrix_X_collection(h_const_Ht_X_collection_mat);
      Ht_homo3pt_const_matrix_Y(h_const_Ht_Y_mat);
    }
    else if (hc_problem == "dualTDOA") {
      // -- \partial H / \partial x --
      Hx_dualTDOA_const_matrix_scalar(h_const_Hx_scalar_mat);
      Hx_dualTDOA_const_matrix_X_collection(h_const_Hx_X_collection_mat);
      Hx_dualTDOA_const_matrix_Y(h_const_Hx_Y_mat);

      // -- \partial H / \partial t --
      Ht_dualTDOA_const_matrix_scalar(h_const_Ht_scalar_mat);
      Ht_dualTDOA_const_matrix_X_collection(h_const_Ht_X_collection_mat);
      Ht_dualTDOA_const_matrix_Y(h_const_Ht_Y_mat);
    }
    else if (hc_problem == "optimalPnPQ") {
      // -- \partial H / \partial x --
      Hx_optimalPnPQ_const_matrix_scalar(h_const_Hx_scalar_mat);
      Hx_optimalPnPQ_const_matrix_X_collection(h_const_Hx_X_collection_mat);
      Hx_optimalPnPQ_const_matrix_Y(h_const_Hx_Y_mat);

      // -- \partial H / \partial t --
      Ht_optimalPnPQ_const_matrix_scalar(h_const_Ht_scalar_mat);
      Ht_optimalPnPQ_const_matrix_X_collection(h_const_Ht_X_collection_mat);
      Ht_optimalPnPQ_const_matrix_Y(h_const_Ht_Y_mat);
    }
    else if (hc_problem == "9pt2radial") {
      // -- \partial H / \partial x --
      Hx_9pt2radial_const_matrix_scalar(h_const_Hx_scalar_mat);
      Hx_9pt2radial_const_matrix_X_collection(h_const_Hx_X_collection_mat);
      Hx_9pt2radial_const_matrix_Y(h_const_Hx_Y_mat);

      // -- \partial H / \partial t --
      Ht_9pt2radial_const_matrix_scalar(h_const_Ht_scalar_mat);
      Ht_9pt2radial_const_matrix_X_collection(h_const_Ht_X_collection_mat);
      Ht_9pt2radial_const_matrix_Y(h_const_Ht_Y_mat);
    }
    else if (hc_problem == "pose_quiver") {
      // -- \partial H / \partial x --
      Hx_pose_quiver_const_matrix_scalar(h_const_Hx_scalar_mat);
      Hx_pose_quiver_const_matrix_X_collection(h_const_Hx_X_collection_mat);
      Hx_pose_quiver_const_matrix_Y(h_const_Hx_Y_mat);

      // -- \partial H / \partial t --
      Ht_pose_quiver_const_matrix_scalar(h_const_Ht_scalar_mat);
      Ht_pose_quiver_const_matrix_X_collection(h_const_Ht_X_collection_mat);
      Ht_pose_quiver_const_matrix_Y(h_const_Ht_Y_mat);
    }
    else if (hc_problem == "5ptRelativePose") {
      // -- \partial H / \partial x --
      Hx_5ptRelativePose_const_matrix_scalar(h_const_Hx_scalar_mat);
      Hx_5ptRelativePose_const_matrix_X_collection(h_const_Hx_X_collection_mat);
      Hx_5ptRelativePose_const_matrix_Y(h_const_Hx_Y_mat);

      // -- \partial H / \partial t --
      Ht_5ptRelativePose_const_matrix_scalar(h_const_Ht_scalar_mat);
      Ht_5ptRelativePose_const_matrix_X_collection(h_const_Ht_X_collection_mat);
      Ht_5ptRelativePose_const_matrix_Y(h_const_Ht_Y_mat);
    }
    else if (hc_problem == "5ptRelativePose_v2") {
      // -- \partial H / \partial x --
      Hx_5ptRelativePose_v2_const_matrix_scalar(h_const_Hx_scalar_mat);
      Hx_5ptRelativePose_v2_const_matrix_X_collection(h_const_Hx_X_collection_mat);
      Hx_5ptRelativePose_v2_const_matrix_Y(h_const_Hx_Y_mat);

      // -- \partial H / \partial t --
      Ht_5ptRelativePose_v2_const_matrix_scalar(h_const_Ht_scalar_mat);
      Ht_5ptRelativePose_v2_const_matrix_X_collection(h_const_Ht_X_collection_mat);
      Ht_5ptRelativePose_v2_const_matrix_Y(h_const_Ht_Y_mat);
    }
    else if (hc_problem == "5ptRelativePose_v4") {
      // -- \partial H / \partial x --
      Hx_5ptRelativePose_v4_const_matrix_scalar(h_const_Hx_scalar_mat);
      Hx_5ptRelativePose_v4_const_matrix_X_collection(h_const_Hx_X_collection_mat);
      Hx_5ptRelativePose_v4_const_matrix_Y(h_const_Hx_Y_mat);

      // -- \partial H / \partial t --
      Ht_5ptRelativePose_v4_const_matrix_scalar(h_const_Ht_scalar_mat);
      Ht_5ptRelativePose_v4_const_matrix_X_collection(h_const_Ht_X_collection_mat);
      Ht_5ptRelativePose_v4_const_matrix_Y(h_const_Ht_Y_mat);
    }
    else if (hc_problem == "5ptRelativePose_v3") {
 /*     // -- \partial H / \partial x --
      Hx_5ptRelativePose_v3_const_matrix_scalar(h_const_Hx_scalar_mat);
      Hx_5ptRelativePose_v3_const_matrix_X_collection(h_const_Hx_X_collection_mat);
      Hx_5ptRelativePose_v3_const_matrix_Y(h_const_Hx_Y_mat);

      // -- \partial H / \partial t --
      Ht_5ptRelativePose_v3_const_matrix_scalar(h_const_Ht_scalar_mat);
      Ht_5ptRelativePose_v3_const_matrix_X_collection(h_const_Ht_X_collection_mat);
      Ht_5ptRelativePose_v3_const_matrix_Y(h_const_Ht_Y_mat);*/

      Hx_5ptRelativePose_const_matrix_scalar(h_const_Hx_scalar_mat);
      Hx_5ptRelativePose_const_matrix_X_collection(h_const_Hx_X_collection_mat);
      Hx_5ptRelativePose_const_matrix_Y(h_const_Hx_Y_mat);

      // -- \partial H / \partial t --
      Ht_5ptRelativePose_const_matrix_scalar(h_const_Ht_scalar_mat);
      Ht_5ptRelativePose_const_matrix_X_collection(h_const_Ht_X_collection_mat);
      Ht_5ptRelativePose_const_matrix_Y(h_const_Ht_Y_mat);
    }
    else if (hc_problem == "P3P") {
      // -- \partial H / \partial x --
      Hx_P3P_const_matrix_scalar(h_const_Hx_scalar_mat);
      Hx_P3P_const_matrix_X_collection(h_const_Hx_X_collection_mat);
      Hx_P3P_const_matrix_Y(h_const_Hx_Y_mat);

      // -- \partial H / \partial t --
      Ht_P3P_const_matrix_scalar(h_const_Ht_scalar_mat);
      Ht_P3P_const_matrix_X_collection(h_const_Ht_X_collection_mat);
      Ht_P3P_const_matrix_Y(h_const_Ht_Y_mat);
    }
    else if (hc_problem == "P3P_v2") {
      // -- \partial H / \partial x --
      Hx_P3P_v2_const_matrix_scalar(h_const_Hx_scalar_mat);
      Hx_P3P_v2_const_matrix_X_collection(h_const_Hx_X_collection_mat);
      Hx_P3P_v2_const_matrix_Y(h_const_Hx_Y_mat);

      // -- \partial H / \partial t --
      Ht_P3P_v2_const_matrix_scalar(h_const_Ht_scalar_mat);
      Ht_P3P_v2_const_matrix_X_collection(h_const_Ht_X_collection_mat);
      Ht_P3P_v2_const_matrix_Y(h_const_Ht_Y_mat);
    }
    else if (hc_problem == "2vTrg_distortion") {
      // -- \partial H / \partial x --
      Hx_2vTrg_distortion_const_matrix_scalar(h_const_Hx_scalar_mat);
      Hx_2vTrg_distortion_const_matrix_X_collection(h_const_Hx_X_collection_mat);
      Hx_2vTrg_distortion_const_matrix_Y(h_const_Hx_Y_mat);

      // -- \partial H / \partial t --
      Ht_2vTrg_distortion_const_matrix_scalar(h_const_Ht_scalar_mat);
      Ht_2vTrg_distortion_const_matrix_X_collection(h_const_Ht_X_collection_mat);
      Ht_2vTrg_distortion_const_matrix_Y(h_const_Ht_Y_mat);
    }
    else if (hc_problem == "R6P1lin") {
      // -- \partial H / \partial x --
      Hx_R6P1lin_const_matrix_scalar(h_const_Hx_scalar_mat);
      Hx_R6P1lin_const_matrix_X_collection(h_const_Hx_X_collection_mat);
      Hx_R6P1lin_const_matrix_Y(h_const_Hx_Y_mat);

      // -- \partial H / \partial t --
      Ht_R6P1lin_const_matrix_scalar(h_const_Ht_scalar_mat);
      Ht_R6P1lin_const_matrix_X_collection(h_const_Ht_X_collection_mat);
      Ht_R6P1lin_const_matrix_Y(h_const_Ht_Y_mat);
    }
    /*else if (hc_problem == "3view_unknownf") {
      // -- \partial H / \partial x --
      Hx_3view_unknownf_const_matrix_scalar(h_const_Hx_scalar_mat);
      Hx_3view_unknownf_const_matrix_X_collection(h_const_Hx_X_collection_mat);
      Hx_3view_unknownf_const_matrix_Y(h_const_Hx_Y_mat);

      // -- \partial H / \partial t --
      Ht_3view_unknownf_const_matrix_scalar(h_const_Ht_scalar_mat);
      Ht_3view_unknownf_const_matrix_X_collection(h_const_Ht_X_collection_mat);
      Ht_3view_unknownf_const_matrix_Y(h_const_Ht_Y_mat);
    }*/
  }
}
}

#endif

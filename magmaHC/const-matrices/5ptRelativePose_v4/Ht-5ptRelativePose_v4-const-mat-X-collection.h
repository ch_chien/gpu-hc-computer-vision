#ifndef Ht_5ptRelativePose_v4_const_mat_X_collection_h
#define Ht_5ptRelativePose_v4_const_mat_X_collection_h
// ============================================================================
// constant matrix indexing the solution vector in \partual H / \partial t out
// 5ptRelativePose_v4 problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Ht_5ptRelativePose_v4_const_matrix_X_collection(magma_int_t* Xt)
  {
    Xt[0] =0;
    Xt[1] =0;
    Xt[2] =0;
    Xt[3] =0;
    Xt[4] =0;
    Xt[5] =6;

    Xt[6] =1;
    Xt[7] =1;
    Xt[8] =1;
    Xt[9] =1;
    Xt[10] =1;
    Xt[11] =0;

    Xt[12] =2;
    Xt[13] =2;
    Xt[14] =2;
    Xt[15] =2;
    Xt[16] =2;
    Xt[17] =1;

    Xt[18] =3;
    Xt[19] =3;
    Xt[20] =3;
    Xt[21] =3;
    Xt[22] =3;
    Xt[23] =2;

    Xt[24] =4;
    Xt[25] =4;
    Xt[26] =4;
    Xt[27] =4;
    Xt[28] =4;
    Xt[29] =3;

    Xt[30] =5;
    Xt[31] =5;
    Xt[32] =5;
    Xt[33] =5;
    Xt[34] =5;
    Xt[35] =6;

    Xt[36] =4;
    Xt[37] =4;
    Xt[38] =4;
    Xt[39] =4;
    Xt[40] =4;
    Xt[41] =6;

    Xt[42] =5;
    Xt[43] =5;
    Xt[44] =5;
    Xt[45] =5;
    Xt[46] =5;
    Xt[47] =6;

    Xt[48] =4;
    Xt[49] =4;
    Xt[50] =4;
    Xt[51] =4;
    Xt[52] =4;
    Xt[53] =6;

    Xt[54] =5;
    Xt[55] =5;
    Xt[56] =5;
    Xt[57] =5;
    Xt[58] =5;
    Xt[59] =6;

    Xt[60] =4;
    Xt[61] =4;
    Xt[62] =4;
    Xt[63] =4;
    Xt[64] =4;
    Xt[65] =6;

    Xt[66] =5;
    Xt[67] =5;
    Xt[68] =5;
    Xt[69] =5;
    Xt[70] =5;
    Xt[71] =6;

    Xt[72] =1;
    Xt[73] =1;
    Xt[74] =1;
    Xt[75] =1;
    Xt[76] =1;
    Xt[77] =6;

    Xt[78] =2;
    Xt[79] =2;
    Xt[80] =2;
    Xt[81] =2;
    Xt[82] =2;
    Xt[83] =6;

    Xt[84] =3;
    Xt[85] =3;
    Xt[86] =3;
    Xt[87] =3;
    Xt[88] =3;
    Xt[89] =6;

    Xt[90] =3;
    Xt[91] =3;
    Xt[92] =3;
    Xt[93] =3;
    Xt[94] =3;
    Xt[95] =6;

    Xt[96] =2;
    Xt[97] =2;
    Xt[98] =2;
    Xt[99] =2;
    Xt[100] =2;
    Xt[101] =6;

    Xt[102] =3;
    Xt[103] =3;
    Xt[104] =3;
    Xt[105] =3;
    Xt[106] =3;
    Xt[107] =6;

    Xt[108] =4;
    Xt[109] =4;
    Xt[110] =4;
    Xt[111] =4;
    Xt[112] =4;
    Xt[113] =6;

    Xt[114] =5;
    Xt[115] =5;
    Xt[116] =5;
    Xt[117] =5;
    Xt[118] =5;
    Xt[119] =6;

    Xt[120] =4;
    Xt[121] =4;
    Xt[122] =4;
    Xt[123] =4;
    Xt[124] =4;
    Xt[125] =6;

    Xt[126] =5;
    Xt[127] =5;
    Xt[128] =5;
    Xt[129] =5;
    Xt[130] =5;
    Xt[131] =6;

    Xt[132] =5;
    Xt[133] =5;
    Xt[134] =5;
    Xt[135] =5;
    Xt[136] =5;
    Xt[137] =6;

    Xt[138] =4;
    Xt[139] =4;
    Xt[140] =4;
    Xt[141] =4;
    Xt[142] =4;
    Xt[143] =6;

    Xt[144] =4;
    Xt[145] =4;
    Xt[146] =4;
    Xt[147] =4;
    Xt[148] =4;
    Xt[149] =6;

    Xt[150] =5;
    Xt[151] =5;
    Xt[152] =5;
    Xt[153] =5;
    Xt[154] =5;
    Xt[155] =6;

    Xt[156] =5;
    Xt[157] =5;
    Xt[158] =5;
    Xt[159] =5;
    Xt[160] =5;
    Xt[161] =6;

    Xt[162] =4;
    Xt[163] =4;
    Xt[164] =4;
    Xt[165] =4;
    Xt[166] =4;
    Xt[167] =6;

    Xt[168] =5;
    Xt[169] =5;
    Xt[170] =5;
    Xt[171] =5;
    Xt[172] =5;
    Xt[173] =6;

    Xt[174] =4;
    Xt[175] =4;
    Xt[176] =4;
    Xt[177] =4;
    Xt[178] =4;
    Xt[179] =6;


    Xt[180] =0;
    Xt[181] =0;
    Xt[182] =0;
    Xt[183] =0;
    Xt[184] =0;
    Xt[185] =6;

    Xt[186] =1;
    Xt[187] =1;
    Xt[188] =1;
    Xt[189] =1;
    Xt[190] =1;
    Xt[191] =0;

    Xt[192] =2;
    Xt[193] =2;
    Xt[194] =2;
    Xt[195] =2;
    Xt[196] =2;
    Xt[197] =1;

    Xt[198] =3;
    Xt[199] =3;
    Xt[200] =3;
    Xt[201] =3;
    Xt[202] =3;
    Xt[203] =2;

    Xt[204] =0;
    Xt[205] =0;
    Xt[206] =0;
    Xt[207] =0;
    Xt[208] =0;
    Xt[209] =3;

    Xt[210] =0;
    Xt[211] =0;
    Xt[212] =0;
    Xt[213] =0;
    Xt[214] =0;
    Xt[215] =6;

    Xt[216] =1;
    Xt[217] =1;
    Xt[218] =1;
    Xt[219] =1;
    Xt[220] =1;
    Xt[221] =6;

    Xt[222] =1;
    Xt[223] =1;
    Xt[224] =1;
    Xt[225] =1;
    Xt[226] =1;
    Xt[227] =6;

    Xt[228] =2;
    Xt[229] =2;
    Xt[230] =2;
    Xt[231] =2;
    Xt[232] =2;
    Xt[233] =6;

    Xt[234] =2;
    Xt[235] =2;
    Xt[236] =2;
    Xt[237] =2;
    Xt[238] =2;
    Xt[239] =6;

    Xt[240] =3;
    Xt[241] =3;
    Xt[242] =3;
    Xt[243] =3;
    Xt[244] =3;
    Xt[245] =6;

    Xt[246] =3;
    Xt[247] =3;
    Xt[248] =3;
    Xt[249] =3;
    Xt[250] =3;
    Xt[251] =6;

    Xt[252] =0;
    Xt[253] =0;
    Xt[254] =0;
    Xt[255] =0;
    Xt[256] =0;
    Xt[257] =6;

    Xt[258] =0;
    Xt[259] =0;
    Xt[260] =0;
    Xt[261] =0;
    Xt[262] =0;
    Xt[263] =6;

    Xt[264] =1;
    Xt[265] =1;
    Xt[266] =1;
    Xt[267] =1;
    Xt[268] =1;
    Xt[269] =6;

    Xt[270] =0;
    Xt[271] =0;
    Xt[272] =0;
    Xt[273] =0;
    Xt[274] =0;
    Xt[275] =6;

    Xt[276] =1;
    Xt[277] =1;
    Xt[278] =1;
    Xt[279] =1;
    Xt[280] =1;
    Xt[281] =6;

    Xt[282] =2;
    Xt[283] =2;
    Xt[284] =2;
    Xt[285] =2;
    Xt[286] =2;
    Xt[287] =6;

    Xt[288] =1;
    Xt[289] =1;
    Xt[290] =1;
    Xt[291] =1;
    Xt[292] =1;
    Xt[293] =6;

    Xt[294] =1;
    Xt[295] =1;
    Xt[296] =1;
    Xt[297] =1;
    Xt[298] =1;
    Xt[299] =6;

    Xt[300] =2;
    Xt[301] =2;
    Xt[302] =2;
    Xt[303] =2;
    Xt[304] =2;
    Xt[305] =6;

    Xt[306] =3;
    Xt[307] =3;
    Xt[308] =3;
    Xt[309] =3;
    Xt[310] =3;
    Xt[311] =6;

    Xt[312] =2;
    Xt[313] =2;
    Xt[314] =2;
    Xt[315] =2;
    Xt[316] =2;
    Xt[317] =6;

    Xt[318] =3;
    Xt[319] =3;
    Xt[320] =3;
    Xt[321] =3;
    Xt[322] =3;
    Xt[323] =6;

    Xt[324] =2;
    Xt[325] =2;
    Xt[326] =2;
    Xt[327] =2;
    Xt[328] =2;
    Xt[329] =6;

    Xt[330] =3;
    Xt[331] =3;
    Xt[332] =3;
    Xt[333] =3;
    Xt[334] =3;
    Xt[335] =6;

    Xt[336] =2;
    Xt[337] =2;
    Xt[338] =2;
    Xt[339] =2;
    Xt[340] =2;
    Xt[341] =6;

    Xt[342] =3;
    Xt[343] =3;
    Xt[344] =3;
    Xt[345] =3;
    Xt[346] =3;
    Xt[347] =6;

    Xt[348] =3;
    Xt[349] =3;
    Xt[350] =3;
    Xt[351] =3;
    Xt[352] =3;
    Xt[353] =6;

    Xt[354] =3;
    Xt[355] =3;
    Xt[356] =3;
    Xt[357] =3;
    Xt[358] =3;
    Xt[359] =6;


    Xt[360] =6;
    Xt[361] =6;
    Xt[362] =6;
    Xt[363] =6;
    Xt[364] =6;
    Xt[365] =6;

    Xt[366] =6;
    Xt[367] =6;
    Xt[368] =6;
    Xt[369] =6;
    Xt[370] =6;
    Xt[371] =6;

    Xt[372] =6;
    Xt[373] =6;
    Xt[374] =6;
    Xt[375] =6;
    Xt[376] =6;
    Xt[377] =6;

    Xt[378] =6;
    Xt[379] =6;
    Xt[380] =6;
    Xt[381] =6;
    Xt[382] =6;
    Xt[383] =6;

    Xt[384] =0;
    Xt[385] =0;
    Xt[386] =0;
    Xt[387] =0;
    Xt[388] =0;
    Xt[389] =6;

    Xt[390] =0;
    Xt[391] =0;
    Xt[392] =0;
    Xt[393] =0;
    Xt[394] =0;
    Xt[395] =6;

    Xt[396] =1;
    Xt[397] =1;
    Xt[398] =1;
    Xt[399] =1;
    Xt[400] =1;
    Xt[401] =6;

    Xt[402] =1;
    Xt[403] =1;
    Xt[404] =1;
    Xt[405] =1;
    Xt[406] =1;
    Xt[407] =6;

    Xt[408] =2;
    Xt[409] =2;
    Xt[410] =2;
    Xt[411] =2;
    Xt[412] =2;
    Xt[413] =6;

    Xt[414] =2;
    Xt[415] =2;
    Xt[416] =2;
    Xt[417] =2;
    Xt[418] =2;
    Xt[419] =6;

    Xt[420] =3;
    Xt[421] =3;
    Xt[422] =3;
    Xt[423] =3;
    Xt[424] =3;
    Xt[425] =6;

    Xt[426] =3;
    Xt[427] =3;
    Xt[428] =3;
    Xt[429] =3;
    Xt[430] =3;
    Xt[431] =6;

    Xt[432] =6;
    Xt[433] =6;
    Xt[434] =6;
    Xt[435] =6;
    Xt[436] =6;
    Xt[437] =6;

    Xt[438] =6;
    Xt[439] =6;
    Xt[440] =6;
    Xt[441] =6;
    Xt[442] =6;
    Xt[443] =6;

    Xt[444] =6;
    Xt[445] =6;
    Xt[446] =6;
    Xt[447] =6;
    Xt[448] =6;
    Xt[449] =6;

    Xt[450] =6;
    Xt[451] =6;
    Xt[452] =6;
    Xt[453] =6;
    Xt[454] =6;
    Xt[455] =6;

    Xt[456] =6;
    Xt[457] =6;
    Xt[458] =6;
    Xt[459] =6;
    Xt[460] =6;
    Xt[461] =6;

    Xt[462] =6;
    Xt[463] =6;
    Xt[464] =6;
    Xt[465] =6;
    Xt[466] =6;
    Xt[467] =6;

    Xt[468] =0;
    Xt[469] =0;
    Xt[470] =0;
    Xt[471] =0;
    Xt[472] =0;
    Xt[473] =6;

    Xt[474] =0;
    Xt[475] =0;
    Xt[476] =0;
    Xt[477] =0;
    Xt[478] =0;
    Xt[479] =6;

    Xt[480] =0;
    Xt[481] =0;
    Xt[482] =0;
    Xt[483] =0;
    Xt[484] =0;
    Xt[485] =6;

    Xt[486] =2;
    Xt[487] =2;
    Xt[488] =2;
    Xt[489] =2;
    Xt[490] =2;
    Xt[491] =6;

    Xt[492] =0;
    Xt[493] =0;
    Xt[494] =0;
    Xt[495] =0;
    Xt[496] =0;
    Xt[497] =6;

    Xt[498] =0;
    Xt[499] =0;
    Xt[500] =0;
    Xt[501] =0;
    Xt[502] =0;
    Xt[503] =6;

    Xt[504] =1;
    Xt[505] =1;
    Xt[506] =1;
    Xt[507] =1;
    Xt[508] =1;
    Xt[509] =6;

    Xt[510] =0;
    Xt[511] =0;
    Xt[512] =0;
    Xt[513] =0;
    Xt[514] =0;
    Xt[515] =6;

    Xt[516] =1;
    Xt[517] =1;
    Xt[518] =1;
    Xt[519] =1;
    Xt[520] =1;
    Xt[521] =6;

    Xt[522] =1;
    Xt[523] =1;
    Xt[524] =1;
    Xt[525] =1;
    Xt[526] =1;
    Xt[527] =6;

    Xt[528] =1;
    Xt[529] =1;
    Xt[530] =1;
    Xt[531] =1;
    Xt[532] =1;
    Xt[533] =6;

    Xt[534] =2;
    Xt[535] =2;
    Xt[536] =2;
    Xt[537] =2;
    Xt[538] =2;
    Xt[539] =6;
  }
}

#endif

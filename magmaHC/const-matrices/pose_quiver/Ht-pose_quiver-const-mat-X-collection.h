#ifndef Ht_pose_quiver_const_mat_X_collection_h
#define Ht_pose_quiver_const_mat_X_collection_h
// ============================================================================
// constant matrix indexing the solution vector in \partual H / \partial t out
// pose_quiver problem
//
// Modifications
//    Chien  21-10-26:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Ht_pose_quiver_const_matrix_X_collection(magma_int_t* Xt)
  {
    Xt[0] =4;
    Xt[1] =4;
    Xt[2] =4;
    Xt[3] =4;

    Xt[4] =1;
    Xt[5] =1;
    Xt[6] =1;
    Xt[7] =1;

    Xt[8] =0;
    Xt[9] =0;
    Xt[10] =0;
    Xt[11] =0;

    Xt[12] =3;
    Xt[13] =3;
    Xt[14] =3;
    Xt[15] =3;

    Xt[16] =2;
    Xt[17] =2;
    Xt[18] =2;
    Xt[19] =2;

    Xt[20] =0;
    Xt[21] =0;
    Xt[22] =0;
    Xt[23] =0;

    Xt[24] =1;
    Xt[25] =1;
    Xt[26] =1;
    Xt[27] =1;

    Xt[28] =2;
    Xt[29] =2;
    Xt[30] =2;
    Xt[31] =2;

    Xt[32] =3;
    Xt[33] =3;
    Xt[34] =3;
    Xt[35] =3;

    Xt[36] =3;
    Xt[37] =3;
    Xt[38] =3;
    Xt[39] =3;

    Xt[40] =3;
    Xt[41] =3;
    Xt[42] =3;
    Xt[43] =3;

    Xt[44] =1;
    Xt[45] =1;
    Xt[46] =1;
    Xt[47] =1;

    Xt[48] =2;
    Xt[49] =2;
    Xt[50] =2;
    Xt[51] =2;

    Xt[52] =3;
    Xt[53] =3;
    Xt[54] =3;
    Xt[55] =3;

    Xt[56] =2;
    Xt[57] =2;
    Xt[58] =2;
    Xt[59] =2;

    Xt[60] =3;
    Xt[61] =3;
    Xt[62] =3;
    Xt[63] =3;

    Xt[64] =3;
    Xt[65] =3;
    Xt[66] =3;
    Xt[67] =3;

    Xt[68] =3;
    Xt[69] =3;
    Xt[70] =3;
    Xt[71] =3;


    Xt[72] =4;
    Xt[73] =4;
    Xt[74] =4;
    Xt[75] =4;

    Xt[76] =4;
    Xt[77] =4;
    Xt[78] =4;
    Xt[79] =4;

    Xt[80] =4;
    Xt[81] =4;
    Xt[82] =4;
    Xt[83] =4;

    Xt[84] =4;
    Xt[85] =4;
    Xt[86] =4;
    Xt[87] =4;

    Xt[88] =4;
    Xt[89] =4;
    Xt[90] =4;
    Xt[91] =4;

    Xt[92] =0;
    Xt[93] =0;
    Xt[94] =0;
    Xt[95] =0;

    Xt[96] =1;
    Xt[97] =1;
    Xt[98] =1;
    Xt[99] =1;

    Xt[100] =2;
    Xt[101] =2;
    Xt[102] =2;
    Xt[103] =2;

    Xt[104] =0;
    Xt[105] =0;
    Xt[106] =0;
    Xt[107] =0;

    Xt[108] =1;
    Xt[109] =1;
    Xt[110] =1;
    Xt[111] =1;

    Xt[112] =2;
    Xt[113] =2;
    Xt[114] =2;
    Xt[115] =2;

    Xt[116] =0;
    Xt[117] =0;
    Xt[118] =0;
    Xt[119] =0;

    Xt[120] =0;
    Xt[121] =0;
    Xt[122] =0;
    Xt[123] =0;

    Xt[124] =0;
    Xt[125] =0;
    Xt[126] =0;
    Xt[127] =0;

    Xt[128] =1;
    Xt[129] =1;
    Xt[130] =1;
    Xt[131] =1;

    Xt[132] =1;
    Xt[133] =1;
    Xt[134] =1;
    Xt[135] =1;

    Xt[136] =2;
    Xt[137] =2;
    Xt[138] =2;
    Xt[139] =2;

    Xt[140] =2;
    Xt[141] =2;
    Xt[142] =2;
    Xt[143] =2;


    Xt[144] =4;
    Xt[145] =4;
    Xt[146] =4;
    Xt[147] =4;

    Xt[148] =4;
    Xt[149] =4;
    Xt[150] =4;
    Xt[151] =4;

    Xt[152] =4;
    Xt[153] =4;
    Xt[154] =4;
    Xt[155] =4;

    Xt[156] =4;
    Xt[157] =4;
    Xt[158] =4;
    Xt[159] =4;

    Xt[160] =4;
    Xt[161] =4;
    Xt[162] =4;
    Xt[163] =4;

    Xt[164] =4;
    Xt[165] =4;
    Xt[166] =4;
    Xt[167] =4;

    Xt[168] =4;
    Xt[169] =4;
    Xt[170] =4;
    Xt[171] =4;

    Xt[172] =4;
    Xt[173] =4;
    Xt[174] =4;
    Xt[175] =4;

    Xt[176] =0;
    Xt[177] =0;
    Xt[178] =0;
    Xt[179] =0;

    Xt[180] =1;
    Xt[181] =1;
    Xt[182] =1;
    Xt[183] =1;

    Xt[184] =2;
    Xt[185] =2;
    Xt[186] =2;
    Xt[187] =2;

    Xt[188] =4;
    Xt[189] =4;
    Xt[190] =4;
    Xt[191] =4;

    Xt[192] =4;
    Xt[193] =4;
    Xt[194] =4;
    Xt[195] =4;

    Xt[196] =4;
    Xt[197] =4;
    Xt[198] =4;
    Xt[199] =4;

    Xt[200] =4;
    Xt[201] =4;
    Xt[202] =4;
    Xt[203] =4;

    Xt[204] =4;
    Xt[205] =4;
    Xt[206] =4;
    Xt[207] =4;

    Xt[208] =0;
    Xt[209] =0;
    Xt[210] =0;
    Xt[211] =0;

    Xt[212] =1;
    Xt[213] =1;
    Xt[214] =1;
    Xt[215] =1;
  }
}

#endif

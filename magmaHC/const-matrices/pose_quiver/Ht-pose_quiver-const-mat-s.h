#ifndef Ht_pose_quiver_const_mat_s_h
#define Ht_pose_quiver_const_mat_s_h
// ============================================================================
// scalar const matrix for Ht of pose_quiver problem
//
// Modifications
//    Chien  21-10-26:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Ht_pose_quiver_const_matrix_scalar(magma_int_t* s)
  {
    s[0] =1;
    s[1] =1;
    s[2] =1;
    s[3] =1;

    s[4] =1;
    s[5] =1;
    s[6] =1;
    s[7] =1;

    s[8] =1;
    s[9] =1;
    s[10] =1;
    s[11] =1;

    s[12] =1;
    s[13] =1;
    s[14] =1;
    s[15] =1;

    s[16] =1;
    s[17] =1;
    s[18] =1;
    s[19] =1;

    s[20] =1;
    s[21] =1;
    s[22] =1;
    s[23] =1;

    s[24] =1;
    s[25] =1;
    s[26] =1;
    s[27] =1;

    s[28] =1;
    s[29] =1;
    s[30] =1;
    s[31] =1;

    s[32] =1;
    s[33] =1;
    s[34] =1;
    s[35] =1;

    s[36] =1;
    s[37] =1;
    s[38] =1;
    s[39] =1;

    s[40] =1;
    s[41] =1;
    s[42] =1;
    s[43] =1;

    s[44] =1;
    s[45] =1;
    s[46] =1;
    s[47] =1;

    s[48] =1;
    s[49] =1;
    s[50] =1;
    s[51] =1;

    s[52] =1;
    s[53] =1;
    s[54] =1;
    s[55] =1;

    s[56] =1;
    s[57] =1;
    s[58] =1;
    s[59] =1;

    s[60] =1;
    s[61] =1;
    s[62] =1;
    s[63] =1;

    s[64] =1;
    s[65] =1;
    s[66] =1;
    s[67] =1;

    s[68] =1;
    s[69] =1;
    s[70] =1;
    s[71] =1;
  }
}

#endif

#ifndef Ht_pose_quiver_const_mat_Y_h
#define Ht_pose_quiver_const_mat_Y_h
// ============================================================================
// cd index vector for Ht of pose_quiver problem
//
// Modifications
//    Chien  21-09-06:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Ht_pose_quiver_const_matrix_Y(magma_int_t* Y)
  {
    Y[0] =13;
    Y[1] =27;
    Y[2] =41;
    Y[3] =55;

    Y[4] =4;
    Y[5] =18;
    Y[6] =32;
    Y[7] =46;

    Y[8] =6;
    Y[9] =20;
    Y[10] =34;
    Y[11] =48;

    Y[12] =10;
    Y[13] =24;
    Y[14] =38;
    Y[15] =52;

    Y[16] =12;
    Y[17] =26;
    Y[18] =40;
    Y[19] =54;

    Y[20] =1;
    Y[21] =15;
    Y[22] =29;
    Y[23] =43;

    Y[24] =7;
    Y[25] =21;
    Y[26] =35;
    Y[27] =49;

    Y[28] =11;
    Y[29] =25;
    Y[30] =39;
    Y[31] =53;

    Y[32] =0;
    Y[33] =14;
    Y[34] =28;
    Y[35] =42;

    Y[36] =0;
    Y[37] =14;
    Y[38] =28;
    Y[39] =42;

    Y[40] =10;
    Y[41] =24;
    Y[42] =38;
    Y[43] =52;

    Y[44] =2;
    Y[45] =16;
    Y[46] =30;
    Y[47] =44;

    Y[48] =4;
    Y[49] =18;
    Y[50] =32;
    Y[51] =46;

    Y[52] =5;
    Y[53] =19;
    Y[54] =33;
    Y[55] =47;

    Y[56] =8;
    Y[57] =22;
    Y[58] =36;
    Y[59] =50;

    Y[60] =9;
    Y[61] =23;
    Y[62] =37;
    Y[63] =51;

    Y[64] =3;
    Y[65] =17;
    Y[66] =31;
    Y[67] =45;

    Y[68] =5;
    Y[69] =19;
    Y[70] =33;
    Y[71] =47;

  }
}

#endif

#ifndef Ht_3view_unknownf_const_mat_X_collection_h
#define Ht_3view_unknownf_const_mat_X_collection_h
// ============================================================================
// constant matrix indexing the solution vector in \partual H / \partial t out
// 3view_unknownf problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Ht_3view_unknownf_const_matrix_X_collection(magma_int_t* Xt)
  {
    Xt[0] =17;
    Xt[1] =17;
    Xt[2] =17;
    Xt[3] =17;
    Xt[4] =17;
    Xt[5] =17;
    Xt[6] =17;
    Xt[7] =17;
    Xt[8] =17;
    Xt[9] =17;
    Xt[10] =17;
    Xt[11] =17;
    Xt[12] =17;
    Xt[13] =17;
    Xt[14] =17;
    Xt[15] =17;
    Xt[16] =18;
    Xt[17] =18;

    Xt[18] =17;
    Xt[19] =17;
    Xt[20] =17;
    Xt[21] =17;
    Xt[22] =17;
    Xt[23] =17;
    Xt[24] =17;
    Xt[25] =17;
    Xt[26] =17;
    Xt[27] =17;
    Xt[28] =17;
    Xt[29] =17;
    Xt[30] =17;
    Xt[31] =17;
    Xt[32] =17;
    Xt[33] =17;
    Xt[34] =0;
    Xt[35] =4;

    Xt[36] =13;
    Xt[37] =13;
    Xt[38] =13;
    Xt[39] =13;
    Xt[40] =14;
    Xt[41] =14;
    Xt[42] =14;
    Xt[43] =14;
    Xt[44] =15;
    Xt[45] =15;
    Xt[46] =15;
    Xt[47] =15;
    Xt[48] =16;
    Xt[49] =16;
    Xt[50] =16;
    Xt[51] =16;
    Xt[52] =1;
    Xt[53] =5;

    Xt[54] =13;
    Xt[55] =13;
    Xt[56] =13;
    Xt[57] =13;
    Xt[58] =14;
    Xt[59] =14;
    Xt[60] =14;
    Xt[61] =14;
    Xt[62] =15;
    Xt[63] =15;
    Xt[64] =15;
    Xt[65] =15;
    Xt[66] =16;
    Xt[67] =16;
    Xt[68] =16;
    Xt[69] =16;
    Xt[70] =2;
    Xt[71] =6;

    Xt[72] =13;
    Xt[73] =13;
    Xt[74] =13;
    Xt[75] =13;
    Xt[76] =14;
    Xt[77] =14;
    Xt[78] =14;
    Xt[79] =14;
    Xt[80] =15;
    Xt[81] =15;
    Xt[82] =15;
    Xt[83] =15;
    Xt[84] =16;
    Xt[85] =16;
    Xt[86] =16;
    Xt[87] =16;
    Xt[88] =3;
    Xt[89] =7;

    Xt[90] =13;
    Xt[91] =13;
    Xt[92] =13;
    Xt[93] =13;
    Xt[94] =14;
    Xt[95] =14;
    Xt[96] =14;
    Xt[97] =14;
    Xt[98] =15;
    Xt[99] =15;
    Xt[100] =15;
    Xt[101] =15;
    Xt[102] =16;
    Xt[103] =16;
    Xt[104] =16;
    Xt[105] =16;
    Xt[106] =18;
    Xt[107] =18;

    Xt[108] =17;
    Xt[109] =17;
    Xt[110] =17;
    Xt[111] =17;
    Xt[112] =17;
    Xt[113] =17;
    Xt[114] =17;
    Xt[115] =17;
    Xt[116] =17;
    Xt[117] =17;
    Xt[118] =17;
    Xt[119] =17;
    Xt[120] =17;
    Xt[121] =17;
    Xt[122] =17;
    Xt[123] =17;
    Xt[124] =18;
    Xt[125] =18;

    Xt[126] =17;
    Xt[127] =17;
    Xt[128] =17;
    Xt[129] =17;
    Xt[130] =17;
    Xt[131] =17;
    Xt[132] =17;
    Xt[133] =17;
    Xt[134] =17;
    Xt[135] =17;
    Xt[136] =17;
    Xt[137] =17;
    Xt[138] =17;
    Xt[139] =17;
    Xt[140] =17;
    Xt[141] =17;
    Xt[142] =18;
    Xt[143] =18;

    Xt[144] =17;
    Xt[145] =17;
    Xt[146] =17;
    Xt[147] =17;
    Xt[148] =17;
    Xt[149] =17;
    Xt[150] =17;
    Xt[151] =17;
    Xt[152] =17;
    Xt[153] =17;
    Xt[154] =17;
    Xt[155] =17;
    Xt[156] =17;
    Xt[157] =17;
    Xt[158] =17;
    Xt[159] =17;
    Xt[160] =18;
    Xt[161] =18;

    Xt[162] =17;
    Xt[163] =17;
    Xt[164] =17;
    Xt[165] =17;
    Xt[166] =17;
    Xt[167] =17;
    Xt[168] =17;
    Xt[169] =17;
    Xt[170] =17;
    Xt[171] =17;
    Xt[172] =17;
    Xt[173] =17;
    Xt[174] =17;
    Xt[175] =17;
    Xt[176] =17;
    Xt[177] =17;
    Xt[178] =18;
    Xt[179] =18;

    Xt[180] =17;
    Xt[181] =17;
    Xt[182] =17;
    Xt[183] =17;
    Xt[184] =17;
    Xt[185] =17;
    Xt[186] =17;
    Xt[187] =17;
    Xt[188] =17;
    Xt[189] =17;
    Xt[190] =17;
    Xt[191] =17;
    Xt[192] =17;
    Xt[193] =17;
    Xt[194] =17;
    Xt[195] =17;
    Xt[196] =18;
    Xt[197] =18;

    Xt[198] =17;
    Xt[199] =17;
    Xt[200] =17;
    Xt[201] =17;
    Xt[202] =17;
    Xt[203] =17;
    Xt[204] =17;
    Xt[205] =17;
    Xt[206] =17;
    Xt[207] =17;
    Xt[208] =17;
    Xt[209] =17;
    Xt[210] =17;
    Xt[211] =17;
    Xt[212] =17;
    Xt[213] =17;
    Xt[214] =18;
    Xt[215] =18;

    Xt[216] =17;
    Xt[217] =17;
    Xt[218] =17;
    Xt[219] =17;
    Xt[220] =17;
    Xt[221] =17;
    Xt[222] =17;
    Xt[223] =17;
    Xt[224] =17;
    Xt[225] =17;
    Xt[226] =17;
    Xt[227] =17;
    Xt[228] =17;
    Xt[229] =17;
    Xt[230] =17;
    Xt[231] =17;
    Xt[232] =18;
    Xt[233] =18;

    Xt[234] =17;
    Xt[235] =17;
    Xt[236] =17;
    Xt[237] =17;
    Xt[238] =17;
    Xt[239] =17;
    Xt[240] =17;
    Xt[241] =17;
    Xt[242] =17;
    Xt[243] =17;
    Xt[244] =17;
    Xt[245] =17;
    Xt[246] =17;
    Xt[247] =17;
    Xt[248] =17;
    Xt[249] =17;
    Xt[250] =18;
    Xt[251] =18;


    Xt[252] =17;
    Xt[253] =8;
    Xt[254] =10;
    Xt[255] =11;
    Xt[256] =17;
    Xt[257] =8;
    Xt[258] =10;
    Xt[259] =11;
    Xt[260] =17;
    Xt[261] =8;
    Xt[262] =10;
    Xt[263] =11;
    Xt[264] =17;
    Xt[265] =8;
    Xt[266] =10;
    Xt[267] =11;
    Xt[268] =18;
    Xt[269] =18;

    Xt[270] =9;
    Xt[271] =9;
    Xt[272] =12;
    Xt[273] =12;
    Xt[274] =9;
    Xt[275] =9;
    Xt[276] =12;
    Xt[277] =12;
    Xt[278] =9;
    Xt[279] =9;
    Xt[280] =12;
    Xt[281] =12;
    Xt[282] =9;
    Xt[283] =9;
    Xt[284] =12;
    Xt[285] =12;
    Xt[286] =18;
    Xt[287] =18;

    Xt[288] =1;
    Xt[289] =1;
    Xt[290] =5;
    Xt[291] =5;
    Xt[292] =1;
    Xt[293] =1;
    Xt[294] =5;
    Xt[295] =5;
    Xt[296] =1;
    Xt[297] =1;
    Xt[298] =5;
    Xt[299] =5;
    Xt[300] =1;
    Xt[301] =1;
    Xt[302] =5;
    Xt[303] =5;
    Xt[304] =18;
    Xt[305] =18;

    Xt[306] =2;
    Xt[307] =2;
    Xt[308] =6;
    Xt[309] =6;
    Xt[310] =2;
    Xt[311] =2;
    Xt[312] =6;
    Xt[313] =6;
    Xt[314] =2;
    Xt[315] =2;
    Xt[316] =6;
    Xt[317] =6;
    Xt[318] =2;
    Xt[319] =2;
    Xt[320] =6;
    Xt[321] =6;
    Xt[322] =18;
    Xt[323] =18;

    Xt[324] =3;
    Xt[325] =3;
    Xt[326] =7;
    Xt[327] =7;
    Xt[328] =3;
    Xt[329] =3;
    Xt[330] =7;
    Xt[331] =7;
    Xt[332] =3;
    Xt[333] =3;
    Xt[334] =7;
    Xt[335] =7;
    Xt[336] =3;
    Xt[337] =3;
    Xt[338] =7;
    Xt[339] =7;
    Xt[340] =18;
    Xt[341] =18;

    Xt[342] =3;
    Xt[343] =3;
    Xt[344] =7;
    Xt[345] =7;
    Xt[346] =3;
    Xt[347] =3;
    Xt[348] =7;
    Xt[349] =7;
    Xt[350] =3;
    Xt[351] =3;
    Xt[352] =7;
    Xt[353] =7;
    Xt[354] =3;
    Xt[355] =3;
    Xt[356] =7;
    Xt[357] =7;
    Xt[358] =18;
    Xt[359] =18;

    Xt[360] =13;
    Xt[361] =13;
    Xt[362] =13;
    Xt[363] =13;
    Xt[364] =14;
    Xt[365] =14;
    Xt[366] =14;
    Xt[367] =14;
    Xt[368] =15;
    Xt[369] =15;
    Xt[370] =15;
    Xt[371] =15;
    Xt[372] =16;
    Xt[373] =16;
    Xt[374] =16;
    Xt[375] =16;
    Xt[376] =18;
    Xt[377] =18;

    Xt[378] =13;
    Xt[379] =13;
    Xt[380] =13;
    Xt[381] =13;
    Xt[382] =14;
    Xt[383] =14;
    Xt[384] =14;
    Xt[385] =14;
    Xt[386] =15;
    Xt[387] =15;
    Xt[388] =15;
    Xt[389] =15;
    Xt[390] =16;
    Xt[391] =16;
    Xt[392] =16;
    Xt[393] =16;
    Xt[394] =18;
    Xt[395] =18;

    Xt[396] =13;
    Xt[397] =13;
    Xt[398] =13;
    Xt[399] =13;
    Xt[400] =14;
    Xt[401] =14;
    Xt[402] =14;
    Xt[403] =14;
    Xt[404] =15;
    Xt[405] =15;
    Xt[406] =15;
    Xt[407] =15;
    Xt[408] =16;
    Xt[409] =16;
    Xt[410] =16;
    Xt[411] =16;
    Xt[412] =18;
    Xt[413] =18;

    Xt[414] =13;
    Xt[415] =13;
    Xt[416] =13;
    Xt[417] =13;
    Xt[418] =14;
    Xt[419] =14;
    Xt[420] =14;
    Xt[421] =14;
    Xt[422] =15;
    Xt[423] =15;
    Xt[424] =15;
    Xt[425] =15;
    Xt[426] =16;
    Xt[427] =16;
    Xt[428] =16;
    Xt[429] =16;
    Xt[430] =18;
    Xt[431] =18;

    Xt[432] =13;
    Xt[433] =13;
    Xt[434] =13;
    Xt[435] =13;
    Xt[436] =14;
    Xt[437] =14;
    Xt[438] =14;
    Xt[439] =14;
    Xt[440] =15;
    Xt[441] =15;
    Xt[442] =15;
    Xt[443] =15;
    Xt[444] =16;
    Xt[445] =16;
    Xt[446] =16;
    Xt[447] =16;
    Xt[448] =18;
    Xt[449] =18;

    Xt[450] =13;
    Xt[451] =13;
    Xt[452] =13;
    Xt[453] =13;
    Xt[454] =14;
    Xt[455] =14;
    Xt[456] =14;
    Xt[457] =14;
    Xt[458] =15;
    Xt[459] =15;
    Xt[460] =15;
    Xt[461] =15;
    Xt[462] =16;
    Xt[463] =16;
    Xt[464] =16;
    Xt[465] =16;
    Xt[466] =18;
    Xt[467] =18;

    Xt[468] =13;
    Xt[469] =13;
    Xt[470] =13;
    Xt[471] =13;
    Xt[472] =14;
    Xt[473] =14;
    Xt[474] =14;
    Xt[475] =14;
    Xt[476] =15;
    Xt[477] =15;
    Xt[478] =15;
    Xt[479] =15;
    Xt[480] =16;
    Xt[481] =16;
    Xt[482] =16;
    Xt[483] =16;
    Xt[484] =18;
    Xt[485] =18;

    Xt[486] =13;
    Xt[487] =13;
    Xt[488] =13;
    Xt[489] =13;
    Xt[490] =14;
    Xt[491] =14;
    Xt[492] =14;
    Xt[493] =14;
    Xt[494] =15;
    Xt[495] =15;
    Xt[496] =15;
    Xt[497] =15;
    Xt[498] =16;
    Xt[499] =16;
    Xt[500] =16;
    Xt[501] =16;
    Xt[502] =18;
    Xt[503] =18;


    Xt[504] =18;
    Xt[505] =17;
    Xt[506] =17;
    Xt[507] =17;
    Xt[508] =18;
    Xt[509] =17;
    Xt[510] =17;
    Xt[511] =17;
    Xt[512] =18;
    Xt[513] =17;
    Xt[514] =17;
    Xt[515] =17;
    Xt[516] =18;
    Xt[517] =17;
    Xt[518] =17;
    Xt[519] =17;
    Xt[520] =18;
    Xt[521] =18;

    Xt[522] =18;
    Xt[523] =18;
    Xt[524] =18;
    Xt[525] =18;
    Xt[526] =18;
    Xt[527] =18;
    Xt[528] =18;
    Xt[529] =18;
    Xt[530] =18;
    Xt[531] =18;
    Xt[532] =18;
    Xt[533] =18;
    Xt[534] =18;
    Xt[535] =18;
    Xt[536] =18;
    Xt[537] =18;
    Xt[538] =18;
    Xt[539] =18;

    Xt[540] =0;
    Xt[541] =0;
    Xt[542] =4;
    Xt[543] =4;
    Xt[544] =0;
    Xt[545] =0;
    Xt[546] =4;
    Xt[547] =4;
    Xt[548] =0;
    Xt[549] =0;
    Xt[550] =4;
    Xt[551] =4;
    Xt[552] =0;
    Xt[553] =0;
    Xt[554] =4;
    Xt[555] =4;
    Xt[556] =18;
    Xt[557] =18;

    Xt[558] =0;
    Xt[559] =0;
    Xt[560] =4;
    Xt[561] =4;
    Xt[562] =0;
    Xt[563] =0;
    Xt[564] =4;
    Xt[565] =4;
    Xt[566] =0;
    Xt[567] =0;
    Xt[568] =4;
    Xt[569] =4;
    Xt[570] =0;
    Xt[571] =0;
    Xt[572] =4;
    Xt[573] =4;
    Xt[574] =18;
    Xt[575] =18;

    Xt[576] =2;
    Xt[577] =2;
    Xt[578] =6;
    Xt[579] =6;
    Xt[580] =2;
    Xt[581] =2;
    Xt[582] =6;
    Xt[583] =6;
    Xt[584] =2;
    Xt[585] =2;
    Xt[586] =6;
    Xt[587] =6;
    Xt[588] =2;
    Xt[589] =2;
    Xt[590] =6;
    Xt[591] =6;
    Xt[592] =18;
    Xt[593] =18;

    Xt[594] =1;
    Xt[595] =1;
    Xt[596] =5;
    Xt[597] =5;
    Xt[598] =1;
    Xt[599] =1;
    Xt[600] =5;
    Xt[601] =5;
    Xt[602] =1;
    Xt[603] =1;
    Xt[604] =5;
    Xt[605] =5;
    Xt[606] =1;
    Xt[607] =1;
    Xt[608] =5;
    Xt[609] =5;
    Xt[610] =18;
    Xt[611] =18;

    Xt[612] =0;
    Xt[613] =0;
    Xt[614] =4;
    Xt[615] =4;
    Xt[616] =0;
    Xt[617] =0;
    Xt[618] =4;
    Xt[619] =4;
    Xt[620] =0;
    Xt[621] =0;
    Xt[622] =4;
    Xt[623] =4;
    Xt[624] =0;
    Xt[625] =0;
    Xt[626] =4;
    Xt[627] =4;
    Xt[628] =18;
    Xt[629] =18;

    Xt[630] =1;
    Xt[631] =1;
    Xt[632] =5;
    Xt[633] =5;
    Xt[634] =1;
    Xt[635] =1;
    Xt[636] =5;
    Xt[637] =5;
    Xt[638] =1;
    Xt[639] =1;
    Xt[640] =5;
    Xt[641] =5;
    Xt[642] =1;
    Xt[643] =1;
    Xt[644] =5;
    Xt[645] =5;
    Xt[646] =18;
    Xt[647] =18;

    Xt[648] =2;
    Xt[649] =2;
    Xt[650] =6;
    Xt[651] =6;
    Xt[652] =2;
    Xt[653] =2;
    Xt[654] =6;
    Xt[655] =6;
    Xt[656] =2;
    Xt[657] =2;
    Xt[658] =6;
    Xt[659] =6;
    Xt[660] =2;
    Xt[661] =2;
    Xt[662] =6;
    Xt[663] =6;
    Xt[664] =18;
    Xt[665] =18;

    Xt[666] =3;
    Xt[667] =3;
    Xt[668] =7;
    Xt[669] =7;
    Xt[670] =3;
    Xt[671] =3;
    Xt[672] =7;
    Xt[673] =7;
    Xt[674] =3;
    Xt[675] =3;
    Xt[676] =7;
    Xt[677] =7;
    Xt[678] =3;
    Xt[679] =3;
    Xt[680] =7;
    Xt[681] =7;
    Xt[682] =18;
    Xt[683] =18;

    Xt[684] =2;
    Xt[685] =3;
    Xt[686] =6;
    Xt[687] =7;
    Xt[688] =2;
    Xt[689] =3;
    Xt[690] =6;
    Xt[691] =7;
    Xt[692] =2;
    Xt[693] =3;
    Xt[694] =6;
    Xt[695] =7;
    Xt[696] =2;
    Xt[697] =3;
    Xt[698] =6;
    Xt[699] =7;
    Xt[700] =18;
    Xt[701] =18;

    Xt[702] =3;
    Xt[703] =1;
    Xt[704] =7;
    Xt[705] =5;
    Xt[706] =3;
    Xt[707] =1;
    Xt[708] =7;
    Xt[709] =5;
    Xt[710] =3;
    Xt[711] =1;
    Xt[712] =7;
    Xt[713] =5;
    Xt[714] =3;
    Xt[715] =1;
    Xt[716] =7;
    Xt[717] =5;
    Xt[718] =18;
    Xt[719] =18;

    Xt[720] =3;
    Xt[721] =3;
    Xt[722] =7;
    Xt[723] =7;
    Xt[724] =3;
    Xt[725] =3;
    Xt[726] =7;
    Xt[727] =7;
    Xt[728] =3;
    Xt[729] =3;
    Xt[730] =7;
    Xt[731] =7;
    Xt[732] =3;
    Xt[733] =3;
    Xt[734] =7;
    Xt[735] =7;
    Xt[736] =18;
    Xt[737] =18;

    Xt[738] =2;
    Xt[739] =2;
    Xt[740] =6;
    Xt[741] =6;
    Xt[742] =2;
    Xt[743] =2;
    Xt[744] =6;
    Xt[745] =6;
    Xt[746] =2;
    Xt[747] =2;
    Xt[748] =6;
    Xt[749] =6;
    Xt[750] =2;
    Xt[751] =2;
    Xt[752] =6;
    Xt[753] =6;
    Xt[754] =18;
    Xt[755] =18;


    Xt[756] =18;
    Xt[757] =18;
    Xt[758] =18;
    Xt[759] =18;
    Xt[760] =18;
    Xt[761] =18;
    Xt[762] =18;
    Xt[763] =18;
    Xt[764] =18;
    Xt[765] =18;
    Xt[766] =18;
    Xt[767] =18;
    Xt[768] =18;
    Xt[769] =18;
    Xt[770] =18;
    Xt[771] =18;
    Xt[772] =18;
    Xt[773] =18;

    Xt[774] =18;
    Xt[775] =18;
    Xt[776] =18;
    Xt[777] =18;
    Xt[778] =18;
    Xt[779] =18;
    Xt[780] =18;
    Xt[781] =18;
    Xt[782] =18;
    Xt[783] =18;
    Xt[784] =18;
    Xt[785] =18;
    Xt[786] =18;
    Xt[787] =18;
    Xt[788] =18;
    Xt[789] =18;
    Xt[790] =18;
    Xt[791] =18;

    Xt[792] =18;
    Xt[793] =18;
    Xt[794] =18;
    Xt[795] =18;
    Xt[796] =18;
    Xt[797] =18;
    Xt[798] =18;
    Xt[799] =18;
    Xt[800] =18;
    Xt[801] =18;
    Xt[802] =18;
    Xt[803] =18;
    Xt[804] =18;
    Xt[805] =18;
    Xt[806] =18;
    Xt[807] =18;
    Xt[808] =18;
    Xt[809] =18;

    Xt[810] =18;
    Xt[811] =18;
    Xt[812] =18;
    Xt[813] =18;
    Xt[814] =18;
    Xt[815] =18;
    Xt[816] =18;
    Xt[817] =18;
    Xt[818] =18;
    Xt[819] =18;
    Xt[820] =18;
    Xt[821] =18;
    Xt[822] =18;
    Xt[823] =18;
    Xt[824] =18;
    Xt[825] =18;
    Xt[826] =18;
    Xt[827] =18;

    Xt[828] =18;
    Xt[829] =18;
    Xt[830] =18;
    Xt[831] =18;
    Xt[832] =18;
    Xt[833] =18;
    Xt[834] =18;
    Xt[835] =18;
    Xt[836] =18;
    Xt[837] =18;
    Xt[838] =18;
    Xt[839] =18;
    Xt[840] =18;
    Xt[841] =18;
    Xt[842] =18;
    Xt[843] =18;
    Xt[844] =18;
    Xt[845] =18;

    Xt[846] =18;
    Xt[847] =18;
    Xt[848] =18;
    Xt[849] =18;
    Xt[850] =18;
    Xt[851] =18;
    Xt[852] =18;
    Xt[853] =18;
    Xt[854] =18;
    Xt[855] =18;
    Xt[856] =18;
    Xt[857] =18;
    Xt[858] =18;
    Xt[859] =18;
    Xt[860] =18;
    Xt[861] =18;
    Xt[862] =18;
    Xt[863] =18;

    Xt[864] =0;
    Xt[865] =0;
    Xt[866] =4;
    Xt[867] =4;
    Xt[868] =0;
    Xt[869] =0;
    Xt[870] =4;
    Xt[871] =4;
    Xt[872] =0;
    Xt[873] =0;
    Xt[874] =4;
    Xt[875] =4;
    Xt[876] =0;
    Xt[877] =0;
    Xt[878] =4;
    Xt[879] =4;
    Xt[880] =18;
    Xt[881] =18;

    Xt[882] =1;
    Xt[883] =1;
    Xt[884] =5;
    Xt[885] =5;
    Xt[886] =1;
    Xt[887] =1;
    Xt[888] =5;
    Xt[889] =5;
    Xt[890] =1;
    Xt[891] =1;
    Xt[892] =5;
    Xt[893] =5;
    Xt[894] =1;
    Xt[895] =1;
    Xt[896] =5;
    Xt[897] =5;
    Xt[898] =18;
    Xt[899] =18;

    Xt[900] =2;
    Xt[901] =2;
    Xt[902] =6;
    Xt[903] =6;
    Xt[904] =2;
    Xt[905] =2;
    Xt[906] =6;
    Xt[907] =6;
    Xt[908] =2;
    Xt[909] =2;
    Xt[910] =6;
    Xt[911] =6;
    Xt[912] =2;
    Xt[913] =2;
    Xt[914] =6;
    Xt[915] =6;
    Xt[916] =18;
    Xt[917] =18;

    Xt[918] =3;
    Xt[919] =3;
    Xt[920] =7;
    Xt[921] =7;
    Xt[922] =3;
    Xt[923] =3;
    Xt[924] =7;
    Xt[925] =7;
    Xt[926] =3;
    Xt[927] =3;
    Xt[928] =7;
    Xt[929] =7;
    Xt[930] =3;
    Xt[931] =3;
    Xt[932] =7;
    Xt[933] =7;
    Xt[934] =18;
    Xt[935] =18;

    Xt[936] =0;
    Xt[937] =2;
    Xt[938] =4;
    Xt[939] =6;
    Xt[940] =0;
    Xt[941] =2;
    Xt[942] =4;
    Xt[943] =6;
    Xt[944] =0;
    Xt[945] =2;
    Xt[946] =4;
    Xt[947] =6;
    Xt[948] =0;
    Xt[949] =2;
    Xt[950] =4;
    Xt[951] =6;
    Xt[952] =18;
    Xt[953] =18;

    Xt[954] =1;
    Xt[955] =0;
    Xt[956] =5;
    Xt[957] =4;
    Xt[958] =1;
    Xt[959] =0;
    Xt[960] =5;
    Xt[961] =4;
    Xt[962] =1;
    Xt[963] =0;
    Xt[964] =5;
    Xt[965] =4;
    Xt[966] =1;
    Xt[967] =0;
    Xt[968] =5;
    Xt[969] =4;
    Xt[970] =18;
    Xt[971] =18;

    Xt[972] =0;
    Xt[973] =0;
    Xt[974] =4;
    Xt[975] =4;
    Xt[976] =0;
    Xt[977] =0;
    Xt[978] =4;
    Xt[979] =4;
    Xt[980] =0;
    Xt[981] =0;
    Xt[982] =4;
    Xt[983] =4;
    Xt[984] =0;
    Xt[985] =0;
    Xt[986] =4;
    Xt[987] =4;
    Xt[988] =18;
    Xt[989] =18;

    Xt[990] =1;
    Xt[991] =1;
    Xt[992] =5;
    Xt[993] =5;
    Xt[994] =1;
    Xt[995] =1;
    Xt[996] =5;
    Xt[997] =5;
    Xt[998] =1;
    Xt[999] =1;
    Xt[1000] =5;
    Xt[1001] =5;
    Xt[1002] =1;
    Xt[1003] =1;
    Xt[1004] =5;
    Xt[1005] =5;
    Xt[1006] =18;
    Xt[1007] =18;


    Xt[1008] =18;
    Xt[1009] =18;
    Xt[1010] =18;
    Xt[1011] =18;
    Xt[1012] =18;
    Xt[1013] =18;
    Xt[1014] =18;
    Xt[1015] =18;
    Xt[1016] =18;
    Xt[1017] =18;
    Xt[1018] =18;
    Xt[1019] =18;
    Xt[1020] =18;
    Xt[1021] =18;
    Xt[1022] =18;
    Xt[1023] =18;
    Xt[1024] =18;
    Xt[1025] =18;

    Xt[1026] =18;
    Xt[1027] =18;
    Xt[1028] =18;
    Xt[1029] =18;
    Xt[1030] =18;
    Xt[1031] =18;
    Xt[1032] =18;
    Xt[1033] =18;
    Xt[1034] =18;
    Xt[1035] =18;
    Xt[1036] =18;
    Xt[1037] =18;
    Xt[1038] =18;
    Xt[1039] =18;
    Xt[1040] =18;
    Xt[1041] =18;
    Xt[1042] =18;
    Xt[1043] =18;

    Xt[1044] =18;
    Xt[1045] =18;
    Xt[1046] =18;
    Xt[1047] =18;
    Xt[1048] =18;
    Xt[1049] =18;
    Xt[1050] =18;
    Xt[1051] =18;
    Xt[1052] =18;
    Xt[1053] =18;
    Xt[1054] =18;
    Xt[1055] =18;
    Xt[1056] =18;
    Xt[1057] =18;
    Xt[1058] =18;
    Xt[1059] =18;
    Xt[1060] =18;
    Xt[1061] =18;

    Xt[1062] =18;
    Xt[1063] =18;
    Xt[1064] =18;
    Xt[1065] =18;
    Xt[1066] =18;
    Xt[1067] =18;
    Xt[1068] =18;
    Xt[1069] =18;
    Xt[1070] =18;
    Xt[1071] =18;
    Xt[1072] =18;
    Xt[1073] =18;
    Xt[1074] =18;
    Xt[1075] =18;
    Xt[1076] =18;
    Xt[1077] =18;
    Xt[1078] =18;
    Xt[1079] =18;

    Xt[1080] =18;
    Xt[1081] =18;
    Xt[1082] =18;
    Xt[1083] =18;
    Xt[1084] =18;
    Xt[1085] =18;
    Xt[1086] =18;
    Xt[1087] =18;
    Xt[1088] =18;
    Xt[1089] =18;
    Xt[1090] =18;
    Xt[1091] =18;
    Xt[1092] =18;
    Xt[1093] =18;
    Xt[1094] =18;
    Xt[1095] =18;
    Xt[1096] =18;
    Xt[1097] =18;

    Xt[1098] =18;
    Xt[1099] =18;
    Xt[1100] =18;
    Xt[1101] =18;
    Xt[1102] =18;
    Xt[1103] =18;
    Xt[1104] =18;
    Xt[1105] =18;
    Xt[1106] =18;
    Xt[1107] =18;
    Xt[1108] =18;
    Xt[1109] =18;
    Xt[1110] =18;
    Xt[1111] =18;
    Xt[1112] =18;
    Xt[1113] =18;
    Xt[1114] =18;
    Xt[1115] =18;

    Xt[1116] =18;
    Xt[1117] =18;
    Xt[1118] =18;
    Xt[1119] =18;
    Xt[1120] =18;
    Xt[1121] =18;
    Xt[1122] =18;
    Xt[1123] =18;
    Xt[1124] =18;
    Xt[1125] =18;
    Xt[1126] =18;
    Xt[1127] =18;
    Xt[1128] =18;
    Xt[1129] =18;
    Xt[1130] =18;
    Xt[1131] =18;
    Xt[1132] =18;
    Xt[1133] =18;

    Xt[1134] =18;
    Xt[1135] =18;
    Xt[1136] =18;
    Xt[1137] =18;
    Xt[1138] =18;
    Xt[1139] =18;
    Xt[1140] =18;
    Xt[1141] =18;
    Xt[1142] =18;
    Xt[1143] =18;
    Xt[1144] =18;
    Xt[1145] =18;
    Xt[1146] =18;
    Xt[1147] =18;
    Xt[1148] =18;
    Xt[1149] =18;
    Xt[1150] =18;
    Xt[1151] =18;

    Xt[1152] =18;
    Xt[1153] =18;
    Xt[1154] =18;
    Xt[1155] =18;
    Xt[1156] =18;
    Xt[1157] =18;
    Xt[1158] =18;
    Xt[1159] =18;
    Xt[1160] =18;
    Xt[1161] =18;
    Xt[1162] =18;
    Xt[1163] =18;
    Xt[1164] =18;
    Xt[1165] =18;
    Xt[1166] =18;
    Xt[1167] =18;
    Xt[1168] =18;
    Xt[1169] =18;

    Xt[1170] =18;
    Xt[1171] =18;
    Xt[1172] =18;
    Xt[1173] =18;
    Xt[1174] =18;
    Xt[1175] =18;
    Xt[1176] =18;
    Xt[1177] =18;
    Xt[1178] =18;
    Xt[1179] =18;
    Xt[1180] =18;
    Xt[1181] =18;
    Xt[1182] =18;
    Xt[1183] =18;
    Xt[1184] =18;
    Xt[1185] =18;
    Xt[1186] =18;
    Xt[1187] =18;

    Xt[1188] =17;
    Xt[1189] =17;
    Xt[1190] =17;
    Xt[1191] =17;
    Xt[1192] =17;
    Xt[1193] =17;
    Xt[1194] =17;
    Xt[1195] =17;
    Xt[1196] =17;
    Xt[1197] =17;
    Xt[1198] =17;
    Xt[1199] =17;
    Xt[1200] =17;
    Xt[1201] =17;
    Xt[1202] =17;
    Xt[1203] =17;
    Xt[1204] =18;
    Xt[1205] =18;

    Xt[1206] =17;
    Xt[1207] =17;
    Xt[1208] =17;
    Xt[1209] =17;
    Xt[1210] =17;
    Xt[1211] =17;
    Xt[1212] =17;
    Xt[1213] =17;
    Xt[1214] =17;
    Xt[1215] =17;
    Xt[1216] =17;
    Xt[1217] =17;
    Xt[1218] =17;
    Xt[1219] =17;
    Xt[1220] =17;
    Xt[1221] =17;
    Xt[1222] =18;
    Xt[1223] =18;

    Xt[1224] =18;
    Xt[1225] =18;
    Xt[1226] =18;
    Xt[1227] =18;
    Xt[1228] =18;
    Xt[1229] =18;
    Xt[1230] =18;
    Xt[1231] =18;
    Xt[1232] =18;
    Xt[1233] =18;
    Xt[1234] =18;
    Xt[1235] =18;
    Xt[1236] =18;
    Xt[1237] =18;
    Xt[1238] =18;
    Xt[1239] =18;
    Xt[1240] =18;
    Xt[1241] =18;

    Xt[1242] =18;
    Xt[1243] =18;
    Xt[1244] =18;
    Xt[1245] =18;
    Xt[1246] =18;
    Xt[1247] =18;
    Xt[1248] =18;
    Xt[1249] =18;
    Xt[1250] =18;
    Xt[1251] =18;
    Xt[1252] =18;
    Xt[1253] =18;
    Xt[1254] =18;
    Xt[1255] =18;
    Xt[1256] =18;
    Xt[1257] =18;
    Xt[1258] =18;
    Xt[1259] =18;
  }
}

#endif

#ifndef Ht_4vTrg_const_mat_X_collection_h
#define Ht_4vTrg_const_mat_X_collection_h
// ============================================================================
// constant matrix indexing the solution vector in \partual H / \partial t out
// 4vTrg problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Ht_4vTrg_const_matrix_X_collection(magma_int_t* Xt)
  {
    Xt[0] =14;
    Xt[1] =14;
    Xt[2] =14;
    Xt[3] =14;
    Xt[4] =14;
    Xt[5] =14;
    Xt[6] =14;
    Xt[7] =14;
    Xt[8] =14;
    Xt[9] =14;
    Xt[10] =14;
    Xt[11] =14;
    Xt[12] =14;
    Xt[13] =14;

    Xt[14] =0;
    Xt[15] =1;
    Xt[16] =2;
    Xt[17] =3;
    Xt[18] =4;
    Xt[19] =5;
    Xt[20] =6;
    Xt[21] =7;
    Xt[22] =0;
    Xt[23] =2;
    Xt[24] =0;
    Xt[25] =0;
    Xt[26] =2;
    Xt[27] =4;

    Xt[28] =8;
    Xt[29] =8;
    Xt[30] =8;
    Xt[31] =8;
    Xt[32] =9;
    Xt[33] =9;
    Xt[34] =11;
    Xt[35] =11;
    Xt[36] =1;
    Xt[37] =3;
    Xt[38] =1;
    Xt[39] =1;
    Xt[40] =3;
    Xt[41] =5;

    Xt[42] =10;
    Xt[43] =10;
    Xt[44] =9;
    Xt[45] =9;
    Xt[46] =10;
    Xt[47] =10;
    Xt[48] =12;
    Xt[49] =12;
    Xt[50] =2;
    Xt[51] =4;
    Xt[52] =4;
    Xt[53] =6;
    Xt[54] =6;
    Xt[55] =6;

    Xt[56] =11;
    Xt[57] =11;
    Xt[58] =12;
    Xt[59] =12;
    Xt[60] =13;
    Xt[61] =13;
    Xt[62] =13;
    Xt[63] =13;
    Xt[64] =3;
    Xt[65] =5;
    Xt[66] =5;
    Xt[67] =7;
    Xt[68] =7;
    Xt[69] =7;

    Xt[70] =8;
    Xt[71] =8;
    Xt[72] =8;
    Xt[73] =8;
    Xt[74] =10;
    Xt[75] =10;
    Xt[76] =11;
    Xt[77] =11;
    Xt[78] =2;
    Xt[79] =4;
    Xt[80] =4;
    Xt[81] =6;
    Xt[82] =6;
    Xt[83] =6;

    Xt[84] =8;
    Xt[85] =8;
    Xt[86] =8;
    Xt[87] =8;
    Xt[88] =10;
    Xt[89] =10;
    Xt[90] =11;
    Xt[91] =11;
    Xt[92] =3;
    Xt[93] =5;
    Xt[94] =5;
    Xt[95] =7;
    Xt[96] =7;
    Xt[97] =7;

    Xt[98] =10;
    Xt[99] =10;
    Xt[100] =9;
    Xt[101] =9;
    Xt[102] =9;
    Xt[103] =9;
    Xt[104] =12;
    Xt[105] =12;
    Xt[106] =2;
    Xt[107] =4;
    Xt[108] =4;
    Xt[109] =6;
    Xt[110] =6;
    Xt[111] =6;

    Xt[112] =10;
    Xt[113] =10;
    Xt[114] =9;
    Xt[115] =9;
    Xt[116] =9;
    Xt[117] =9;
    Xt[118] =12;
    Xt[119] =12;
    Xt[120] =3;
    Xt[121] =5;
    Xt[122] =5;
    Xt[123] =7;
    Xt[124] =7;
    Xt[125] =7;

    Xt[126] =11;
    Xt[127] =11;
    Xt[128] =12;
    Xt[129] =12;
    Xt[130] =13;
    Xt[131] =13;
    Xt[132] =13;
    Xt[133] =13;
    Xt[134] =14;
    Xt[135] =14;
    Xt[136] =14;
    Xt[137] =14;
    Xt[138] =14;
    Xt[139] =14;

    Xt[140] =11;
    Xt[141] =11;
    Xt[142] =12;
    Xt[143] =12;
    Xt[144] =13;
    Xt[145] =13;
    Xt[146] =13;
    Xt[147] =13;
    Xt[148] =14;
    Xt[149] =14;
    Xt[150] =14;
    Xt[151] =14;
    Xt[152] =14;
    Xt[153] =14;


    Xt[154] =14;
    Xt[155] =14;
    Xt[156] =14;
    Xt[157] =14;
    Xt[158] =14;
    Xt[159] =14;
    Xt[160] =14;
    Xt[161] =14;
    Xt[162] =14;
    Xt[163] =14;
    Xt[164] =14;
    Xt[165] =14;
    Xt[166] =14;
    Xt[167] =14;

    Xt[168] =14;
    Xt[169] =14;
    Xt[170] =14;
    Xt[171] =14;
    Xt[172] =14;
    Xt[173] =14;
    Xt[174] =14;
    Xt[175] =14;
    Xt[176] =14;
    Xt[177] =14;
    Xt[178] =14;
    Xt[179] =14;
    Xt[180] =14;
    Xt[181] =14;

    Xt[182] =14;
    Xt[183] =14;
    Xt[184] =14;
    Xt[185] =14;
    Xt[186] =14;
    Xt[187] =14;
    Xt[188] =14;
    Xt[189] =14;
    Xt[190] =14;
    Xt[191] =14;
    Xt[192] =14;
    Xt[193] =14;
    Xt[194] =14;
    Xt[195] =14;

    Xt[196] =14;
    Xt[197] =14;
    Xt[198] =14;
    Xt[199] =14;
    Xt[200] =14;
    Xt[201] =14;
    Xt[202] =14;
    Xt[203] =14;
    Xt[204] =14;
    Xt[205] =14;
    Xt[206] =14;
    Xt[207] =14;
    Xt[208] =14;
    Xt[209] =14;

    Xt[210] =14;
    Xt[211] =14;
    Xt[212] =14;
    Xt[213] =14;
    Xt[214] =14;
    Xt[215] =14;
    Xt[216] =14;
    Xt[217] =14;
    Xt[218] =14;
    Xt[219] =14;
    Xt[220] =14;
    Xt[221] =14;
    Xt[222] =14;
    Xt[223] =14;

    Xt[224] =2;
    Xt[225] =2;
    Xt[226] =0;
    Xt[227] =0;
    Xt[228] =0;
    Xt[229] =0;
    Xt[230] =0;
    Xt[231] =0;
    Xt[232] =0;
    Xt[233] =2;
    Xt[234] =0;
    Xt[235] =0;
    Xt[236] =2;
    Xt[237] =4;

    Xt[238] =3;
    Xt[239] =3;
    Xt[240] =1;
    Xt[241] =1;
    Xt[242] =1;
    Xt[243] =1;
    Xt[244] =1;
    Xt[245] =1;
    Xt[246] =0;
    Xt[247] =2;
    Xt[248] =0;
    Xt[249] =0;
    Xt[250] =2;
    Xt[251] =4;

    Xt[252] =4;
    Xt[253] =4;
    Xt[254] =4;
    Xt[255] =4;
    Xt[256] =2;
    Xt[257] =2;
    Xt[258] =2;
    Xt[259] =2;
    Xt[260] =1;
    Xt[261] =3;
    Xt[262] =1;
    Xt[263] =1;
    Xt[264] =3;
    Xt[265] =5;

    Xt[266] =5;
    Xt[267] =5;
    Xt[268] =5;
    Xt[269] =5;
    Xt[270] =3;
    Xt[271] =3;
    Xt[272] =3;
    Xt[273] =3;
    Xt[274] =1;
    Xt[275] =3;
    Xt[276] =1;
    Xt[277] =1;
    Xt[278] =3;
    Xt[279] =5;

    Xt[280] =6;
    Xt[281] =6;
    Xt[282] =6;
    Xt[283] =6;
    Xt[284] =6;
    Xt[285] =6;
    Xt[286] =4;
    Xt[287] =4;
    Xt[288] =14;
    Xt[289] =14;
    Xt[290] =14;
    Xt[291] =14;
    Xt[292] =14;
    Xt[293] =14;

    Xt[294] =7;
    Xt[295] =7;
    Xt[296] =7;
    Xt[297] =7;
    Xt[298] =7;
    Xt[299] =7;
    Xt[300] =5;
    Xt[301] =5;
    Xt[302] =14;
    Xt[303] =14;
    Xt[304] =14;
    Xt[305] =14;
    Xt[306] =14;
    Xt[307] =14;
  }
}

#endif

#ifndef Ht_4vTrg_const_mat_Y_h
#define Ht_4vTrg_const_mat_Y_h
// ============================================================================
// cd index vector for Ht of 4vTrg problem
//
// Modifications
//    Chien  21-09-06:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Ht_4vTrg_const_matrix_Y(magma_int_t* Y)
  {
    Y[0] =10;
    Y[1] =20;
    Y[2] =28;
    Y[3] =36;
    Y[4] =42;
    Y[5] =48;
    Y[6] =52;
    Y[7] =56;
    Y[8] =57;
    Y[9] =58;
    Y[10] =59;
    Y[11] =60;
    Y[12] =61;
    Y[13] =62;

    Y[14] =0;
    Y[15] =0;
    Y[16] =0;
    Y[17] =0;
    Y[18] =0;
    Y[19] =0;
    Y[20] =0;
    Y[21] =0;
    Y[22] =7;
    Y[23] =26;
    Y[24] =8;
    Y[25] =9;
    Y[26] =27;
    Y[27] =41;

    Y[28] =7;
    Y[29] =17;
    Y[30] =25;
    Y[31] =33;
    Y[32] =39;
    Y[33] =45;
    Y[34] =49;
    Y[35] =53;
    Y[36] =17;
    Y[37] =34;
    Y[38] =18;
    Y[39] =19;
    Y[40] =35;
    Y[41] =47;

    Y[42] =8;
    Y[43] =18;
    Y[44] =26;
    Y[45] =34;
    Y[46] =40;
    Y[47] =46;
    Y[48] =50;
    Y[49] =54;
    Y[50] =25;
    Y[51] =39;
    Y[52] =40;
    Y[53] =49;
    Y[54] =50;
    Y[55] =51;

    Y[56] =9;
    Y[57] =19;
    Y[58] =27;
    Y[59] =35;
    Y[60] =41;
    Y[61] =47;
    Y[62] =51;
    Y[63] =55;
    Y[64] =33;
    Y[65] =45;
    Y[66] =46;
    Y[67] =53;
    Y[68] =54;
    Y[69] =55;

    Y[70] =1;
    Y[71] =11;
    Y[72] =1;
    Y[73] =2;
    Y[74] =3;
    Y[75] =4;
    Y[76] =5;
    Y[77] =6;
    Y[78] =1;
    Y[79] =21;
    Y[80] =3;
    Y[81] =5;
    Y[82] =23;
    Y[83] =37;

    Y[84] =2;
    Y[85] =12;
    Y[86] =11;
    Y[87] =12;
    Y[88] =13;
    Y[89] =14;
    Y[90] =15;
    Y[91] =16;
    Y[92] =2;
    Y[93] =22;
    Y[94] =4;
    Y[95] =6;
    Y[96] =24;
    Y[97] =38;

    Y[98] =3;
    Y[99] =13;
    Y[100] =21;
    Y[101] =29;
    Y[102] =21;
    Y[103] =22;
    Y[104] =23;
    Y[105] =24;
    Y[106] =11;
    Y[107] =29;
    Y[108] =13;
    Y[109] =15;
    Y[110] =31;
    Y[111] =43;

    Y[112] =4;
    Y[113] =14;
    Y[114] =22;
    Y[115] =30;
    Y[116] =29;
    Y[117] =30;
    Y[118] =31;
    Y[119] =32;
    Y[120] =12;
    Y[121] =30;
    Y[122] =14;
    Y[123] =16;
    Y[124] =32;
    Y[125] =44;

    Y[126] =5;
    Y[127] =15;
    Y[128] =23;
    Y[129] =31;
    Y[130] =37;
    Y[131] =43;
    Y[132] =37;
    Y[133] =38;
    Y[134] =0;
    Y[135] =0;
    Y[136] =0;
    Y[137] =0;
    Y[138] =0;
    Y[139] =0;

    Y[140] =6;
    Y[141] =16;
    Y[142] =24;
    Y[143] =32;
    Y[144] =38;
    Y[145] =44;
    Y[146] =43;
    Y[147] =44;
    Y[148] =0;
    Y[149] =0;
    Y[150] =0;
    Y[151] =0;
    Y[152] =0;
    Y[153] =0;

  }
}

#endif

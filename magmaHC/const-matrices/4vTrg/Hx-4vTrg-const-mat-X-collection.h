#ifndef Hx_4vTrg_const_mat_X_h
#define Hx_4vTrg_const_mat_X_h
// ============================================================================
// indices of solution matrix for the 4vTrg problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Hx_4vTrg_const_matrix_X_collection(magma_int_t* X)
  {
    X[0] =14;
    X[1] =14;
    X[2] =8;
    X[3] =8;
    X[4] =10;
    X[5] =10;
    X[6] =11;
    X[7] =11;
    X[8] =14;
    X[9] =14;
    X[10] =14;
    X[11] =14;
    X[12] =14;
    X[13] =14;
    X[14] =14;
    X[15] =14;
    X[16] =8;
    X[17] =8;
    X[18] =10;
    X[19] =10;
    X[20] =11;
    X[21] =11;
    X[22] =14;
    X[23] =14;
    X[24] =14;
    X[25] =14;
    X[26] =14;
    X[27] =14;
    X[28] =8;
    X[29] =8;
    X[30] =14;
    X[31] =14;
    X[32] =9;
    X[33] =9;
    X[34] =12;
    X[35] =12;
    X[36] =14;
    X[37] =14;
    X[38] =14;
    X[39] =14;
    X[40] =14;
    X[41] =14;
    X[42] =8;
    X[43] =8;
    X[44] =14;
    X[45] =14;
    X[46] =9;
    X[47] =9;
    X[48] =12;
    X[49] =12;
    X[50] =14;
    X[51] =14;
    X[52] =14;
    X[53] =14;
    X[54] =14;
    X[55] =14;
    X[56] =10;
    X[57] =10;
    X[58] =9;
    X[59] =9;
    X[60] =14;
    X[61] =14;
    X[62] =13;
    X[63] =13;
    X[64] =14;
    X[65] =14;
    X[66] =14;
    X[67] =14;
    X[68] =14;
    X[69] =14;
    X[70] =10;
    X[71] =10;
    X[72] =9;
    X[73] =9;
    X[74] =14;
    X[75] =14;
    X[76] =13;
    X[77] =13;
    X[78] =14;
    X[79] =14;
    X[80] =14;
    X[81] =14;
    X[82] =14;
    X[83] =14;
    X[84] =11;
    X[85] =11;
    X[86] =12;
    X[87] =12;
    X[88] =13;
    X[89] =13;
    X[90] =14;
    X[91] =14;
    X[92] =14;
    X[93] =14;
    X[94] =14;
    X[95] =14;
    X[96] =14;
    X[97] =14;
    X[98] =11;
    X[99] =11;
    X[100] =12;
    X[101] =12;
    X[102] =13;
    X[103] =13;
    X[104] =14;
    X[105] =14;
    X[106] =14;
    X[107] =14;
    X[108] =14;
    X[109] =14;
    X[110] =14;
    X[111] =14;
    X[112] =14;
    X[113] =14;
    X[114] =14;
    X[115] =14;
    X[116] =14;
    X[117] =14;
    X[118] =14;
    X[119] =14;
    X[120] =14;
    X[121] =14;
    X[122] =14;
    X[123] =14;
    X[124] =14;
    X[125] =14;
    X[126] =14;
    X[127] =14;
    X[128] =14;
    X[129] =14;
    X[130] =14;
    X[131] =14;
    X[132] =14;
    X[133] =14;
    X[134] =14;
    X[135] =14;
    X[136] =14;
    X[137] =14;
    X[138] =14;
    X[139] =14;
    X[140] =14;
    X[141] =14;
    X[142] =14;
    X[143] =14;
    X[144] =14;
    X[145] =14;
    X[146] =14;
    X[147] =14;
    X[148] =14;
    X[149] =14;
    X[150] =14;
    X[151] =14;
    X[152] =14;
    X[153] =14;
    X[154] =14;
    X[155] =14;
    X[156] =14;
    X[157] =14;
    X[158] =14;
    X[159] =14;
    X[160] =14;
    X[161] =14;
    X[162] =14;
    X[163] =14;
    X[164] =14;
    X[165] =14;
    X[166] =14;
    X[167] =14;
    X[168] =14;
    X[169] =14;
    X[170] =14;
    X[171] =14;
    X[172] =14;
    X[173] =14;
    X[174] =14;
    X[175] =14;
    X[176] =14;
    X[177] =14;
    X[178] =14;
    X[179] =14;
    X[180] =14;
    X[181] =14;
    X[182] =14;
    X[183] =14;
    X[184] =14;
    X[185] =14;
    X[186] =14;
    X[187] =14;
    X[188] =14;
    X[189] =14;
    X[190] =14;
    X[191] =14;
    X[192] =14;
    X[193] =14;
    X[194] =14;
    X[195] =14;
    X[196] =14;
    X[197] =14;
    X[198] =14;
    X[199] =14;
    X[200] =14;
    X[201] =14;
    X[202] =14;
    X[203] =14;
    X[204] =2;
    X[205] =14;
    X[206] =4;
    X[207] =6;
    X[208] =14;
    X[209] =14;
    X[210] =14;
    X[211] =14;
    X[212] =14;
    X[213] =14;
    X[214] =14;
    X[215] =14;
    X[216] =14;
    X[217] =14;
    X[218] =2;
    X[219] =14;
    X[220] =4;
    X[221] =6;
    X[222] =14;
    X[223] =14;
    X[224] =14;
    X[225] =14;
    X[226] =14;
    X[227] =14;
    X[228] =14;
    X[229] =14;
    X[230] =14;
    X[231] =14;
    X[232] =0;
    X[233] =4;
    X[234] =14;
    X[235] =14;
    X[236] =6;
    X[237] =14;
    X[238] =14;
    X[239] =14;
    X[240] =14;
    X[241] =14;
    X[242] =14;
    X[243] =14;
    X[244] =14;
    X[245] =14;
    X[246] =0;
    X[247] =4;
    X[248] =14;
    X[249] =14;
    X[250] =6;
    X[251] =14;
    X[252] =14;
    X[253] =14;
    X[254] =14;
    X[255] =14;
    X[256] =14;
    X[257] =14;
    X[258] =14;
    X[259] =14;
    X[260] =14;
    X[261] =2;
    X[262] =0;
    X[263] =14;
    X[264] =14;
    X[265] =6;
    X[266] =14;
    X[267] =14;
    X[268] =14;
    X[269] =14;
    X[270] =14;
    X[271] =14;
    X[272] =14;
    X[273] =14;
    X[274] =14;
    X[275] =2;
    X[276] =0;
    X[277] =14;
    X[278] =14;
    X[279] =6;
    X[280] =14;
    X[281] =14;
    X[282] =14;
    X[283] =14;
    X[284] =14;
    X[285] =14;
    X[286] =14;
    X[287] =14;
    X[288] =14;
    X[289] =14;
    X[290] =14;
    X[291] =0;
    X[292] =2;
    X[293] =4;
    X[294] =14;
    X[295] =14;
    X[296] =14;
    X[297] =14;
    X[298] =14;
    X[299] =14;
    X[300] =14;
    X[301] =14;
    X[302] =14;
    X[303] =14;
    X[304] =14;
    X[305] =0;
    X[306] =2;
    X[307] =4;
    X[308] =2;
    X[309] =2;
    X[310] =0;
    X[311] =0;
    X[312] =14;
    X[313] =14;
    X[314] =14;
    X[315] =14;
    X[316] =14;
    X[317] =14;
    X[318] =14;
    X[319] =14;
    X[320] =14;
    X[321] =14;
    X[322] =14;
    X[323] =14;
    X[324] =4;
    X[325] =4;
    X[326] =2;
    X[327] =2;
    X[328] =14;
    X[329] =14;
    X[330] =14;
    X[331] =14;
    X[332] =14;
    X[333] =14;
    X[334] =14;
    X[335] =14;
    X[336] =4;
    X[337] =4;
    X[338] =14;
    X[339] =14;
    X[340] =0;
    X[341] =0;
    X[342] =14;
    X[343] =14;
    X[344] =14;
    X[345] =14;
    X[346] =14;
    X[347] =14;
    X[348] =14;
    X[349] =14;
    X[350] =6;
    X[351] =6;
    X[352] =14;
    X[353] =14;
    X[354] =14;
    X[355] =14;
    X[356] =0;
    X[357] =0;
    X[358] =14;
    X[359] =14;
    X[360] =14;
    X[361] =14;
    X[362] =14;
    X[363] =14;
    X[364] =14;
    X[365] =14;
    X[366] =6;
    X[367] =6;
    X[368] =14;
    X[369] =14;
    X[370] =2;
    X[371] =2;
    X[372] =14;
    X[373] =14;
    X[374] =14;
    X[375] =14;
    X[376] =14;
    X[377] =14;
    X[378] =14;
    X[379] =14;
    X[380] =14;
    X[381] =14;
    X[382] =6;
    X[383] =6;
    X[384] =4;
    X[385] =4;
    X[386] =14;
    X[387] =14;
    X[388] =14;
    X[389] =14;
    X[390] =14;
    X[391] =14;
    X[392] =14;
    X[393] =14;
    X[394] =14;
    X[395] =14;
    X[396] =14;
    X[397] =14;
    X[398] =14;
    X[399] =14;
    X[400] =3;
    X[401] =14;
    X[402] =5;
    X[403] =7;
    X[404] =14;
    X[405] =14;
    X[406] =14;
    X[407] =14;
    X[408] =14;
    X[409] =14;
    X[410] =14;
    X[411] =14;
    X[412] =14;
    X[413] =14;
    X[414] =3;
    X[415] =14;
    X[416] =5;
    X[417] =7;
    X[418] =14;
    X[419] =14;
    X[420] =14;
    X[421] =14;
    X[422] =14;
    X[423] =14;
    X[424] =14;
    X[425] =14;
    X[426] =14;
    X[427] =14;
    X[428] =1;
    X[429] =5;
    X[430] =14;
    X[431] =14;
    X[432] =7;
    X[433] =14;
    X[434] =14;
    X[435] =14;
    X[436] =14;
    X[437] =14;
    X[438] =14;
    X[439] =14;
    X[440] =14;
    X[441] =14;
    X[442] =1;
    X[443] =5;
    X[444] =14;
    X[445] =14;
    X[446] =7;
    X[447] =14;
    X[448] =14;
    X[449] =14;
    X[450] =14;
    X[451] =14;
    X[452] =14;
    X[453] =14;
    X[454] =14;
    X[455] =14;
    X[456] =14;
    X[457] =3;
    X[458] =1;
    X[459] =14;
    X[460] =14;
    X[461] =7;
    X[462] =14;
    X[463] =14;
    X[464] =14;
    X[465] =14;
    X[466] =14;
    X[467] =14;
    X[468] =14;
    X[469] =14;
    X[470] =14;
    X[471] =3;
    X[472] =1;
    X[473] =14;
    X[474] =14;
    X[475] =7;
    X[476] =14;
    X[477] =14;
    X[478] =14;
    X[479] =14;
    X[480] =14;
    X[481] =14;
    X[482] =14;
    X[483] =14;
    X[484] =14;
    X[485] =14;
    X[486] =14;
    X[487] =1;
    X[488] =3;
    X[489] =5;
    X[490] =14;
    X[491] =14;
    X[492] =14;
    X[493] =14;
    X[494] =14;
    X[495] =14;
    X[496] =14;
    X[497] =14;
    X[498] =14;
    X[499] =14;
    X[500] =14;
    X[501] =1;
    X[502] =3;
    X[503] =5;
    X[504] =3;
    X[505] =3;
    X[506] =1;
    X[507] =1;
    X[508] =14;
    X[509] =14;
    X[510] =14;
    X[511] =14;
    X[512] =14;
    X[513] =14;
    X[514] =14;
    X[515] =14;
    X[516] =14;
    X[517] =14;
    X[518] =14;
    X[519] =14;
    X[520] =5;
    X[521] =5;
    X[522] =3;
    X[523] =3;
    X[524] =14;
    X[525] =14;
    X[526] =14;
    X[527] =14;
    X[528] =14;
    X[529] =14;
    X[530] =14;
    X[531] =14;
    X[532] =5;
    X[533] =5;
    X[534] =14;
    X[535] =14;
    X[536] =1;
    X[537] =1;
    X[538] =14;
    X[539] =14;
    X[540] =14;
    X[541] =14;
    X[542] =14;
    X[543] =14;
    X[544] =14;
    X[545] =14;
    X[546] =7;
    X[547] =7;
    X[548] =14;
    X[549] =14;
    X[550] =14;
    X[551] =14;
    X[552] =1;
    X[553] =1;
    X[554] =14;
    X[555] =14;
    X[556] =14;
    X[557] =14;
    X[558] =14;
    X[559] =14;
    X[560] =14;
    X[561] =14;
    X[562] =7;
    X[563] =7;
    X[564] =14;
    X[565] =14;
    X[566] =3;
    X[567] =3;
    X[568] =14;
    X[569] =14;
    X[570] =14;
    X[571] =14;
    X[572] =14;
    X[573] =14;
    X[574] =14;
    X[575] =14;
    X[576] =14;
    X[577] =14;
    X[578] =7;
    X[579] =7;
    X[580] =5;
    X[581] =5;
    X[582] =14;
    X[583] =14;
    X[584] =14;
    X[585] =14;
    X[586] =14;
    X[587] =14;

  }
}

#endif

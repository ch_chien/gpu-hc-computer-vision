#ifndef Ht_optpose4pt_const_mat_s_h
#define Ht_optpose4pt_const_mat_s_h
// ============================================================================
// scalar const matrix for Ht of optpose4pt problem
//
// Modifications
//    Chien  21-09-14:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Ht_optpose4pt_const_matrix_scalar(magma_int_t* s)
  {
    s[0] =1;
    s[1] =1;
    s[2] =1;
    s[3] =1;
    s[4] =1;

    s[5] =2;
    s[6] =2;
    s[7] =2;
    s[8] =2;
    s[9] =1;

    s[10] =1;
    s[11] =1;
    s[12] =1;
    s[13] =1;
    s[14] =1;

    s[15] =1;
    s[16] =1;
    s[17] =1;
    s[18] =1;
    s[19] =0;

    s[20] =1;
    s[21] =1;
    s[22] =1;
    s[23] =1;
    s[24] =0;

    s[25] =1;
    s[26] =1;
    s[27] =1;
    s[28] =1;
    s[29] =0;

    s[30] =1;
    s[31] =1;
    s[32] =1;
    s[33] =1;
    s[34] =0;

    s[35] =1;
    s[36] =1;
    s[37] =1;
    s[38] =1;
    s[39] =0;

    s[40] =1;
    s[41] =1;
    s[42] =1;
    s[43] =1;
    s[44] =0;

    s[45] =1;
    s[46] =1;
    s[47] =1;
    s[48] =1;
    s[49] =0;

    s[50] =1;
    s[51] =1;
    s[52] =1;
    s[53] =1;
    s[54] =0;

    s[55] =1;
    s[56] =1;
    s[57] =1;
    s[58] =1;
    s[59] =0;

    s[60] =1;
    s[61] =1;
    s[62] =1;
    s[63] =1;
    s[64] =0;

    s[65] =1;
    s[66] =1;
    s[67] =1;
    s[68] =1;
    s[69] =0;

    s[70] =1;
    s[71] =1;
    s[72] =1;
    s[73] =1;
    s[74] =0;

    s[75] =1;
    s[76] =1;
    s[77] =1;
    s[78] =1;
    s[79] =0;

    s[80] =1;
    s[81] =1;
    s[82] =1;
    s[83] =1;
    s[84] =0;

    s[85] =1;
    s[86] =1;
    s[87] =1;
    s[88] =1;
    s[89] =0;

    s[90] =1;
    s[91] =1;
    s[92] =1;
    s[93] =1;
    s[94] =0;

    s[95] =1;
    s[96] =1;
    s[97] =1;
    s[98] =1;
    s[99] =0;

    s[100] =1;
    s[101] =1;
    s[102] =1;
    s[103] =1;
    s[104] =0;
  }
}

#endif

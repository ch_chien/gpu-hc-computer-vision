#ifndef Ht_optpose4pt_const_mat_X_collection_h
#define Ht_optpose4pt_const_mat_X_collection_h
// ============================================================================
// constant matrix indexing the solution vector in \partual H / \partial t out
// optpose4pt problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Ht_optpose4pt_const_matrix_X_collection(magma_int_t* Xt)
  {
    Xt[0] =5;
    Xt[1] =5;
    Xt[2] =5;
    Xt[3] =5;
    Xt[4] =5;

    Xt[5] =0;
    Xt[6] =0;
    Xt[7] =0;
    Xt[8] =0;
    Xt[9] =3;

    Xt[10] =1;
    Xt[11] =1;
    Xt[12] =1;
    Xt[13] =1;
    Xt[14] =4;

    Xt[15] =2;
    Xt[16] =2;
    Xt[17] =2;
    Xt[18] =2;
    Xt[19] =5;

    Xt[20] =3;
    Xt[21] =3;
    Xt[22] =3;
    Xt[23] =3;
    Xt[24] =5;

    Xt[25] =4;
    Xt[26] =4;
    Xt[27] =4;
    Xt[28] =4;
    Xt[29] =5;

    Xt[30] =0;
    Xt[31] =0;
    Xt[32] =0;
    Xt[33] =0;
    Xt[34] =5;

    Xt[35] =1;
    Xt[36] =1;
    Xt[37] =1;
    Xt[38] =1;
    Xt[39] =5;

    Xt[40] =2;
    Xt[41] =2;
    Xt[42] =2;
    Xt[43] =2;
    Xt[44] =5;

    Xt[45] =3;
    Xt[46] =3;
    Xt[47] =3;
    Xt[48] =3;
    Xt[49] =5;

    Xt[50] =4;
    Xt[51] =4;
    Xt[52] =4;
    Xt[53] =4;
    Xt[54] =5;

    Xt[55] =1;
    Xt[56] =1;
    Xt[57] =1;
    Xt[58] =1;
    Xt[59] =5;

    Xt[60] =2;
    Xt[61] =2;
    Xt[62] =2;
    Xt[63] =2;
    Xt[64] =5;

    Xt[65] =3;
    Xt[66] =3;
    Xt[67] =3;
    Xt[68] =3;
    Xt[69] =5;

    Xt[70] =4;
    Xt[71] =4;
    Xt[72] =4;
    Xt[73] =4;
    Xt[74] =5;

    Xt[75] =2;
    Xt[76] =2;
    Xt[77] =2;
    Xt[78] =2;
    Xt[79] =5;

    Xt[80] =3;
    Xt[81] =3;
    Xt[82] =3;
    Xt[83] =3;
    Xt[84] =5;

    Xt[85] =4;
    Xt[86] =4;
    Xt[87] =4;
    Xt[88] =4;
    Xt[89] =5;

    Xt[90] =3;
    Xt[91] =3;
    Xt[92] =3;
    Xt[93] =3;
    Xt[94] =5;

    Xt[95] =4;
    Xt[96] =4;
    Xt[97] =4;
    Xt[98] =4;
    Xt[99] =5;

    Xt[100] =4;
    Xt[101] =4;
    Xt[102] =4;
    Xt[103] =4;
    Xt[104] =5;


    Xt[105] =5;
    Xt[106] =5;
    Xt[107] =5;
    Xt[108] =5;
    Xt[109] =5;

    Xt[110] =5;
    Xt[111] =5;
    Xt[112] =5;
    Xt[113] =5;
    Xt[114] =3;

    Xt[115] =5;
    Xt[116] =5;
    Xt[117] =5;
    Xt[118] =5;
    Xt[119] =4;

    Xt[120] =5;
    Xt[121] =5;
    Xt[122] =5;
    Xt[123] =5;
    Xt[124] =5;

    Xt[125] =5;
    Xt[126] =5;
    Xt[127] =5;
    Xt[128] =5;
    Xt[129] =5;

    Xt[130] =5;
    Xt[131] =5;
    Xt[132] =5;
    Xt[133] =5;
    Xt[134] =5;

    Xt[135] =0;
    Xt[136] =0;
    Xt[137] =0;
    Xt[138] =0;
    Xt[139] =5;

    Xt[140] =1;
    Xt[141] =1;
    Xt[142] =1;
    Xt[143] =1;
    Xt[144] =5;

    Xt[145] =2;
    Xt[146] =2;
    Xt[147] =2;
    Xt[148] =2;
    Xt[149] =5;

    Xt[150] =3;
    Xt[151] =3;
    Xt[152] =3;
    Xt[153] =3;
    Xt[154] =5;

    Xt[155] =4;
    Xt[156] =4;
    Xt[157] =4;
    Xt[158] =4;
    Xt[159] =5;

    Xt[160] =0;
    Xt[161] =0;
    Xt[162] =0;
    Xt[163] =0;
    Xt[164] =5;

    Xt[165] =0;
    Xt[166] =0;
    Xt[167] =0;
    Xt[168] =0;
    Xt[169] =5;

    Xt[170] =0;
    Xt[171] =0;
    Xt[172] =0;
    Xt[173] =0;
    Xt[174] =5;

    Xt[175] =0;
    Xt[176] =0;
    Xt[177] =0;
    Xt[178] =0;
    Xt[179] =5;

    Xt[180] =1;
    Xt[181] =1;
    Xt[182] =1;
    Xt[183] =1;
    Xt[184] =5;

    Xt[185] =1;
    Xt[186] =1;
    Xt[187] =1;
    Xt[188] =1;
    Xt[189] =5;

    Xt[190] =1;
    Xt[191] =1;
    Xt[192] =1;
    Xt[193] =1;
    Xt[194] =5;

    Xt[195] =2;
    Xt[196] =2;
    Xt[197] =2;
    Xt[198] =2;
    Xt[199] =5;

    Xt[200] =2;
    Xt[201] =2;
    Xt[202] =2;
    Xt[203] =2;
    Xt[204] =5;

    Xt[205] =3;
    Xt[206] =3;
    Xt[207] =3;
    Xt[208] =3;
    Xt[209] =5;
  }
}

#endif

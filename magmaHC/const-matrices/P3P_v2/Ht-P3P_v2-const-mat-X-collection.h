#ifndef Ht_P3P_v2_const_mat_X_collection_h
#define Ht_P3P_v2_const_mat_X_collection_h
// ============================================================================
// constant matrix indexing the solution vector in \partual H / \partial t out
// P3P_v2 problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Ht_P3P_v2_const_matrix_X_collection(magma_int_t* Xt)
  {
    Xt[0] =3;
    Xt[1] =3;
    Xt[2] =3;

    Xt[3] =0;
    Xt[4] =0;
    Xt[5] =1;

    Xt[6] =1;
    Xt[7] =2;
    Xt[8] =2;

    Xt[9] =1;
    Xt[10] =2;
    Xt[11] =2;


    Xt[12] =3;
    Xt[13] =3;
    Xt[14] =3;

    Xt[15] =0;
    Xt[16] =0;
    Xt[17] =1;

    Xt[18] =1;
    Xt[19] =2;
    Xt[20] =2;

    Xt[21] =0;
    Xt[22] =0;
    Xt[23] =1;
  }
}

#endif

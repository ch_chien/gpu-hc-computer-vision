#ifndef Hx_P3P_v2_const_mat_Y_h
#define Hx_P3P_v2_const_mat_Y_h
// ============================================================================
// cdt index matrix for Hx of P3P_v2 problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Hx_P3P_v2_const_matrix_Y(magma_int_t* Y)
  {
    Y[0] =0;
    Y[1] =0;
    Y[2] =9;

    Y[3] =1;
    Y[4] =9;
    Y[5] =2;

    Y[6] =9;
    Y[7] =4;
    Y[8] =5;

    Y[9] =1;
    Y[10] =4;
    Y[11] =9;

    Y[12] =2;
    Y[13] =9;
    Y[14] =7;

    Y[15] =9;
    Y[16] =5;
    Y[17] =7;
  }
}

#endif

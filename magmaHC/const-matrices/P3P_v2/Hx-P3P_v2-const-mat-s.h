#ifndef Hx_P3P_v2_const_mat_s_h
#define Hx_P3P_v2_const_mat_s_h
// ============================================================================
// scalar const matrix for Hx in P3P_v2 problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Hx_P3P_v2_const_matrix_scalar(magma_int_t* s)
  {
    s[0] =2;
    s[1] =2;
    s[2] =0;

    s[3] =1;
    s[4] =0;
    s[5] =2;

    s[6] =0;
    s[7] =1;
    s[8] =2;

    s[9] =1;
    s[10] =1;
    s[11] =0;

    s[12] =2;
    s[13] =0;
    s[14] =1;

    s[15] =0;
    s[16] =2;
    s[17] =1;
  }
}

#endif

#ifndef Ht_P3P_v2_const_mat_Y_h
#define Ht_P3P_v2_const_mat_Y_h
// ============================================================================
// cd index vector for Ht of P3P_v2 problem
//
// Modifications
//    Chien  21-09-06:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Ht_P3P_v2_const_matrix_Y(magma_int_t* Y)
  {
    Y[0] =3;
    Y[1] =6;
    Y[2] =8;

    Y[3] =0;
    Y[4] =0;
    Y[5] =2;

    Y[6] =2;
    Y[7] =5;
    Y[8] =5;

    Y[9] =1;
    Y[10] =4;
    Y[11] =7;
  }
}

#endif

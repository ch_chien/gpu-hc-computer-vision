#ifndef Ht_P3P_v2_const_mat_s_h
#define Ht_P3P_v2_const_mat_s_h
// ============================================================================
// scalar const matrix for Ht of P3P_v2 problem
//
// Modifications
//    Chien  21-11-16:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Ht_P3P_v2_const_matrix_scalar(magma_int_t* s)
  {
    s[0] =1;
    s[1] =1;
    s[2] =1;

    s[3] =1;
    s[4] =1;
    s[5] =1;

    s[6] =1;
    s[7] =1;
    s[8] =1;

    s[9] =1;
    s[10] =1;
    s[11] =1;
  }
}

#endif

#ifndef Hx_P3P_v2_const_mat_X_h
#define Hx_P3P_v2_const_mat_X_h
// ============================================================================
// indices of solution matrix for the P3P_v2 problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Hx_P3P_v2_const_matrix_X_collection(magma_int_t* X)
  {
    X[0] =0;
    X[1] =0;
    X[2] =3;
    X[3] =0;
    X[4] =3;
    X[5] =1;
    X[6] =3;
    X[7] =0;
    X[8] =2;
    X[9] =1;
    X[10] =2;
    X[11] =3;
    X[12] =1;
    X[13] =3;
    X[14] =2;
    X[15] =3;
    X[16] =2;
    X[17] =1;
  }
}

#endif

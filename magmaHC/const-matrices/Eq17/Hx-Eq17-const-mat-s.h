#ifndef Hx_Eq17_const_mat_s_h
#define Hx_Eq17_const_mat_s_h
// ============================================================================
// scalar const matrix for Hx in Eq17 problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Hx_Eq17_const_matrix_scalar(magma_int_t* s)
  {
    s[0] =2;
    s[1] =0;
    s[2] =0;
    s[3] =0;
    s[4] =2;
    s[5] =0;
    s[6] =2;
    s[7] =2;
    s[8] =2;
    s[9] =2;

    s[10] =0;
    s[11] =2;
    s[12] =0;
    s[13] =0;
    s[14] =2;
    s[15] =0;
    s[16] =2;
    s[17] =2;
    s[18] =2;
    s[19] =2;

    s[20] =0;
    s[21] =0;
    s[22] =2;
    s[23] =0;
    s[24] =0;
    s[25] =2;
    s[26] =2;
    s[27] =2;
    s[28] =2;
    s[29] =2;

    s[30] =0;
    s[31] =0;
    s[32] =0;
    s[33] =2;
    s[34] =0;
    s[35] =2;
    s[36] =2;
    s[37] =2;
    s[38] =2;
    s[39] =2;

    s[40] =2;
    s[41] =2;
    s[42] =0;
    s[43] =0;
    s[44] =0;
    s[45] =0;
    s[46] =2;
    s[47] =2;
    s[48] =2;
    s[49] =2;

    s[50] =0;
    s[51] =0;
    s[52] =2;
    s[53] =2;
    s[54] =0;
    s[55] =0;
    s[56] =2;
    s[57] =2;
    s[58] =2;
    s[59] =2;

    s[60] =2;
    s[61] =2;
    s[62] =2;
    s[63] =2;
    s[64] =2;
    s[65] =2;
    s[66] =0;
    s[67] =0;
    s[68] =0;
    s[69] =0;

    s[70] =2;
    s[71] =2;
    s[72] =2;
    s[73] =2;
    s[74] =2;
    s[75] =2;
    s[76] =0;
    s[77] =0;
    s[78] =0;
    s[79] =0;

    s[80] =2;
    s[81] =2;
    s[82] =2;
    s[83] =2;
    s[84] =2;
    s[85] =2;
    s[86] =0;
    s[87] =0;
    s[88] =0;
    s[89] =0;

    s[90] =2;
    s[91] =2;
    s[92] =2;
    s[93] =2;
    s[94] =2;
    s[95] =2;
    s[96] =0;
    s[97] =0;
    s[98] =0;
    s[99] =0;

    s[100] =0;
    s[101] =0;
    s[102] =0;
    s[103] =0;
    s[104] =2;
    s[105] =0;
    s[106] =0;
    s[107] =0;
    s[108] =0;
    s[109] =0;

    s[110] =0;
    s[111] =0;
    s[112] =0;
    s[113] =0;
    s[114] =2;
    s[115] =0;
    s[116] =0;
    s[117] =0;
    s[118] =0;
    s[119] =0;

    s[120] =0;
    s[121] =0;
    s[122] =0;
    s[123] =0;
    s[124] =0;
    s[125] =2;
    s[126] =0;
    s[127] =0;
    s[128] =0;
    s[129] =0;

    s[130] =0;
    s[131] =0;
    s[132] =0;
    s[133] =0;
    s[134] =0;
    s[135] =2;
    s[136] =0;
    s[137] =0;
    s[138] =0;
    s[139] =0;

    s[140] =2;
    s[141] =2;
    s[142] =0;
    s[143] =0;
    s[144] =0;
    s[145] =0;
    s[146] =2;
    s[147] =2;
    s[148] =2;
    s[149] =2;

    s[150] =0;
    s[151] =0;
    s[152] =2;
    s[153] =2;
    s[154] =0;
    s[155] =0;
    s[156] =2;
    s[157] =2;
    s[158] =2;
    s[159] =2;

    s[160] =0;
    s[161] =0;
    s[162] =0;
    s[163] =0;
    s[164] =2;
    s[165] =2;
    s[166] =0;
    s[167] =0;
    s[168] =0;
    s[169] =0;

    s[170] =0;
    s[171] =0;
    s[172] =0;
    s[173] =0;
    s[174] =2;
    s[175] =2;
    s[176] =0;
    s[177] =0;
    s[178] =0;
    s[179] =0;

    s[180] =0;
    s[181] =0;
    s[182] =0;
    s[183] =0;
    s[184] =2;
    s[185] =2;
    s[186] =0;
    s[187] =0;
    s[188] =0;
    s[189] =0;

    s[190] =0;
    s[191] =0;
    s[192] =0;
    s[193] =0;
    s[194] =2;
    s[195] =2;
    s[196] =0;
    s[197] =0;
    s[198] =0;
    s[199] =0;

    s[200] =0;
    s[201] =0;
    s[202] =0;
    s[203] =0;
    s[204] =2;
    s[205] =0;
    s[206] =0;
    s[207] =0;
    s[208] =0;
    s[209] =0;

    s[210] =0;
    s[211] =0;
    s[212] =0;
    s[213] =0;
    s[214] =2;
    s[215] =0;
    s[216] =0;
    s[217] =0;
    s[218] =0;
    s[219] =0;

    s[220] =0;
    s[221] =0;
    s[222] =0;
    s[223] =0;
    s[224] =0;
    s[225] =2;
    s[226] =0;
    s[227] =0;
    s[228] =0;
    s[229] =0;

    s[230] =0;
    s[231] =0;
    s[232] =0;
    s[233] =0;
    s[234] =0;
    s[235] =2;
    s[236] =0;
    s[237] =0;
    s[238] =0;
    s[239] =0;

    s[240] =2;
    s[241] =2;
    s[242] =0;
    s[243] =0;
    s[244] =0;
    s[245] =0;
    s[246] =2;
    s[247] =2;
    s[248] =2;
    s[249] =2;

    s[250] =0;
    s[251] =0;
    s[252] =2;
    s[253] =2;
    s[254] =0;
    s[255] =0;
    s[256] =2;
    s[257] =2;
    s[258] =2;
    s[259] =2;

    s[260] =0;
    s[261] =0;
    s[262] =0;
    s[263] =0;
    s[264] =2;
    s[265] =2;
    s[266] =0;
    s[267] =0;
    s[268] =0;
    s[269] =0;

    s[270] =0;
    s[271] =0;
    s[272] =0;
    s[273] =0;
    s[274] =2;
    s[275] =2;
    s[276] =0;
    s[277] =0;
    s[278] =0;
    s[279] =0;

    s[280] =0;
    s[281] =0;
    s[282] =0;
    s[283] =0;
    s[284] =2;
    s[285] =2;
    s[286] =0;
    s[287] =0;
    s[288] =0;
    s[289] =0;

    s[290] =0;
    s[291] =0;
    s[292] =0;
    s[293] =0;
    s[294] =2;
    s[295] =2;
    s[296] =0;
    s[297] =0;
    s[298] =0;
    s[299] =0;

    s[300] =0;
    s[301] =0;
    s[302] =0;
    s[303] =0;
    s[304] =2;
    s[305] =0;
    s[306] =0;
    s[307] =0;
    s[308] =0;
    s[309] =0;

    s[310] =0;
    s[311] =0;
    s[312] =0;
    s[313] =0;
    s[314] =2;
    s[315] =0;
    s[316] =0;
    s[317] =0;
    s[318] =0;
    s[319] =0;

    s[320] =0;
    s[321] =0;
    s[322] =0;
    s[323] =0;
    s[324] =0;
    s[325] =2;
    s[326] =0;
    s[327] =0;
    s[328] =0;
    s[329] =0;

    s[330] =0;
    s[331] =0;
    s[332] =0;
    s[333] =0;
    s[334] =0;
    s[335] =2;
    s[336] =0;
    s[337] =0;
    s[338] =0;
    s[339] =0;

    s[340] =2;
    s[341] =2;
    s[342] =0;
    s[343] =0;
    s[344] =0;
    s[345] =0;
    s[346] =0;
    s[347] =0;
    s[348] =0;
    s[349] =0;

    s[350] =0;
    s[351] =0;
    s[352] =2;
    s[353] =2;
    s[354] =0;
    s[355] =0;
    s[356] =0;
    s[357] =0;
    s[358] =0;
    s[359] =0;

    s[360] =0;
    s[361] =0;
    s[362] =0;
    s[363] =0;
    s[364] =0;
    s[365] =0;
    s[366] =0;
    s[367] =0;
    s[368] =0;
    s[369] =0;

    s[370] =0;
    s[371] =0;
    s[372] =0;
    s[373] =0;
    s[374] =0;
    s[375] =0;
    s[376] =0;
    s[377] =0;
    s[378] =0;
    s[379] =0;

    s[380] =0;
    s[381] =0;
    s[382] =0;
    s[383] =0;
    s[384] =0;
    s[385] =0;
    s[386] =0;
    s[387] =0;
    s[388] =0;
    s[389] =0;

    s[390] =0;
    s[391] =0;
    s[392] =0;
    s[393] =0;
    s[394] =0;
    s[395] =0;
    s[396] =0;
    s[397] =0;
    s[398] =0;
    s[399] =0;

  }
}

#endif

#ifndef Ht_5ptRelativePose_v2_const_mat_Y_h
#define Ht_5ptRelativePose_v2_const_mat_Y_h
// ============================================================================
// cd index vector for Ht of 5ptRelativePose_v2 problem
//
// Modifications
//    Chien  21-09-06:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Ht_5ptRelativePose_v2_const_matrix_Y(magma_int_t* Y)
  {
    Y[0] =19;
    Y[1] =39;
    Y[2] =59;

    Y[3] =9;
    Y[4] =29;
    Y[5] =49;

    Y[6] =15;
    Y[7] =35;
    Y[8] =55;

    Y[9] =18;
    Y[10] =38;
    Y[11] =58;

    Y[12] =0;
    Y[13] =20;
    Y[14] =40;

    Y[15] =3;
    Y[16] =23;
    Y[17] =43;

    Y[18] =10;
    Y[19] =30;
    Y[20] =50;

    Y[21] =12;
    Y[22] =32;
    Y[23] =52;

    Y[24] =16;
    Y[25] =36;
    Y[26] =56;

    Y[27] =17;
    Y[28] =37;
    Y[29] =57;

    Y[30] =1;
    Y[31] =21;
    Y[32] =41;

    Y[33] =2;
    Y[34] =22;
    Y[35] =42;

    Y[36] =4;
    Y[37] =24;
    Y[38] =44;

    Y[39] =7;
    Y[40] =27;
    Y[41] =47;

    Y[42] =11;
    Y[43] =31;
    Y[44] =51;

    Y[45] =13;
    Y[46] =33;
    Y[47] =53;

    Y[48] =6;
    Y[49] =26;
    Y[50] =46;

    Y[51] =8;
    Y[52] =28;
    Y[53] =48;

    Y[54] =14;
    Y[55] =34;
    Y[56] =54;

    Y[57] =5;
    Y[58] =25;
    Y[59] =45;

  }
}

#endif

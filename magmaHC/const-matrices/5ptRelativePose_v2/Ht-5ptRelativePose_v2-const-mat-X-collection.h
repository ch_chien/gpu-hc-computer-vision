#ifndef Ht_5ptRelativePose_v2_const_mat_X_collection_h
#define Ht_5ptRelativePose_v2_const_mat_X_collection_h
// ============================================================================
// constant matrix indexing the solution vector in \partual H / \partial t out
// 5ptRelativePose_v2 problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Ht_5ptRelativePose_v2_const_matrix_X_collection(magma_int_t* Xt)
  {
    Xt[0] =3;
    Xt[1] =3;
    Xt[2] =3;

    Xt[3] =0;
    Xt[4] =0;
    Xt[5] =0;

    Xt[6] =1;
    Xt[7] =1;
    Xt[8] =1;

    Xt[9] =2;
    Xt[10] =2;
    Xt[11] =2;

    Xt[12] =0;
    Xt[13] =0;
    Xt[14] =0;

    Xt[15] =0;
    Xt[16] =0;
    Xt[17] =0;

    Xt[18] =1;
    Xt[19] =1;
    Xt[20] =1;

    Xt[21] =1;
    Xt[22] =1;
    Xt[23] =1;

    Xt[24] =2;
    Xt[25] =2;
    Xt[26] =2;

    Xt[27] =2;
    Xt[28] =2;
    Xt[29] =2;

    Xt[30] =1;
    Xt[31] =1;
    Xt[32] =1;

    Xt[33] =2;
    Xt[34] =2;
    Xt[35] =2;

    Xt[36] =1;
    Xt[37] =1;
    Xt[38] =1;

    Xt[39] =2;
    Xt[40] =2;
    Xt[41] =2;

    Xt[42] =2;
    Xt[43] =2;
    Xt[44] =2;

    Xt[45] =2;
    Xt[46] =2;
    Xt[47] =2;

    Xt[48] =1;
    Xt[49] =1;
    Xt[50] =1;

    Xt[51] =2;
    Xt[52] =2;
    Xt[53] =2;

    Xt[54] =2;
    Xt[55] =2;
    Xt[56] =2;

    Xt[57] =2;
    Xt[58] =2;
    Xt[59] =2;


    Xt[60] =3;
    Xt[61] =3;
    Xt[62] =3;

    Xt[63] =3;
    Xt[64] =3;
    Xt[65] =3;

    Xt[66] =3;
    Xt[67] =3;
    Xt[68] =3;

    Xt[69] =3;
    Xt[70] =3;
    Xt[71] =3;

    Xt[72] =0;
    Xt[73] =0;
    Xt[74] =0;

    Xt[75] =0;
    Xt[76] =0;
    Xt[77] =0;

    Xt[78] =1;
    Xt[79] =1;
    Xt[80] =1;

    Xt[81] =1;
    Xt[82] =1;
    Xt[83] =1;

    Xt[84] =2;
    Xt[85] =2;
    Xt[86] =2;

    Xt[87] =2;
    Xt[88] =2;
    Xt[89] =2;

    Xt[90] =0;
    Xt[91] =0;
    Xt[92] =0;

    Xt[93] =0;
    Xt[94] =0;
    Xt[95] =0;

    Xt[96] =0;
    Xt[97] =0;
    Xt[98] =0;

    Xt[99] =0;
    Xt[100] =0;
    Xt[101] =0;

    Xt[102] =1;
    Xt[103] =1;
    Xt[104] =1;

    Xt[105] =1;
    Xt[106] =1;
    Xt[107] =1;

    Xt[108] =0;
    Xt[109] =0;
    Xt[110] =0;

    Xt[111] =0;
    Xt[112] =0;
    Xt[113] =0;

    Xt[114] =1;
    Xt[115] =1;
    Xt[116] =1;

    Xt[117] =1;
    Xt[118] =1;
    Xt[119] =1;


    Xt[120] =3;
    Xt[121] =3;
    Xt[122] =3;

    Xt[123] =3;
    Xt[124] =3;
    Xt[125] =3;

    Xt[126] =3;
    Xt[127] =3;
    Xt[128] =3;

    Xt[129] =3;
    Xt[130] =3;
    Xt[131] =3;

    Xt[132] =0;
    Xt[133] =0;
    Xt[134] =0;

    Xt[135] =3;
    Xt[136] =3;
    Xt[137] =3;

    Xt[138] =1;
    Xt[139] =1;
    Xt[140] =1;

    Xt[141] =3;
    Xt[142] =3;
    Xt[143] =3;

    Xt[144] =2;
    Xt[145] =2;
    Xt[146] =2;

    Xt[147] =3;
    Xt[148] =3;
    Xt[149] =3;

    Xt[150] =0;
    Xt[151] =0;
    Xt[152] =0;

    Xt[153] =0;
    Xt[154] =0;
    Xt[155] =0;

    Xt[156] =1;
    Xt[157] =1;
    Xt[158] =1;

    Xt[159] =2;
    Xt[160] =2;
    Xt[161] =2;

    Xt[162] =1;
    Xt[163] =1;
    Xt[164] =1;

    Xt[165] =2;
    Xt[166] =2;
    Xt[167] =2;

    Xt[168] =3;
    Xt[169] =3;
    Xt[170] =3;

    Xt[171] =3;
    Xt[172] =3;
    Xt[173] =3;

    Xt[174] =3;
    Xt[175] =3;
    Xt[176] =3;

    Xt[177] =0;
    Xt[178] =0;
    Xt[179] =0;
  }
}

#endif

#ifndef Hx_5ptRelativePose_v2_const_mat_s_h
#define Hx_5ptRelativePose_v2_const_mat_s_h
// ============================================================================
// scalar const matrix for Hx in 5ptRelativePose_v2 problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Hx_5ptRelativePose_v2_const_matrix_scalar(magma_int_t* s)
  {
    s[0] =1;
    s[1] =1;
    s[2] =1;

    s[3] =1;
    s[4] =1;
    s[5] =1;

    s[6] =1;
    s[7] =1;
    s[8] =1;

    s[9] =3;
    s[10] =3;
    s[11] =3;

    s[12] =1;
    s[13] =1;
    s[14] =1;

    s[15] =1;
    s[16] =1;
    s[17] =1;

    s[18] =1;
    s[19] =1;
    s[20] =1;

    s[21] =3;
    s[22] =3;
    s[23] =3;

    s[24] =1;
    s[25] =1;
    s[26] =1;

    s[27] =1;
    s[28] =1;
    s[29] =1;

    s[30] =1;
    s[31] =1;
    s[32] =1;

    s[33] =3;
    s[34] =3;
    s[35] =3;

    s[36] =2;
    s[37] =2;
    s[38] =2;

    s[39] =1;
    s[40] =1;
    s[41] =1;

    s[42] =1;
    s[43] =1;
    s[44] =1;

    s[45] =1;
    s[46] =1;
    s[47] =1;

    s[48] =2;
    s[49] =2;
    s[50] =2;

    s[51] =1;
    s[52] =1;
    s[53] =1;

    s[54] =1;
    s[55] =1;
    s[56] =1;

    s[57] =1;
    s[58] =1;
    s[59] =1;

    s[60] =2;
    s[61] =2;
    s[62] =2;

    s[63] =2;
    s[64] =2;
    s[65] =2;

    s[66] =2;
    s[67] =2;
    s[68] =2;

    s[69] =1;
    s[70] =1;
    s[71] =1;

    s[72] =2;
    s[73] =2;
    s[74] =2;

    s[75] =1;
    s[76] =1;
    s[77] =1;

    s[78] =2;
    s[79] =2;
    s[80] =2;

    s[81] =1;
    s[82] =1;
    s[83] =1;

    s[84] =2;
    s[85] =2;
    s[86] =2;

    s[87] =2;
    s[88] =2;
    s[89] =2;
  }
}

#endif

#ifndef Hx_5ptRelativePose_v2_const_mat_Y_h
#define Hx_5ptRelativePose_v2_const_mat_Y_h
// ============================================================================
// cdt index matrix for Hx of 5ptRelativePose_v2 problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Hx_5ptRelativePose_v2_const_matrix_Y(magma_int_t* Y)
  {
    Y[0] =9;
    Y[1] =29;
    Y[2] =49;

    Y[3] =15;
    Y[4] =35;
    Y[5] =55;

    Y[6] =18;
    Y[7] =38;
    Y[8] =58;

    Y[9] =0;
    Y[10] =20;
    Y[11] =40;

    Y[12] =1;
    Y[13] =21;
    Y[14] =41;

    Y[15] =2;
    Y[16] =22;
    Y[17] =42;

    Y[18] =4;
    Y[19] =24;
    Y[20] =44;

    Y[21] =10;
    Y[22] =30;
    Y[23] =50;

    Y[24] =11;
    Y[25] =31;
    Y[26] =51;

    Y[27] =7;
    Y[28] =27;
    Y[29] =47;

    Y[30] =13;
    Y[31] =33;
    Y[32] =53;

    Y[33] =16;
    Y[34] =36;
    Y[35] =56;

    Y[36] =3;
    Y[37] =23;
    Y[38] =43;

    Y[39] =6;
    Y[40] =26;
    Y[41] =46;

    Y[42] =8;
    Y[43] =28;
    Y[44] =48;

    Y[45] =6;
    Y[46] =26;
    Y[47] =46;

    Y[48] =12;
    Y[49] =32;
    Y[50] =52;

    Y[51] =14;
    Y[52] =34;
    Y[53] =54;

    Y[54] =8;
    Y[55] =28;
    Y[56] =48;

    Y[57] =14;
    Y[58] =34;
    Y[59] =54;

    Y[60] =17;
    Y[61] =37;
    Y[62] =57;

    Y[63] =1;
    Y[64] =21;
    Y[65] =41;

    Y[66] =4;
    Y[67] =24;
    Y[68] =44;

    Y[69] =5;
    Y[70] =25;
    Y[71] =45;

    Y[72] =2;
    Y[73] =22;
    Y[74] =42;

    Y[75] =5;
    Y[76] =25;
    Y[77] =45;

    Y[78] =7;
    Y[79] =27;
    Y[80] =47;

    Y[81] =5;
    Y[82] =25;
    Y[83] =45;

    Y[84] =11;
    Y[85] =31;
    Y[86] =51;

    Y[87] =13;
    Y[88] =33;
    Y[89] =53;
  }
}

#endif

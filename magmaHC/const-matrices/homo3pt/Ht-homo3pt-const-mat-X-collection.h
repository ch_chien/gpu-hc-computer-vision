#ifndef Ht_homo3pt_const_mat_X_collection_h
#define Ht_homo3pt_const_mat_X_collection_h
// ============================================================================
// constant matrix indexing the solution vector in \partual H / \partial t out
// homo3pt problem
//
// Modifications
//    Chien  21-10-26:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Ht_homo3pt_const_matrix_X_collection(magma_int_t* Xt)
  {
    Xt[0] =8;
    Xt[1] =8;
    Xt[2] =8;
    Xt[3] =8;
    Xt[4] =8;
    Xt[5] =8;
    Xt[6] =8;
    Xt[7] =8;

    Xt[8] =3;
    Xt[9] =3;
    Xt[10] =3;
    Xt[11] =3;
    Xt[12] =3;
    Xt[13] =3;
    Xt[14] =0;
    Xt[15] =3;

    Xt[16] =4;
    Xt[17] =4;
    Xt[18] =4;
    Xt[19] =4;
    Xt[20] =4;
    Xt[21] =4;
    Xt[22] =1;
    Xt[23] =4;

    Xt[24] =5;
    Xt[25] =5;
    Xt[26] =5;
    Xt[27] =5;
    Xt[28] =5;
    Xt[29] =5;
    Xt[30] =2;
    Xt[31] =8;

    Xt[32] =5;
    Xt[33] =5;
    Xt[34] =5;
    Xt[35] =5;
    Xt[36] =5;
    Xt[37] =5;
    Xt[38] =8;
    Xt[39] =8;

    Xt[40] =5;
    Xt[41] =5;
    Xt[42] =5;
    Xt[43] =5;
    Xt[44] =5;
    Xt[45] =5;
    Xt[46] =8;
    Xt[47] =8;

    Xt[48] =7;
    Xt[49] =6;
    Xt[50] =7;
    Xt[51] =6;
    Xt[52] =7;
    Xt[53] =6;
    Xt[54] =8;
    Xt[55] =8;

    Xt[56] =7;
    Xt[57] =6;
    Xt[58] =7;
    Xt[59] =6;
    Xt[60] =7;
    Xt[61] =6;
    Xt[62] =8;
    Xt[63] =8;

    Xt[64] =7;
    Xt[65] =6;
    Xt[66] =7;
    Xt[67] =6;
    Xt[68] =7;
    Xt[69] =6;
    Xt[70] =8;
    Xt[71] =8;


    Xt[72] =8;
    Xt[73] =8;
    Xt[74] =8;
    Xt[75] =8;
    Xt[76] =8;
    Xt[77] =8;
    Xt[78] =8;
    Xt[79] =8;

    Xt[80] =8;
    Xt[81] =8;
    Xt[82] =8;
    Xt[83] =8;
    Xt[84] =8;
    Xt[85] =8;
    Xt[86] =0;
    Xt[87] =3;

    Xt[88] =8;
    Xt[89] =8;
    Xt[90] =8;
    Xt[91] =8;
    Xt[92] =8;
    Xt[93] =8;
    Xt[94] =1;
    Xt[95] =4;

    Xt[96] =0;
    Xt[97] =0;
    Xt[98] =0;
    Xt[99] =0;
    Xt[100] =0;
    Xt[101] =0;
    Xt[102] =2;
    Xt[103] =8;

    Xt[104] =1;
    Xt[105] =1;
    Xt[106] =1;
    Xt[107] =1;
    Xt[108] =1;
    Xt[109] =1;
    Xt[110] =8;
    Xt[111] =8;

    Xt[112] =2;
    Xt[113] =2;
    Xt[114] =2;
    Xt[115] =2;
    Xt[116] =2;
    Xt[117] =2;
    Xt[118] =8;
    Xt[119] =8;

    Xt[120] =5;
    Xt[121] =5;
    Xt[122] =5;
    Xt[123] =5;
    Xt[124] =5;
    Xt[125] =5;
    Xt[126] =8;
    Xt[127] =8;

    Xt[128] =5;
    Xt[129] =5;
    Xt[130] =5;
    Xt[131] =5;
    Xt[132] =5;
    Xt[133] =5;
    Xt[134] =8;
    Xt[135] =8;

    Xt[136] =5;
    Xt[137] =5;
    Xt[138] =5;
    Xt[139] =5;
    Xt[140] =5;
    Xt[141] =5;
    Xt[142] =8;
    Xt[143] =8;


    Xt[144] =8;
    Xt[145] =8;
    Xt[146] =8;
    Xt[147] =8;
    Xt[148] =8;
    Xt[149] =8;
    Xt[150] =8;
    Xt[151] =8;

    Xt[152] =8;
    Xt[153] =8;
    Xt[154] =8;
    Xt[155] =8;
    Xt[156] =8;
    Xt[157] =8;
    Xt[158] =8;
    Xt[159] =8;

    Xt[160] =8;
    Xt[161] =8;
    Xt[162] =8;
    Xt[163] =8;
    Xt[164] =8;
    Xt[165] =8;
    Xt[166] =8;
    Xt[167] =8;

    Xt[168] =8;
    Xt[169] =8;
    Xt[170] =8;
    Xt[171] =8;
    Xt[172] =8;
    Xt[173] =8;
    Xt[174] =8;
    Xt[175] =8;

    Xt[176] =8;
    Xt[177] =8;
    Xt[178] =8;
    Xt[179] =8;
    Xt[180] =8;
    Xt[181] =8;
    Xt[182] =8;
    Xt[183] =8;

    Xt[184] =8;
    Xt[185] =8;
    Xt[186] =8;
    Xt[187] =8;
    Xt[188] =8;
    Xt[189] =8;
    Xt[190] =8;
    Xt[191] =8;

    Xt[192] =0;
    Xt[193] =1;
    Xt[194] =0;
    Xt[195] =1;
    Xt[196] =0;
    Xt[197] =1;
    Xt[198] =8;
    Xt[199] =8;

    Xt[200] =1;
    Xt[201] =0;
    Xt[202] =1;
    Xt[203] =0;
    Xt[204] =1;
    Xt[205] =0;
    Xt[206] =8;
    Xt[207] =8;

    Xt[208] =2;
    Xt[209] =2;
    Xt[210] =2;
    Xt[211] =2;
    Xt[212] =2;
    Xt[213] =2;
    Xt[214] =8;
    Xt[215] =8;
  }
}

#endif

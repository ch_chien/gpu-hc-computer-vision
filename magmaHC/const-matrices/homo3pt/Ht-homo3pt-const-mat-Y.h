#ifndef Ht_homo3pt_const_mat_Y_h
#define Ht_homo3pt_const_mat_Y_h
// ============================================================================
// cd index vector for Ht of homo3pt problem
//
// Modifications
//    Chien  21-09-06:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Ht_homo3pt_const_matrix_Y(magma_int_t* Y)
  {
    Y[0] =8;
    Y[1] =13;
    Y[2] =22;
    Y[3] =27;
    Y[4] =36;
    Y[5] =41;
    Y[6] =42;
    Y[7] =42;

    Y[8] =6;
    Y[9] =0;
    Y[10] =20;
    Y[11] =14;
    Y[12] =34;
    Y[13] =28;
    Y[14] =43;
    Y[15] =43;

    Y[16] =7;
    Y[17] =6;
    Y[18] =21;
    Y[19] =20;
    Y[20] =35;
    Y[21] =34;
    Y[22] =43;
    Y[23] =43;

    Y[24] =1;
    Y[25] =9;
    Y[26] =15;
    Y[27] =23;
    Y[28] =29;
    Y[29] =37;
    Y[30] =43;
    Y[31] =0;

    Y[32] =3;
    Y[33] =10;
    Y[34] =17;
    Y[35] =24;
    Y[36] =31;
    Y[37] =38;
    Y[38] =0;
    Y[39] =0;

    Y[40] =5;
    Y[41] =12;
    Y[42] =19;
    Y[43] =26;
    Y[44] =33;
    Y[45] =40;
    Y[46] =0;
    Y[47] =0;

    Y[48] =0;
    Y[49] =6;
    Y[50] =14;
    Y[51] =20;
    Y[52] =28;
    Y[53] =34;
    Y[54] =0;
    Y[55] =0;

    Y[56] =2;
    Y[57] =7;
    Y[58] =16;
    Y[59] =21;
    Y[60] =30;
    Y[61] =35;
    Y[62] =0;
    Y[63] =0;

    Y[64] =4;
    Y[65] =11;
    Y[66] =18;
    Y[67] =25;
    Y[68] =32;
    Y[69] =39;
    Y[70] =0;
    Y[71] =0;
  }
}

#endif

#ifndef Hx_P3P_const_mat_s_h
#define Hx_P3P_const_mat_s_h
// ============================================================================
// scalar const matrix for Hx in P3P problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Hx_P3P_const_matrix_scalar(magma_int_t* s)
  {
    s[0] =1;
    s[1] =1;
    s[2] =1;
    s[3] =1;
    s[4] =1;
    s[5] =1;
    s[6] =1;
    s[7] =1;
    s[8] =1;
    s[9] =2;

    s[10] =1;
    s[11] =2;
    s[12] =1;
    s[13] =1;
    s[14] =1;
    s[15] =2;
    s[16] =1;
    s[17] =1;
    s[18] =2;
    s[19] =2;

    s[20] =1;
    s[21] =1;
    s[22] =1;
    s[23] =1;
    s[24] =1;
    s[25] =2;
    s[26] =1;
    s[27] =1;
    s[28] =2;
    s[29] =2;

    s[30] =1;
    s[31] =1;
    s[32] =1;
    s[33] =1;
    s[34] =1;
    s[35] =1;
    s[36] =1;
    s[37] =1;
    s[38] =1;
    s[39] =2;

    s[40] =1;
    s[41] =0;
    s[42] =0;
    s[43] =1;
    s[44] =0;
    s[45] =0;
    s[46] =1;
    s[47] =0;
    s[48] =0;
    s[49] =0;

    s[50] =0;
    s[51] =1;
    s[52] =0;
    s[53] =0;
    s[54] =1;
    s[55] =0;
    s[56] =0;
    s[57] =1;
    s[58] =0;
    s[59] =0;

    s[60] =0;
    s[61] =0;
    s[62] =1;
    s[63] =0;
    s[64] =0;
    s[65] =1;
    s[66] =0;
    s[67] =0;
    s[68] =1;
    s[69] =0;

    s[70] =1;
    s[71] =1;
    s[72] =1;
    s[73] =0;
    s[74] =0;
    s[75] =0;
    s[76] =0;
    s[77] =0;
    s[78] =0;
    s[79] =0;

    s[80] =0;
    s[81] =0;
    s[82] =0;
    s[83] =1;
    s[84] =1;
    s[85] =1;
    s[86] =0;
    s[87] =0;
    s[88] =0;
    s[89] =0;

    s[90] =0;
    s[91] =0;
    s[92] =0;
    s[93] =0;
    s[94] =0;
    s[95] =0;
    s[96] =1;
    s[97] =1;
    s[98] =1;
    s[99] =0;

    s[100] =1;
    s[101] =1;
    s[102] =1;
    s[103] =1;
    s[104] =1;
    s[105] =1;
    s[106] =1;
    s[107] =1;
    s[108] =1;
    s[109] =0;

    s[110] =1;
    s[111] =1;
    s[112] =2;
    s[113] =1;
    s[114] =2;
    s[115] =1;
    s[116] =1;
    s[117] =2;
    s[118] =1;
    s[119] =0;

    s[120] =1;
    s[121] =1;
    s[122] =1;
    s[123] =1;
    s[124] =1;
    s[125] =1;
    s[126] =1;
    s[127] =1;
    s[128] =1;
    s[129] =0;

    s[130] =1;
    s[131] =2;
    s[132] =1;
    s[133] =1;
    s[134] =2;
    s[135] =1;
    s[136] =1;
    s[137] =2;
    s[138] =1;
    s[139] =0;

    s[140] =0;
    s[141] =0;
    s[142] =0;
    s[143] =0;
    s[144] =0;
    s[145] =0;
    s[146] =0;
    s[147] =0;
    s[148] =0;
    s[149] =0;

    s[150] =0;
    s[151] =0;
    s[152] =0;
    s[153] =0;
    s[154] =0;
    s[155] =0;
    s[156] =0;
    s[157] =0;
    s[158] =0;
    s[159] =0;

    s[160] =0;
    s[161] =0;
    s[162] =0;
    s[163] =0;
    s[164] =0;
    s[165] =0;
    s[166] =0;
    s[167] =0;
    s[168] =0;
    s[169] =0;

    s[170] =1;
    s[171] =1;
    s[172] =1;
    s[173] =0;
    s[174] =0;
    s[175] =0;
    s[176] =0;
    s[177] =0;
    s[178] =0;
    s[179] =0;

    s[180] =0;
    s[181] =0;
    s[182] =0;
    s[183] =1;
    s[184] =1;
    s[185] =1;
    s[186] =0;
    s[187] =0;
    s[188] =0;
    s[189] =0;

    s[190] =0;
    s[191] =0;
    s[192] =0;
    s[193] =0;
    s[194] =0;
    s[195] =0;
    s[196] =1;
    s[197] =1;
    s[198] =1;
    s[199] =0;

    s[200] =0;
    s[201] =0;
    s[202] =0;
    s[203] =0;
    s[204] =0;
    s[205] =0;
    s[206] =0;
    s[207] =0;
    s[208] =0;
    s[209] =0;

    s[210] =0;
    s[211] =1;
    s[212] =1;
    s[213] =0;
    s[214] =1;
    s[215] =1;
    s[216] =0;
    s[217] =1;
    s[218] =1;
    s[219] =0;

    s[220] =2;
    s[221] =0;
    s[222] =2;
    s[223] =2;
    s[224] =0;
    s[225] =1;
    s[226] =2;
    s[227] =0;
    s[228] =1;
    s[229] =0;

    s[230] =2;
    s[231] =1;
    s[232] =0;
    s[233] =2;
    s[234] =1;
    s[235] =0;
    s[236] =2;
    s[237] =1;
    s[238] =0;
    s[239] =0;

    s[240] =0;
    s[241] =0;
    s[242] =0;
    s[243] =0;
    s[244] =0;
    s[245] =0;
    s[246] =0;
    s[247] =0;
    s[248] =0;
    s[249] =0;

    s[250] =0;
    s[251] =0;
    s[252] =0;
    s[253] =0;
    s[254] =0;
    s[255] =0;
    s[256] =0;
    s[257] =0;
    s[258] =0;
    s[259] =0;

    s[260] =0;
    s[261] =0;
    s[262] =0;
    s[263] =0;
    s[264] =0;
    s[265] =0;
    s[266] =0;
    s[267] =0;
    s[268] =0;
    s[269] =0;

    s[270] =1;
    s[271] =1;
    s[272] =1;
    s[273] =0;
    s[274] =0;
    s[275] =0;
    s[276] =0;
    s[277] =0;
    s[278] =0;
    s[279] =0;

    s[280] =0;
    s[281] =0;
    s[282] =0;
    s[283] =1;
    s[284] =1;
    s[285] =1;
    s[286] =0;
    s[287] =0;
    s[288] =0;
    s[289] =0;

    s[290] =0;
    s[291] =0;
    s[292] =0;
    s[293] =0;
    s[294] =0;
    s[295] =0;
    s[296] =1;
    s[297] =1;
    s[298] =1;
    s[299] =0;

    s[300] =0;
    s[301] =0;
    s[302] =0;
    s[303] =0;
    s[304] =0;
    s[305] =0;
    s[306] =0;
    s[307] =0;
    s[308] =0;
    s[309] =0;

    s[310] =0;
    s[311] =0;
    s[312] =0;
    s[313] =0;
    s[314] =0;
    s[315] =0;
    s[316] =0;
    s[317] =0;
    s[318] =0;
    s[319] =0;

    s[320] =0;
    s[321] =0;
    s[322] =0;
    s[323] =0;
    s[324] =0;
    s[325] =0;
    s[326] =0;
    s[327] =0;
    s[328] =0;
    s[329] =0;

    s[330] =0;
    s[331] =0;
    s[332] =0;
    s[333] =0;
    s[334] =0;
    s[335] =0;
    s[336] =0;
    s[337] =0;
    s[338] =0;
    s[339] =0;

    s[340] =0;
    s[341] =0;
    s[342] =0;
    s[343] =0;
    s[344] =0;
    s[345] =0;
    s[346] =0;
    s[347] =0;
    s[348] =0;
    s[349] =0;

    s[350] =0;
    s[351] =0;
    s[352] =0;
    s[353] =0;
    s[354] =0;
    s[355] =0;
    s[356] =0;
    s[357] =0;
    s[358] =0;
    s[359] =0;

    s[360] =0;
    s[361] =0;
    s[362] =0;
    s[363] =0;
    s[364] =0;
    s[365] =0;
    s[366] =0;
    s[367] =0;
    s[368] =0;
    s[369] =0;

    s[370] =1;
    s[371] =1;
    s[372] =1;
    s[373] =0;
    s[374] =0;
    s[375] =0;
    s[376] =0;
    s[377] =0;
    s[378] =0;
    s[379] =0;

    s[380] =0;
    s[381] =0;
    s[382] =0;
    s[383] =1;
    s[384] =1;
    s[385] =1;
    s[386] =0;
    s[387] =0;
    s[388] =0;
    s[389] =0;

    s[390] =0;
    s[391] =0;
    s[392] =0;
    s[393] =0;
    s[394] =0;
    s[395] =0;
    s[396] =1;
    s[397] =1;
    s[398] =1;
    s[399] =0;

    s[400] =0;
    s[401] =0;
    s[402] =0;
    s[403] =0;
    s[404] =0;
    s[405] =0;
    s[406] =0;
    s[407] =0;
    s[408] =0;
    s[409] =0;

    s[410] =0;
    s[411] =0;
    s[412] =0;
    s[413] =0;
    s[414] =0;
    s[415] =0;
    s[416] =0;
    s[417] =0;
    s[418] =0;
    s[419] =0;

    s[420] =0;
    s[421] =0;
    s[422] =0;
    s[423] =0;
    s[424] =0;
    s[425] =0;
    s[426] =0;
    s[427] =0;
    s[428] =0;
    s[429] =0;

    s[430] =0;
    s[431] =0;
    s[432] =0;
    s[433] =0;
    s[434] =0;
    s[435] =0;
    s[436] =0;
    s[437] =0;
    s[438] =0;
    s[439] =0;

    s[440] =0;
    s[441] =0;
    s[442] =0;
    s[443] =0;
    s[444] =0;
    s[445] =0;
    s[446] =0;
    s[447] =0;
    s[448] =0;
    s[449] =0;

    s[450] =0;
    s[451] =0;
    s[452] =0;
    s[453] =0;
    s[454] =0;
    s[455] =0;
    s[456] =0;
    s[457] =0;
    s[458] =0;
    s[459] =0;

    s[460] =0;
    s[461] =0;
    s[462] =0;
    s[463] =0;
    s[464] =0;
    s[465] =0;
    s[466] =0;
    s[467] =0;
    s[468] =0;
    s[469] =0;

    s[470] =1;
    s[471] =1;
    s[472] =1;
    s[473] =0;
    s[474] =0;
    s[475] =0;
    s[476] =0;
    s[477] =0;
    s[478] =0;
    s[479] =0;

    s[480] =0;
    s[481] =0;
    s[482] =0;
    s[483] =1;
    s[484] =1;
    s[485] =1;
    s[486] =0;
    s[487] =0;
    s[488] =0;
    s[489] =0;

    s[490] =0;
    s[491] =0;
    s[492] =0;
    s[493] =0;
    s[494] =0;
    s[495] =0;
    s[496] =1;
    s[497] =1;
    s[498] =1;
    s[499] =0;

    s[500] =0;
    s[501] =0;
    s[502] =0;
    s[503] =0;
    s[504] =0;
    s[505] =0;
    s[506] =0;
    s[507] =0;
    s[508] =0;
    s[509] =0;

    s[510] =0;
    s[511] =0;
    s[512] =0;
    s[513] =0;
    s[514] =0;
    s[515] =0;
    s[516] =0;
    s[517] =0;
    s[518] =0;
    s[519] =0;

    s[520] =0;
    s[521] =0;
    s[522] =0;
    s[523] =0;
    s[524] =0;
    s[525] =0;
    s[526] =0;
    s[527] =0;
    s[528] =0;
    s[529] =0;

    s[530] =0;
    s[531] =0;
    s[532] =0;
    s[533] =0;
    s[534] =0;
    s[535] =0;
    s[536] =0;
    s[537] =0;
    s[538] =0;
    s[539] =0;

    s[540] =0;
    s[541] =0;
    s[542] =0;
    s[543] =0;
    s[544] =0;
    s[545] =0;
    s[546] =0;
    s[547] =0;
    s[548] =0;
    s[549] =0;

    s[550] =0;
    s[551] =0;
    s[552] =0;
    s[553] =0;
    s[554] =0;
    s[555] =0;
    s[556] =0;
    s[557] =0;
    s[558] =0;
    s[559] =0;

    s[560] =0;
    s[561] =0;
    s[562] =0;
    s[563] =0;
    s[564] =0;
    s[565] =0;
    s[566] =0;
    s[567] =0;
    s[568] =0;
    s[569] =0;

    s[570] =1;
    s[571] =1;
    s[572] =1;
    s[573] =0;
    s[574] =0;
    s[575] =0;
    s[576] =0;
    s[577] =0;
    s[578] =0;
    s[579] =0;

    s[580] =0;
    s[581] =0;
    s[582] =0;
    s[583] =1;
    s[584] =1;
    s[585] =1;
    s[586] =0;
    s[587] =0;
    s[588] =0;
    s[589] =0;

    s[590] =0;
    s[591] =0;
    s[592] =0;
    s[593] =0;
    s[594] =0;
    s[595] =0;
    s[596] =1;
    s[597] =1;
    s[598] =1;
    s[599] =0;

    s[600] =0;
    s[601] =0;
    s[602] =0;
    s[603] =0;
    s[604] =0;
    s[605] =0;
    s[606] =0;
    s[607] =0;
    s[608] =0;
    s[609] =0;

    s[610] =0;
    s[611] =0;
    s[612] =0;
    s[613] =0;
    s[614] =0;
    s[615] =0;
    s[616] =0;
    s[617] =0;
    s[618] =0;
    s[619] =0;

    s[620] =0;
    s[621] =0;
    s[622] =0;
    s[623] =0;
    s[624] =0;
    s[625] =0;
    s[626] =0;
    s[627] =0;
    s[628] =0;
    s[629] =0;

    s[630] =0;
    s[631] =0;
    s[632] =0;
    s[633] =0;
    s[634] =0;
    s[635] =0;
    s[636] =0;
    s[637] =0;
    s[638] =0;
    s[639] =0;

    s[640] =0;
    s[641] =0;
    s[642] =0;
    s[643] =0;
    s[644] =0;
    s[645] =0;
    s[646] =0;
    s[647] =0;
    s[648] =0;
    s[649] =0;

    s[650] =0;
    s[651] =0;
    s[652] =0;
    s[653] =0;
    s[654] =0;
    s[655] =0;
    s[656] =0;
    s[657] =0;
    s[658] =0;
    s[659] =0;

    s[660] =0;
    s[661] =0;
    s[662] =0;
    s[663] =0;
    s[664] =0;
    s[665] =0;
    s[666] =0;
    s[667] =0;
    s[668] =0;
    s[669] =0;

    s[670] =1;
    s[671] =1;
    s[672] =1;
    s[673] =0;
    s[674] =0;
    s[675] =0;
    s[676] =0;
    s[677] =0;
    s[678] =0;
    s[679] =0;

    s[680] =0;
    s[681] =0;
    s[682] =0;
    s[683] =1;
    s[684] =1;
    s[685] =1;
    s[686] =0;
    s[687] =0;
    s[688] =0;
    s[689] =0;

    s[690] =0;
    s[691] =0;
    s[692] =0;
    s[693] =0;
    s[694] =0;
    s[695] =0;
    s[696] =1;
    s[697] =1;
    s[698] =1;
    s[699] =0;

  }
}

#endif

#ifndef Ht_P3P_const_mat_Y_h
#define Ht_P3P_const_mat_Y_h
// ============================================================================
// cd index vector for Ht of P3P problem
//
// Modifications
//    Chien  21-09-06:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Ht_P3P_const_matrix_Y(magma_int_t* Y)
  {
    Y[0] =6;
    Y[1] =10;
    Y[2] =11;
    Y[3] =16;
    Y[4] =19;
    Y[5] =20;
    Y[6] =25;
    Y[7] =28;
    Y[8] =29;
    Y[9] =4;

    Y[10] =4;
    Y[11] =4;
    Y[12] =4;
    Y[13] =4;
    Y[14] =4;
    Y[15] =4;
    Y[16] =4;
    Y[17] =4;
    Y[18] =4;
    Y[19] =30;

    Y[20] =5;
    Y[21] =9;
    Y[22] =4;
    Y[23] =15;
    Y[24] =18;
    Y[25] =4;
    Y[26] =24;
    Y[27] =27;
    Y[28] =4;
    Y[29] =30;

    Y[30] =3;
    Y[31] =1;
    Y[32] =7;
    Y[33] =14;
    Y[34] =12;
    Y[35] =7;
    Y[36] =23;
    Y[37] =21;
    Y[38] =7;
    Y[39] =30;

    Y[40] =3;
    Y[41] =1;
    Y[42] =7;
    Y[43] =14;
    Y[44] =12;
    Y[45] =7;
    Y[46] =23;
    Y[47] =21;
    Y[48] =7;
    Y[49] =30;

    Y[50] =0;
    Y[51] =0;
    Y[52] =2;
    Y[53] =0;
    Y[54] =0;
    Y[55] =13;
    Y[56] =0;
    Y[57] =0;
    Y[58] =22;
    Y[59] =0;

    Y[60] =0;
    Y[61] =7;
    Y[62] =3;
    Y[63] =0;
    Y[64] =7;
    Y[65] =14;
    Y[66] =0;
    Y[67] =7;
    Y[68] =23;
    Y[69] =0;

    Y[70] =1;
    Y[71] =8;
    Y[72] =2;
    Y[73] =12;
    Y[74] =17;
    Y[75] =13;
    Y[76] =21;
    Y[77] =26;
    Y[78] =22;
    Y[79] =0;

    Y[80] =2;
    Y[81] =8;
    Y[82] =8;
    Y[83] =13;
    Y[84] =17;
    Y[85] =17;
    Y[86] =22;
    Y[87] =26;
    Y[88] =26;
    Y[89] =0;

  }
}

#endif

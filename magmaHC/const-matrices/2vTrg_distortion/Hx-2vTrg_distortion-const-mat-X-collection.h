#ifndef Hx_2vTrg_distortion_const_mat_X_h
#define Hx_2vTrg_distortion_const_mat_X_h
// ============================================================================
// indices of solution matrix for the 2vTrg_distortion problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Hx_2vTrg_distortion_const_matrix_X_collection(magma_int_t* X)
  {
    X[0] =5;
    X[1] =5;
    X[2] =5;
    X[3] =4;
    X[4] =4;
    X[5] =5;
    X[6] =5;
    X[7] =5;
    X[8] =4;
    X[9] =4;
    X[10] =5;
    X[11] =4;
    X[12] =4;
    X[13] =5;
    X[14] =5;
    X[15] =5;
    X[16] =4;
    X[17] =4;
    X[18] =5;
    X[19] =5;
    X[20] =5;
    X[21] =5;
    X[22] =5;
    X[23] =5;
    X[24] =5;
    X[25] =2;
    X[26] =4;
    X[27] =5;
    X[28] =4;
    X[29] =4;
    X[30] =2;
    X[31] =5;
    X[32] =4;
    X[33] =4;
    X[34] =4;
    X[35] =0;
    X[36] =4;
    X[37] =4;
    X[38] =4;
    X[39] =5;
    X[40] =0;
    X[41] =4;
    X[42] =4;
    X[43] =5;
    X[44] =4;
    X[45] =5;
    X[46] =2;
    X[47] =2;
    X[48] =0;
    X[49] =0;
    X[50] =3;
    X[51] =4;
    X[52] =5;
    X[53] =4;
    X[54] =4;
    X[55] =3;
    X[56] =5;
    X[57] =4;
    X[58] =4;
    X[59] =4;
    X[60] =1;
    X[61] =4;
    X[62] =4;
    X[63] =4;
    X[64] =5;
    X[65] =1;
    X[66] =4;
    X[67] =4;
    X[68] =5;
    X[69] =4;
    X[70] =5;
    X[71] =3;
    X[72] =3;
    X[73] =1;
    X[74] =1;
    X[75] =0;
    X[76] =4;
    X[77] =5;
    X[78] =4;
    X[79] =4;
    X[80] =1;
    X[81] =5;
    X[82] =4;
    X[83] =4;
    X[84] =4;
    X[85] =0;
    X[86] =4;
    X[87] =4;
    X[88] =4;
    X[89] =5;
    X[90] =0;
    X[91] =4;
    X[92] =4;
    X[93] =5;
    X[94] =4;
    X[95] =5;
    X[96] =0;
    X[97] =1;
    X[98] =0;
    X[99] =0;
    X[100] =2;
    X[101] =4;
    X[102] =5;
    X[103] =5;
    X[104] =5;
    X[105] =2;
    X[106] =5;
    X[107] =4;
    X[108] =5;
    X[109] =5;
    X[110] =1;
    X[111] =5;
    X[112] =5;
    X[113] =4;
    X[114] =5;
    X[115] =1;
    X[116] =5;
    X[117] =5;
    X[118] =5;
    X[119] =4;
    X[120] =5;
    X[121] =2;
    X[122] =2;
    X[123] =1;
    X[124] =1;
    X[125] =3;
    X[126] =4;
    X[127] =5;
    X[128] =5;
    X[129] =5;
    X[130] =3;
    X[131] =5;
    X[132] =4;
    X[133] =5;
    X[134] =5;
    X[135] =2;
    X[136] =5;
    X[137] =5;
    X[138] =4;
    X[139] =5;
    X[140] =3;
    X[141] =5;
    X[142] =5;
    X[143] =5;
    X[144] =4;
    X[145] =5;
    X[146] =3;
    X[147] =3;
    X[148] =2;
    X[149] =3;
    X[150] =2;
    X[151] =5;
    X[152] =5;
    X[153] =5;
    X[154] =5;
    X[155] =2;
    X[156] =5;
    X[157] =5;
    X[158] =5;
    X[159] =5;
    X[160] =2;
    X[161] =5;
    X[162] =5;
    X[163] =5;
    X[164] =5;
    X[165] =3;
    X[166] =5;
    X[167] =5;
    X[168] =5;
    X[169] =5;
    X[170] =5;
    X[171] =2;
    X[172] =2;
    X[173] =2;
    X[174] =3;
    X[175] =3;
    X[176] =5;
    X[177] =5;
    X[178] =5;
    X[179] =5;
    X[180] =3;
    X[181] =5;
    X[182] =5;
    X[183] =5;
    X[184] =5;
    X[185] =2;
    X[186] =5;
    X[187] =5;
    X[188] =5;
    X[189] =5;
    X[190] =3;
    X[191] =5;
    X[192] =5;
    X[193] =5;
    X[194] =5;
    X[195] =5;
    X[196] =3;
    X[197] =3;
    X[198] =2;
    X[199] =3;
    X[200] =2;
    X[201] =5;
    X[202] =5;
    X[203] =5;
    X[204] =5;
    X[205] =2;
    X[206] =5;
    X[207] =5;
    X[208] =5;
    X[209] =5;
    X[210] =2;
    X[211] =5;
    X[212] =5;
    X[213] =5;
    X[214] =5;
    X[215] =3;
    X[216] =5;
    X[217] =5;
    X[218] =5;
    X[219] =5;
    X[220] =5;
    X[221] =2;
    X[222] =2;
    X[223] =2;
    X[224] =3;
    X[225] =3;
    X[226] =5;
    X[227] =5;
    X[228] =5;
    X[229] =5;
    X[230] =3;
    X[231] =5;
    X[232] =5;
    X[233] =5;
    X[234] =5;
    X[235] =2;
    X[236] =5;
    X[237] =5;
    X[238] =5;
    X[239] =5;
    X[240] =3;
    X[241] =5;
    X[242] =5;
    X[243] =5;
    X[244] =5;
    X[245] =5;
    X[246] =3;
    X[247] =3;
    X[248] =2;
    X[249] =3;

    X[250] =5;
    X[251] =5;
    X[252] =5;
    X[253] =5;
    X[254] =5;
    X[255] =5;
    X[256] =5;
    X[257] =5;
    X[258] =5;
    X[259] =5;
    X[260] =5;
    X[261] =5;
    X[262] =5;
    X[263] =5;
    X[264] =5;
    X[265] =5;
    X[266] =5;
    X[267] =5;
    X[268] =5;
    X[269] =5;
    X[270] =5;
    X[271] =5;
    X[272] =5;
    X[273] =5;
    X[274] =5;
    X[275] =2;
    X[276] =5;
    X[277] =5;
    X[278] =0;
    X[279] =3;
    X[280] =2;
    X[281] =5;
    X[282] =5;
    X[283] =1;
    X[284] =3;
    X[285] =0;
    X[286] =0;
    X[287] =1;
    X[288] =5;
    X[289] =5;
    X[290] =0;
    X[291] =0;
    X[292] =1;
    X[293] =5;
    X[294] =5;
    X[295] =5;
    X[296] =2;
    X[297] =2;
    X[298] =0;
    X[299] =0;
    X[300] =3;
    X[301] =2;
    X[302] =5;
    X[303] =2;
    X[304] =0;
    X[305] =3;
    X[306] =5;
    X[307] =2;
    X[308] =2;
    X[309] =1;
    X[310] =1;
    X[311] =2;
    X[312] =2;
    X[313] =0;
    X[314] =5;
    X[315] =1;
    X[316] =3;
    X[317] =3;
    X[318] =5;
    X[319] =0;
    X[320] =5;
    X[321] =3;
    X[322] =3;
    X[323] =1;
    X[324] =1;
    X[325] =5;
    X[326] =3;
    X[327] =5;
    X[328] =2;
    X[329] =3;
    X[330] =5;
    X[331] =5;
    X[332] =3;
    X[333] =2;
    X[334] =3;
    X[335] =5;
    X[336] =2;
    X[337] =2;
    X[338] =1;
    X[339] =5;
    X[340] =5;
    X[341] =3;
    X[342] =3;
    X[343] =5;
    X[344] =1;
    X[345] =5;
    X[346] =5;
    X[347] =5;
    X[348] =5;
    X[349] =5;
    X[350] =5;
    X[351] =2;
    X[352] =5;
    X[353] =5;
    X[354] =5;
    X[355] =5;
    X[356] =5;
    X[357] =2;
    X[358] =5;
    X[359] =5;
    X[360] =5;
    X[361] =5;
    X[362] =5;
    X[363] =0;
    X[364] =5;
    X[365] =5;
    X[366] =5;
    X[367] =5;
    X[368] =5;
    X[369] =0;
    X[370] =5;
    X[371] =5;
    X[372] =5;
    X[373] =5;
    X[374] =5;
    X[375] =5;
    X[376] =3;
    X[377] =5;
    X[378] =5;
    X[379] =5;
    X[380] =5;
    X[381] =5;
    X[382] =3;
    X[383] =5;
    X[384] =5;
    X[385] =5;
    X[386] =5;
    X[387] =5;
    X[388] =1;
    X[389] =5;
    X[390] =5;
    X[391] =5;
    X[392] =5;
    X[393] =5;
    X[394] =1;
    X[395] =5;
    X[396] =5;
    X[397] =5;
    X[398] =5;
    X[399] =5;
    X[400] =0;
    X[401] =5;
    X[402] =5;
    X[403] =5;
    X[404] =5;
    X[405] =1;
    X[406] =5;
    X[407] =5;
    X[408] =5;
    X[409] =5;
    X[410] =0;
    X[411] =5;
    X[412] =5;
    X[413] =5;
    X[414] =5;
    X[415] =0;
    X[416] =5;
    X[417] =5;
    X[418] =5;
    X[419] =5;
    X[420] =5;
    X[421] =0;
    X[422] =1;
    X[423] =0;
    X[424] =0;
    X[425] =0;
    X[426] =5;
    X[427] =5;
    X[428] =5;
    X[429] =5;
    X[430] =1;
    X[431] =5;
    X[432] =5;
    X[433] =5;
    X[434] =5;
    X[435] =1;
    X[436] =5;
    X[437] =5;
    X[438] =5;
    X[439] =5;
    X[440] =1;
    X[441] =5;
    X[442] =5;
    X[443] =5;
    X[444] =5;
    X[445] =5;
    X[446] =0;
    X[447] =1;
    X[448] =1;
    X[449] =1;
    X[450] =0;
    X[451] =5;
    X[452] =5;
    X[453] =5;
    X[454] =5;
    X[455] =1;
    X[456] =5;
    X[457] =5;
    X[458] =5;
    X[459] =5;
    X[460] =0;
    X[461] =5;
    X[462] =5;
    X[463] =5;
    X[464] =5;
    X[465] =0;
    X[466] =5;
    X[467] =5;
    X[468] =5;
    X[469] =5;
    X[470] =5;
    X[471] =0;
    X[472] =1;
    X[473] =0;
    X[474] =0;
    X[475] =0;
    X[476] =5;
    X[477] =5;
    X[478] =5;
    X[479] =5;
    X[480] =1;
    X[481] =5;
    X[482] =5;
    X[483] =5;
    X[484] =5;
    X[485] =1;
    X[486] =5;
    X[487] =5;
    X[488] =5;
    X[489] =5;
    X[490] =1;
    X[491] =5;
    X[492] =5;
    X[493] =5;
    X[494] =5;
    X[495] =5;
    X[496] =0;
    X[497] =1;
    X[498] =1;
    X[499] =1;

    X[500] =5;
    X[501] =5;
    X[502] =5;
    X[503] =5;
    X[504] =5;
    X[505] =5;
    X[506] =5;
    X[507] =5;
    X[508] =5;
    X[509] =5;
    X[510] =5;
    X[511] =5;
    X[512] =5;
    X[513] =5;
    X[514] =5;
    X[515] =5;
    X[516] =5;
    X[517] =5;
    X[518] =5;
    X[519] =5;
    X[520] =5;
    X[521] =5;
    X[522] =5;
    X[523] =5;
    X[524] =5;
    X[525] =5;
    X[526] =5;
    X[527] =5;
    X[528] =5;
    X[529] =5;
    X[530] =5;
    X[531] =5;
    X[532] =5;
    X[533] =5;
    X[534] =5;
    X[535] =5;
    X[536] =5;
    X[537] =5;
    X[538] =5;
    X[539] =5;
    X[540] =5;
    X[541] =5;
    X[542] =5;
    X[543] =5;
    X[544] =5;
    X[545] =5;
    X[546] =5;
    X[547] =5;
    X[548] =5;
    X[549] =5;
    X[550] =5;
    X[551] =5;
    X[552] =5;
    X[553] =5;
    X[554] =5;
    X[555] =5;
    X[556] =5;
    X[557] =5;
    X[558] =5;
    X[559] =5;
    X[560] =5;
    X[561] =5;
    X[562] =5;
    X[563] =5;
    X[564] =5;
    X[565] =5;
    X[566] =5;
    X[567] =5;
    X[568] =5;
    X[569] =5;
    X[570] =5;
    X[571] =5;
    X[572] =5;
    X[573] =5;
    X[574] =5;
    X[575] =5;
    X[576] =5;
    X[577] =5;
    X[578] =0;
    X[579] =0;
    X[580] =5;
    X[581] =5;
    X[582] =5;
    X[583] =1;
    X[584] =1;
    X[585] =5;
    X[586] =0;
    X[587] =1;
    X[588] =5;
    X[589] =5;
    X[590] =5;
    X[591] =0;
    X[592] =1;
    X[593] =5;
    X[594] =5;
    X[595] =5;
    X[596] =5;
    X[597] =5;
    X[598] =5;
    X[599] =5;
    X[600] =5;
    X[601] =2;
    X[602] =5;
    X[603] =5;
    X[604] =5;
    X[605] =5;
    X[606] =5;
    X[607] =2;
    X[608] =5;
    X[609] =5;
    X[610] =5;
    X[611] =5;
    X[612] =5;
    X[613] =0;
    X[614] =5;
    X[615] =5;
    X[616] =5;
    X[617] =5;
    X[618] =5;
    X[619] =0;
    X[620] =5;
    X[621] =5;
    X[622] =5;
    X[623] =5;
    X[624] =5;
    X[625] =5;
    X[626] =3;
    X[627] =5;
    X[628] =5;
    X[629] =5;
    X[630] =5;
    X[631] =5;
    X[632] =3;
    X[633] =5;
    X[634] =5;
    X[635] =5;
    X[636] =5;
    X[637] =5;
    X[638] =1;
    X[639] =5;
    X[640] =5;
    X[641] =5;
    X[642] =5;
    X[643] =5;
    X[644] =1;
    X[645] =5;
    X[646] =5;
    X[647] =5;
    X[648] =5;
    X[649] =5;
    X[650] =5;
    X[651] =5;
    X[652] =5;
    X[653] =5;
    X[654] =5;
    X[655] =5;
    X[656] =5;
    X[657] =5;
    X[658] =5;
    X[659] =5;
    X[660] =5;
    X[661] =5;
    X[662] =5;
    X[663] =5;
    X[664] =5;
    X[665] =5;
    X[666] =5;
    X[667] =5;
    X[668] =5;
    X[669] =5;
    X[670] =5;
    X[671] =5;
    X[672] =5;
    X[673] =5;
    X[674] =5;
    X[675] =5;
    X[676] =5;
    X[677] =5;
    X[678] =5;
    X[679] =5;
    X[680] =5;
    X[681] =5;
    X[682] =5;
    X[683] =5;
    X[684] =5;
    X[685] =5;
    X[686] =5;
    X[687] =5;
    X[688] =5;
    X[689] =5;
    X[690] =5;
    X[691] =5;
    X[692] =5;
    X[693] =5;
    X[694] =5;
    X[695] =5;
    X[696] =5;
    X[697] =5;
    X[698] =5;
    X[699] =5;
    X[700] =2;
    X[701] =5;
    X[702] =5;
    X[703] =5;
    X[704] =5;
    X[705] =2;
    X[706] =5;
    X[707] =5;
    X[708] =5;
    X[709] =5;
    X[710] =0;
    X[711] =5;
    X[712] =5;
    X[713] =5;
    X[714] =5;
    X[715] =0;
    X[716] =5;
    X[717] =5;
    X[718] =5;
    X[719] =5;
    X[720] =5;
    X[721] =2;
    X[722] =2;
    X[723] =0;
    X[724] =0;
    X[725] =3;
    X[726] =5;
    X[727] =5;
    X[728] =5;
    X[729] =5;
    X[730] =3;
    X[731] =5;
    X[732] =5;
    X[733] =5;
    X[734] =5;
    X[735] =1;
    X[736] =5;
    X[737] =5;
    X[738] =5;
    X[739] =5;
    X[740] =1;
    X[741] =5;
    X[742] =5;
    X[743] =5;
    X[744] =5;
    X[745] =5;
    X[746] =3;
    X[747] =3;
    X[748] =1;
    X[749] =1;

  }
}

#endif

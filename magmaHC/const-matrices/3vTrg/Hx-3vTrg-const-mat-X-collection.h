#ifndef Hx_3vTrg_const_mat_X_h
#define Hx_3vTrg_const_mat_X_h
// ============================================================================
// indices of solution matrix for the 3vTrg problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Hx_3vTrg_const_matrix_X_collection(magma_int_t* X)
  {
    X[0] =9;
    X[1] =9;
    X[2] =6;
    X[3] =6;
    X[4] =8;
    X[5] =8;
    X[6] =9;
    X[7] =9;
    X[8] =9;
    X[9] =9;
    X[10] =9;
    X[11] =6;
    X[12] =6;
    X[13] =8;
    X[14] =8;
    X[15] =9;
    X[16] =9;
    X[17] =9;
    X[18] =6;
    X[19] =6;
    X[20] =9;
    X[21] =9;
    X[22] =7;
    X[23] =7;
    X[24] =9;
    X[25] =9;
    X[26] =9;
    X[27] =6;
    X[28] =6;
    X[29] =9;
    X[30] =9;
    X[31] =7;
    X[32] =7;
    X[33] =9;
    X[34] =9;
    X[35] =9;
    X[36] =8;
    X[37] =8;
    X[38] =7;
    X[39] =7;
    X[40] =9;
    X[41] =9;
    X[42] =9;
    X[43] =9;
    X[44] =9;
    X[45] =8;
    X[46] =8;
    X[47] =7;
    X[48] =7;
    X[49] =9;
    X[50] =9;
    X[51] =9;
    X[52] =9;
    X[53] =9;
    X[54] =9;
    X[55] =9;
    X[56] =9;
    X[57] =9;
    X[58] =9;
    X[59] =9;
    X[60] =9;
    X[61] =9;
    X[62] =9;
    X[63] =9;
    X[64] =9;
    X[65] =9;
    X[66] =9;
    X[67] =9;
    X[68] =9;
    X[69] =9;
    X[70] =9;
    X[71] =9;
    X[72] =9;
    X[73] =9;
    X[74] =9;
    X[75] =9;
    X[76] =9;
    X[77] =9;
    X[78] =9;
    X[79] =9;
    X[80] =9;
    X[81] =9;
    X[82] =9;
    X[83] =9;
    X[84] =9;
    X[85] =9;
    X[86] =9;
    X[87] =2;
    X[88] =9;
    X[89] =4;
    X[90] =9;
    X[91] =9;
    X[92] =9;
    X[93] =9;
    X[94] =9;
    X[95] =9;
    X[96] =2;
    X[97] =9;
    X[98] =4;
    X[99] =9;
    X[100] =9;
    X[101] =9;
    X[102] =9;
    X[103] =9;
    X[104] =9;
    X[105] =0;
    X[106] =4;
    X[107] =9;
    X[108] =9;
    X[109] =9;
    X[110] =9;
    X[111] =9;
    X[112] =9;
    X[113] =9;
    X[114] =0;
    X[115] =4;
    X[116] =9;
    X[117] =9;
    X[118] =9;
    X[119] =9;
    X[120] =9;
    X[121] =9;
    X[122] =9;
    X[123] =9;
    X[124] =2;
    X[125] =0;
    X[126] =9;
    X[127] =9;
    X[128] =9;
    X[129] =9;
    X[130] =9;
    X[131] =9;
    X[132] =9;
    X[133] =2;
    X[134] =0;
    X[135] =2;
    X[136] =2;
    X[137] =0;
    X[138] =0;
    X[139] =9;
    X[140] =9;
    X[141] =9;
    X[142] =9;
    X[143] =9;
    X[144] =9;
    X[145] =9;
    X[146] =4;
    X[147] =4;
    X[148] =2;
    X[149] =2;
    X[150] =9;
    X[151] =9;
    X[152] =9;
    X[153] =4;
    X[154] =4;
    X[155] =9;
    X[156] =9;
    X[157] =0;
    X[158] =0;
    X[159] =9;
    X[160] =9;
    X[161] =9;
    X[162] =9;
    X[163] =9;
    X[164] =9;
    X[165] =9;
    X[166] =9;
    X[167] =9;
    X[168] =3;
    X[169] =9;
    X[170] =5;
    X[171] =9;
    X[172] =9;
    X[173] =9;
    X[174] =9;
    X[175] =9;
    X[176] =9;
    X[177] =3;
    X[178] =9;
    X[179] =5;
    X[180] =9;
    X[181] =9;
    X[182] =9;
    X[183] =9;
    X[184] =9;
    X[185] =9;
    X[186] =1;
    X[187] =5;
    X[188] =9;
    X[189] =9;
    X[190] =9;
    X[191] =9;
    X[192] =9;
    X[193] =9;
    X[194] =9;
    X[195] =1;
    X[196] =5;
    X[197] =9;
    X[198] =9;
    X[199] =9;
    X[200] =9;
    X[201] =9;
    X[202] =9;
    X[203] =9;
    X[204] =9;
    X[205] =3;
    X[206] =1;
    X[207] =9;
    X[208] =9;
    X[209] =9;
    X[210] =9;
    X[211] =9;
    X[212] =9;
    X[213] =9;
    X[214] =3;
    X[215] =1;
    X[216] =3;
    X[217] =3;
    X[218] =1;
    X[219] =1;
    X[220] =9;
    X[221] =9;
    X[222] =9;
    X[223] =9;
    X[224] =9;
    X[225] =9;
    X[226] =9;
    X[227] =5;
    X[228] =5;
    X[229] =3;
    X[230] =3;
    X[231] =9;
    X[232] =9;
    X[233] =9;
    X[234] =5;
    X[235] =5;
    X[236] =9;
    X[237] =9;
    X[238] =1;
    X[239] =1;
    X[240] =9;
    X[241] =9;
    X[242] =9;
  }
}

#endif

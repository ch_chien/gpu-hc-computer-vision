#ifndef Ht_3vTrg_const_mat_Y_h
#define Ht_3vTrg_const_mat_Y_h
// ============================================================================
// cd index vector for Ht of 3vTrg problem
//
// Modifications
//    Chien  21-09-06:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Ht_3vTrg_const_matrix_Y(magma_int_t* Y)
  {
    Y[0] =27;
    Y[1] =28;
    Y[2] =29;
    Y[3] =30;
    Y[4] =31;
    Y[5] =32;
    Y[6] =8;
    Y[7] =17;
    Y[8] =26;

    Y[9] =2;
    Y[10] =5;
    Y[11] =6;
    Y[12] =7;
    Y[13] =15;
    Y[14] =16;
    Y[15] =2;
    Y[16] =11;
    Y[17] =20;

    Y[18] =20;
    Y[19] =23;
    Y[20] =11;
    Y[21] =14;
    Y[22] =24;
    Y[23] =25;
    Y[24] =5;
    Y[25] =14;
    Y[26] =23;

    Y[27] =33;
    Y[28] =33;
    Y[29] =33;
    Y[30] =33;
    Y[31] =33;
    Y[32] =33;
    Y[33] =6;
    Y[34] =15;
    Y[35] =24;

    Y[36] =0;
    Y[37] =3;
    Y[38] =0;
    Y[39] =1;
    Y[40] =9;
    Y[41] =10;
    Y[42] =7;
    Y[43] =16;
    Y[44] =25;

    Y[45] =1;
    Y[46] =4;
    Y[47] =3;
    Y[48] =4;
    Y[49] =12;
    Y[50] =13;
    Y[51] =0;
    Y[52] =9;
    Y[53] =18;

    Y[54] =18;
    Y[55] =21;
    Y[56] =9;
    Y[57] =12;
    Y[58] =18;
    Y[59] =19;
    Y[60] =1;
    Y[61] =10;
    Y[62] =19;

    Y[63] =19;
    Y[64] =22;
    Y[65] =10;
    Y[66] =13;
    Y[67] =21;
    Y[68] =22;
    Y[69] =3;
    Y[70] =12;
    Y[71] =21;

    Y[72] =0;
    Y[73] =0;
    Y[74] =0;
    Y[75] =0;
    Y[76] =0;
    Y[77] =0;
    Y[78] =4;
    Y[79] =13;
    Y[80] =22;
  }
}

#endif

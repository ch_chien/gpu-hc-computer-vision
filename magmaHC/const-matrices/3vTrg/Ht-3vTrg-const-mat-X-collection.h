#ifndef Ht_3vTrg_const_mat_X_collection_h
#define Ht_3vTrg_const_mat_X_collection_h
// ============================================================================
// constant matrix indexing the solution vector in \partual H / \partial t out
// 3vTrg problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Ht_3vTrg_const_matrix_X_collection(magma_int_t* Xt)
  {
    Xt[0] =9;
    Xt[1] =9;
    Xt[2] =9;
    Xt[3] =9;
    Xt[4] =9;
    Xt[5] =9;
    Xt[6] =9;
    Xt[7] =9;
    Xt[8] =9;

    Xt[9] =6;
    Xt[10] =6;
    Xt[11] =6;
    Xt[12] =6;
    Xt[13] =7;
    Xt[14] =7;
    Xt[15] =0;
    Xt[16] =2;
    Xt[17] =0;

    Xt[18] =8;
    Xt[19] =8;
    Xt[20] =7;
    Xt[21] =7;
    Xt[22] =8;
    Xt[23] =8;
    Xt[24] =1;
    Xt[25] =3;
    Xt[26] =1;

    Xt[27] =0;
    Xt[28] =1;
    Xt[29] =2;
    Xt[30] =3;
    Xt[31] =4;
    Xt[32] =5;
    Xt[33] =2;
    Xt[34] =4;
    Xt[35] =4;

    Xt[36] =6;
    Xt[37] =6;
    Xt[38] =6;
    Xt[39] =6;
    Xt[40] =7;
    Xt[41] =7;
    Xt[42] =3;
    Xt[43] =5;
    Xt[44] =5;

    Xt[45] =6;
    Xt[46] =6;
    Xt[47] =6;
    Xt[48] =6;
    Xt[49] =7;
    Xt[50] =7;
    Xt[51] =2;
    Xt[52] =4;
    Xt[53] =4;

    Xt[54] =8;
    Xt[55] =8;
    Xt[56] =7;
    Xt[57] =7;
    Xt[58] =8;
    Xt[59] =8;
    Xt[60] =3;
    Xt[61] =5;
    Xt[62] =5;

    Xt[63] =8;
    Xt[64] =8;
    Xt[65] =7;
    Xt[66] =7;
    Xt[67] =8;
    Xt[68] =8;
    Xt[69] =2;
    Xt[70] =4;
    Xt[71] =4;

    Xt[72] =9;
    Xt[73] =9;
    Xt[74] =9;
    Xt[75] =9;
    Xt[76] =9;
    Xt[77] =9;
    Xt[78] =3;
    Xt[79] =5;
    Xt[80] =5;


    Xt[81] =9;
    Xt[82] =9;
    Xt[83] =9;
    Xt[84] =9;
    Xt[85] =9;
    Xt[86] =9;
    Xt[87] =9;
    Xt[88] =9;
    Xt[89] =9;

    Xt[90] =9;
    Xt[91] =9;
    Xt[92] =9;
    Xt[93] =9;
    Xt[94] =9;
    Xt[95] =9;
    Xt[96] =9;
    Xt[97] =9;
    Xt[98] =9;

    Xt[99] =9;
    Xt[100] =9;
    Xt[101] =9;
    Xt[102] =9;
    Xt[103] =9;
    Xt[104] =9;
    Xt[105] =9;
    Xt[106] =9;
    Xt[107] =9;

    Xt[108] =9;
    Xt[109] =9;
    Xt[110] =9;
    Xt[111] =9;
    Xt[112] =9;
    Xt[113] =9;
    Xt[114] =9;
    Xt[115] =9;
    Xt[116] =9;

    Xt[117] =2;
    Xt[118] =2;
    Xt[119] =0;
    Xt[120] =0;
    Xt[121] =2;
    Xt[122] =2;
    Xt[123] =9;
    Xt[124] =9;
    Xt[125] =9;

    Xt[126] =3;
    Xt[127] =3;
    Xt[128] =1;
    Xt[129] =1;
    Xt[130] =3;
    Xt[131] =3;
    Xt[132] =0;
    Xt[133] =2;
    Xt[134] =0;

    Xt[135] =4;
    Xt[136] =4;
    Xt[137] =4;
    Xt[138] =4;
    Xt[139] =0;
    Xt[140] =0;
    Xt[141] =0;
    Xt[142] =2;
    Xt[143] =0;

    Xt[144] =5;
    Xt[145] =5;
    Xt[146] =5;
    Xt[147] =5;
    Xt[148] =1;
    Xt[149] =1;
    Xt[150] =1;
    Xt[151] =3;
    Xt[152] =1;

    Xt[153] =9;
    Xt[154] =9;
    Xt[155] =9;
    Xt[156] =9;
    Xt[157] =9;
    Xt[158] =9;
    Xt[159] =1;
    Xt[160] =3;
    Xt[161] =1;
  }
}

#endif

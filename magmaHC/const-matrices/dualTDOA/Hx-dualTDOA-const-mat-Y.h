#ifndef Hx_dualTDOA_const_mat_Y_h
#define Hx_dualTDOA_const_mat_Y_h
// ============================================================================
// cdt index matrix for Hx of dualTDOA problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "../define-const-matrices.h"

namespace magmaHCWrapper {

  extern "C"
  void Hx_dualTDOA_const_matrix_Y(magma_int_t* Y)
  {
    Y[0] =6;
    Y[1] =23;
    Y[2] =39;
    Y[3] =57;
    Y[4] =75;

    Y[5] =7;
    Y[6] =24;
    Y[7] =41;
    Y[8] =61;
    Y[9] =76;

    Y[10] =11;
    Y[11] =28;
    Y[12] =45;
    Y[13] =63;
    Y[14] =80;

    Y[15] =14;
    Y[16] =31;
    Y[17] =48;
    Y[18] =66;
    Y[19] =83;

    Y[20] =16;
    Y[21] =87;
    Y[22] =50;
    Y[23] =68;
    Y[24] =85;

    Y[25] =3;
    Y[26] =20;
    Y[27] =36;
    Y[28] =54;
    Y[29] =72;

    Y[30] =7;
    Y[31] =24;
    Y[32] =41;
    Y[33] =58;
    Y[34] =76;

    Y[35] =10;
    Y[36] =27;
    Y[37] =44;
    Y[38] =62;
    Y[39] =79;

    Y[40] =5;
    Y[41] =22;
    Y[42] =38;
    Y[43] =56;
    Y[44] =74;

    Y[45] =4;
    Y[46] =21;
    Y[47] =37;
    Y[48] =55;
    Y[49] =73;

    Y[50] =6;
    Y[51] =23;
    Y[52] =39;
    Y[53] =57;
    Y[54] =75;

    Y[55] =9;
    Y[56] =26;
    Y[57] =43;
    Y[58] =60;
    Y[59] =78;

    Y[60] =10;
    Y[61] =27;
    Y[62] =44;
    Y[63] =62;
    Y[64] =79;

    Y[65] =8;
    Y[66] =25;
    Y[67] =42;
    Y[68] =59;
    Y[69] =77;

    Y[70] =5;
    Y[71] =22;
    Y[72] =38;
    Y[73] =56;
    Y[74] =74;

    Y[75] =1;
    Y[76] =19;
    Y[77] =35;
    Y[78] =53;
    Y[79] =71;

    Y[80] =1;
    Y[81] =19;
    Y[82] =35;
    Y[83] =53;
    Y[84] =71;

    Y[85] =0;
    Y[86] =18;
    Y[87] =37;
    Y[88] =52;
    Y[89] =70;

    Y[90] =12;
    Y[91] =29;
    Y[92] =46;
    Y[93] =64;
    Y[94] =81;

    Y[95] =13;
    Y[96] =30;
    Y[97] =47;
    Y[98] =65;
    Y[99] =82;

    Y[100] =5;
    Y[101] =22;
    Y[102] =38;
    Y[103] =56;
    Y[104] =74;

    Y[105] =5;
    Y[106] =22;
    Y[107] =38;
    Y[108] =56;
    Y[109] =74;

    Y[110] =4;
    Y[111] =21;
    Y[112] =40;
    Y[113] =55;
    Y[114] =73;

    Y[115] =13;
    Y[116] =30;
    Y[117] =47;
    Y[118] =65;
    Y[119] =82;

    Y[120] =15;
    Y[121] =32;
    Y[122] =49;
    Y[123] =67;
    Y[124] =84;

    Y[125] =2;
    Y[126] =2;
    Y[127] =2;
    Y[128] =2;
    Y[129] =2;

    Y[130] =2;
    Y[131] =2;
    Y[132] =2;
    Y[133] =2;
    Y[134] =2;

    Y[135] =8;
    Y[136] =25;
    Y[137] =42;
    Y[138] =59;
    Y[139] =77;

    Y[140] =2;
    Y[141] =2;
    Y[142] =2;
    Y[143] =2;
    Y[144] =2;

    Y[145] =2;
    Y[146] =2;
    Y[147] =2;
    Y[148] =2;
    Y[149] =2;

    Y[150] =4;
    Y[151] =21;
    Y[152] =37;
    Y[153] =55;
    Y[154] =73;

    Y[155] =8;
    Y[156] =25;
    Y[157] =42;
    Y[158] =59;
    Y[159] =77;

    Y[160] =2;
    Y[161] =2;
    Y[162] =2;
    Y[163] =2;
    Y[164] =2;

    Y[165] =3;
    Y[166] =20;
    Y[167] =36;
    Y[168] =54;
    Y[169] =72;

    Y[170] =4;
    Y[171] =21;
    Y[172] =37;
    Y[173] =55;
    Y[174] =73;

    Y[175] =0;
    Y[176] =18;
    Y[177] =34;
    Y[178] =52;
    Y[179] =70;

    Y[180] =0;
    Y[181] =18;
    Y[182] =35;
    Y[183] =52;
    Y[184] =70;

    Y[185] =2;
    Y[186] =2;
    Y[187] =2;
    Y[188] =2;
    Y[189] =2;

    Y[190] =4;
    Y[191] =21;
    Y[192] =37;
    Y[193] =55;
    Y[194] =73;

    Y[195] =6;
    Y[196] =23;
    Y[197] =39;
    Y[198] =57;
    Y[199] =75;

    Y[200] =1;
    Y[201] =19;
    Y[202] =35;
    Y[203] =53;
    Y[204] =71;

    Y[205] =1;
    Y[206] =19;
    Y[207] =40;
    Y[208] =53;
    Y[209] =71;

    Y[210] =1;
    Y[211] =19;
    Y[212] =35;
    Y[213] =53;
    Y[214] =71;

    Y[215] =7;
    Y[216] =24;
    Y[217] =41;
    Y[218] =58;
    Y[219] =76;

    Y[220] =8;
    Y[221] =25;
    Y[222] =42;
    Y[223] =59;
    Y[224] =77;

    Y[225] =2;
    Y[226] =2;
    Y[227] =2;
    Y[228] =2;
    Y[229] =2;

    Y[230] =2;
    Y[231] =2;
    Y[232] =2;
    Y[233] =2;
    Y[234] =2;

    Y[235] =1;
    Y[236] =19;
    Y[237] =35;
    Y[238] =53;
    Y[239] =71;

    Y[240] =8;
    Y[241] =25;
    Y[242] =42;
    Y[243] =59;
    Y[244] =77;

    Y[245] =9;
    Y[246] =26;
    Y[247] =43;
    Y[248] =60;
    Y[249] =78;

    Y[250] =88;
    Y[251] =88;
    Y[252] =88;
    Y[253] =88;
    Y[254] =88;

    Y[255] =88;
    Y[256] =88;
    Y[257] =88;
    Y[258] =88;
    Y[259] =88;

    Y[260] =88;
    Y[261] =88;
    Y[262] =88;
    Y[263] =88;
    Y[264] =88;

    Y[265] =10;
    Y[266] =27;
    Y[267] =44;
    Y[268] =62;
    Y[269] =79;

    Y[270] =10;
    Y[271] =27;
    Y[272] =44;
    Y[273] =62;
    Y[274] =79;

    Y[275] =88;
    Y[276] =88;
    Y[277] =88;
    Y[278] =88;
    Y[279] =88;

    Y[280] =88;
    Y[281] =88;
    Y[282] =88;
    Y[283] =88;
    Y[284] =88;

    Y[285] =88;
    Y[286] =88;
    Y[287] =88;
    Y[288] =88;
    Y[289] =88;

    Y[290] =0;
    Y[291] =18;
    Y[292] =34;
    Y[293] =52;
    Y[294] =70;

    Y[295] =0;
    Y[296] =18;
    Y[297] =35;
    Y[298] =52;
    Y[299] =70;

    Y[300] =88;
    Y[301] =88;
    Y[302] =88;
    Y[303] =88;
    Y[304] =88;

    Y[305] =88;
    Y[306] =88;
    Y[307] =88;
    Y[308] =88;
    Y[309] =88;

    Y[310] =88;
    Y[311] =88;
    Y[312] =88;
    Y[313] =88;
    Y[314] =88;

    Y[315] =1;
    Y[316] =19;
    Y[317] =35;
    Y[318] =53;
    Y[319] =71;

    Y[320] =1;
    Y[321] =19;
    Y[322] =35;
    Y[323] =53;
    Y[324] =71;

    Y[325] =88;
    Y[326] =88;
    Y[327] =88;
    Y[328] =88;
    Y[329] =88;

    Y[330] =88;
    Y[331] =88;
    Y[332] =88;
    Y[333] =88;
    Y[334] =88;

    Y[335] =88;
    Y[336] =88;
    Y[337] =88;
    Y[338] =88;
    Y[339] =88;

    Y[340] =1;
    Y[341] =19;
    Y[342] =35;
    Y[343] =53;
    Y[344] =71;

    Y[345] =1;
    Y[346] =19;
    Y[347] =40;
    Y[348] =53;
    Y[349] =71;

    Y[350] =88;
    Y[351] =88;
    Y[352] =88;
    Y[353] =88;
    Y[354] =88;

    Y[355] =88;
    Y[356] =88;
    Y[357] =88;
    Y[358] =88;
    Y[359] =88;

    Y[360] =88;
    Y[361] =88;
    Y[362] =88;
    Y[363] =88;
    Y[364] =88;

    Y[365] =2;
    Y[366] =2;
    Y[367] =2;
    Y[368] =2;
    Y[369] =2;

    Y[370] =2;
    Y[371] =2;
    Y[372] =2;
    Y[373] =2;
    Y[374] =2;

  }
}

#endif

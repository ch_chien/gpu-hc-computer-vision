#ifndef params2coeffs_optpose4pt_h
#define params2coeffs_optpose4pt_h
// =======================================================================
//
// Modifications
//    Chien  21-10-24:   Initially Created
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min

namespace magmaHCWrapper {

  void params2coeffs_optpose4pt(magmaFloatComplex *h_target_params, magmaFloatComplex *h_target_coeffs)
  {
    magmaFloatComplex x6 = h_target_params[0];
    magmaFloatComplex x7 = h_target_params[1];
    magmaFloatComplex x8 = h_target_params[2];
    magmaFloatComplex x9 = h_target_params[3];
    magmaFloatComplex x10 = h_target_params[4];
    magmaFloatComplex x11 = h_target_params[5];
    magmaFloatComplex x12 = h_target_params[6];
    magmaFloatComplex x13 = h_target_params[7];
    magmaFloatComplex x14 = h_target_params[8];
    magmaFloatComplex x15 = h_target_params[9];
    magmaFloatComplex x16 = h_target_params[10];
    magmaFloatComplex x17 = h_target_params[11];
    magmaFloatComplex x18 = h_target_params[12];
    magmaFloatComplex x19 = h_target_params[13];
    magmaFloatComplex x20 = h_target_params[14];
    magmaFloatComplex x21 = h_target_params[15];
    magmaFloatComplex x22 = h_target_params[16];
    magmaFloatComplex x23 = h_target_params[17];
    magmaFloatComplex x24 = h_target_params[18];
    magmaFloatComplex x25 = h_target_params[19];
    magmaFloatComplex x26 = h_target_params[20];
    magmaFloatComplex x27 = h_target_params[21];
    magmaFloatComplex x28 = h_target_params[22];
    magmaFloatComplex x29 = h_target_params[23];
    magmaFloatComplex x30 = h_target_params[24];
    magmaFloatComplex x31 = h_target_params[25];
    magmaFloatComplex x32 = h_target_params[26];
    magmaFloatComplex x33 = h_target_params[27];
    magmaFloatComplex x34 = h_target_params[28];
    magmaFloatComplex x35 = h_target_params[29];
    magmaFloatComplex x36 = h_target_params[30];
    magmaFloatComplex x37 = h_target_params[31];
    magmaFloatComplex x38 = h_target_params[32];
    magmaFloatComplex x39 = h_target_params[33];
    magmaFloatComplex x40 = h_target_params[34];
    magmaFloatComplex x41 = h_target_params[35];
    magmaFloatComplex x42 = h_target_params[36];
    magmaFloatComplex x43 = h_target_params[37];
    magmaFloatComplex x44 = h_target_params[38];
    magmaFloatComplex x45 = h_target_params[39];
    magmaFloatComplex x46 = h_target_params[40];
    magmaFloatComplex x47 = h_target_params[41];
    magmaFloatComplex x48 = h_target_params[42];
    magmaFloatComplex x49 = h_target_params[43];
    magmaFloatComplex x50 = h_target_params[44];
    magmaFloatComplex x51 = h_target_params[45];
    magmaFloatComplex x52 = h_target_params[46];
    magmaFloatComplex x53 = h_target_params[47];

    h_target_coeffs[0] = (x18);
    h_target_coeffs[1] = (x19 + x21);
    h_target_coeffs[2] = (x20 + x24);
    h_target_coeffs[3] = (x7*x19 + x7*x21 + x8*x20 + x8*x24);
    h_target_coeffs[4] = (x7*x20 + x7*x24 - x8*x19 - x8*x21);
    h_target_coeffs[5] = (x6*x18);
    h_target_coeffs[6] = (x22);
    h_target_coeffs[7] = (x23 + x25);
    h_target_coeffs[8] = (2*x7*x22 + x8*x23 + x8*x25);
    h_target_coeffs[9] = (x7*x23 + x7*x25 - 2*x8*x22);
    h_target_coeffs[10] = (x6*x19 + x6*x21);
    h_target_coeffs[11] = (x26);
    h_target_coeffs[12] = (x7*x23 + x7*x25 + 2*x8*x26);
    h_target_coeffs[13] = (2*x7*x26 - x8*x23 - x8*x25);
    h_target_coeffs[14] = (x6*x20 + x6*x24);
    h_target_coeffs[15] = (x7*x7*x22 + x7*x8*x23 + x7*x8*x25 + x8*x8*x26);
    h_target_coeffs[16] = (x7*x7*x23 + x7*x7*x25 - 2*x7*x8*x22 + 2*x7*x8*x26 - x8*x8*x23 - x8*x8*x25);
    h_target_coeffs[17] = (x6*x7*x19 + x6*x7*x21 + x6*x8*x20 + x6*x8*x24);
    h_target_coeffs[18] = (x7*x7*x26 - x7*x8*x23 - x7*x8*x25 + x8*x8*x22);
    h_target_coeffs[19] = (x6*x7*x20 + x6*x7*x24 - x6*x8*x19 - x6*x8*x21);
    h_target_coeffs[20] = (x6*x6*x18);
    h_target_coeffs[21] = (x27);
    h_target_coeffs[22] = (x28 + x30);
    h_target_coeffs[23] = (x29 + x33);
    h_target_coeffs[24] = (x10*x28 + x10*x30 + x11*x29 + x11*x33);
    h_target_coeffs[25] = (x10*x29 + x10*x33 - x11*x28 - x11*x30);
    h_target_coeffs[26] = (x9*x27);
    h_target_coeffs[27] = (x31);
    h_target_coeffs[28] = (x32 + x34);
    h_target_coeffs[29] = (2*x10*x31 + x11*x32 + x11*x34);
    h_target_coeffs[30] = (x10*x32 + x10*x34 - 2*x11*x31);
    h_target_coeffs[31] = (x9*x28 + x9*x30);
    h_target_coeffs[32] = (x35);
    h_target_coeffs[33] = (x10*x32 + x10*x34 + 2*x11*x35);
    h_target_coeffs[34] = (2*x10*x35 - x11*x32 - x11*x34);
    h_target_coeffs[35] = (x9*x29 + x9*x33);
    h_target_coeffs[36] = (x10*x10*x31 + x10*x11*x32 + x10*x11*x34 + x11*x11*x35);
    h_target_coeffs[37] = (x10*x10*x32 + x10*x10*x34 - 2*x10*x11*x31 + 2*x10*x11*x35 - x11*x11*x32 - x11*x11*x34);
    h_target_coeffs[38] = (x9*x10*x28 + x9*x10*x30 + x9*x11*x29 + x9*x11*x33);
    h_target_coeffs[39] = (x10*x10*x35 - x10*x11*x32 - x10*x11*x34 + x11*x11*x31);
    h_target_coeffs[40] = (x9*x10*x29 + x9*x10*x33 - x9*x11*x28 - x9*x11*x30);
    h_target_coeffs[41] = (x9*x9*x27);
    h_target_coeffs[42] = (x36);
    h_target_coeffs[43] = (x37 + x39);
    h_target_coeffs[44] = (x38 + x42);
    h_target_coeffs[45] = (x13*x37 + x13*x39 + x14*x38 + x14*x42);
    h_target_coeffs[46] = (x13*x38 + x13*x42 - x14*x37 - x14*x39);
    h_target_coeffs[47] = (x12*x36);
    h_target_coeffs[48] = (x40);
    h_target_coeffs[49] = (x41 + x43);
    h_target_coeffs[50] = (2*x13*x40 + x14*x41 + x14*x43);
    h_target_coeffs[51] = (x13*x41 + x13*x43 - 2*x14*x40);
    h_target_coeffs[52] = (x12*x37 + x12*x39);
    h_target_coeffs[53] = (x44);
    h_target_coeffs[54] = (x13*x41 + x13*x43 + 2*x14*x44);
    h_target_coeffs[55] = (2*x13*x44 - x14*x41 - x14*x43);
    h_target_coeffs[56] = (x12*x38 + x12*x42);
    h_target_coeffs[57] = (x13*x13*x40 + x13*x14*x41 + x13*x14*x43 + x14*x14*x44);
    h_target_coeffs[58] = (x13*x13*x41 + x13*x13*x43 - 2*x13*x14*x40 + 2*x13*x14*x44 - x14*x14*x41 - x14*x14*x43);
    h_target_coeffs[59] = (x12*x13*x37 + x12*x13*x39 + x12*x14*x38 + x12*x14*x42);
    h_target_coeffs[60] = (x13*x13*x44 - x13*x14*x41 - x13*x14*x43 + x14*x14*x40);
    h_target_coeffs[61] = (x12*x13*x38 + x12*x13*x42 - x12*x14*x37 - x12*x14*x39);
    h_target_coeffs[62] = (x12*x12*x36);
    h_target_coeffs[63] = (x45);
    h_target_coeffs[64] = (x46 + x48);
    h_target_coeffs[65] = (x47 + x51);
    h_target_coeffs[66] = (x16*x46 + x16*x48 + x17*x47 + x17*x51);
    h_target_coeffs[67] = (x16*x47 + x16*x51 - x17*x46 - x17*x48);
    h_target_coeffs[68] = (x15*x45);
    h_target_coeffs[69] = (x49);
    h_target_coeffs[70] = (x50 + x52);
    h_target_coeffs[71] = (2*x16*x49 + x17*x50 + x17*x52);
    h_target_coeffs[72] = (x16*x50 + x16*x52 - 2*x17*x49);
    h_target_coeffs[73] = (x15*x46 + x15*x48);
    h_target_coeffs[74] = (x53);
    h_target_coeffs[75] = (x16*x50 + x16*x52 + 2*x17*x53);
    h_target_coeffs[76] = (2*x16*x53 - x17*x50 - x17*x52);
    h_target_coeffs[77] = (x15*x47 + x15*x51);
    h_target_coeffs[78] = (x16*x16*x49 + x16*x17*x50 + x16*x17*x52 + x17*x17*x53);
    h_target_coeffs[79] = (x16*x16*x50 + x16*x16*x52 - 2*x16*x17*x49 + 2*x16*x17*x53 - x17*x17*x50 - x17*x17*x52);
    h_target_coeffs[80] = (x15*x16*x46 + x15*x16*x48 + x15*x17*x47 + x15*x17*x51);
    h_target_coeffs[81] = (x16*x16*x53 - x16*x17*x50 - x16*x17*x52 + x17*x17*x49);
    h_target_coeffs[82] = (x15*x16*x47 + x15*x16*x51 - x15*x17*x46 - x15*x17*x48);
    h_target_coeffs[83] = (x15*x15*x45);
    h_target_coeffs[84] = MAGMA_C_ONE;
    h_target_coeffs[85] = MAGMA_C_ONE;
    h_target_coeffs[86] = MAGMA_C_NEG_ONE;
  }
} // end of namespace

#endif

#ifndef params2coeffs_trifocal_rel_pose_f_h
#define params2coeffs_trifocal_rel_pose_f_h
// =======================================================================
//
// Modifications
//    Chien  21-12-01:   Initially created, revised from original 
//                       3view_unknownf problem
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min

namespace magmaHCWrapper {

  void params2coeffs_trifocal_rel_pose_f(magmaFloatComplex *h_target_params, magmaFloatComplex *h_target_coeffs)
  {
    magmaFloatComplex x20 = h_target_params[0];
    magmaFloatComplex x21 = h_target_params[1];
    magmaFloatComplex x22 = h_target_params[2];
    magmaFloatComplex x23 = h_target_params[3];
    magmaFloatComplex x24 = h_target_params[4];
    magmaFloatComplex x25 = h_target_params[5];
    magmaFloatComplex x26 = h_target_params[6];
    magmaFloatComplex x27 = h_target_params[7];
    magmaFloatComplex x28 = h_target_params[8];
    magmaFloatComplex x29 = h_target_params[9];
    magmaFloatComplex x30 = h_target_params[10];
    magmaFloatComplex x31 = h_target_params[11];
    magmaFloatComplex x32 = h_target_params[12];
    magmaFloatComplex x33 = h_target_params[13];
    magmaFloatComplex x34 = h_target_params[14];
    magmaFloatComplex x35 = h_target_params[15];
    magmaFloatComplex x36 = h_target_params[16];
    magmaFloatComplex x37 = h_target_params[17];
    magmaFloatComplex x38 = h_target_params[18];
    magmaFloatComplex x39 = h_target_params[19];
    magmaFloatComplex x40 = h_target_params[20];
    magmaFloatComplex x41 = h_target_params[21];
    magmaFloatComplex x42 = h_target_params[22];
    magmaFloatComplex x43 = h_target_params[23];
    magmaFloatComplex x44 = h_target_params[24];
    magmaFloatComplex x45 = h_target_params[25];

    h_target_coeffs[0] = (x24 - x22);
    h_target_coeffs[1] = (2*x20*x21 - 2*x20*x23 - 2*x21*x24 + 2*x23*x24);
    h_target_coeffs[2] = MAGMA_C_MAKE(-2.0, 0.0);
    h_target_coeffs[3] = (2*x20*x22 + 2*x20*x24 - 2*x22*x24 - 2*x20*x20);
    h_target_coeffs[4] = (2*x23 - 2*x21);
    h_target_coeffs[5] = (2*x20 - x22 - x24);
    h_target_coeffs[6] = (2*x21 - 2*x23);
    h_target_coeffs[7] = (2*x22*x24 - 2*x20*x24 - 2*x20*x22 + 2*x20*x20);
    h_target_coeffs[8] = (x22 - x24);
    h_target_coeffs[9] = (x24 - x20);
    h_target_coeffs[10] = (x22 - 2*x20 + x24);
    h_target_coeffs[11] = MAGMA_C_NEG_ONE;
    h_target_coeffs[12] = (x25 - x23);
    h_target_coeffs[13] = MAGMA_C_MAKE(2.0, 0.0);
    h_target_coeffs[14] = (2*x23*x25 - 2*x21*x25 - 2*x21*x23 + 2*x21*x21);
    h_target_coeffs[15] = (2*x21*x22 - 2*x20*x21 + 2*x20*x25 - 2*x22*x25);
    h_target_coeffs[16] = (2*x20 - 2*x22);
    h_target_coeffs[17] = (x23 - x25);
    h_target_coeffs[18] = (2*x20*x21 - 2*x21*x22 - 2*x20*x25 + 2*x22*x25);
    h_target_coeffs[19] = (2*x21 - x23 - x25);
    h_target_coeffs[20] = (x25 - x21);
    h_target_coeffs[21] = (x23 - 2*x21 + x25);
    h_target_coeffs[22] = (x26 - x22);
    h_target_coeffs[23] = (2*x20*x21 - 2*x20*x23 - 2*x21*x26 + 2*x23*x26);
    h_target_coeffs[24] = (2*x20*x22 + 2*x20*x26 - 2*x22*x26 - 2*x20*x20);
    h_target_coeffs[25] = (2*x20 - x22 - x26);
    h_target_coeffs[26] = (2*x22*x26 - 2*x20*x26 - 2*x20*x22 + 2*x20*x20);
    h_target_coeffs[27] = (x22 - x26);
    h_target_coeffs[28] = (x26 - x20);
    h_target_coeffs[29] = (x22 - 2*x20 + x26);
    h_target_coeffs[30] = (x27 - x23);
    h_target_coeffs[31] = (2*x23*x27 - 2*x21*x27 - 2*x21*x23 + 2*x21*x21);
    h_target_coeffs[32] = (2*x21*x22 - 2*x20*x21 + 2*x20*x27 - 2*x22*x27);
    h_target_coeffs[33] = (x23 - x27);
    h_target_coeffs[34] = (2*x20*x21 - 2*x21*x22 - 2*x20*x27 + 2*x22*x27);
    h_target_coeffs[35] = (2*x21 - x23 - x27);
    h_target_coeffs[36] = (x27 - x21);
    h_target_coeffs[37] = (x23 - 2*x21 + x27);
    h_target_coeffs[38] = (x30 - x28);
    h_target_coeffs[39] = (2*x20*x21 - 2*x20*x29 - 2*x21*x30 + 2*x29*x30);
    h_target_coeffs[40] = (2*x20*x28 + 2*x20*x30 - 2*x28*x30 - 2*x20*x20);
    h_target_coeffs[41] = (2*x29 - 2*x21);
    h_target_coeffs[42] = (2*x20 - x28 - x30);
    h_target_coeffs[43] = (2*x21 - 2*x29);
    h_target_coeffs[44] = (2*x28*x30 - 2*x20*x30 - 2*x20*x28 + 2*x20*x20);
    h_target_coeffs[45] = (x28 - x30);
    h_target_coeffs[46] = (x30 - x20);
    h_target_coeffs[47] = (x28 - 2*x20 + x30);
    h_target_coeffs[48] = (x31 - x29);
    h_target_coeffs[49] = (2*x29*x31 - 2*x21*x31 - 2*x21*x29 + 2*x21*x21);
    h_target_coeffs[50] = (2*x21*x28 - 2*x20*x21 + 2*x20*x31 - 2*x28*x31);
    h_target_coeffs[51] = (2*x20 - 2*x28);
    h_target_coeffs[52] = (x29 - x31);
    h_target_coeffs[53] = (2*x20*x21 - 2*x21*x28 - 2*x20*x31 + 2*x28*x31);
    h_target_coeffs[54] = (2*x21 - x29 - x31);
    h_target_coeffs[55] = (x31 - x21);
    h_target_coeffs[56] = (x29 - 2*x21 + x31);
    h_target_coeffs[57] = (x32 - x28);
    h_target_coeffs[58] = (2*x20*x21 - 2*x20*x29 - 2*x21*x32 + 2*x29*x32);
    h_target_coeffs[59] = (2*x20*x28 + 2*x20*x32 - 2*x28*x32 - 2*x20*x20);
    h_target_coeffs[60] = (2*x20 - x28 - x32);
    h_target_coeffs[61] = (2*x28*x32 - 2*x20*x32 - 2*x20*x28 + 2*x20*x20);
    h_target_coeffs[62] = (x28 - x32);
    h_target_coeffs[63] = (x32 - x20);
    h_target_coeffs[64] = (x28 - 2*x20 + x32);
    h_target_coeffs[65] = (x33 - x29);
    h_target_coeffs[66] = (2*x29*x33 - 2*x21*x33 - 2*x21*x29 + 2*x21*x21);
    h_target_coeffs[67] = (2*x21*x28 - 2*x20*x21 + 2*x20*x33 - 2*x28*x33);
    h_target_coeffs[68] = (x29 - x33);
    h_target_coeffs[69] = (2*x20*x21 - 2*x21*x28 - 2*x20*x33 + 2*x28*x33);
    h_target_coeffs[70] = (2*x21 - x29 - x33);
    h_target_coeffs[71] = (x33 - x21);
    h_target_coeffs[72] = (x29 - 2*x21 + x33);
    h_target_coeffs[73] = (x36 - x34);
    h_target_coeffs[74] = (2*x20*x21 - 2*x20*x35 - 2*x21*x36 + 2*x35*x36);
    h_target_coeffs[75] = (2*x20*x34 + 2*x20*x36 - 2*x34*x36 - 2*x20*x20);
    h_target_coeffs[76] = (2*x35 - 2*x21);
    h_target_coeffs[77] = (2*x20 - x34 - x36);
    h_target_coeffs[78] = (2*x21 - 2*x35);
    h_target_coeffs[79] = (2*x34*x36 - 2*x20*x36 - 2*x20*x34 + 2*x20*x20);
    h_target_coeffs[80] = (x34 - x36);
    h_target_coeffs[81] = (x36 - x20);
    h_target_coeffs[82] = (x34 - 2*x20 + x36);
    h_target_coeffs[83] = (x37 - x35);
    h_target_coeffs[84] = (2*x35*x37 - 2*x21*x37 - 2*x21*x35 + 2*x21*x21);
    h_target_coeffs[85] = (2*x21*x34 - 2*x20*x21 + 2*x20*x37 - 2*x34*x37);
    h_target_coeffs[86] = (2*x20 - 2*x34);
    h_target_coeffs[87] = (x35 - x37);
    h_target_coeffs[88] = (2*x20*x21 - 2*x21*x34 - 2*x20*x37 + 2*x34*x37);
    h_target_coeffs[89] = (2*x21 - x35 - x37);
    h_target_coeffs[90] = (x37 - x21);
    h_target_coeffs[91] = (x35 - 2*x21 + x37);
    h_target_coeffs[92] = (x38 - x34);
    h_target_coeffs[93] = (2*x20*x21 - 2*x20*x35 - 2*x21*x38 + 2*x35*x38);
    h_target_coeffs[94] = (2*x20*x34 + 2*x20*x38 - 2*x34*x38 - 2*x20*x20);
    h_target_coeffs[95] = (2*x20 - x34 - x38);
    h_target_coeffs[96] = (2*x34*x38 - 2*x20*x38 - 2*x20*x34 + 2*x20*x20);
    h_target_coeffs[97] = (x34 - x38);
    h_target_coeffs[98] = (x38 - x20);
    h_target_coeffs[99] = (x34 - 2*x20 + x38);
    h_target_coeffs[100] = (x39 - x35);
    h_target_coeffs[101] = (2*x35*x39 - 2*x21*x39 - 2*x21*x35 + 2*x21*x21);
    h_target_coeffs[102] = (2*x21*x34 - 2*x20*x21 + 2*x20*x39 - 2*x34*x39);
    h_target_coeffs[103] = (x35 - x39);
    h_target_coeffs[104] = (2*x20*x21 - 2*x21*x34 - 2*x20*x39 + 2*x34*x39);
    h_target_coeffs[105] = (2*x21 - x35 - x39);
    h_target_coeffs[106] = (x39 - x21);
    h_target_coeffs[107] = (x35 - 2*x21 + x39);
    h_target_coeffs[108] = (x42 - x40);
    h_target_coeffs[109] = (2*x20*x21 - 2*x20*x41 - 2*x21*x42 + 2*x41*x42);
    h_target_coeffs[110] = (2*x20*x40 + 2*x20*x42 - 2*x40*x42 - 2*x20*x20);
    h_target_coeffs[111] = (2*x41 - 2*x21);
    h_target_coeffs[112] = (2*x20 - x40 - x42);
    h_target_coeffs[113] = (2*x21 - 2*x41);
    h_target_coeffs[114] = (2*x40*x42 - 2*x20*x42 - 2*x20*x40 + 2*x20*x20);
    h_target_coeffs[115] = (x40 - x42);
    h_target_coeffs[116] = (x42 - x20);
    h_target_coeffs[117] = (x40 - 2*x20 + x42);
    h_target_coeffs[118] = (x43 - x41);
    h_target_coeffs[119] = (2*x41*x43 - 2*x21*x43 - 2*x21*x41 + 2*x21*x21);
    h_target_coeffs[120] = (2*x21*x40 - 2*x20*x21 + 2*x20*x43 - 2*x40*x43);
    h_target_coeffs[121] = (2*x20 - 2*x40);
    h_target_coeffs[122] = (x41 - x43);
    h_target_coeffs[123] = (2*x20*x21 - 2*x21*x40 - 2*x20*x43 + 2*x40*x43);
    h_target_coeffs[124] = (2*x21 - x41 - x43);
    h_target_coeffs[125] = (x43 - x21);
    h_target_coeffs[126] = (x41 - 2*x21 + x43);
    h_target_coeffs[127] = (x44 - x40);
    h_target_coeffs[128] = (2*x20*x21 - 2*x20*x41 - 2*x21*x44 + 2*x41*x44);
    h_target_coeffs[129] = (2*x20*x40 + 2*x20*x44 - 2*x40*x44 - 2*x20*x20);
    h_target_coeffs[130] = (2*x20 - x40 - x44);
    h_target_coeffs[131] = (2*x40*x44 - 2*x20*x44 - 2*x20*x40 + 2*x20*x20);
    h_target_coeffs[132] = (x40 - x44);
    h_target_coeffs[133] = (x44 - x20);
    h_target_coeffs[134] = (x40 - 2*x20 + x44);
    h_target_coeffs[135] = (x45 - x41);
    h_target_coeffs[136] = (2*x41*x45 - 2*x21*x45 - 2*x21*x41 + 2*x21*x21);
    h_target_coeffs[137] = (2*x21*x40 - 2*x20*x21 + 2*x20*x45 - 2*x40*x45);
    h_target_coeffs[138] = (x41 - x45);
    h_target_coeffs[139] = (2*x20*x21 - 2*x21*x40 - 2*x20*x45 + 2*x40*x45);
    h_target_coeffs[140] = (2*x21 - x41 - x45);
    h_target_coeffs[141] = (x45 - x21);
    h_target_coeffs[142] = (x41 - 2*x21 + x45);
  }
} // end of namespace

#endif

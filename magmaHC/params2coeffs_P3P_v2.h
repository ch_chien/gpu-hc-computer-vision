#ifndef params2coeffs_P3P_v2_h
#define params2coeffs_P3P_v2_h
// =======================================================================
//
// Modifications
//    Chien  21-11-03:   Initially Created
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min

namespace magmaHCWrapper {

  void params2coeffs_P3P_v2(magmaFloatComplex *h_target_params, magmaFloatComplex *h_target_coeffs)
  {
    magmaFloatComplex x4 = h_target_params[0];
    magmaFloatComplex x5 = h_target_params[1];
    magmaFloatComplex x6 = h_target_params[2];
    magmaFloatComplex x7 = h_target_params[3];
    magmaFloatComplex x8 = h_target_params[4];
    magmaFloatComplex x9 = h_target_params[5];
    magmaFloatComplex x10 = h_target_params[6];
    magmaFloatComplex x11 = h_target_params[7];
    magmaFloatComplex x12 = h_target_params[8];
    magmaFloatComplex x13 = h_target_params[9];
    magmaFloatComplex x14 = h_target_params[10];
    magmaFloatComplex x15 = h_target_params[11];
    magmaFloatComplex x16 = h_target_params[12];
    magmaFloatComplex x17 = h_target_params[13];
    magmaFloatComplex x18 = h_target_params[14];
    magmaFloatComplex x19 = h_target_params[15];
    magmaFloatComplex x20 = h_target_params[16];
    magmaFloatComplex x21 = h_target_params[17];

    h_target_coeffs[0] = (x4*x4 + x5*x5 + x6*x6);
    h_target_coeffs[1] = (- 2*x4*x7 - 2*x5*x8 - 2*x6*x9);
    h_target_coeffs[2] = (x7*x7 + x8*x8 + x9*x9);
    h_target_coeffs[3] = (2*x13*x16 + 2*x14*x17 + 2*x15*x18 - x13*x13 - x14*x14 - x15*x15 - x16*x16 - x17*x17 - x18*x18);
    h_target_coeffs[4] = (- 2*x4*x10 - 2*x5*x11 - 2*x6*x12);
    h_target_coeffs[5] = (x10*x10 + x11*x11 + x12*x12);
    h_target_coeffs[6] = (2*x13*x19 + 2*x14*x20 + 2*x15*x21 - x13*x13 - x14*x14 - x15*x15 - x19*x19 - x20*x20 - x21*x21);
    h_target_coeffs[7] = (- 2*x7*x10 - 2*x8*x11 - 2*x9*x12);
    h_target_coeffs[8] = (2*x16*x19 + 2*x17*x20 + 2*x18*x21 - x16*x16 - x17*x17 - x18*x18 - x19*x19 - x20*x20 - x21*x21);
  }
} // end of namespace

#endif


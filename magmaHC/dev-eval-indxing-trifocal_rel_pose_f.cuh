#ifndef dev_eval_indxing_3view_unknownf_cuh_
#define dev_eval_indxing_3view_unknownf_cuh_
// ============================================================================
// Device function for evaluating the parallel indexing for Hx, Ht, and H of
// 3view_unknownf problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// -- cuda included --
#include <cuda_runtime.h>

// -- magma included --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

namespace magmaHCWrapper {

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_cdt_3view_unknownf(
        const int tx, float t, magmaFloatComplex s_vector_cdt[coefsCount],
        magmaFloatComplex r_startCoefs[coefsCount], magmaFloatComplex r_targetCoefs[coefsCount])
    {
        #pragma unroll
        for (int i = 0; i < 8; i++) {
            s_vector_cdt[ tx + i * N ] = r_targetCoefs[tx + i * N] * t - r_startCoefs[tx + i * N] * (t-1);
        }
        if (tx < 9) {
          s_vector_cdt[ tx + 144 ] = r_targetCoefs[tx + 144] * t - r_startCoefs[tx + 144] * (t-1);
        }
    }

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_Jacobian_3view_unknownf(
        const int tx, magmaFloatComplex *s_track, magmaFloatComplex r_cgesvA[N], const magma_int_t* d_const_mat_s,
        const magma_int_t* d_Hx_const_mat_X1, const magma_int_t* d_Hx_const_mat_X2, 
        const magma_int_t* d_Hx_const_mat_X3, const magma_int_t* d_Hx_const_mat_X4, const magma_int_t* d_const_mat_Y,
        magmaFloatComplex s_cdt[coefsCount])
    {
        // -- the maximal terms of Hx in 3view_unknownf problem is 12 --
        // -- 18(vars)*18(vars)*12(terms) = 3688
        #pragma unroll
        for(int i = 0; i < N; i++) {
          r_cgesvA[i] = d_const_mat_s[tx + i*18] * s_track[ d_Hx_const_mat_X1[tx + i*18] ] * s_track[ d_Hx_const_mat_X2[tx + i*18] ] * s_track[ d_Hx_const_mat_X3[tx + i*18] ] * s_track[ d_Hx_const_mat_X4[tx + i*18] ] * s_cdt[ d_const_mat_Y[tx + i*18] ]
                      + d_const_mat_s[tx + i*18 + 324] * s_track[ d_Hx_const_mat_X1[tx + i*18 + 324] ] * s_track[ d_Hx_const_mat_X2[tx + i*18 + 324] ] * s_track[ d_Hx_const_mat_X3[tx + i*18 + 324] ] * s_track[ d_Hx_const_mat_X4[tx + i*18 + 324] ] * s_cdt[ d_const_mat_Y[tx + i*18 + 324] ]
                      + d_const_mat_s[tx + i*18 + 648] * s_track[ d_Hx_const_mat_X1[tx + i*18 + 648] ] * s_track[ d_Hx_const_mat_X2[tx + i*18 + 648] ] * s_track[ d_Hx_const_mat_X3[tx + i*18 + 648] ] * s_track[ d_Hx_const_mat_X4[tx + i*18 + 648] ] * s_cdt[ d_const_mat_Y[tx + i*18 + 648] ]
                      + d_const_mat_s[tx + i*18 + 972] * s_track[ d_Hx_const_mat_X1[tx + i*18 + 972] ] * s_track[ d_Hx_const_mat_X2[tx + i*18 + 972] ] * s_track[ d_Hx_const_mat_X3[tx + i*18 + 972] ] * s_track[ d_Hx_const_mat_X4[tx + i*18 + 972] ] * s_cdt[ d_const_mat_Y[tx + i*18 + 972] ]
                      + d_const_mat_s[tx + i*18 + 1296] * s_track[ d_Hx_const_mat_X1[tx + i*18 + 1296] ] * s_track[ d_Hx_const_mat_X2[tx + i*18 + 1296] ] * s_track[ d_Hx_const_mat_X3[tx + i*18 + 1296] ] * s_track[ d_Hx_const_mat_X4[tx + i*18 + 1296] ] * s_cdt[ d_const_mat_Y[tx + i*18 + 1296] ]
                      + d_const_mat_s[tx + i*18 + 1620] * s_track[ d_Hx_const_mat_X1[tx + i*18 + 1620] ] * s_track[ d_Hx_const_mat_X2[tx + i*18 + 1620] ] * s_track[ d_Hx_const_mat_X3[tx + i*18 + 1620] ] * s_track[ d_Hx_const_mat_X4[tx + i*18 + 1620] ] * s_cdt[ d_const_mat_Y[tx + i*18 + 1620] ]
                      + d_const_mat_s[tx + i*18 + 1944] * s_track[ d_Hx_const_mat_X1[tx + i*18 + 1944] ] * s_track[ d_Hx_const_mat_X2[tx + i*18 + 1944] ] * s_track[ d_Hx_const_mat_X3[tx + i*18 + 1944] ] * s_track[ d_Hx_const_mat_X4[tx + i*18 + 1944] ] * s_cdt[ d_const_mat_Y[tx + i*18 + 1944] ]
                      + d_const_mat_s[tx + i*18 + 2268] * s_track[ d_Hx_const_mat_X1[tx + i*18 + 2268] ] * s_track[ d_Hx_const_mat_X2[tx + i*18 + 2268] ] * s_track[ d_Hx_const_mat_X3[tx + i*18 + 2268] ] * s_track[ d_Hx_const_mat_X4[tx + i*18 + 2268] ] * s_cdt[ d_const_mat_Y[tx + i*18 + 2268] ]
                      + d_const_mat_s[tx + i*18 + 2592] * s_track[ d_Hx_const_mat_X1[tx + i*18 + 2592] ] * s_track[ d_Hx_const_mat_X2[tx + i*18 + 2592] ] * s_track[ d_Hx_const_mat_X3[tx + i*18 + 2592] ] * s_track[ d_Hx_const_mat_X4[tx + i*18 + 2592] ] * s_cdt[ d_const_mat_Y[tx + i*18 + 2592] ]
                      + d_const_mat_s[tx + i*18 + 2916] * s_track[ d_Hx_const_mat_X1[tx + i*18 + 2916] ] * s_track[ d_Hx_const_mat_X2[tx + i*18 + 2916] ] * s_track[ d_Hx_const_mat_X3[tx + i*18 + 2916] ] * s_track[ d_Hx_const_mat_X4[tx + i*18 + 2916] ] * s_cdt[ d_const_mat_Y[tx + i*18 + 2916] ]
                      + d_const_mat_s[tx + i*18 + 3240] * s_track[ d_Hx_const_mat_X1[tx + i*18 + 3240] ] * s_track[ d_Hx_const_mat_X2[tx + i*18 + 3240] ] * s_track[ d_Hx_const_mat_X3[tx + i*18 + 3240] ] * s_track[ d_Hx_const_mat_X4[tx + i*18 + 3240] ] * s_cdt[ d_const_mat_Y[tx + i*18 + 3240] ]
                      + d_const_mat_s[tx + i*18 + 3564] * s_track[ d_Hx_const_mat_X1[tx + i*18 + 3564] ] * s_track[ d_Hx_const_mat_X2[tx + i*18 + 3564] ] * s_track[ d_Hx_const_mat_X3[tx + i*18 + 3564] ] * s_track[ d_Hx_const_mat_X4[tx + i*18 + 3564] ] * s_cdt[ d_const_mat_Y[tx + i*18 + 3564] ];
        }

        /*#pragma unroll
        for(int i = 0; i < N; i++) {
          r_cgesvA[i] = d_const_mat_s[tx + i*18] * s_track[ d_Hx_const_mat_X1[tx + i*18] ] * s_track[ d_Hx_const_mat_X2[tx + i*18] ] * s_track[ d_Hx_const_mat_X3[tx + i*18] ] * s_track[ d_Hx_const_mat_X4[tx + i*18] ] * s_cdt[ d_const_mat_Y[tx + i*18] ];
        }*/
    }

    __device__ __inline__ void
    eval_Ht_3view_unknownf(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* d_Ht_const_mat_s, const magma_int_t* d_Ht_const_mat_X,
      const magma_int_t* d_Ht_const_mat_Y, const magmaFloatComplex* d_const_cd)
    {
      // -- the maximal terms of Ht in 3view_unknownf problem is 14 --
      // -- N*maximal terms of Ht = 18*14 = 252
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_Ht_const_mat_X[tx] ] * s_track[ d_Ht_const_mat_X[tx + 252] ] * s_track[ d_Ht_const_mat_X[tx + 504] ] * s_track[ d_Ht_const_mat_X[tx + 756] ] * s_track[ d_Ht_const_mat_X[tx + 1008] ] * d_const_cd[ d_Ht_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + 18] * s_track[ d_Ht_const_mat_X[tx + 18] ] * s_track[ d_Ht_const_mat_X[tx+ 18 + 252] ] * s_track[ d_Ht_const_mat_X[tx+ 18 + 504] ] * s_track[ d_Ht_const_mat_X[tx+ 18 + 756] ] * s_track[ d_Ht_const_mat_X[tx+ 18 + 1008] ] * d_const_cd[ d_Ht_const_mat_Y[tx+ 18] ]
               + d_Ht_const_mat_s[tx + 36] * s_track[ d_Ht_const_mat_X[tx + 36] ] * s_track[ d_Ht_const_mat_X[tx + 36 + 252] ] * s_track[ d_Ht_const_mat_X[tx + 36 + 504] ] * s_track[ d_Ht_const_mat_X[tx + 36 + 756] ] * s_track[ d_Ht_const_mat_X[tx + 36 + 1008] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 36] ]
               + d_Ht_const_mat_s[tx + 54] * s_track[ d_Ht_const_mat_X[tx + 54] ] * s_track[ d_Ht_const_mat_X[tx + 54 + 252] ] * s_track[ d_Ht_const_mat_X[tx + 54 + 504] ] * s_track[ d_Ht_const_mat_X[tx + 54 + 756] ] * s_track[ d_Ht_const_mat_X[tx + 54 + 1008] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 54] ]
               + d_Ht_const_mat_s[tx + 72] * s_track[ d_Ht_const_mat_X[tx + 72] ] * s_track[ d_Ht_const_mat_X[tx + 72 + 252] ] * s_track[ d_Ht_const_mat_X[tx + 72 + 504] ] * s_track[ d_Ht_const_mat_X[tx + 72 + 756] ] * s_track[ d_Ht_const_mat_X[tx + 72 + 1008] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 72] ]
               + d_Ht_const_mat_s[tx + 90] * s_track[ d_Ht_const_mat_X[tx + 90] ] * s_track[ d_Ht_const_mat_X[tx + 90 + 252] ] * s_track[ d_Ht_const_mat_X[tx + 90 + 504] ] * s_track[ d_Ht_const_mat_X[tx + 90 + 756] ] * s_track[ d_Ht_const_mat_X[tx + 90 + 1008] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 90] ]
               + d_Ht_const_mat_s[tx + 108] * s_track[ d_Ht_const_mat_X[tx + 108] ] * s_track[ d_Ht_const_mat_X[tx + 108 + 252] ] * s_track[ d_Ht_const_mat_X[tx + 108 + 504] ] * s_track[ d_Ht_const_mat_X[tx + 108 + 756] ] * s_track[ d_Ht_const_mat_X[tx + 108 + 1008] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 108] ]
               + d_Ht_const_mat_s[tx + 126] * s_track[ d_Ht_const_mat_X[tx + 126] ] * s_track[ d_Ht_const_mat_X[tx + 126 + 252] ] * s_track[ d_Ht_const_mat_X[tx + 126 + 504] ] * s_track[ d_Ht_const_mat_X[tx + 126 + 756] ] * s_track[ d_Ht_const_mat_X[tx + 126 + 1008] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 126] ]
               + d_Ht_const_mat_s[tx + 144] * s_track[ d_Ht_const_mat_X[tx + 144] ] * s_track[ d_Ht_const_mat_X[tx + 144 + 252] ] * s_track[ d_Ht_const_mat_X[tx + 144 + 504] ] * s_track[ d_Ht_const_mat_X[tx + 144 + 756] ] * s_track[ d_Ht_const_mat_X[tx + 144 + 1008] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 144] ]
               + d_Ht_const_mat_s[tx + 162] * s_track[ d_Ht_const_mat_X[tx + 162] ] * s_track[ d_Ht_const_mat_X[tx + 162 + 252] ] * s_track[ d_Ht_const_mat_X[tx + 162 + 504] ] * s_track[ d_Ht_const_mat_X[tx + 162 + 756] ] * s_track[ d_Ht_const_mat_X[tx + 162 + 1008] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 162] ]
               + d_Ht_const_mat_s[tx + 180] * s_track[ d_Ht_const_mat_X[tx + 180] ] * s_track[ d_Ht_const_mat_X[tx + 180 + 252] ] * s_track[ d_Ht_const_mat_X[tx + 180 + 504] ] * s_track[ d_Ht_const_mat_X[tx + 180 + 756] ] * s_track[ d_Ht_const_mat_X[tx + 180 + 1008] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 180] ]
               + d_Ht_const_mat_s[tx + 198] * s_track[ d_Ht_const_mat_X[tx + 198] ] * s_track[ d_Ht_const_mat_X[tx + 198 + 252] ] * s_track[ d_Ht_const_mat_X[tx + 198 + 504] ] * s_track[ d_Ht_const_mat_X[tx + 198 + 756] ] * s_track[ d_Ht_const_mat_X[tx + 198 + 1008] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 198] ]
               + d_Ht_const_mat_s[tx + 216] * s_track[ d_Ht_const_mat_X[tx + 216] ] * s_track[ d_Ht_const_mat_X[tx + 216 + 252] ] * s_track[ d_Ht_const_mat_X[tx + 216 + 504] ] * s_track[ d_Ht_const_mat_X[tx + 216 + 756] ] * s_track[ d_Ht_const_mat_X[tx + 216 + 1008] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 216] ]
               + d_Ht_const_mat_s[tx + 234] * s_track[ d_Ht_const_mat_X[tx + 234] ] * s_track[ d_Ht_const_mat_X[tx + 234 + 252] ] * s_track[ d_Ht_const_mat_X[tx + 234 + 504] ] * s_track[ d_Ht_const_mat_X[tx + 234 + 756] ] * s_track[ d_Ht_const_mat_X[tx + 234 + 1008] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 234] ];
    }

    template<int coefsCount>
    __device__ __inline__ void
    eval_H_3view_unknownf(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* d_Ht_const_mat_s, const magma_int_t* d_H_const_mat_X,
      const magma_int_t* d_H_const_mat_Y, magmaFloatComplex s_cdt[coefsCount])
    {
      // -- same as Ht, except replacing cd by cdt --
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_H_const_mat_X[tx] ] * s_track[ d_H_const_mat_X[tx + 252] ] * s_track[ d_H_const_mat_X[tx + 504] ] * s_track[ d_H_const_mat_X[tx + 756] ] * s_track[ d_H_const_mat_X[tx + 1008] ] * s_cdt[ d_H_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + 18] * s_track[ d_H_const_mat_X[tx + 18] ] * s_track[ d_H_const_mat_X[tx+ 18 + 252] ] * s_track[ d_H_const_mat_X[tx+ 18 + 504] ] * s_track[ d_H_const_mat_X[tx+ 18 + 756] ] * s_track[ d_H_const_mat_X[tx+ 18 + 1008] ] * s_cdt[ d_H_const_mat_Y[tx+ 18] ]
               + d_Ht_const_mat_s[tx + 36] * s_track[ d_H_const_mat_X[tx + 36] ] * s_track[ d_H_const_mat_X[tx + 36 + 252] ] * s_track[ d_H_const_mat_X[tx + 36 + 504] ] * s_track[ d_H_const_mat_X[tx + 36 + 756] ] * s_track[ d_H_const_mat_X[tx + 36 + 1008] ] * s_cdt[ d_H_const_mat_Y[tx + 36] ]
               + d_Ht_const_mat_s[tx + 54] * s_track[ d_H_const_mat_X[tx + 54] ] * s_track[ d_H_const_mat_X[tx + 54 + 252] ] * s_track[ d_H_const_mat_X[tx + 54 + 504] ] * s_track[ d_H_const_mat_X[tx + 54 + 756] ] * s_track[ d_H_const_mat_X[tx + 54 + 1008] ] * s_cdt[ d_H_const_mat_Y[tx + 54] ]
               + d_Ht_const_mat_s[tx + 72] * s_track[ d_H_const_mat_X[tx + 72] ] * s_track[ d_H_const_mat_X[tx + 72 + 252] ] * s_track[ d_H_const_mat_X[tx + 72 + 504] ] * s_track[ d_H_const_mat_X[tx + 72 + 756] ] * s_track[ d_H_const_mat_X[tx + 72 + 1008] ] * s_cdt[ d_H_const_mat_Y[tx + 72] ]
               + d_Ht_const_mat_s[tx + 90] * s_track[ d_H_const_mat_X[tx + 90] ] * s_track[ d_H_const_mat_X[tx + 90 + 252] ] * s_track[ d_H_const_mat_X[tx + 90 + 504] ] * s_track[ d_H_const_mat_X[tx + 90 + 756] ] * s_track[ d_H_const_mat_X[tx + 90 + 1008] ] * s_cdt[ d_H_const_mat_Y[tx + 90] ]
               + d_Ht_const_mat_s[tx + 108] * s_track[ d_H_const_mat_X[tx + 108] ] * s_track[ d_H_const_mat_X[tx + 108 + 252] ] * s_track[ d_H_const_mat_X[tx + 108 + 504] ] * s_track[ d_H_const_mat_X[tx + 108 + 756] ] * s_track[ d_H_const_mat_X[tx + 108 + 1008] ] * s_cdt[ d_H_const_mat_Y[tx + 108] ]
               + d_Ht_const_mat_s[tx + 126] * s_track[ d_H_const_mat_X[tx + 126] ] * s_track[ d_H_const_mat_X[tx + 126 + 252] ] * s_track[ d_H_const_mat_X[tx + 126 + 504] ] * s_track[ d_H_const_mat_X[tx + 126 + 756] ] * s_track[ d_H_const_mat_X[tx + 126 + 1008] ] * s_cdt[ d_H_const_mat_Y[tx + 126] ]
               + d_Ht_const_mat_s[tx + 144] * s_track[ d_H_const_mat_X[tx + 144] ] * s_track[ d_H_const_mat_X[tx + 144 + 252] ] * s_track[ d_H_const_mat_X[tx + 144 + 504] ] * s_track[ d_H_const_mat_X[tx + 144 + 756] ] * s_track[ d_H_const_mat_X[tx + 144 + 1008] ] * s_cdt[ d_H_const_mat_Y[tx + 144] ]
               + d_Ht_const_mat_s[tx + 162] * s_track[ d_H_const_mat_X[tx + 162] ] * s_track[ d_H_const_mat_X[tx + 162 + 252] ] * s_track[ d_H_const_mat_X[tx + 162 + 504] ] * s_track[ d_H_const_mat_X[tx + 162 + 756] ] * s_track[ d_H_const_mat_X[tx + 162 + 1008] ] * s_cdt[ d_H_const_mat_Y[tx + 162] ]
               + d_Ht_const_mat_s[tx + 180] * s_track[ d_H_const_mat_X[tx + 180] ] * s_track[ d_H_const_mat_X[tx + 180 + 252] ] * s_track[ d_H_const_mat_X[tx + 180 + 504] ] * s_track[ d_H_const_mat_X[tx + 180 + 756] ] * s_track[ d_H_const_mat_X[tx + 180 + 1008] ] * s_cdt[ d_H_const_mat_Y[tx + 180] ]
               + d_Ht_const_mat_s[tx + 198] * s_track[ d_H_const_mat_X[tx + 198] ] * s_track[ d_H_const_mat_X[tx + 198 + 252] ] * s_track[ d_H_const_mat_X[tx + 198 + 504] ] * s_track[ d_H_const_mat_X[tx + 198 + 756] ] * s_track[ d_H_const_mat_X[tx + 198 + 1008] ] * s_cdt[ d_H_const_mat_Y[tx + 198] ]
               + d_Ht_const_mat_s[tx + 216] * s_track[ d_H_const_mat_X[tx + 216] ] * s_track[ d_H_const_mat_X[tx + 216 + 252] ] * s_track[ d_H_const_mat_X[tx + 216 + 504] ] * s_track[ d_H_const_mat_X[tx + 216 + 756] ] * s_track[ d_H_const_mat_X[tx + 216 + 1008] ] * s_cdt[ d_H_const_mat_Y[tx + 216] ]
               + d_Ht_const_mat_s[tx + 234] * s_track[ d_H_const_mat_X[tx + 234] ] * s_track[ d_H_const_mat_X[tx + 234 + 252] ] * s_track[ d_H_const_mat_X[tx + 234 + 504] ] * s_track[ d_H_const_mat_X[tx + 234 + 756] ] * s_track[ d_H_const_mat_X[tx + 234 + 1008] ] * s_cdt[ d_H_const_mat_Y[tx + 234] ];
    }
}

#endif

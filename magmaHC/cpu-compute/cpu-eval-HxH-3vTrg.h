#ifndef cpu_eval_HxH_3vTrg_h
#define cpu_eval_HxH_3vTrg_h
// ============================================================================
// partial derivative evaluations of the alea6 problem for cpu HC computation
//
// Modifications
//    Chien  21-06-30:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min

namespace magmaHCWrapper {

  extern "C"
  void cpu_eval_HxH_3vTrg(
      magma_int_t s, float t, int N, magmaFloatComplex* s_track,
      const magmaFloatComplex &C0, const magmaFloatComplex &C1, const magmaFloatComplex &C2,
      magmaFloatComplex* s_startCoefs, magmaFloatComplex* s_targetCoefs,
      magmaFloatComplex* r_cgesvA, magmaFloatComplex* r_cgesvB )
  {
    magmaFloatComplex G1 = C0 - t;
    magmaFloatComplex G2 = G1 * s_startCoefs[33];
    magmaFloatComplex G3 = t * s_targetCoefs[33];
    magmaFloatComplex G4 = G2 + G3;
    magmaFloatComplex G5 = G1 * s_startCoefs[0];
    magmaFloatComplex G6 = t * s_targetCoefs[0];
    magmaFloatComplex G7 = G5 + G6;
    magmaFloatComplex G8 = G7 * s_track[6];
    magmaFloatComplex G9 = G1 * s_startCoefs[1];
    magmaFloatComplex G10 = t * s_targetCoefs[1];
    magmaFloatComplex G11 = G9 + G10;
    magmaFloatComplex G12 = G11 * s_track[6];
    magmaFloatComplex G13 = G1 * s_startCoefs[18];
    magmaFloatComplex G14 = t * s_targetCoefs[18];
    magmaFloatComplex G15 = G13 + G14;
    magmaFloatComplex G16 = G15 * s_track[8];
    magmaFloatComplex G17 = G1 * s_startCoefs[19];
    magmaFloatComplex G18 = t * s_targetCoefs[19];
    magmaFloatComplex G19 = G17 + G18;
    magmaFloatComplex G20 = G19 * s_track[8];
    magmaFloatComplex G21 = G7 * s_track[2];
    magmaFloatComplex G22 = G11 * s_track[3];
    magmaFloatComplex G23 = G21 + G22;
    magmaFloatComplex G24 = G1 * s_startCoefs[2];
    magmaFloatComplex G25 = t * s_targetCoefs[2];
    magmaFloatComplex G26 = G24 + G25;
    magmaFloatComplex G27 = G23 + G26;
    magmaFloatComplex G28 = G15 * s_track[4];
    magmaFloatComplex G29 = G19 * s_track[5];
    magmaFloatComplex G30 = G28 + G29;
    magmaFloatComplex G31 = G1 * s_startCoefs[20];
    magmaFloatComplex G32 = t * s_targetCoefs[20];
    magmaFloatComplex G33 = G31 + G32;
    magmaFloatComplex G34 = G30 + G33;
    magmaFloatComplex G35 = G1 * s_startCoefs[3];
    magmaFloatComplex G36 = t * s_targetCoefs[3];
    magmaFloatComplex G37 = G35 + G36;
    magmaFloatComplex G38 = G37 * s_track[6];
    magmaFloatComplex G39 = G1 * s_startCoefs[4];
    magmaFloatComplex G40 = t * s_targetCoefs[4];
    magmaFloatComplex G41 = G39 + G40;
    magmaFloatComplex G42 = G41 * s_track[6];
    magmaFloatComplex G43 = G1 * s_startCoefs[21];
    magmaFloatComplex G44 = t * s_targetCoefs[21];
    magmaFloatComplex G45 = G43 + G44;
    magmaFloatComplex G46 = G45 * s_track[8];
    magmaFloatComplex G47 = G1 * s_startCoefs[22];
    magmaFloatComplex G48 = t * s_targetCoefs[22];
    magmaFloatComplex G49 = G47 + G48;
    magmaFloatComplex G50 = G49 * s_track[8];
    magmaFloatComplex G51 = G37 * s_track[2];
    magmaFloatComplex G52 = G41 * s_track[3];
    magmaFloatComplex G53 = G51 + G52;
    magmaFloatComplex G54 = G1 * s_startCoefs[5];
    magmaFloatComplex G55 = t * s_targetCoefs[5];
    magmaFloatComplex G56 = G54 + G55;
    magmaFloatComplex G57 = G53 + G56;
    magmaFloatComplex G58 = G45 * s_track[4];
    magmaFloatComplex G59 = G49 * s_track[5];
    magmaFloatComplex G60 = G58 + G59;
    magmaFloatComplex G61 = G1 * s_startCoefs[23];
    magmaFloatComplex G62 = t * s_targetCoefs[23];
    magmaFloatComplex G63 = G61 + G62;
    magmaFloatComplex G64 = G60 + G63;
    magmaFloatComplex G65 = G1 * s_startCoefs[9];
    magmaFloatComplex G66 = t * s_targetCoefs[9];
    magmaFloatComplex G67 = G65 + G66;
    magmaFloatComplex G68 = G67 * s_track[7];
    magmaFloatComplex G69 = G1 * s_startCoefs[10];
    magmaFloatComplex G70 = t * s_targetCoefs[10];
    magmaFloatComplex G71 = G69 + G70;
    magmaFloatComplex G72 = G71 * s_track[7];
    magmaFloatComplex G73 = G7 * s_track[0];
    magmaFloatComplex G74 = G37 * s_track[1];
    magmaFloatComplex G75 = G73 + G74;
    magmaFloatComplex G76 = G1 * s_startCoefs[6];
    magmaFloatComplex G77 = t * s_targetCoefs[6];
    magmaFloatComplex G78 = G76 + G77;
    magmaFloatComplex G79 = G75 + G78;
    magmaFloatComplex G80 = G67 * s_track[4];
    magmaFloatComplex G81 = G71 * s_track[5];
    magmaFloatComplex G82 = G80 + G81;
    magmaFloatComplex G83 = G1 * s_startCoefs[11];
    magmaFloatComplex G84 = t * s_targetCoefs[11];
    magmaFloatComplex G85 = G83 + G84;
    magmaFloatComplex G86 = G82 + G85;
    magmaFloatComplex G87 = G1 * s_startCoefs[12];
    magmaFloatComplex G88 = t * s_targetCoefs[12];
    magmaFloatComplex G89 = G87 + G88;
    magmaFloatComplex G90 = G89 * s_track[7];
    magmaFloatComplex G91 = G1 * s_startCoefs[13];
    magmaFloatComplex G92 = t * s_targetCoefs[13];
    magmaFloatComplex G93 = G91 + G92;
    magmaFloatComplex G94 = G93 * s_track[7];
    magmaFloatComplex G95 = G11 * s_track[0];
    magmaFloatComplex G96 = G41 * s_track[1];
    magmaFloatComplex G97 = G95 + G96;
    magmaFloatComplex G98 = G1 * s_startCoefs[7];
    magmaFloatComplex G99 = t * s_targetCoefs[7];
    magmaFloatComplex G100 = G98 + G99;
    magmaFloatComplex G101 = G97 + G100;
    magmaFloatComplex G102 = G89 * s_track[4];
    magmaFloatComplex G103 = G93 * s_track[5];
    magmaFloatComplex G104 = G102 + G103;
    magmaFloatComplex G105 = G1 * s_startCoefs[14];
    magmaFloatComplex G106 = t * s_targetCoefs[14];
    magmaFloatComplex G107 = G105 + G106;
    magmaFloatComplex G108 = G104 + G107;
    magmaFloatComplex G109 = G67 * s_track[2];
    magmaFloatComplex G110 = G89 * s_track[3];
    magmaFloatComplex G111 = G109 + G110;
    magmaFloatComplex G112 = G1 * s_startCoefs[15];
    magmaFloatComplex G113 = t * s_targetCoefs[15];
    magmaFloatComplex G114 = G112 + G113;
    magmaFloatComplex G115 = G111 + G114;
    magmaFloatComplex G116 = G15 * s_track[0];
    magmaFloatComplex G117 = G45 * s_track[1];
    magmaFloatComplex G118 = G116 + G117;
    magmaFloatComplex G119 = G1 * s_startCoefs[24];
    magmaFloatComplex G120 = t * s_targetCoefs[24];
    magmaFloatComplex G121 = G119 + G120;
    magmaFloatComplex G122 = G118 + G121;
    magmaFloatComplex G123 = G71 * s_track[2];
    magmaFloatComplex G124 = G93 * s_track[3];
    magmaFloatComplex G125 = G123 + G124;
    magmaFloatComplex G126 = G1 * s_startCoefs[16];
    magmaFloatComplex G127 = t * s_targetCoefs[16];
    magmaFloatComplex G128 = G126 + G127;
    magmaFloatComplex G129 = G125 + G128;
    magmaFloatComplex G130 = G19 * s_track[0];
    magmaFloatComplex G131 = G49 * s_track[1];
    magmaFloatComplex G132 = G130 + G131;
    magmaFloatComplex G133 = G1 * s_startCoefs[25];
    magmaFloatComplex G134 = t * s_targetCoefs[25];
    magmaFloatComplex G135 = G133 + G134;
    magmaFloatComplex G136 = G132 + G135;
    magmaFloatComplex G137 = G4 * s_track[0];
    magmaFloatComplex G138 = s_track[2] * s_track[6];
    magmaFloatComplex G139 = G138 * G7;
    magmaFloatComplex G140 = G137 + G139;
    magmaFloatComplex G141 = s_track[3] * s_track[6];
    magmaFloatComplex G142 = G141 * G11;
    magmaFloatComplex G143 = G140 + G142;
    magmaFloatComplex G144 = s_track[4] * s_track[8];
    magmaFloatComplex G145 = G144 * G15;
    magmaFloatComplex G146 = G143 + G145;
    magmaFloatComplex G147 = s_track[5] * s_track[8];
    magmaFloatComplex G148 = G147 * G19;
    magmaFloatComplex G149 = G146 + G148;
    magmaFloatComplex G150 = s_track[6] * G26;
    magmaFloatComplex G151 = G149 + G150;
    magmaFloatComplex G152 = s_track[8] * G33;
    magmaFloatComplex G153 = G151 + G152;
    magmaFloatComplex G154 = G1 * s_startCoefs[27];
    magmaFloatComplex G155 = t * s_targetCoefs[27];
    magmaFloatComplex G156 = G154 + G155;
    magmaFloatComplex G157 = G153 + G156;
    magmaFloatComplex G158 = G4 * s_track[1];
    magmaFloatComplex G159 = G138 * G37;
    magmaFloatComplex G160 = G158 + G159;
    magmaFloatComplex G161 = G141 * G41;
    magmaFloatComplex G162 = G160 + G161;
    magmaFloatComplex G163 = G144 * G45;
    magmaFloatComplex G164 = G162 + G163;
    magmaFloatComplex G165 = G147 * G49;
    magmaFloatComplex G166 = G164 + G165;
    magmaFloatComplex G167 = s_track[6] * G56;
    magmaFloatComplex G168 = G166 + G167;
    magmaFloatComplex G169 = s_track[8] * G63;
    magmaFloatComplex G170 = G168 + G169;
    magmaFloatComplex G171 = G1 * s_startCoefs[28];
    magmaFloatComplex G172 = t * s_targetCoefs[28];
    magmaFloatComplex G173 = G171 + G172;
    magmaFloatComplex G174 = G170 + G173;
    magmaFloatComplex G175 = s_track[0] * s_track[6];
    magmaFloatComplex G176 = G175 * G7;
    magmaFloatComplex G177 = s_track[1] * s_track[6];
    magmaFloatComplex G178 = G177 * G37;
    magmaFloatComplex G179 = G176 + G178;
    magmaFloatComplex G180 = G4 * s_track[2];
    magmaFloatComplex G181 = G179 + G180;
    magmaFloatComplex G182 = s_track[4] * s_track[7];
    magmaFloatComplex G183 = G182 * G67;
    magmaFloatComplex G184 = G181 + G183;
    magmaFloatComplex G185 = s_track[5] * s_track[7];
    magmaFloatComplex G186 = G185 * G71;
    magmaFloatComplex G187 = G184 + G186;
    magmaFloatComplex G188 = s_track[6] * G78;
    magmaFloatComplex G189 = G187 + G188;
    magmaFloatComplex G190 = s_track[7] * G85;
    magmaFloatComplex G191 = G189 + G190;
    magmaFloatComplex G192 = G1 * s_startCoefs[29];
    magmaFloatComplex G193 = t * s_targetCoefs[29];
    magmaFloatComplex G194 = G192 + G193;
    magmaFloatComplex G195 = G191 + G194;
    magmaFloatComplex G196 = G175 * G11;
    magmaFloatComplex G197 = G177 * G41;
    magmaFloatComplex G198 = G196 + G197;
    magmaFloatComplex G199 = G4 * s_track[3];
    magmaFloatComplex G200 = G198 + G199;
    magmaFloatComplex G201 = G182 * G89;
    magmaFloatComplex G202 = G200 + G201;
    magmaFloatComplex G203 = G185 * G93;
    magmaFloatComplex G204 = G202 + G203;
    magmaFloatComplex G205 = s_track[6] * G100;
    magmaFloatComplex G206 = G204 + G205;
    magmaFloatComplex G207 = s_track[7] * G107;
    magmaFloatComplex G208 = G206 + G207;
    magmaFloatComplex G209 = G1 * s_startCoefs[30];
    magmaFloatComplex G210 = t * s_targetCoefs[30];
    magmaFloatComplex G211 = G209 + G210;
    magmaFloatComplex G212 = G208 + G211;
    magmaFloatComplex G213 = s_track[0] * s_track[8];
    magmaFloatComplex G214 = G213 * G15;
    magmaFloatComplex G215 = s_track[1] * s_track[8];
    magmaFloatComplex G216 = G215 * G45;
    magmaFloatComplex G217 = G214 + G216;
    magmaFloatComplex G218 = s_track[2] * s_track[7];
    magmaFloatComplex G219 = G218 * G67;
    magmaFloatComplex G220 = G217 + G219;
    magmaFloatComplex G221 = s_track[3] * s_track[7];
    magmaFloatComplex G222 = G221 * G89;
    magmaFloatComplex G223 = G220 + G222;
    magmaFloatComplex G224 = G4 * s_track[4];
    magmaFloatComplex G225 = G223 + G224;
    magmaFloatComplex G226 = s_track[7] * G114;
    magmaFloatComplex G227 = G225 + G226;
    magmaFloatComplex G228 = s_track[8] * G121;
    magmaFloatComplex G229 = G227 + G228;
    magmaFloatComplex G230 = G1 * s_startCoefs[31];
    magmaFloatComplex G231 = t * s_targetCoefs[31];
    magmaFloatComplex G232 = G230 + G231;
    magmaFloatComplex G233 = G229 + G232;
    magmaFloatComplex G234 = G213 * G19;
    magmaFloatComplex G235 = G215 * G49;
    magmaFloatComplex G236 = G234 + G235;
    magmaFloatComplex G237 = G218 * G71;
    magmaFloatComplex G238 = G236 + G237;
    magmaFloatComplex G239 = G221 * G93;
    magmaFloatComplex G240 = G238 + G239;
    magmaFloatComplex G241 = G4 * s_track[5];
    magmaFloatComplex G242 = G240 + G241;
    magmaFloatComplex G243 = s_track[7] * G128;
    magmaFloatComplex G244 = G242 + G243;
    magmaFloatComplex G245 = s_track[8] * G135;
    magmaFloatComplex G246 = G244 + G245;
    magmaFloatComplex G247 = G1 * s_startCoefs[32];
    magmaFloatComplex G248 = t * s_targetCoefs[32];
    magmaFloatComplex G249 = G247 + G248;
    magmaFloatComplex G250 = G246 + G249;
    magmaFloatComplex G251 = s_track[0] * s_track[2];
    magmaFloatComplex G252 = G251 * G7;
    magmaFloatComplex G253 = s_track[0] * s_track[3];
    magmaFloatComplex G254 = G253 * G11;
    magmaFloatComplex G255 = G252 + G254;
    magmaFloatComplex G256 = s_track[0] * G26;
    magmaFloatComplex G257 = G255 + G256;
    magmaFloatComplex G258 = s_track[1] * s_track[2];
    magmaFloatComplex G259 = G258 * G37;
    magmaFloatComplex G260 = G257 + G259;
    magmaFloatComplex G261 = s_track[1] * s_track[3];
    magmaFloatComplex G262 = G261 * G41;
    magmaFloatComplex G263 = G260 + G262;
    magmaFloatComplex G264 = s_track[1] * G56;
    magmaFloatComplex G265 = G263 + G264;
    magmaFloatComplex G266 = s_track[2] * G78;
    magmaFloatComplex G267 = G265 + G266;
    magmaFloatComplex G268 = s_track[3] * G100;
    magmaFloatComplex G269 = G267 + G268;
    magmaFloatComplex G270 = G1 * s_startCoefs[8];
    magmaFloatComplex G271 = t * s_targetCoefs[8];
    magmaFloatComplex G272 = G270 + G271;
    magmaFloatComplex G273 = G269 + G272;
    magmaFloatComplex G274 = s_track[2] * s_track[4];
    magmaFloatComplex G275 = G274 * G67;
    magmaFloatComplex G276 = s_track[2] * s_track[5];
    magmaFloatComplex G277 = G276 * G71;
    magmaFloatComplex G278 = G275 + G277;
    magmaFloatComplex G279 = s_track[2] * G85;
    magmaFloatComplex G280 = G278 + G279;
    magmaFloatComplex G281 = s_track[3] * s_track[4];
    magmaFloatComplex G282 = G281 * G89;
    magmaFloatComplex G283 = G280 + G282;
    magmaFloatComplex G284 = s_track[3] * s_track[5];
    magmaFloatComplex G285 = G284 * G93;
    magmaFloatComplex G286 = G283 + G285;
    magmaFloatComplex G287 = s_track[3] * G107;
    magmaFloatComplex G288 = G286 + G287;
    magmaFloatComplex G289 = s_track[4] * G114;
    magmaFloatComplex G290 = G288 + G289;
    magmaFloatComplex G291 = s_track[5] * G128;
    magmaFloatComplex G292 = G290 + G291;
    magmaFloatComplex G293 = G1 * s_startCoefs[17];
    magmaFloatComplex G294 = t * s_targetCoefs[17];
    magmaFloatComplex G295 = G293 + G294;
    magmaFloatComplex G296 = G292 + G295;
    magmaFloatComplex G297 = s_track[0] * s_track[4];
    magmaFloatComplex G298 = G297 * G15;
    magmaFloatComplex G299 = s_track[0] * s_track[5];
    magmaFloatComplex G300 = G299 * G19;
    magmaFloatComplex G301 = G298 + G300;
    magmaFloatComplex G302 = s_track[0] * G33;
    magmaFloatComplex G303 = G301 + G302;
    magmaFloatComplex G304 = s_track[1] * s_track[4];
    magmaFloatComplex G305 = G304 * G45;
    magmaFloatComplex G306 = G303 + G305;
    magmaFloatComplex G307 = s_track[1] * s_track[5];
    magmaFloatComplex G308 = G307 * G49;
    magmaFloatComplex G309 = G306 + G308;
    magmaFloatComplex G310 = s_track[1] * G63;
    magmaFloatComplex G311 = G309 + G310;
    magmaFloatComplex G312 = s_track[4] * G121;
    magmaFloatComplex G313 = G311 + G312;
    magmaFloatComplex G314 = s_track[5] * G135;
    magmaFloatComplex G315 = G313 + G314;
    magmaFloatComplex G316 = G1 * s_startCoefs[26];
    magmaFloatComplex G317 = t * s_targetCoefs[26];
    magmaFloatComplex G318 = G316 + G317;
    magmaFloatComplex G319 = G315 + G318;

    r_cgesvA[0] = G4;
    r_cgesvA[1] = C2;
    r_cgesvA[2] = G8;
    r_cgesvA[3] = G12;
    r_cgesvA[4] = G16;
    r_cgesvA[5] = G20;
    r_cgesvA[6] = G27;
    r_cgesvA[7] = C2;
    r_cgesvA[8] = G34;
    r_cgesvB[0] = G157;

    r_cgesvA[9] = C2;
    r_cgesvA[10] = G4;
    r_cgesvA[11] = G38;
    r_cgesvA[12] = G42;
    r_cgesvA[13] = G46;
    r_cgesvA[14] = G50;
    r_cgesvA[15] = G57;
    r_cgesvA[16] = C2;
    r_cgesvA[17] = G64;
    r_cgesvB[1] = G174;

    r_cgesvA[18] = G8;
    r_cgesvA[19] = G38;
    r_cgesvA[20] = G4;
    r_cgesvA[21] = C2;
    r_cgesvA[22] = G68;
    r_cgesvA[23] = G72;
    r_cgesvA[24] = G79;
    r_cgesvA[25] = G86;
    r_cgesvA[26] = C2;
    r_cgesvB[2] = G195;

    r_cgesvA[27] = G12;
    r_cgesvA[28] = G42;
    r_cgesvA[29] = C2;
    r_cgesvA[30] = G4;
    r_cgesvA[31] = G90;
    r_cgesvA[32] = G94;
    r_cgesvA[33] = G101;
    r_cgesvA[34] = G108;
    r_cgesvA[35] = C2;
    r_cgesvB[3] = G212;

    r_cgesvA[36] = G16;
    r_cgesvA[37] = G46;
    r_cgesvA[38] = G68;
    r_cgesvA[39] = G90;
    r_cgesvA[40] = G4;
    r_cgesvA[41] = C2;
    r_cgesvA[42] = C2;
    r_cgesvA[43] = G115;
    r_cgesvA[44] = G122;
    r_cgesvB[4] = G233;

    r_cgesvA[45] = G20;
    r_cgesvA[46] = G50;
    r_cgesvA[47] = G72;
    r_cgesvA[48] = G94;
    r_cgesvA[49] = C2;
    r_cgesvA[50] = G4;
    r_cgesvA[51] = C2;
    r_cgesvA[52] = G129;
    r_cgesvA[53] = G136;
    r_cgesvB[5] = G250;

    r_cgesvA[54] = G27;
    r_cgesvA[55] = G57;
    r_cgesvA[56] = G79;
    r_cgesvA[57] = G101;
    r_cgesvA[58] = C2;
    r_cgesvA[59] = C2;
    r_cgesvA[60] = C2;
    r_cgesvA[61] = C2;
    r_cgesvA[62] = C2;
    r_cgesvB[6] = G273;

    r_cgesvA[63] = C2;
    r_cgesvA[64] = C2;
    r_cgesvA[65] = G86;
    r_cgesvA[66] = G108;
    r_cgesvA[67] = G115;
    r_cgesvA[68] = G129;
    r_cgesvA[69] = C2;
    r_cgesvA[70] = C2;
    r_cgesvA[71] = C2;
    r_cgesvB[7] = G296;

    r_cgesvA[72] = G34;
    r_cgesvA[73] = G64;
    r_cgesvA[74] = C2;
    r_cgesvA[75] = C2;
    r_cgesvA[76] = G122;
    r_cgesvA[77] = G136;
    r_cgesvA[78] = C2;
    r_cgesvA[79] = C2;
    r_cgesvA[80] = C2;
    r_cgesvB[8] = G319;
  }
}

#endif

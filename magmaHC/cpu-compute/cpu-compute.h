#ifndef cpu_compute_h
#define cpu_compute_h
// ============================================================================
// Header file declaring all kernels
//
// Modifications
//    Chien  21-6-16:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "/gpfs/runtime/opt/openblas/0.3.7/include/cblas.h"

// -- magma --
#include "flops.h"
#include "magma_v2.h"

extern "C" {
namespace magmaHCWrapper {

  // -- cpu computation of homotopy continuation for 3-view triangulation problem --
  real_Double_t cpu_hc_solver_3vTrg(
    magmaFloatComplex *h_Track_cpu, magma_int_t *h_Track_Success,
    magmaFloatComplex *h_Sols_cpu, magmaFloatComplex *h_Track_gpu,
    magmaFloatComplex *h_startCoefs, magmaFloatComplex *h_targetCoefs,
    magmaFloatComplex *h_cgesvA, magmaFloatComplex *h_cgesvB,
    magma_int_t batchCount, magma_int_t coefsCount, magma_int_t N,
    magma_queue_t my_queue, int max_steps, std::ofstream &tracks_success_file
  );

  // -- cpu computation of homotopy continuation for 4-view triangulation problem --
  real_Double_t cpu_hc_solver_4vTrg(
    magmaFloatComplex *h_Track_cpu, magma_int_t *h_Track_Success,
    magmaFloatComplex *h_Sols_cpu, magmaFloatComplex *h_Track_gpu,
    magmaFloatComplex *h_startCoefs, magmaFloatComplex *h_targetCoefs,
    magmaFloatComplex *h_cgesvA, magmaFloatComplex *h_cgesvB,
    magma_int_t batchCount, magma_int_t coefsCount, magma_int_t N,
    magma_queue_t my_queue, int max_steps, std::ofstream &tracks_success_file
  );

  // -- cpu computation of homotopy continuation for Eq17 problem --
  real_Double_t cpu_hc_solver_Eq17(
    magmaFloatComplex *h_Track_cpu, magma_int_t *h_Track_Success,
    magmaFloatComplex *h_Sols_cpu, magmaFloatComplex *h_Track_gpu,
    magmaFloatComplex *h_startCoefs, magmaFloatComplex *h_targetCoefs,
    magmaFloatComplex *h_cgesvA, magmaFloatComplex *h_cgesvB,
    magma_int_t batchCount, magma_int_t coefsCount, magma_int_t N,
    magma_queue_t my_queue, int max_steps, std::ofstream &tracks_success_file
  );

  // -- cpu computation of homotopy continuation for optimal pose 4 point problem --
  real_Double_t cpu_hc_solver_optpose4pt(
    magmaFloatComplex *h_Track_cpu, magma_int_t *h_Track_Success,
    magmaFloatComplex *h_Sols_cpu, magmaFloatComplex *h_Track_gpu,
    magmaFloatComplex *h_startCoefs, magmaFloatComplex *h_targetCoefs,
    magmaFloatComplex *h_cgesvA, magmaFloatComplex *h_cgesvB,
    magma_int_t batchCount, magma_int_t coefsCount, magma_int_t N,
    magma_queue_t my_queue, int max_steps, std::ofstream &tracks_success_file
  );

  // -- cpu computation of homotopy continuation for homo 3 points problem --
  real_Double_t cpu_hc_solver_homo3pt(
    magmaFloatComplex *h_Track_cpu, magma_int_t *h_Track_Success,
    magmaFloatComplex *h_Sols_cpu, magmaFloatComplex *h_Track_gpu,
    magmaFloatComplex *h_startCoefs, magmaFloatComplex *h_targetCoefs,
    magmaFloatComplex *h_cgesvA, magmaFloatComplex *h_cgesvB,
    magma_int_t batchCount, magma_int_t coefsCount, magma_int_t N,
    magma_queue_t my_queue, int max_steps, std::ofstream &tracks_success_file
  );

  // -- cpu computation of homotopy continuation for dual TDOA problem --
  real_Double_t cpu_hc_solver_dualTDOA(
    magmaFloatComplex *h_Track_cpu, magma_int_t *h_Track_Success,
    magmaFloatComplex *h_Sols_cpu, magmaFloatComplex *h_Track_gpu,
    magmaFloatComplex *h_startCoefs, magmaFloatComplex *h_targetCoefs,
    magmaFloatComplex *h_cgesvA, magmaFloatComplex *h_cgesvB,
    magma_int_t batchCount, magma_int_t coefsCount, magma_int_t N,
    magma_queue_t my_queue, int max_steps, std::ofstream &tracks_success_file
  );

  // -- cpu computation of homotopy continuation for optimal PnPQ problem --
  real_Double_t cpu_hc_solver_optimalPnPQ(
    magmaFloatComplex *h_Track_cpu, magma_int_t *h_Track_Success,
    magmaFloatComplex *h_Sols_cpu, magmaFloatComplex *h_Track_gpu,
    magmaFloatComplex *h_startCoefs, magmaFloatComplex *h_targetCoefs,
    magmaFloatComplex *h_cgesvA, magmaFloatComplex *h_cgesvB,
    magma_int_t batchCount, magma_int_t coefsCount, magma_int_t N,
    magma_queue_t my_queue, int max_steps, std::ofstream &tracks_success_file
  );

  // -- cpu computation of homotopy continuation for 9 points 2 radial problem --
  real_Double_t cpu_hc_solver_9pt2radial(
    magmaFloatComplex *h_Track_cpu, magma_int_t *h_Track_Success,
    magmaFloatComplex *h_Sols_cpu, magmaFloatComplex *h_Track_gpu,
    magmaFloatComplex *h_startCoefs, magmaFloatComplex *h_targetCoefs,
    magmaFloatComplex *h_cgesvA, magmaFloatComplex *h_cgesvB,
    magma_int_t batchCount, magma_int_t coefsCount, magma_int_t N,
    magma_queue_t my_queue, int max_steps, std::ofstream &tracks_success_file
  );

  // -- cpu computation of homotopy continuation for pose_quiver problem --
  real_Double_t cpu_hc_solver_pose_quiver(
    magmaFloatComplex *h_Track_cpu, magma_int_t *h_Track_Success,
    magmaFloatComplex *h_Sols_cpu, magmaFloatComplex *h_Track_gpu,
    magmaFloatComplex *h_startCoefs, magmaFloatComplex *h_targetCoefs,
    magmaFloatComplex *h_cgesvA, magmaFloatComplex *h_cgesvB,
    magma_int_t batchCount, magma_int_t coefsCount, magma_int_t N,
    magma_queue_t my_queue, int max_steps, std::ofstream &tracks_success_file
  );

  // -- cpu computation of homotopy continuation for 5 points relative pose estimation problem --
  real_Double_t cpu_hc_solver_5ptRelativePose(
    magmaFloatComplex *h_Track_cpu, magma_int_t *h_Track_Success,
    magmaFloatComplex *h_Sols_cpu, magmaFloatComplex *h_Track_gpu,
    magmaFloatComplex *h_startCoefs, magmaFloatComplex *h_targetCoefs,
    magmaFloatComplex *h_cgesvA, magmaFloatComplex *h_cgesvB,
    magma_int_t batchCount, magma_int_t coefsCount, magma_int_t N,
    magma_queue_t my_queue, int max_steps, std::ofstream &tracks_success_file
  );

  // -- cpu computation of homotopy continuation for 5 points relative pose estimation problem (version 2) --
  real_Double_t cpu_hc_solver_5ptRelativePose_v2(
    magmaFloatComplex *h_Track_cpu, magma_int_t *h_Track_Success,
    magmaFloatComplex *h_Sols_cpu, magmaFloatComplex *h_Track_gpu,
    magmaFloatComplex *h_startCoefs, magmaFloatComplex *h_targetCoefs,
    magmaFloatComplex *h_cgesvA, magmaFloatComplex *h_cgesvB,
    magma_int_t batchCount, magma_int_t coefsCount, magma_int_t N,
    magma_queue_t my_queue, int max_steps, std::ofstream &tracks_success_file
  );

  // -- cpu computation of homotopy continuation for 5 points relative pose estimation problem (version 4) --
  real_Double_t cpu_hc_solver_5ptRelativePose_v4(
    magmaFloatComplex *h_Track_cpu, magma_int_t *h_Track_Success,
    magmaFloatComplex *h_Sols_cpu, magmaFloatComplex *h_Track_gpu,
    magmaFloatComplex *h_startCoefs, magmaFloatComplex *h_targetCoefs,
    magmaFloatComplex *h_cgesvA, magmaFloatComplex *h_cgesvB,
    magma_int_t batchCount, magma_int_t coefsCount, magma_int_t N,
    magma_queue_t my_queue, int max_steps, std::ofstream &tracks_success_file
  );

  // -- cpu computation of homotopy continuation for P3P problem --
  real_Double_t cpu_hc_solver_P3P(
    magmaFloatComplex *h_Track_cpu, magma_int_t *h_Track_Success,
    magmaFloatComplex *h_Sols_cpu, magmaFloatComplex *h_Track_gpu,
    magmaFloatComplex *h_startCoefs, magmaFloatComplex *h_targetCoefs,
    magmaFloatComplex *h_cgesvA, magmaFloatComplex *h_cgesvB,
    magma_int_t batchCount, magma_int_t coefsCount, magma_int_t N,
    magma_queue_t my_queue, int max_steps, std::ofstream &tracks_success_file
  );

  // -- cpu computation of homotopy continuation for P3P problem --
  real_Double_t cpu_hc_solver_P3P_v2(
    magmaFloatComplex *h_Track_cpu, magma_int_t *h_Track_Success,
    magmaFloatComplex *h_Sols_cpu, magmaFloatComplex *h_Track_gpu,
    magmaFloatComplex *h_startCoefs, magmaFloatComplex *h_targetCoefs,
    magmaFloatComplex *h_cgesvA, magmaFloatComplex *h_cgesvB,
    magma_int_t batchCount, magma_int_t coefsCount, magma_int_t N,
    magma_queue_t my_queue, int max_steps, std::ofstream &tracks_success_file
  );

  // -- cpu computation of homotopy continuation for R6P1lin problem --
  real_Double_t cpu_hc_solver_R6P1lin(
    magmaFloatComplex *h_Track_cpu, magma_int_t *h_Track_Success,
    magmaFloatComplex *h_Sols_cpu, magmaFloatComplex *h_Track_gpu,
    magmaFloatComplex *h_startCoefs, magmaFloatComplex *h_targetCoefs,
    magmaFloatComplex *h_cgesvA, magmaFloatComplex *h_cgesvB,
    magma_int_t batchCount, magma_int_t coefsCount, magma_int_t N,
    magma_queue_t my_queue, int max_steps, std::ofstream &tracks_success_file
  );

  // -- cpu computation of homotopy continuation for 2vTrg_distortion problem --
  real_Double_t cpu_hc_solver_2vTrg_distortion(
    magmaFloatComplex *h_Track_cpu, magma_int_t *h_Track_Success,
    magmaFloatComplex *h_Sols_cpu, magmaFloatComplex *h_Track_gpu,
    magmaFloatComplex *h_startCoefs, magmaFloatComplex *h_targetCoefs,
    magmaFloatComplex *h_cgesvA, magmaFloatComplex *h_cgesvB,
    magma_int_t batchCount, magma_int_t coefsCount, magma_int_t N,
    magma_queue_t my_queue, int max_steps, std::ofstream &tracks_success_file
  );

  // -- cpu computation of homotopy continuation for trifocal_rel_pose_f problem --
  real_Double_t cpu_hc_solver_trifocal_rel_pose_f(
    magmaFloatComplex *h_Track_cpu, magma_int_t *h_Track_Success,
    magmaFloatComplex *h_Sols_cpu, magmaFloatComplex *h_Track_gpu,
    magmaFloatComplex *h_startCoefs, magmaFloatComplex *h_targetCoefs,
    magmaFloatComplex *h_cgesvA, magmaFloatComplex *h_cgesvB,
    magma_int_t batchCount, magma_int_t coefsCount, magma_int_t N,
    magma_queue_t my_queue, int max_steps, std::ofstream &tracks_success_file
  );
}
}

#endif

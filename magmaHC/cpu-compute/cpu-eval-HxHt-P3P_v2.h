#ifndef cpu_eval_HxHt_P3P_v2_h
#define cpu_eval_HxHt_P3P_v2_h
// ============================================================================
// partial derivative evaluations of the cyclic problem for cpu HC computation
//
// Modifications
//    Chien  21-10-24:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min

namespace magmaHCWrapper {

  extern "C"
  void cpu_eval_HxHt_P3P_v2(
      magma_int_t s, float t, int N, magmaFloatComplex* s_track,
      const magmaFloatComplex &C0, const magmaFloatComplex &C1, const magmaFloatComplex &C2,
      magmaFloatComplex* s_startCoefs, magmaFloatComplex* s_targetCoefs,
      magmaFloatComplex* r_cgesvA, magmaFloatComplex* r_cgesvB )
  {
    magmaFloatComplex G1 = C0 - t;
    magmaFloatComplex G2 = G1 * s_startCoefs[0];
    magmaFloatComplex G3 = t * s_targetCoefs[0];
    magmaFloatComplex G4 = G2 + G3;
    magmaFloatComplex G5 = s_track[0] + s_track[0];
    magmaFloatComplex G6 = G4 * G5;
    magmaFloatComplex G7 = G1 * s_startCoefs[1];
    magmaFloatComplex G8 = t * s_targetCoefs[1];
    magmaFloatComplex G9 = G7 + G8;
    magmaFloatComplex G10 = s_track[1] * G9;
    magmaFloatComplex G11 = G6 + G10;
    magmaFloatComplex G12 = G1 * s_startCoefs[4];
    magmaFloatComplex G13 = t * s_targetCoefs[4];
    magmaFloatComplex G14 = G12 + G13;
    magmaFloatComplex G15 = s_track[2] * G14;
    magmaFloatComplex G16 = G6 + G15;
    magmaFloatComplex G17 = G9 * s_track[0];
    magmaFloatComplex G18 = G1 * s_startCoefs[2];
    magmaFloatComplex G19 = t * s_targetCoefs[2];
    magmaFloatComplex G20 = G18 + G19;
    magmaFloatComplex G21 = s_track[1] + s_track[1];
    magmaFloatComplex G22 = G20 * G21;
    magmaFloatComplex G23 = G17 + G22;
    magmaFloatComplex G24 = G1 * s_startCoefs[7];
    magmaFloatComplex G25 = t * s_targetCoefs[7];
    magmaFloatComplex G26 = G24 + G25;
    magmaFloatComplex G27 = s_track[2] * G26;
    magmaFloatComplex G28 = G22 + G27;
    magmaFloatComplex G29 = G14 * s_track[0];
    magmaFloatComplex G30 = G1 * s_startCoefs[5];
    magmaFloatComplex G31 = t * s_targetCoefs[5];
    magmaFloatComplex G32 = G30 + G31;
    magmaFloatComplex G33 = s_track[2] + s_track[2];
    magmaFloatComplex G34 = G32 * G33;
    magmaFloatComplex G35 = G29 + G34;
    magmaFloatComplex G36 = G26 * s_track[1];
    magmaFloatComplex G37 = G36 + G34;
    magmaFloatComplex G38 = s_track[0] * s_track[0];
    magmaFloatComplex G40 = s_targetCoefs[0] - s_startCoefs[0];
    magmaFloatComplex G41 = G38 * G40;
    magmaFloatComplex G43 = s_targetCoefs[1] - s_startCoefs[1];
    magmaFloatComplex G44 = s_track[0] * G43;
    magmaFloatComplex G45 = s_track[1] * G44;
    magmaFloatComplex G46 = G41 + G45;
    magmaFloatComplex G47 = s_track[1] * s_track[1];
    magmaFloatComplex G49 = s_targetCoefs[2] - s_startCoefs[2];
    magmaFloatComplex G50 = G47 * G49;
    magmaFloatComplex G51 = G46 + G50;
    magmaFloatComplex G53 = s_targetCoefs[3] - s_startCoefs[3];
    magmaFloatComplex G54 = G51 + G53;
    magmaFloatComplex G56 = s_targetCoefs[4] - s_startCoefs[4];
    magmaFloatComplex G57 = s_track[0] * G56;
    magmaFloatComplex G58 = s_track[2] * G57;
    magmaFloatComplex G59 = G41 + G58;
    magmaFloatComplex G60 = s_track[2] * s_track[2];
    magmaFloatComplex G62 = s_targetCoefs[5] - s_startCoefs[5];
    magmaFloatComplex G63 = G60 * G62;
    magmaFloatComplex G64 = G59 + G63;
    magmaFloatComplex G66 = s_targetCoefs[6] - s_startCoefs[6];
    magmaFloatComplex G67 = G64 + G66;
    magmaFloatComplex G69 = s_targetCoefs[7] - s_startCoefs[7];
    magmaFloatComplex G70 = s_track[1] * G69;
    magmaFloatComplex G71 = s_track[2] * G70;
    magmaFloatComplex G72 = G50 + G71;
    magmaFloatComplex G73 = G72 + G63;
    magmaFloatComplex G75 = s_targetCoefs[8] - s_startCoefs[8];
    magmaFloatComplex G76 = G73 + G75;

    r_cgesvA[0] = G11;
    r_cgesvA[1] = G16;
    r_cgesvA[2] = C2;
    r_cgesvB[0] = -G54;

    r_cgesvA[3] = G23;
    r_cgesvA[4] = C2;
    r_cgesvA[5] = G28;
    r_cgesvB[1] = -G67;

    r_cgesvA[6] = C2;
    r_cgesvA[7] = G35;
    r_cgesvA[8] = G37;
    r_cgesvB[2] = -G76;
  }
}

#endif


#ifndef cpu_eval_HxHt_3vTrg_h
#define cpu_eval_HxHt_3vTrg_h
// ============================================================================
// partial derivative evaluations of the cyclic problem for cpu HC computation
//
// Modifications
//    Chien  21-09-15:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min

namespace magmaHCWrapper {

  extern "C"
  void cpu_eval_HxHt_3vTrg(
      magma_int_t s, float t, int N, magmaFloatComplex* s_track,
      const magmaFloatComplex &C0, const magmaFloatComplex &C1, const magmaFloatComplex &C2,
      magmaFloatComplex* s_startCoefs, magmaFloatComplex* s_targetCoefs,
      magmaFloatComplex* r_cgesvA, magmaFloatComplex* r_cgesvB )
  {
    magmaFloatComplex G1 = C0 - t;
    magmaFloatComplex G2 = G1 * s_startCoefs[33];
    magmaFloatComplex G3 = t * s_targetCoefs[33];
    magmaFloatComplex G4 = G2 + G3;
    magmaFloatComplex G5 = G1 * s_startCoefs[0];
    magmaFloatComplex G6 = t * s_targetCoefs[0];
    magmaFloatComplex G7 = G5 + G6;
    magmaFloatComplex G8 = G7 * s_track[6];
    magmaFloatComplex G9 = G1 * s_startCoefs[1];
    magmaFloatComplex G10 = t * s_targetCoefs[1];
    magmaFloatComplex G11 = G9 + G10;
    magmaFloatComplex G12 = G11 * s_track[6];
    magmaFloatComplex G13 = G1 * s_startCoefs[18];
    magmaFloatComplex G14 = t * s_targetCoefs[18];
    magmaFloatComplex G15 = G13 + G14;
    magmaFloatComplex G16 = G15 * s_track[8];
    magmaFloatComplex G17 = G1 * s_startCoefs[19];
    magmaFloatComplex G18 = t * s_targetCoefs[19];
    magmaFloatComplex G19 = G17 + G18;
    magmaFloatComplex G20 = G19 * s_track[8];
    magmaFloatComplex G21 = G7 * s_track[2];
    magmaFloatComplex G22 = G11 * s_track[3];
    magmaFloatComplex G23 = G21 + G22;
    magmaFloatComplex G24 = G1 * s_startCoefs[2];
    magmaFloatComplex G25 = t * s_targetCoefs[2];
    magmaFloatComplex G26 = G24 + G25;
    magmaFloatComplex G27 = G23 + G26;
    magmaFloatComplex G28 = G15 * s_track[4];
    magmaFloatComplex G29 = G19 * s_track[5];
    magmaFloatComplex G30 = G28 + G29;
    magmaFloatComplex G31 = G1 * s_startCoefs[20];
    magmaFloatComplex G32 = t * s_targetCoefs[20];
    magmaFloatComplex G33 = G31 + G32;
    magmaFloatComplex G34 = G30 + G33;
    magmaFloatComplex G35 = G1 * s_startCoefs[3];
    magmaFloatComplex G36 = t * s_targetCoefs[3];
    magmaFloatComplex G37 = G35 + G36;
    magmaFloatComplex G38 = G37 * s_track[6];
    magmaFloatComplex G39 = G1 * s_startCoefs[4];
    magmaFloatComplex G40 = t * s_targetCoefs[4];
    magmaFloatComplex G41 = G39 + G40;
    magmaFloatComplex G42 = G41 * s_track[6];
    magmaFloatComplex G43 = G1 * s_startCoefs[21];
    magmaFloatComplex G44 = t * s_targetCoefs[21];
    magmaFloatComplex G45 = G43 + G44;
    magmaFloatComplex G46 = G45 * s_track[8];
    magmaFloatComplex G47 = G1 * s_startCoefs[22];
    magmaFloatComplex G48 = t * s_targetCoefs[22];
    magmaFloatComplex G49 = G47 + G48;
    magmaFloatComplex G50 = G49 * s_track[8];
    magmaFloatComplex G51 = G37 * s_track[2];
    magmaFloatComplex G52 = G41 * s_track[3];
    magmaFloatComplex G53 = G51 + G52;
    magmaFloatComplex G54 = G1 * s_startCoefs[5];
    magmaFloatComplex G55 = t * s_targetCoefs[5];
    magmaFloatComplex G56 = G54 + G55;
    magmaFloatComplex G57 = G53 + G56;
    magmaFloatComplex G58 = G45 * s_track[4];
    magmaFloatComplex G59 = G49 * s_track[5];
    magmaFloatComplex G60 = G58 + G59;
    magmaFloatComplex G61 = G1 * s_startCoefs[23];
    magmaFloatComplex G62 = t * s_targetCoefs[23];
    magmaFloatComplex G63 = G61 + G62;
    magmaFloatComplex G64 = G60 + G63;
    magmaFloatComplex G65 = G1 * s_startCoefs[9];
    magmaFloatComplex G66 = t * s_targetCoefs[9];
    magmaFloatComplex G67 = G65 + G66;
    magmaFloatComplex G68 = G67 * s_track[7];
    magmaFloatComplex G69 = G1 * s_startCoefs[10];
    magmaFloatComplex G70 = t * s_targetCoefs[10];
    magmaFloatComplex G71 = G69 + G70;
    magmaFloatComplex G72 = G71 * s_track[7];
    magmaFloatComplex G73 = G7 * s_track[0];
    magmaFloatComplex G74 = G37 * s_track[1];
    magmaFloatComplex G75 = G73 + G74;
    magmaFloatComplex G76 = G1 * s_startCoefs[6];
    magmaFloatComplex G77 = t * s_targetCoefs[6];
    magmaFloatComplex G78 = G76 + G77;
    magmaFloatComplex G79 = G75 + G78;
    magmaFloatComplex G80 = G67 * s_track[4];
    magmaFloatComplex G81 = G71 * s_track[5];
    magmaFloatComplex G82 = G80 + G81;
    magmaFloatComplex G83 = G1 * s_startCoefs[11];
    magmaFloatComplex G84 = t * s_targetCoefs[11];
    magmaFloatComplex G85 = G83 + G84;
    magmaFloatComplex G86 = G82 + G85;
    magmaFloatComplex G87 = G1 * s_startCoefs[12];
    magmaFloatComplex G88 = t * s_targetCoefs[12];
    magmaFloatComplex G89 = G87 + G88;
    magmaFloatComplex G90 = G89 * s_track[7];
    magmaFloatComplex G91 = G1 * s_startCoefs[13];
    magmaFloatComplex G92 = t * s_targetCoefs[13];
    magmaFloatComplex G93 = G91 + G92;
    magmaFloatComplex G94 = G93 * s_track[7];
    magmaFloatComplex G95 = G11 * s_track[0];
    magmaFloatComplex G96 = G41 * s_track[1];
    magmaFloatComplex G97 = G95 + G96;
    magmaFloatComplex G98 = G1 * s_startCoefs[7];
    magmaFloatComplex G99 = t * s_targetCoefs[7];
    magmaFloatComplex G100 = G98 + G99;
    magmaFloatComplex G101 = G97 + G100;
    magmaFloatComplex G102 = G89 * s_track[4];
    magmaFloatComplex G103 = G93 * s_track[5];
    magmaFloatComplex G104 = G102 + G103;
    magmaFloatComplex G105 = G1 * s_startCoefs[14];
    magmaFloatComplex G106 = t * s_targetCoefs[14];
    magmaFloatComplex G107 = G105 + G106;
    magmaFloatComplex G108 = G104 + G107;
    magmaFloatComplex G109 = G67 * s_track[2];
    magmaFloatComplex G110 = G89 * s_track[3];
    magmaFloatComplex G111 = G109 + G110;
    magmaFloatComplex G112 = G1 * s_startCoefs[15];
    magmaFloatComplex G113 = t * s_targetCoefs[15];
    magmaFloatComplex G114 = G112 + G113;
    magmaFloatComplex G115 = G111 + G114;
    magmaFloatComplex G116 = G15 * s_track[0];
    magmaFloatComplex G117 = G45 * s_track[1];
    magmaFloatComplex G118 = G116 + G117;
    magmaFloatComplex G119 = G1 * s_startCoefs[24];
    magmaFloatComplex G120 = t * s_targetCoefs[24];
    magmaFloatComplex G121 = G119 + G120;
    magmaFloatComplex G122 = G118 + G121;
    magmaFloatComplex G123 = G71 * s_track[2];
    magmaFloatComplex G124 = G93 * s_track[3];
    magmaFloatComplex G125 = G123 + G124;
    magmaFloatComplex G126 = G1 * s_startCoefs[16];
    magmaFloatComplex G127 = t * s_targetCoefs[16];
    magmaFloatComplex G128 = G126 + G127;
    magmaFloatComplex G129 = G125 + G128;
    magmaFloatComplex G130 = G19 * s_track[0];
    magmaFloatComplex G131 = G49 * s_track[1];
    magmaFloatComplex G132 = G130 + G131;
    magmaFloatComplex G133 = G1 * s_startCoefs[25];
    magmaFloatComplex G134 = t * s_targetCoefs[25];
    magmaFloatComplex G135 = G133 + G134;
    magmaFloatComplex G136 = G132 + G135;
    magmaFloatComplex G138 = s_targetCoefs[33] - s_startCoefs[33];
    magmaFloatComplex G139 = s_track[0] * G138;
    magmaFloatComplex G140 = s_track[2] * s_track[6];
    magmaFloatComplex G142 = s_targetCoefs[0] - s_startCoefs[0];
    magmaFloatComplex G143 = G140 * G142;
    magmaFloatComplex G144 = G139 + G143;
    magmaFloatComplex G145 = s_track[3] * s_track[6];
    magmaFloatComplex G147 = s_targetCoefs[1] - s_startCoefs[1];
    magmaFloatComplex G148 = G145 * G147;
    magmaFloatComplex G149 = G144 + G148;
    magmaFloatComplex G150 = s_track[4] * s_track[8];
    magmaFloatComplex G152 = s_targetCoefs[18] - s_startCoefs[18];
    magmaFloatComplex G153 = G150 * G152;
    magmaFloatComplex G154 = G149 + G153;
    magmaFloatComplex G155 = s_track[5] * s_track[8];
    magmaFloatComplex G157 = s_targetCoefs[19] - s_startCoefs[19];
    magmaFloatComplex G158 = G155 * G157;
    magmaFloatComplex G159 = G154 + G158;
    magmaFloatComplex G161 = s_targetCoefs[2] - s_startCoefs[2];
    magmaFloatComplex G162 = s_track[6] * G161;
    magmaFloatComplex G163 = G159 + G162;
    magmaFloatComplex G165 = s_targetCoefs[20] - s_startCoefs[20];
    magmaFloatComplex G166 = s_track[8] * G165;
    magmaFloatComplex G167 = G163 + G166;
    magmaFloatComplex G169 = s_targetCoefs[27] - s_startCoefs[27];
    magmaFloatComplex G170 = G167 + G169;
    magmaFloatComplex G171 = s_track[1] * G138;
    magmaFloatComplex G173 = s_targetCoefs[3] - s_startCoefs[3];
    magmaFloatComplex G174 = G140 * G173;
    magmaFloatComplex G175 = G171 + G174;
    magmaFloatComplex G177 = s_targetCoefs[4] - s_startCoefs[4];
    magmaFloatComplex G178 = G145 * G177;
    magmaFloatComplex G179 = G175 + G178;
    magmaFloatComplex G181 = s_targetCoefs[21] - s_startCoefs[21];
    magmaFloatComplex G182 = G150 * G181;
    magmaFloatComplex G183 = G179 + G182;
    magmaFloatComplex G185 = s_targetCoefs[22] - s_startCoefs[22];
    magmaFloatComplex G186 = G155 * G185;
    magmaFloatComplex G187 = G183 + G186;
    magmaFloatComplex G189 = s_targetCoefs[5] - s_startCoefs[5];
    magmaFloatComplex G190 = s_track[6] * G189;
    magmaFloatComplex G191 = G187 + G190;
    magmaFloatComplex G193 = s_targetCoefs[23] - s_startCoefs[23];
    magmaFloatComplex G194 = s_track[8] * G193;
    magmaFloatComplex G195 = G191 + G194;
    magmaFloatComplex G197 = s_targetCoefs[28] - s_startCoefs[28];
    magmaFloatComplex G198 = G195 + G197;
    magmaFloatComplex G199 = s_track[0] * s_track[6];
    magmaFloatComplex G200 = G199 * G142;
    magmaFloatComplex G201 = s_track[1] * s_track[6];
    magmaFloatComplex G202 = G201 * G173;
    magmaFloatComplex G203 = G200 + G202;
    magmaFloatComplex G204 = s_track[2] * G138;
    magmaFloatComplex G205 = G203 + G204;
    magmaFloatComplex G206 = s_track[4] * s_track[7];
    magmaFloatComplex G208 = s_targetCoefs[9] - s_startCoefs[9];
    magmaFloatComplex G209 = G206 * G208;
    magmaFloatComplex G210 = G205 + G209;
    magmaFloatComplex G211 = s_track[5] * s_track[7];
    magmaFloatComplex G213 = s_targetCoefs[10] - s_startCoefs[10];
    magmaFloatComplex G214 = G211 * G213;
    magmaFloatComplex G215 = G210 + G214;
    magmaFloatComplex G217 = s_targetCoefs[6] - s_startCoefs[6];
    magmaFloatComplex G218 = s_track[6] * G217;
    magmaFloatComplex G219 = G215 + G218;
    magmaFloatComplex G221 = s_targetCoefs[11] - s_startCoefs[11];
    magmaFloatComplex G222 = s_track[7] * G221;
    magmaFloatComplex G223 = G219 + G222;
    magmaFloatComplex G225 = s_targetCoefs[29] - s_startCoefs[29];
    magmaFloatComplex G226 = G223 + G225;
    magmaFloatComplex G227 = G199 * G147;
    magmaFloatComplex G228 = G201 * G177;
    magmaFloatComplex G229 = G227 + G228;
    magmaFloatComplex G230 = s_track[3] * G138;
    magmaFloatComplex G231 = G229 + G230;
    magmaFloatComplex G233 = s_targetCoefs[12] - s_startCoefs[12];
    magmaFloatComplex G234 = G206 * G233;
    magmaFloatComplex G235 = G231 + G234;
    magmaFloatComplex G237 = s_targetCoefs[13] - s_startCoefs[13];
    magmaFloatComplex G238 = G211 * G237;
    magmaFloatComplex G239 = G235 + G238;
    magmaFloatComplex G241 = s_targetCoefs[7] - s_startCoefs[7];
    magmaFloatComplex G242 = s_track[6] * G241;
    magmaFloatComplex G243 = G239 + G242;
    magmaFloatComplex G245 = s_targetCoefs[14] - s_startCoefs[14];
    magmaFloatComplex G246 = s_track[7] * G245;
    magmaFloatComplex G247 = G243 + G246;
    magmaFloatComplex G249 = s_targetCoefs[30] - s_startCoefs[30];
    magmaFloatComplex G250 = G247 + G249;
    magmaFloatComplex G251 = s_track[0] * s_track[8];
    magmaFloatComplex G252 = G251 * G152;
    magmaFloatComplex G253 = s_track[1] * s_track[8];
    magmaFloatComplex G254 = G253 * G181;
    magmaFloatComplex G255 = G252 + G254;
    magmaFloatComplex G256 = s_track[2] * s_track[7];
    magmaFloatComplex G257 = G256 * G208;
    magmaFloatComplex G258 = G255 + G257;
    magmaFloatComplex G259 = s_track[3] * s_track[7];
    magmaFloatComplex G260 = G259 * G233;
    magmaFloatComplex G261 = G258 + G260;
    magmaFloatComplex G262 = s_track[4] * G138;
    magmaFloatComplex G263 = G261 + G262;
    magmaFloatComplex G265 = s_targetCoefs[15] - s_startCoefs[15];
    magmaFloatComplex G266 = s_track[7] * G265;
    magmaFloatComplex G267 = G263 + G266;
    magmaFloatComplex G269 = s_targetCoefs[24] - s_startCoefs[24];
    magmaFloatComplex G270 = s_track[8] * G269;
    magmaFloatComplex G271 = G267 + G270;
    magmaFloatComplex G273 = s_targetCoefs[31] - s_startCoefs[31];
    magmaFloatComplex G274 = G271 + G273;
    magmaFloatComplex G275 = G251 * G157;
    magmaFloatComplex G276 = G253 * G185;
    magmaFloatComplex G277 = G275 + G276;
    magmaFloatComplex G278 = G256 * G213;
    magmaFloatComplex G279 = G277 + G278;
    magmaFloatComplex G280 = G259 * G237;
    magmaFloatComplex G281 = G279 + G280;
    magmaFloatComplex G282 = s_track[5] * G138;
    magmaFloatComplex G283 = G281 + G282;
    magmaFloatComplex G285 = s_targetCoefs[16] - s_startCoefs[16];
    magmaFloatComplex G286 = s_track[7] * G285;
    magmaFloatComplex G287 = G283 + G286;
    magmaFloatComplex G289 = s_targetCoefs[25] - s_startCoefs[25];
    magmaFloatComplex G290 = s_track[8] * G289;
    magmaFloatComplex G291 = G287 + G290;
    magmaFloatComplex G293 = s_targetCoefs[32] - s_startCoefs[32];
    magmaFloatComplex G294 = G291 + G293;
    magmaFloatComplex G295 = s_track[0] * s_track[2];
    magmaFloatComplex G296 = G295 * G142;
    magmaFloatComplex G297 = s_track[0] * s_track[3];
    magmaFloatComplex G298 = G297 * G147;
    magmaFloatComplex G299 = G296 + G298;
    magmaFloatComplex G300 = s_track[0] * G161;
    magmaFloatComplex G301 = G299 + G300;
    magmaFloatComplex G302 = s_track[1] * s_track[2];
    magmaFloatComplex G303 = G302 * G173;
    magmaFloatComplex G304 = G301 + G303;
    magmaFloatComplex G305 = s_track[1] * s_track[3];
    magmaFloatComplex G306 = G305 * G177;
    magmaFloatComplex G307 = G304 + G306;
    magmaFloatComplex G308 = s_track[1] * G189;
    magmaFloatComplex G309 = G307 + G308;
    magmaFloatComplex G310 = s_track[2] * G217;
    magmaFloatComplex G311 = G309 + G310;
    magmaFloatComplex G312 = s_track[3] * G241;
    magmaFloatComplex G313 = G311 + G312;
    magmaFloatComplex G315 = s_targetCoefs[8] - s_startCoefs[8];
    magmaFloatComplex G316 = G313 + G315;
    magmaFloatComplex G317 = s_track[2] * s_track[4];
    magmaFloatComplex G318 = G317 * G208;
    magmaFloatComplex G319 = s_track[2] * s_track[5];
    magmaFloatComplex G320 = G319 * G213;
    magmaFloatComplex G321 = G318 + G320;
    magmaFloatComplex G322 = s_track[2] * G221;
    magmaFloatComplex G323 = G321 + G322;
    magmaFloatComplex G324 = s_track[3] * s_track[4];
    magmaFloatComplex G325 = G324 * G233;
    magmaFloatComplex G326 = G323 + G325;
    magmaFloatComplex G327 = s_track[3] * s_track[5];
    magmaFloatComplex G328 = G327 * G237;
    magmaFloatComplex G329 = G326 + G328;
    magmaFloatComplex G330 = s_track[3] * G245;
    magmaFloatComplex G331 = G329 + G330;
    magmaFloatComplex G332 = s_track[4] * G265;
    magmaFloatComplex G333 = G331 + G332;
    magmaFloatComplex G334 = s_track[5] * G285;
    magmaFloatComplex G335 = G333 + G334;
    magmaFloatComplex G337 = s_targetCoefs[17] - s_startCoefs[17];
    magmaFloatComplex G338 = G335 + G337;
    magmaFloatComplex G339 = s_track[0] * s_track[4];
    magmaFloatComplex G340 = G339 * G152;
    magmaFloatComplex G341 = s_track[0] * s_track[5];
    magmaFloatComplex G342 = G341 * G157;
    magmaFloatComplex G343 = G340 + G342;
    magmaFloatComplex G344 = s_track[0] * G165;
    magmaFloatComplex G345 = G343 + G344;
    magmaFloatComplex G346 = s_track[1] * s_track[4];
    magmaFloatComplex G347 = G346 * G181;
    magmaFloatComplex G348 = G345 + G347;
    magmaFloatComplex G349 = s_track[1] * s_track[5];
    magmaFloatComplex G350 = G349 * G185;
    magmaFloatComplex G351 = G348 + G350;
    magmaFloatComplex G352 = s_track[1] * G193;
    magmaFloatComplex G353 = G351 + G352;
    magmaFloatComplex G354 = s_track[4] * G269;
    magmaFloatComplex G355 = G353 + G354;
    magmaFloatComplex G356 = s_track[5] * G289;
    magmaFloatComplex G357 = G355 + G356;
    magmaFloatComplex G359 = s_targetCoefs[26] - s_startCoefs[26];
    magmaFloatComplex G360 = G357 + G359;

    r_cgesvA[0] = G4;
    r_cgesvA[1] = C2;
    r_cgesvA[2] = G8;
    r_cgesvA[3] = G12;
    r_cgesvA[4] = G16;
    r_cgesvA[5] = G20;
    r_cgesvA[6] = G27;
    r_cgesvA[7] = C2;
    r_cgesvA[8] = G34;
    r_cgesvB[0] = -G170;

    r_cgesvA[9] = C2;
    r_cgesvA[10] = G4;
    r_cgesvA[11] = G38;
    r_cgesvA[12] = G42;
    r_cgesvA[13] = G46;
    r_cgesvA[14] = G50;
    r_cgesvA[15] = G57;
    r_cgesvA[16] = C2;
    r_cgesvA[17] = G64;
    r_cgesvB[1] = -G198;

    r_cgesvA[18] = G8;
    r_cgesvA[19] = G38;
    r_cgesvA[20] = G4;
    r_cgesvA[21] = C2;
    r_cgesvA[22] = G68;
    r_cgesvA[23] = G72;
    r_cgesvA[24] = G79;
    r_cgesvA[25] = G86;
    r_cgesvA[26] = C2;
    r_cgesvB[2] = -G226;

    r_cgesvA[27] = G12;
    r_cgesvA[28] = G42;
    r_cgesvA[29] = C2;
    r_cgesvA[30] = G4;
    r_cgesvA[31] = G90;
    r_cgesvA[32] = G94;
    r_cgesvA[33] = G101;
    r_cgesvA[34] = G108;
    r_cgesvA[35] = C2;
    r_cgesvB[3] = -G250;

    r_cgesvA[36] = G16;
    r_cgesvA[37] = G46;
    r_cgesvA[38] = G68;
    r_cgesvA[39] = G90;
    r_cgesvA[40] = G4;
    r_cgesvA[41] = C2;
    r_cgesvA[42] = C2;
    r_cgesvA[43] = G115;
    r_cgesvA[44] = G122;
    r_cgesvB[4] = -G274;

    r_cgesvA[45] = G20;
    r_cgesvA[46] = G50;
    r_cgesvA[47] = G72;
    r_cgesvA[48] = G94;
    r_cgesvA[49] = C2;
    r_cgesvA[50] = G4;
    r_cgesvA[51] = C2;
    r_cgesvA[52] = G129;
    r_cgesvA[53] = G136;
    r_cgesvB[5] = -G294;

    r_cgesvA[54] = G27;
    r_cgesvA[55] = G57;
    r_cgesvA[56] = G79;
    r_cgesvA[57] = G101;
    r_cgesvA[58] = C2;
    r_cgesvA[59] = C2;
    r_cgesvA[60] = C2;
    r_cgesvA[61] = C2;
    r_cgesvA[62] = C2;
    r_cgesvB[6] = -G316;

    r_cgesvA[63] = C2;
    r_cgesvA[64] = C2;
    r_cgesvA[65] = G86;
    r_cgesvA[66] = G108;
    r_cgesvA[67] = G115;
    r_cgesvA[68] = G129;
    r_cgesvA[69] = C2;
    r_cgesvA[70] = C2;
    r_cgesvA[71] = C2;
    r_cgesvB[7] = -G338;

    r_cgesvA[72] = G34;
    r_cgesvA[73] = G64;
    r_cgesvA[74] = C2;
    r_cgesvA[75] = C2;
    r_cgesvA[76] = G122;
    r_cgesvA[77] = G136;
    r_cgesvA[78] = C2;
    r_cgesvA[79] = C2;
    r_cgesvA[80] = C2;
    r_cgesvB[8] = -G360;
  }
}

#endif

#ifndef cpu_eval_HxH_P3P_v2_h
#define cpu_eval_HxH_P3P_v2_h
// ============================================================================
// partial derivative evaluations of the alea6 problem for cpu HC computation
//
// Modifications
//    Chien  21-10-24:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min

namespace magmaHCWrapper {

  extern "C"
  void cpu_eval_HxH_P3P_v2(
      magma_int_t s, float t, int N, magmaFloatComplex* s_track,
      const magmaFloatComplex &C0, const magmaFloatComplex &C1, const magmaFloatComplex &C2,
      magmaFloatComplex* s_startCoefs, magmaFloatComplex* s_targetCoefs,
      magmaFloatComplex* r_cgesvA, magmaFloatComplex* r_cgesvB )
  {
    magmaFloatComplex G1 = C0 - t;
    magmaFloatComplex G2 = G1 * s_startCoefs[0];
    magmaFloatComplex G3 = t * s_targetCoefs[0];
    magmaFloatComplex G4 = G2 + G3;
    magmaFloatComplex G5 = s_track[0] + s_track[0];
    magmaFloatComplex G6 = G4 * G5;
    magmaFloatComplex G7 = G1 * s_startCoefs[1];
    magmaFloatComplex G8 = t * s_targetCoefs[1];
    magmaFloatComplex G9 = G7 + G8;
    magmaFloatComplex G10 = s_track[1] * G9;
    magmaFloatComplex G11 = G6 + G10;
    magmaFloatComplex G12 = G1 * s_startCoefs[4];
    magmaFloatComplex G13 = t * s_targetCoefs[4];
    magmaFloatComplex G14 = G12 + G13;
    magmaFloatComplex G15 = s_track[2] * G14;
    magmaFloatComplex G16 = G6 + G15;
    magmaFloatComplex G17 = G9 * s_track[0];
    magmaFloatComplex G18 = G1 * s_startCoefs[2];
    magmaFloatComplex G19 = t * s_targetCoefs[2];
    magmaFloatComplex G20 = G18 + G19;
    magmaFloatComplex G21 = s_track[1] + s_track[1];
    magmaFloatComplex G22 = G20 * G21;
    magmaFloatComplex G23 = G17 + G22;
    magmaFloatComplex G24 = G1 * s_startCoefs[7];
    magmaFloatComplex G25 = t * s_targetCoefs[7];
    magmaFloatComplex G26 = G24 + G25;
    magmaFloatComplex G27 = s_track[2] * G26;
    magmaFloatComplex G28 = G22 + G27;
    magmaFloatComplex G29 = G14 * s_track[0];
    magmaFloatComplex G30 = G1 * s_startCoefs[5];
    magmaFloatComplex G31 = t * s_targetCoefs[5];
    magmaFloatComplex G32 = G30 + G31;
    magmaFloatComplex G33 = s_track[2] + s_track[2];
    magmaFloatComplex G34 = G32 * G33;
    magmaFloatComplex G35 = G29 + G34;
    magmaFloatComplex G36 = G26 * s_track[1];
    magmaFloatComplex G37 = G36 + G34;
    magmaFloatComplex G38 = s_track[0] * s_track[0];
    magmaFloatComplex G39 = G4 * G38;
    magmaFloatComplex G40 = G17 * s_track[1];
    magmaFloatComplex G41 = G39 + G40;
    magmaFloatComplex G42 = s_track[1] * s_track[1];
    magmaFloatComplex G43 = G20 * G42;
    magmaFloatComplex G44 = G41 + G43;
    magmaFloatComplex G45 = G1 * s_startCoefs[3];
    magmaFloatComplex G46 = t * s_targetCoefs[3];
    magmaFloatComplex G47 = G45 + G46;
    magmaFloatComplex G48 = G44 + G47;
    magmaFloatComplex G49 = G29 * s_track[2];
    magmaFloatComplex G50 = G39 + G49;
    magmaFloatComplex G51 = s_track[2] * s_track[2];
    magmaFloatComplex G52 = G32 * G51;
    magmaFloatComplex G53 = G50 + G52;
    magmaFloatComplex G54 = G1 * s_startCoefs[6];
    magmaFloatComplex G55 = t * s_targetCoefs[6];
    magmaFloatComplex G56 = G54 + G55;
    magmaFloatComplex G57 = G53 + G56;
    magmaFloatComplex G58 = G36 * s_track[2];
    magmaFloatComplex G59 = G43 + G58;
    magmaFloatComplex G60 = G59 + G52;
    magmaFloatComplex G61 = G1 * s_startCoefs[8];
    magmaFloatComplex G62 = t * s_targetCoefs[8];
    magmaFloatComplex G63 = G61 + G62;
    magmaFloatComplex G64 = G60 + G63;

    r_cgesvA[0] = G11;
    r_cgesvA[1] = G16;
    r_cgesvA[2] = C2;
    r_cgesvB[0] = G48;

    r_cgesvA[3] = G23;
    r_cgesvA[4] = C2;
    r_cgesvA[5] = G28;
    r_cgesvB[1] = G57;

    r_cgesvA[6] = C2;
    r_cgesvA[7] = G35;
    r_cgesvA[8] = G37;
    r_cgesvB[2] = G64;
  }
}

#endif


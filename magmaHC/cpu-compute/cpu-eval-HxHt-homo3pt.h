#ifndef cpu_eval_HxHt_homo3pt_h
#define cpu_eval_HxHt_homo3pt_h
// ============================================================================
// partial derivative evaluations of the cyclic problem for cpu HC computation
//
// Modifications
//    Chien  21-10-24:   Originally created
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min

namespace magmaHCWrapper {

  extern "C"
  void cpu_eval_HxHt_homo3pt(
      magma_int_t s, float t, int N, magmaFloatComplex* s_track,
      const magmaFloatComplex &C0, const magmaFloatComplex &C1, const magmaFloatComplex &C2,
      magmaFloatComplex* s_startCoefs, magmaFloatComplex* s_targetCoefs,
      magmaFloatComplex* r_cgesvA, magmaFloatComplex* r_cgesvB )
  {
    magmaFloatComplex G1 = C0 - t;
    magmaFloatComplex G2 = G1 * s_startCoefs[0];
    magmaFloatComplex G3 = t * s_targetCoefs[0];
    magmaFloatComplex G4 = G2 + G3;
    magmaFloatComplex G5 = s_track[7] * s_track[5];
    magmaFloatComplex G6 = G4 * G5;
    magmaFloatComplex G7 = G1 * s_startCoefs[1];
    magmaFloatComplex G8 = t * s_targetCoefs[1];
    magmaFloatComplex G9 = G7 + G8;
    magmaFloatComplex G10 = G9 * s_track[5];
    magmaFloatComplex G11 = G6 + G10;
    magmaFloatComplex G12 = G1 * s_startCoefs[7];
    magmaFloatComplex G13 = t * s_targetCoefs[7];
    magmaFloatComplex G14 = G12 + G13;
    magmaFloatComplex G15 = s_track[6] * s_track[5];
    magmaFloatComplex G16 = G14 * G15;
    magmaFloatComplex G17 = G1 * s_startCoefs[9];
    magmaFloatComplex G18 = t * s_targetCoefs[9];
    magmaFloatComplex G19 = G17 + G18;
    magmaFloatComplex G20 = G19 * s_track[5];
    magmaFloatComplex G21 = G16 + G20;
    magmaFloatComplex G22 = G1 * s_startCoefs[14];
    magmaFloatComplex G23 = t * s_targetCoefs[14];
    magmaFloatComplex G24 = G22 + G23;
    magmaFloatComplex G25 = G24 * G5;
    magmaFloatComplex G26 = G1 * s_startCoefs[15];
    magmaFloatComplex G27 = t * s_targetCoefs[15];
    magmaFloatComplex G28 = G26 + G27;
    magmaFloatComplex G29 = G28 * s_track[5];
    magmaFloatComplex G30 = G25 + G29;
    magmaFloatComplex G31 = G1 * s_startCoefs[21];
    magmaFloatComplex G32 = t * s_targetCoefs[21];
    magmaFloatComplex G33 = G31 + G32;
    magmaFloatComplex G34 = G33 * G15;
    magmaFloatComplex G35 = G1 * s_startCoefs[23];
    magmaFloatComplex G36 = t * s_targetCoefs[23];
    magmaFloatComplex G37 = G35 + G36;
    magmaFloatComplex G38 = G37 * s_track[5];
    magmaFloatComplex G39 = G34 + G38;
    magmaFloatComplex G40 = G1 * s_startCoefs[28];
    magmaFloatComplex G41 = t * s_targetCoefs[28];
    magmaFloatComplex G42 = G40 + G41;
    magmaFloatComplex G43 = G42 * G5;
    magmaFloatComplex G44 = G1 * s_startCoefs[29];
    magmaFloatComplex G45 = t * s_targetCoefs[29];
    magmaFloatComplex G46 = G44 + G45;
    magmaFloatComplex G47 = G46 * s_track[5];
    magmaFloatComplex G48 = G43 + G47;
    magmaFloatComplex G49 = G1 * s_startCoefs[35];
    magmaFloatComplex G50 = t * s_targetCoefs[35];
    magmaFloatComplex G51 = G49 + G50;
    magmaFloatComplex G52 = G51 * G15;
    magmaFloatComplex G53 = G1 * s_startCoefs[37];
    magmaFloatComplex G54 = t * s_targetCoefs[37];
    magmaFloatComplex G55 = G53 + G54;
    magmaFloatComplex G56 = G55 * s_track[5];
    magmaFloatComplex G57 = G52 + G56;
    magmaFloatComplex G58 = G1 * s_startCoefs[43];
    magmaFloatComplex G59 = t * s_targetCoefs[43];
    magmaFloatComplex G60 = G58 + G59;
    magmaFloatComplex G61 = s_track[0] + s_track[0];
    magmaFloatComplex G62 = G60 * G61;
    magmaFloatComplex G63 = G1 * s_startCoefs[2];
    magmaFloatComplex G64 = t * s_targetCoefs[2];
    magmaFloatComplex G65 = G63 + G64;
    magmaFloatComplex G66 = G65 * G5;
    magmaFloatComplex G67 = G1 * s_startCoefs[3];
    magmaFloatComplex G68 = t * s_targetCoefs[3];
    magmaFloatComplex G69 = G67 + G68;
    magmaFloatComplex G70 = G69 * s_track[5];
    magmaFloatComplex G71 = G66 + G70;
    magmaFloatComplex G72 = G1 * s_startCoefs[6];
    magmaFloatComplex G73 = t * s_targetCoefs[6];
    magmaFloatComplex G74 = G72 + G73;
    magmaFloatComplex G75 = G74 * G15;
    magmaFloatComplex G76 = G1 * s_startCoefs[10];
    magmaFloatComplex G77 = t * s_targetCoefs[10];
    magmaFloatComplex G78 = G76 + G77;
    magmaFloatComplex G79 = G78 * s_track[5];
    magmaFloatComplex G80 = G75 + G79;
    magmaFloatComplex G81 = G1 * s_startCoefs[16];
    magmaFloatComplex G82 = t * s_targetCoefs[16];
    magmaFloatComplex G83 = G81 + G82;
    magmaFloatComplex G84 = G83 * G5;
    magmaFloatComplex G85 = G1 * s_startCoefs[17];
    magmaFloatComplex G86 = t * s_targetCoefs[17];
    magmaFloatComplex G87 = G85 + G86;
    magmaFloatComplex G88 = G87 * s_track[5];
    magmaFloatComplex G89 = G84 + G88;
    magmaFloatComplex G90 = G1 * s_startCoefs[20];
    magmaFloatComplex G91 = t * s_targetCoefs[20];
    magmaFloatComplex G92 = G90 + G91;
    magmaFloatComplex G93 = G92 * G15;
    magmaFloatComplex G94 = G1 * s_startCoefs[24];
    magmaFloatComplex G95 = t * s_targetCoefs[24];
    magmaFloatComplex G96 = G94 + G95;
    magmaFloatComplex G97 = G96 * s_track[5];
    magmaFloatComplex G98 = G93 + G97;
    magmaFloatComplex G99 = G1 * s_startCoefs[30];
    magmaFloatComplex G100 = t * s_targetCoefs[30];
    magmaFloatComplex G101 = G99 + G100;
    magmaFloatComplex G102 = G101 * G5;
    magmaFloatComplex G103 = G1 * s_startCoefs[31];
    magmaFloatComplex G104 = t * s_targetCoefs[31];
    magmaFloatComplex G105 = G103 + G104;
    magmaFloatComplex G106 = G105 * s_track[5];
    magmaFloatComplex G107 = G102 + G106;
    magmaFloatComplex G108 = G1 * s_startCoefs[34];
    magmaFloatComplex G109 = t * s_targetCoefs[34];
    magmaFloatComplex G110 = G108 + G109;
    magmaFloatComplex G111 = G110 * G15;
    magmaFloatComplex G112 = G1 * s_startCoefs[38];
    magmaFloatComplex G113 = t * s_targetCoefs[38];
    magmaFloatComplex G114 = G112 + G113;
    magmaFloatComplex G115 = G114 * s_track[5];
    magmaFloatComplex G116 = G111 + G115;
    magmaFloatComplex G117 = s_track[1] + s_track[1];
    magmaFloatComplex G118 = G60 * G117;
    magmaFloatComplex G119 = G1 * s_startCoefs[4];
    magmaFloatComplex G120 = t * s_targetCoefs[4];
    magmaFloatComplex G121 = G119 + G120;
    magmaFloatComplex G122 = G121 * G5;
    magmaFloatComplex G123 = G1 * s_startCoefs[5];
    magmaFloatComplex G124 = t * s_targetCoefs[5];
    magmaFloatComplex G125 = G123 + G124;
    magmaFloatComplex G126 = G125 * s_track[5];
    magmaFloatComplex G127 = G122 + G126;
    magmaFloatComplex G128 = G1 * s_startCoefs[11];
    magmaFloatComplex G129 = t * s_targetCoefs[11];
    magmaFloatComplex G130 = G128 + G129;
    magmaFloatComplex G131 = G130 * G15;
    magmaFloatComplex G132 = G1 * s_startCoefs[12];
    magmaFloatComplex G133 = t * s_targetCoefs[12];
    magmaFloatComplex G134 = G132 + G133;
    magmaFloatComplex G135 = G134 * s_track[5];
    magmaFloatComplex G136 = G131 + G135;
    magmaFloatComplex G137 = G1 * s_startCoefs[18];
    magmaFloatComplex G138 = t * s_targetCoefs[18];
    magmaFloatComplex G139 = G137 + G138;
    magmaFloatComplex G140 = G139 * G5;
    magmaFloatComplex G141 = G1 * s_startCoefs[19];
    magmaFloatComplex G142 = t * s_targetCoefs[19];
    magmaFloatComplex G143 = G141 + G142;
    magmaFloatComplex G144 = G143 * s_track[5];
    magmaFloatComplex G145 = G140 + G144;
    magmaFloatComplex G146 = G1 * s_startCoefs[25];
    magmaFloatComplex G147 = t * s_targetCoefs[25];
    magmaFloatComplex G148 = G146 + G147;
    magmaFloatComplex G149 = G148 * G15;
    magmaFloatComplex G150 = G1 * s_startCoefs[26];
    magmaFloatComplex G151 = t * s_targetCoefs[26];
    magmaFloatComplex G152 = G150 + G151;
    magmaFloatComplex G153 = G152 * s_track[5];
    magmaFloatComplex G154 = G149 + G153;
    magmaFloatComplex G155 = G1 * s_startCoefs[32];
    magmaFloatComplex G156 = t * s_targetCoefs[32];
    magmaFloatComplex G157 = G155 + G156;
    magmaFloatComplex G158 = G157 * G5;
    magmaFloatComplex G159 = G1 * s_startCoefs[33];
    magmaFloatComplex G160 = t * s_targetCoefs[33];
    magmaFloatComplex G161 = G159 + G160;
    magmaFloatComplex G162 = G161 * s_track[5];
    magmaFloatComplex G163 = G158 + G162;
    magmaFloatComplex G164 = G1 * s_startCoefs[39];
    magmaFloatComplex G165 = t * s_targetCoefs[39];
    magmaFloatComplex G166 = G164 + G165;
    magmaFloatComplex G167 = G166 * G15;
    magmaFloatComplex G168 = G1 * s_startCoefs[40];
    magmaFloatComplex G169 = t * s_targetCoefs[40];
    magmaFloatComplex G170 = G168 + G169;
    magmaFloatComplex G171 = G170 * s_track[5];
    magmaFloatComplex G172 = G167 + G171;
    magmaFloatComplex G173 = s_track[2] + s_track[2];
    magmaFloatComplex G174 = G60 * G173;
    magmaFloatComplex G175 = s_track[3] + s_track[3];
    magmaFloatComplex G176 = G60 * G175;
    magmaFloatComplex G177 = s_track[4] + s_track[4];
    magmaFloatComplex G178 = G60 * G177;
    magmaFloatComplex G179 = s_track[7] * s_track[0];
    magmaFloatComplex G180 = G4 * G179;
    magmaFloatComplex G181 = G9 * s_track[0];
    magmaFloatComplex G182 = G180 + G181;
    magmaFloatComplex G183 = s_track[7] * s_track[1];
    magmaFloatComplex G184 = G65 * G183;
    magmaFloatComplex G185 = G182 + G184;
    magmaFloatComplex G186 = G69 * s_track[1];
    magmaFloatComplex G187 = G185 + G186;
    magmaFloatComplex G188 = s_track[7] * s_track[2];
    magmaFloatComplex G189 = G121 * G188;
    magmaFloatComplex G190 = G187 + G189;
    magmaFloatComplex G191 = G125 * s_track[2];
    magmaFloatComplex G192 = G190 + G191;
    magmaFloatComplex G193 = s_track[6] * s_track[0];
    magmaFloatComplex G194 = G14 * G193;
    magmaFloatComplex G195 = G19 * s_track[0];
    magmaFloatComplex G196 = G194 + G195;
    magmaFloatComplex G197 = s_track[6] * s_track[1];
    magmaFloatComplex G198 = G74 * G197;
    magmaFloatComplex G199 = G196 + G198;
    magmaFloatComplex G200 = G78 * s_track[1];
    magmaFloatComplex G201 = G199 + G200;
    magmaFloatComplex G202 = s_track[6] * s_track[2];
    magmaFloatComplex G203 = G130 * G202;
    magmaFloatComplex G204 = G201 + G203;
    magmaFloatComplex G205 = G134 * s_track[2];
    magmaFloatComplex G206 = G204 + G205;
    magmaFloatComplex G207 = G24 * G179;
    magmaFloatComplex G208 = G28 * s_track[0];
    magmaFloatComplex G209 = G207 + G208;
    magmaFloatComplex G210 = G83 * G183;
    magmaFloatComplex G211 = G209 + G210;
    magmaFloatComplex G212 = G87 * s_track[1];
    magmaFloatComplex G213 = G211 + G212;
    magmaFloatComplex G214 = G139 * G188;
    magmaFloatComplex G215 = G213 + G214;
    magmaFloatComplex G216 = G143 * s_track[2];
    magmaFloatComplex G217 = G215 + G216;
    magmaFloatComplex G218 = G33 * G193;
    magmaFloatComplex G219 = G37 * s_track[0];
    magmaFloatComplex G220 = G218 + G219;
    magmaFloatComplex G221 = G92 * G197;
    magmaFloatComplex G222 = G220 + G221;
    magmaFloatComplex G223 = G96 * s_track[1];
    magmaFloatComplex G224 = G222 + G223;
    magmaFloatComplex G225 = G148 * G202;
    magmaFloatComplex G226 = G224 + G225;
    magmaFloatComplex G227 = G152 * s_track[2];
    magmaFloatComplex G228 = G226 + G227;
    magmaFloatComplex G229 = G42 * G179;
    magmaFloatComplex G230 = G46 * s_track[0];
    magmaFloatComplex G231 = G229 + G230;
    magmaFloatComplex G232 = G101 * G183;
    magmaFloatComplex G233 = G231 + G232;
    magmaFloatComplex G234 = G105 * s_track[1];
    magmaFloatComplex G235 = G233 + G234;
    magmaFloatComplex G236 = G157 * G188;
    magmaFloatComplex G237 = G235 + G236;
    magmaFloatComplex G238 = G161 * s_track[2];
    magmaFloatComplex G239 = G237 + G238;
    magmaFloatComplex G240 = G51 * G193;
    magmaFloatComplex G241 = G55 * s_track[0];
    magmaFloatComplex G242 = G240 + G241;
    magmaFloatComplex G243 = G110 * G197;
    magmaFloatComplex G244 = G242 + G243;
    magmaFloatComplex G245 = G114 * s_track[1];
    magmaFloatComplex G246 = G244 + G245;
    magmaFloatComplex G247 = G166 * G202;
    magmaFloatComplex G248 = G246 + G247;
    magmaFloatComplex G249 = G170 * s_track[2];
    magmaFloatComplex G250 = G248 + G249;
    magmaFloatComplex G251 = s_track[0] * s_track[5];
    magmaFloatComplex G252 = G14 * G251;
    magmaFloatComplex G253 = s_track[1] * s_track[5];
    magmaFloatComplex G254 = G74 * G253;
    magmaFloatComplex G255 = G252 + G254;
    magmaFloatComplex G256 = s_track[2] * s_track[5];
    magmaFloatComplex G257 = G130 * G256;
    magmaFloatComplex G258 = G255 + G257;
    magmaFloatComplex G259 = G33 * G251;
    magmaFloatComplex G260 = G92 * G253;
    magmaFloatComplex G261 = G259 + G260;
    magmaFloatComplex G262 = G148 * G256;
    magmaFloatComplex G263 = G261 + G262;
    magmaFloatComplex G264 = G51 * G251;
    magmaFloatComplex G265 = G110 * G253;
    magmaFloatComplex G266 = G264 + G265;
    magmaFloatComplex G267 = G166 * G256;
    magmaFloatComplex G268 = G266 + G267;
    magmaFloatComplex G269 = G4 * G251;
    magmaFloatComplex G270 = G65 * G253;
    magmaFloatComplex G271 = G269 + G270;
    magmaFloatComplex G272 = G121 * G256;
    magmaFloatComplex G273 = G271 + G272;
    magmaFloatComplex G274 = G24 * G251;
    magmaFloatComplex G275 = G83 * G253;
    magmaFloatComplex G276 = G274 + G275;
    magmaFloatComplex G277 = G139 * G256;
    magmaFloatComplex G278 = G276 + G277;
    magmaFloatComplex G279 = G42 * G251;
    magmaFloatComplex G280 = G101 * G253;
    magmaFloatComplex G281 = G279 + G280;
    magmaFloatComplex G282 = G157 * G256;
    magmaFloatComplex G283 = G281 + G282;
    magmaFloatComplex G284 = G251 * s_track[7];
    magmaFloatComplex G286 = s_targetCoefs[0] - s_startCoefs[0];
    magmaFloatComplex G287 = G284 * G286;
    magmaFloatComplex G289 = s_targetCoefs[1] - s_startCoefs[1];
    magmaFloatComplex G290 = G251 * G289;
    magmaFloatComplex G291 = G287 + G290;
    magmaFloatComplex G292 = G253 * s_track[7];
    magmaFloatComplex G294 = s_targetCoefs[2] - s_startCoefs[2];
    magmaFloatComplex G295 = G292 * G294;
    magmaFloatComplex G296 = G291 + G295;
    magmaFloatComplex G298 = s_targetCoefs[3] - s_startCoefs[3];
    magmaFloatComplex G299 = G253 * G298;
    magmaFloatComplex G300 = G296 + G299;
    magmaFloatComplex G301 = G256 * s_track[7];
    magmaFloatComplex G303 = s_targetCoefs[4] - s_startCoefs[4];
    magmaFloatComplex G304 = G301 * G303;
    magmaFloatComplex G305 = G300 + G304;
    magmaFloatComplex G307 = s_targetCoefs[5] - s_startCoefs[5];
    magmaFloatComplex G308 = G256 * G307;
    magmaFloatComplex G309 = G305 + G308;
    magmaFloatComplex G311 = s_targetCoefs[6] - s_startCoefs[6];
    magmaFloatComplex G312 = s_track[3] * G311;
    magmaFloatComplex G313 = G309 + G312;
    magmaFloatComplex G315 = s_targetCoefs[7] - s_startCoefs[7];
    magmaFloatComplex G316 = s_track[4] * G315;
    magmaFloatComplex G317 = G313 + G316;
    magmaFloatComplex G319 = s_targetCoefs[8] - s_startCoefs[8];
    magmaFloatComplex G320 = G317 + G319;
    magmaFloatComplex G321 = G251 * s_track[6];
    magmaFloatComplex G322 = G321 * G315;
    magmaFloatComplex G324 = s_targetCoefs[9] - s_startCoefs[9];
    magmaFloatComplex G325 = G251 * G324;
    magmaFloatComplex G326 = G322 + G325;
    magmaFloatComplex G327 = G253 * s_track[6];
    magmaFloatComplex G328 = G327 * G311;
    magmaFloatComplex G329 = G326 + G328;
    magmaFloatComplex G331 = s_targetCoefs[10] - s_startCoefs[10];
    magmaFloatComplex G332 = G253 * G331;
    magmaFloatComplex G333 = G329 + G332;
    magmaFloatComplex G334 = G256 * s_track[6];
    magmaFloatComplex G336 = s_targetCoefs[11] - s_startCoefs[11];
    magmaFloatComplex G337 = G334 * G336;
    magmaFloatComplex G338 = G333 + G337;
    magmaFloatComplex G340 = s_targetCoefs[12] - s_startCoefs[12];
    magmaFloatComplex G341 = G256 * G340;
    magmaFloatComplex G342 = G338 + G341;
    magmaFloatComplex G343 = s_track[3] * G286;
    magmaFloatComplex G344 = G342 + G343;
    magmaFloatComplex G345 = s_track[4] * G311;
    magmaFloatComplex G346 = G344 + G345;
    magmaFloatComplex G348 = s_targetCoefs[13] - s_startCoefs[13];
    magmaFloatComplex G349 = G346 + G348;
    magmaFloatComplex G351 = s_targetCoefs[14] - s_startCoefs[14];
    magmaFloatComplex G352 = G284 * G351;
    magmaFloatComplex G354 = s_targetCoefs[15] - s_startCoefs[15];
    magmaFloatComplex G355 = G251 * G354;
    magmaFloatComplex G356 = G352 + G355;
    magmaFloatComplex G358 = s_targetCoefs[16] - s_startCoefs[16];
    magmaFloatComplex G359 = G292 * G358;
    magmaFloatComplex G360 = G356 + G359;
    magmaFloatComplex G362 = s_targetCoefs[17] - s_startCoefs[17];
    magmaFloatComplex G363 = G253 * G362;
    magmaFloatComplex G364 = G360 + G363;
    magmaFloatComplex G366 = s_targetCoefs[18] - s_startCoefs[18];
    magmaFloatComplex G367 = G301 * G366;
    magmaFloatComplex G368 = G364 + G367;
    magmaFloatComplex G370 = s_targetCoefs[19] - s_startCoefs[19];
    magmaFloatComplex G371 = G256 * G370;
    magmaFloatComplex G372 = G368 + G371;
    magmaFloatComplex G374 = s_targetCoefs[20] - s_startCoefs[20];
    magmaFloatComplex G375 = s_track[3] * G374;
    magmaFloatComplex G376 = G372 + G375;
    magmaFloatComplex G378 = s_targetCoefs[21] - s_startCoefs[21];
    magmaFloatComplex G379 = s_track[4] * G378;
    magmaFloatComplex G380 = G376 + G379;
    magmaFloatComplex G382 = s_targetCoefs[22] - s_startCoefs[22];
    magmaFloatComplex G383 = G380 + G382;
    magmaFloatComplex G384 = G321 * G378;
    magmaFloatComplex G386 = s_targetCoefs[23] - s_startCoefs[23];
    magmaFloatComplex G387 = G251 * G386;
    magmaFloatComplex G388 = G384 + G387;
    magmaFloatComplex G389 = G327 * G374;
    magmaFloatComplex G390 = G388 + G389;
    magmaFloatComplex G392 = s_targetCoefs[24] - s_startCoefs[24];
    magmaFloatComplex G393 = G253 * G392;
    magmaFloatComplex G394 = G390 + G393;
    magmaFloatComplex G396 = s_targetCoefs[25] - s_startCoefs[25];
    magmaFloatComplex G397 = G334 * G396;
    magmaFloatComplex G398 = G394 + G397;
    magmaFloatComplex G400 = s_targetCoefs[26] - s_startCoefs[26];
    magmaFloatComplex G401 = G256 * G400;
    magmaFloatComplex G402 = G398 + G401;
    magmaFloatComplex G403 = s_track[3] * G351;
    magmaFloatComplex G404 = G402 + G403;
    magmaFloatComplex G405 = s_track[4] * G374;
    magmaFloatComplex G406 = G404 + G405;
    magmaFloatComplex G408 = s_targetCoefs[27] - s_startCoefs[27];
    magmaFloatComplex G409 = G406 + G408;
    magmaFloatComplex G411 = s_targetCoefs[28] - s_startCoefs[28];
    magmaFloatComplex G412 = G284 * G411;
    magmaFloatComplex G414 = s_targetCoefs[29] - s_startCoefs[29];
    magmaFloatComplex G415 = G251 * G414;
    magmaFloatComplex G416 = G412 + G415;
    magmaFloatComplex G418 = s_targetCoefs[30] - s_startCoefs[30];
    magmaFloatComplex G419 = G292 * G418;
    magmaFloatComplex G420 = G416 + G419;
    magmaFloatComplex G422 = s_targetCoefs[31] - s_startCoefs[31];
    magmaFloatComplex G423 = G253 * G422;
    magmaFloatComplex G424 = G420 + G423;
    magmaFloatComplex G426 = s_targetCoefs[32] - s_startCoefs[32];
    magmaFloatComplex G427 = G301 * G426;
    magmaFloatComplex G428 = G424 + G427;
    magmaFloatComplex G430 = s_targetCoefs[33] - s_startCoefs[33];
    magmaFloatComplex G431 = G256 * G430;
    magmaFloatComplex G432 = G428 + G431;
    magmaFloatComplex G434 = s_targetCoefs[34] - s_startCoefs[34];
    magmaFloatComplex G435 = s_track[3] * G434;
    magmaFloatComplex G436 = G432 + G435;
    magmaFloatComplex G438 = s_targetCoefs[35] - s_startCoefs[35];
    magmaFloatComplex G439 = s_track[4] * G438;
    magmaFloatComplex G440 = G436 + G439;
    magmaFloatComplex G442 = s_targetCoefs[36] - s_startCoefs[36];
    magmaFloatComplex G443 = G440 + G442;
    magmaFloatComplex G444 = G321 * G438;
    magmaFloatComplex G446 = s_targetCoefs[37] - s_startCoefs[37];
    magmaFloatComplex G447 = G251 * G446;
    magmaFloatComplex G448 = G444 + G447;
    magmaFloatComplex G449 = G327 * G434;
    magmaFloatComplex G450 = G448 + G449;
    magmaFloatComplex G452 = s_targetCoefs[38] - s_startCoefs[38];
    magmaFloatComplex G453 = G253 * G452;
    magmaFloatComplex G454 = G450 + G453;
    magmaFloatComplex G456 = s_targetCoefs[39] - s_startCoefs[39];
    magmaFloatComplex G457 = G334 * G456;
    magmaFloatComplex G458 = G454 + G457;
    magmaFloatComplex G460 = s_targetCoefs[40] - s_startCoefs[40];
    magmaFloatComplex G461 = G256 * G460;
    magmaFloatComplex G462 = G458 + G461;
    magmaFloatComplex G463 = s_track[3] * G411;
    magmaFloatComplex G464 = G462 + G463;
    magmaFloatComplex G465 = s_track[4] * G434;
    magmaFloatComplex G466 = G464 + G465;
    magmaFloatComplex G468 = s_targetCoefs[41] - s_startCoefs[41];
    magmaFloatComplex G469 = G466 + G468;
    magmaFloatComplex G470 = s_track[0] * s_track[0];
    magmaFloatComplex G472 = s_targetCoefs[43] - s_startCoefs[43];
    magmaFloatComplex G473 = G470 * G472;
    magmaFloatComplex G474 = s_track[1] * s_track[1];
    magmaFloatComplex G475 = G474 * G472;
    magmaFloatComplex G476 = G473 + G475;
    magmaFloatComplex G477 = s_track[2] * s_track[2];
    magmaFloatComplex G478 = G477 * G472;
    magmaFloatComplex G479 = G476 + G478;
    magmaFloatComplex G481 = s_targetCoefs[42] - s_startCoefs[42];
    magmaFloatComplex G482 = G479 + G481;
    magmaFloatComplex G483 = s_track[3] * s_track[3];
    magmaFloatComplex G484 = G483 * G472;
    magmaFloatComplex G485 = s_track[4] * s_track[4];
    magmaFloatComplex G486 = G485 * G472;
    magmaFloatComplex G487 = G484 + G486;
    magmaFloatComplex G488 = G487 + G481;

    r_cgesvA[0] = G11;
    r_cgesvA[1] = G21;
    r_cgesvA[2] = G30;
    r_cgesvA[3] = G39;
    r_cgesvA[4] = G48;
    r_cgesvA[5] = G57;
    r_cgesvA[6] = G62;
    r_cgesvA[7] = C2;
    r_cgesvB[0] = -G320;

    r_cgesvA[8] = G71;
    r_cgesvA[9] = G80;
    r_cgesvA[10] = G89;
    r_cgesvA[11] = G98;
    r_cgesvA[12] = G107;
    r_cgesvA[13] = G116;
    r_cgesvA[14] = G118;
    r_cgesvA[15] = C2;
    r_cgesvB[1] = -G349;

    r_cgesvA[16] = G127;
    r_cgesvA[17] = G136;
    r_cgesvA[18] = G145;
    r_cgesvA[19] = G154;
    r_cgesvA[20] = G163;
    r_cgesvA[21] = G172;
    r_cgesvA[22] = G174;
    r_cgesvA[23] = C2;
    r_cgesvB[2] = -G383;

    r_cgesvA[24] = G74;
    r_cgesvA[25] = G4;
    r_cgesvA[26] = G92;
    r_cgesvA[27] = G24;
    r_cgesvA[28] = G110;
    r_cgesvA[29] = G42;
    r_cgesvA[30] = C2;
    r_cgesvA[31] = G176;
    r_cgesvB[3] = -G409;

    r_cgesvA[32] = G14;
    r_cgesvA[33] = G74;
    r_cgesvA[34] = G33;
    r_cgesvA[35] = G92;
    r_cgesvA[36] = G51;
    r_cgesvA[37] = G110;
    r_cgesvA[38] = C2;
    r_cgesvA[39] = G178;
    r_cgesvB[4] = -G443;

    r_cgesvA[40] = G192;
    r_cgesvA[41] = G206;
    r_cgesvA[42] = G217;
    r_cgesvA[43] = G228;
    r_cgesvA[44] = G239;
    r_cgesvA[45] = G250;
    r_cgesvA[46] = C2;
    r_cgesvA[47] = C2;
    r_cgesvB[5] = -G469;

    r_cgesvA[48] = C2;
    r_cgesvA[49] = G258;
    r_cgesvA[50] = C2;
    r_cgesvA[51] = G263;
    r_cgesvA[52] = C2;
    r_cgesvA[53] = G268;
    r_cgesvA[54] = C2;
    r_cgesvA[55] = C2;
    r_cgesvB[6] = -G482;

    r_cgesvA[56] = G273;
    r_cgesvA[57] = C2;
    r_cgesvA[58] = G278;
    r_cgesvA[59] = C2;
    r_cgesvA[60] = G283;
    r_cgesvA[61] = C2;
    r_cgesvA[62] = C2;
    r_cgesvA[63] = C2;
    r_cgesvB[7] = -G488;
  }
}

#endif

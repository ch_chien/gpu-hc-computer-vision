#ifndef dev_eval_indxing_R6P1lin_cuh_
#define dev_eval_indxing_R6P1lin_cuh_
// ============================================================================
// Device function for evaluating the parallel indexing for Hx, Ht, and H of
// R6P1lin problem
//
// Modifications
//    Chien  21-11-22:   Originally created
//
// ============================================================================
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// -- cuda included --
#include <cuda_runtime.h>

// -- magma included --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

namespace magmaHCWrapper {

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_cdt_R6P1lin(
        const int tx, float t, magmaFloatComplex s_vector_cdt[coefsCount],
        magmaFloatComplex r_startCoefs[coefsCount], magmaFloatComplex r_targetCoefs[coefsCount])
    {
        #pragma unroll
        for (int i = 0; i < 9; i++) {
            s_vector_cdt[ tx + i * N ] = r_targetCoefs[tx + i * N] * t - r_startCoefs[tx + i * N] * (t-1);
        }
        if (tx < 2) {
          s_vector_cdt[ tx + 162 ] = r_targetCoefs[tx + 162] * t - r_startCoefs[tx + 162] * (t-1);
        }
    }

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_Jacobian_R6P1lin(
        const int tx, magmaFloatComplex *s_track, magmaFloatComplex r_cgesvA[N], const magma_int_t* __restrict__ d_const_mat_s,
        const magma_int_t* __restrict__ d_const_mat_X, const magma_int_t* __restrict__ d_const_mat_Y,
        magmaFloatComplex s_cdt[coefsCount])
    {
        // -- the maximal terms of Hx in R6P1lin problem is 12 --
        // -- 3888 = 18(vars) * 18(vars) * 12(terms) --
        #pragma unroll
        for(int i = 0; i < N; i++) {
          r_cgesvA[i] = d_const_mat_s[tx + i*N] * s_track[ d_const_mat_X[tx + i*N] ] * s_track[ d_const_mat_X[tx + i*N + 3888] ] * s_cdt[ d_const_mat_Y[tx + i*N] ]
                      + d_const_mat_s[tx + i*N + N*N] * s_track[ d_const_mat_X[tx + i*N + N*N] ] * s_track[ d_const_mat_X[tx + i*N + N*N + 3888] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N] ]
                      + d_const_mat_s[tx + i*N + N*N*2] * s_track[ d_const_mat_X[tx + i*N + N*N*2] ] * s_track[ d_const_mat_X[tx + i*N + N*N*2 + 3888] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*2] ]
                      + d_const_mat_s[tx + i*N + N*N*3] * s_track[ d_const_mat_X[tx + i*N + N*N*3] ] * s_track[ d_const_mat_X[tx + i*N + N*N*3 + 3888] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*3] ]
                      + d_const_mat_s[tx + i*N + N*N*4] * s_track[ d_const_mat_X[tx + i*N + N*N*4] ] * s_track[ d_const_mat_X[tx + i*N + N*N*4 + 3888] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*4] ]
                      + d_const_mat_s[tx + i*N + N*N*5] * s_track[ d_const_mat_X[tx + i*N + N*N*5] ] * s_track[ d_const_mat_X[tx + i*N + N*N*5 + 3888] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*5] ]
                      + d_const_mat_s[tx + i*N + N*N*6] * s_track[ d_const_mat_X[tx + i*N + N*N*6] ] * s_track[ d_const_mat_X[tx + i*N + N*N*6 + 3888] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*6] ]
                      + d_const_mat_s[tx + i*N + N*N*7] * s_track[ d_const_mat_X[tx + i*N + N*N*7] ] * s_track[ d_const_mat_X[tx + i*N + N*N*7 + 3888] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*7] ]
                      + d_const_mat_s[tx + i*N + N*N*8] * s_track[ d_const_mat_X[tx + i*N + N*N*8] ] * s_track[ d_const_mat_X[tx + i*N + N*N*8 + 3888] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*8] ]
                      + d_const_mat_s[tx + i*N + N*N*9] * s_track[ d_const_mat_X[tx + i*N + N*N*9] ] * s_track[ d_const_mat_X[tx + i*N + N*N*9 + 3888] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*9] ]
                      + d_const_mat_s[tx + i*N + N*N*10] * s_track[ d_const_mat_X[tx + i*N + N*N*10] ] * s_track[ d_const_mat_X[tx + i*N + N*N*10 + 3888] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*10] ]
                      + d_const_mat_s[tx + i*N + N*N*11] * s_track[ d_const_mat_X[tx + i*N + N*N*11] ] * s_track[ d_const_mat_X[tx + i*N + N*N*11 + 3888] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*11] ];
        }
    }

    template<int N>
    __device__ __inline__ void
    eval_Ht_R6P1lin(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_Ht_const_mat_X,
      const magma_int_t* __restrict__ d_Ht_const_mat_Y, const magmaFloatComplex* __restrict__ d_const_cd)
    {
      // -- the maximal terms of Ht in R6P1lin problem is 36 --
      // -- the maximal parts for R6P1lin problem is 3 --
      // -- N*maximal terms of Ht = 18*36 = 648
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_Ht_const_mat_X[tx] ] * s_track[ d_Ht_const_mat_X[tx + 648] ] * s_track[ d_Ht_const_mat_X[tx + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + N] * s_track[ d_Ht_const_mat_X[tx + N] ] * s_track[ d_Ht_const_mat_X[tx + N + 648] ] * s_track[ d_Ht_const_mat_X[tx + N + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N] ]
               + d_Ht_const_mat_s[tx + N*2] * s_track[ d_Ht_const_mat_X[tx + N*2] ] * s_track[ d_Ht_const_mat_X[tx + N*2 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*2 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*2] ]
               + d_Ht_const_mat_s[tx + N*3] * s_track[ d_Ht_const_mat_X[tx + N*3] ] * s_track[ d_Ht_const_mat_X[tx + N*3 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*3 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*3] ]
               + d_Ht_const_mat_s[tx + N*4] * s_track[ d_Ht_const_mat_X[tx + N*4] ] * s_track[ d_Ht_const_mat_X[tx + N*4 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*4 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*4] ]
               + d_Ht_const_mat_s[tx + N*5] * s_track[ d_Ht_const_mat_X[tx + N*5] ] * s_track[ d_Ht_const_mat_X[tx + N*5 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*5 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*5] ]
               + d_Ht_const_mat_s[tx + N*6] * s_track[ d_Ht_const_mat_X[tx + N*6] ] * s_track[ d_Ht_const_mat_X[tx + N*6 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*6 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*6] ]
               + d_Ht_const_mat_s[tx + N*7] * s_track[ d_Ht_const_mat_X[tx + N*7] ] * s_track[ d_Ht_const_mat_X[tx + N*7 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*7 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*7] ]
               + d_Ht_const_mat_s[tx + N*8] * s_track[ d_Ht_const_mat_X[tx + N*8] ] * s_track[ d_Ht_const_mat_X[tx + N*8 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*8 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*8] ]
               + d_Ht_const_mat_s[tx + N*9] * s_track[ d_Ht_const_mat_X[tx + N*9] ] * s_track[ d_Ht_const_mat_X[tx + N*9 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*9 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*9] ]
               + d_Ht_const_mat_s[tx + N*10] * s_track[ d_Ht_const_mat_X[tx + N*10] ] * s_track[ d_Ht_const_mat_X[tx + N*10 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*10 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*10] ]
               + d_Ht_const_mat_s[tx + N*11] * s_track[ d_Ht_const_mat_X[tx + N*11] ] * s_track[ d_Ht_const_mat_X[tx + N*11 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*11 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*11] ]
               + d_Ht_const_mat_s[tx + N*12] * s_track[ d_Ht_const_mat_X[tx + N*12] ] * s_track[ d_Ht_const_mat_X[tx + N*12 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*12 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*12] ]
               + d_Ht_const_mat_s[tx + N*13] * s_track[ d_Ht_const_mat_X[tx + N*13] ] * s_track[ d_Ht_const_mat_X[tx + N*13 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*13 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*13] ]
               + d_Ht_const_mat_s[tx + N*14] * s_track[ d_Ht_const_mat_X[tx + N*14] ] * s_track[ d_Ht_const_mat_X[tx + N*14 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*14 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*14] ]
               + d_Ht_const_mat_s[tx + N*15] * s_track[ d_Ht_const_mat_X[tx + N*15] ] * s_track[ d_Ht_const_mat_X[tx + N*15 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*15 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*15] ]
               + d_Ht_const_mat_s[tx + N*16] * s_track[ d_Ht_const_mat_X[tx + N*16] ] * s_track[ d_Ht_const_mat_X[tx + N*16 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*16 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*16] ]
               + d_Ht_const_mat_s[tx + N*17] * s_track[ d_Ht_const_mat_X[tx + N*17] ] * s_track[ d_Ht_const_mat_X[tx + N*17 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*17 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*17] ]
               + d_Ht_const_mat_s[tx + N*18] * s_track[ d_Ht_const_mat_X[tx + N*18] ] * s_track[ d_Ht_const_mat_X[tx + N*18 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*18 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*18] ]
               + d_Ht_const_mat_s[tx + N*19] * s_track[ d_Ht_const_mat_X[tx + N*19] ] * s_track[ d_Ht_const_mat_X[tx + N*19 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*19 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*19] ]
               + d_Ht_const_mat_s[tx + N*20] * s_track[ d_Ht_const_mat_X[tx + N*20] ] * s_track[ d_Ht_const_mat_X[tx + N*20 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*20 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*20] ]
               + d_Ht_const_mat_s[tx + N*21] * s_track[ d_Ht_const_mat_X[tx + N*21] ] * s_track[ d_Ht_const_mat_X[tx + N*21 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*21 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*21] ]
               + d_Ht_const_mat_s[tx + N*22] * s_track[ d_Ht_const_mat_X[tx + N*22] ] * s_track[ d_Ht_const_mat_X[tx + N*22 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*22 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*22] ]
               + d_Ht_const_mat_s[tx + N*23] * s_track[ d_Ht_const_mat_X[tx + N*23] ] * s_track[ d_Ht_const_mat_X[tx + N*23 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*23 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*23] ]
               + d_Ht_const_mat_s[tx + N*24] * s_track[ d_Ht_const_mat_X[tx + N*24] ] * s_track[ d_Ht_const_mat_X[tx + N*24 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*24 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*24] ]
               + d_Ht_const_mat_s[tx + N*25] * s_track[ d_Ht_const_mat_X[tx + N*25] ] * s_track[ d_Ht_const_mat_X[tx + N*25 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*25 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*25] ]
               + d_Ht_const_mat_s[tx + N*26] * s_track[ d_Ht_const_mat_X[tx + N*26] ] * s_track[ d_Ht_const_mat_X[tx + N*26 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*26 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*26] ]
               + d_Ht_const_mat_s[tx + N*27] * s_track[ d_Ht_const_mat_X[tx + N*27] ] * s_track[ d_Ht_const_mat_X[tx + N*27 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*27 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*27] ]
               + d_Ht_const_mat_s[tx + N*28] * s_track[ d_Ht_const_mat_X[tx + N*28] ] * s_track[ d_Ht_const_mat_X[tx + N*28 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*28 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*28] ]
               + d_Ht_const_mat_s[tx + N*29] * s_track[ d_Ht_const_mat_X[tx + N*29] ] * s_track[ d_Ht_const_mat_X[tx + N*29 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*29 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*29] ]
               + d_Ht_const_mat_s[tx + N*30] * s_track[ d_Ht_const_mat_X[tx + N*30] ] * s_track[ d_Ht_const_mat_X[tx + N*30 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*30 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*30] ]
               + d_Ht_const_mat_s[tx + N*31] * s_track[ d_Ht_const_mat_X[tx + N*31] ] * s_track[ d_Ht_const_mat_X[tx + N*31 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*31 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*31] ]
               + d_Ht_const_mat_s[tx + N*32] * s_track[ d_Ht_const_mat_X[tx + N*32] ] * s_track[ d_Ht_const_mat_X[tx + N*32 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*32 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*32] ]
               + d_Ht_const_mat_s[tx + N*33] * s_track[ d_Ht_const_mat_X[tx + N*33] ] * s_track[ d_Ht_const_mat_X[tx + N*33 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*33 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*33] ]
               + d_Ht_const_mat_s[tx + N*34] * s_track[ d_Ht_const_mat_X[tx + N*34] ] * s_track[ d_Ht_const_mat_X[tx + N*34 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*34 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*34] ]
               + d_Ht_const_mat_s[tx + N*35] * s_track[ d_Ht_const_mat_X[tx + N*35] ] * s_track[ d_Ht_const_mat_X[tx + N*35 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*35 + 1296] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*35] ];
    }

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_H_R6P1lin(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_Ht_const_mat_X,
      const magma_int_t* __restrict__ d_Ht_const_mat_Y, magmaFloatComplex s_cdt[coefsCount])
    {
      // -- same as Ht, except replacing cd by cdt --
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_Ht_const_mat_X[tx] ] * s_track[ d_Ht_const_mat_X[tx + 648] ] * s_track[ d_Ht_const_mat_X[tx + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + N] * s_track[ d_Ht_const_mat_X[tx + N] ] * s_track[ d_Ht_const_mat_X[tx + N + 648] ] * s_track[ d_Ht_const_mat_X[tx + N + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N] ]
               + d_Ht_const_mat_s[tx + N*2] * s_track[ d_Ht_const_mat_X[tx + N*2] ] * s_track[ d_Ht_const_mat_X[tx + N*2 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*2 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*2] ]
               + d_Ht_const_mat_s[tx + N*3] * s_track[ d_Ht_const_mat_X[tx + N*3] ] * s_track[ d_Ht_const_mat_X[tx + N*3 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*3 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*3] ]
               + d_Ht_const_mat_s[tx + N*4] * s_track[ d_Ht_const_mat_X[tx + N*4] ] * s_track[ d_Ht_const_mat_X[tx + N*4 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*4 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*4] ]
               + d_Ht_const_mat_s[tx + N*5] * s_track[ d_Ht_const_mat_X[tx + N*5] ] * s_track[ d_Ht_const_mat_X[tx + N*5 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*5 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*5] ]
               + d_Ht_const_mat_s[tx + N*6] * s_track[ d_Ht_const_mat_X[tx + N*6] ] * s_track[ d_Ht_const_mat_X[tx + N*6 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*6 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*6] ]
               + d_Ht_const_mat_s[tx + N*7] * s_track[ d_Ht_const_mat_X[tx + N*7] ] * s_track[ d_Ht_const_mat_X[tx + N*7 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*7 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*7] ]
               + d_Ht_const_mat_s[tx + N*8] * s_track[ d_Ht_const_mat_X[tx + N*8] ] * s_track[ d_Ht_const_mat_X[tx + N*8 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*8 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*8] ]
               + d_Ht_const_mat_s[tx + N*9] * s_track[ d_Ht_const_mat_X[tx + N*9] ] * s_track[ d_Ht_const_mat_X[tx + N*9 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*9 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*9] ]
               + d_Ht_const_mat_s[tx + N*10] * s_track[ d_Ht_const_mat_X[tx + N*10] ] * s_track[ d_Ht_const_mat_X[tx + N*10 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*10 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*10] ]
               + d_Ht_const_mat_s[tx + N*11] * s_track[ d_Ht_const_mat_X[tx + N*11] ] * s_track[ d_Ht_const_mat_X[tx + N*11 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*11 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*11] ]
               + d_Ht_const_mat_s[tx + N*12] * s_track[ d_Ht_const_mat_X[tx + N*12] ] * s_track[ d_Ht_const_mat_X[tx + N*12 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*12 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*12] ]
               + d_Ht_const_mat_s[tx + N*13] * s_track[ d_Ht_const_mat_X[tx + N*13] ] * s_track[ d_Ht_const_mat_X[tx + N*13 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*13 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*13] ]
               + d_Ht_const_mat_s[tx + N*14] * s_track[ d_Ht_const_mat_X[tx + N*14] ] * s_track[ d_Ht_const_mat_X[tx + N*14 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*14 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*14] ]
               + d_Ht_const_mat_s[tx + N*15] * s_track[ d_Ht_const_mat_X[tx + N*15] ] * s_track[ d_Ht_const_mat_X[tx + N*15 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*15 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*15] ]
               + d_Ht_const_mat_s[tx + N*16] * s_track[ d_Ht_const_mat_X[tx + N*16] ] * s_track[ d_Ht_const_mat_X[tx + N*16 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*16 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*16] ]
               + d_Ht_const_mat_s[tx + N*17] * s_track[ d_Ht_const_mat_X[tx + N*17] ] * s_track[ d_Ht_const_mat_X[tx + N*17 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*17 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*17] ]
               + d_Ht_const_mat_s[tx + N*18] * s_track[ d_Ht_const_mat_X[tx + N*18] ] * s_track[ d_Ht_const_mat_X[tx + N*18 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*18 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*18] ]
               + d_Ht_const_mat_s[tx + N*19] * s_track[ d_Ht_const_mat_X[tx + N*19] ] * s_track[ d_Ht_const_mat_X[tx + N*19 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*19 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*19] ]
               + d_Ht_const_mat_s[tx + N*20] * s_track[ d_Ht_const_mat_X[tx + N*20] ] * s_track[ d_Ht_const_mat_X[tx + N*20 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*20 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*20] ]
               + d_Ht_const_mat_s[tx + N*21] * s_track[ d_Ht_const_mat_X[tx + N*21] ] * s_track[ d_Ht_const_mat_X[tx + N*21 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*21 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*21] ]
               + d_Ht_const_mat_s[tx + N*22] * s_track[ d_Ht_const_mat_X[tx + N*22] ] * s_track[ d_Ht_const_mat_X[tx + N*22 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*22 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*22] ]
               + d_Ht_const_mat_s[tx + N*23] * s_track[ d_Ht_const_mat_X[tx + N*23] ] * s_track[ d_Ht_const_mat_X[tx + N*23 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*23 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*23] ]
               + d_Ht_const_mat_s[tx + N*24] * s_track[ d_Ht_const_mat_X[tx + N*24] ] * s_track[ d_Ht_const_mat_X[tx + N*24 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*24 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*24] ]
               + d_Ht_const_mat_s[tx + N*25] * s_track[ d_Ht_const_mat_X[tx + N*25] ] * s_track[ d_Ht_const_mat_X[tx + N*25 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*25 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*25] ]
               + d_Ht_const_mat_s[tx + N*26] * s_track[ d_Ht_const_mat_X[tx + N*26] ] * s_track[ d_Ht_const_mat_X[tx + N*26 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*26 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*26] ]
               + d_Ht_const_mat_s[tx + N*27] * s_track[ d_Ht_const_mat_X[tx + N*27] ] * s_track[ d_Ht_const_mat_X[tx + N*27 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*27 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*27] ]
               + d_Ht_const_mat_s[tx + N*28] * s_track[ d_Ht_const_mat_X[tx + N*28] ] * s_track[ d_Ht_const_mat_X[tx + N*28 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*28 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*28] ]
               + d_Ht_const_mat_s[tx + N*29] * s_track[ d_Ht_const_mat_X[tx + N*29] ] * s_track[ d_Ht_const_mat_X[tx + N*29 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*29 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*29] ]
               + d_Ht_const_mat_s[tx + N*30] * s_track[ d_Ht_const_mat_X[tx + N*30] ] * s_track[ d_Ht_const_mat_X[tx + N*30 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*30 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*30] ]
               + d_Ht_const_mat_s[tx + N*31] * s_track[ d_Ht_const_mat_X[tx + N*31] ] * s_track[ d_Ht_const_mat_X[tx + N*31 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*31 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*31] ]
               + d_Ht_const_mat_s[tx + N*32] * s_track[ d_Ht_const_mat_X[tx + N*32] ] * s_track[ d_Ht_const_mat_X[tx + N*32 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*32 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*32] ]
               + d_Ht_const_mat_s[tx + N*33] * s_track[ d_Ht_const_mat_X[tx + N*33] ] * s_track[ d_Ht_const_mat_X[tx + N*33 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*33 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*33] ]
               + d_Ht_const_mat_s[tx + N*34] * s_track[ d_Ht_const_mat_X[tx + N*34] ] * s_track[ d_Ht_const_mat_X[tx + N*34 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*34 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*34] ]
               + d_Ht_const_mat_s[tx + N*35] * s_track[ d_Ht_const_mat_X[tx + N*35] ] * s_track[ d_Ht_const_mat_X[tx + N*35 + 648] ] * s_track[ d_Ht_const_mat_X[tx + N*35 + 1296] ] * s_cdt[ d_Ht_const_mat_Y[tx + N*35] ];
    }
}

#endif

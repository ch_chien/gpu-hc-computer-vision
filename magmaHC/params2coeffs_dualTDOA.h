#ifndef params2coeffs_dualTDOA_h
#define params2coeffs_dualTDOA_h
// =======================================================================
//
// Modifications
//    Chien  21-10-12:   Initially Created
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min

namespace magmaHCWrapper {

  void params2coeffs_dualTDOA(magmaFloatComplex *h_target_params, magmaFloatComplex *h_target_coeffs)
  {
    magmaFloatComplex d6 = h_target_params[0];
    magmaFloatComplex d7 = h_target_params[1];
    magmaFloatComplex d8 = h_target_params[2];
    magmaFloatComplex d9 = h_target_params[3];
    magmaFloatComplex d10 = h_target_params[4];
    magmaFloatComplex d11 = h_target_params[5];
    magmaFloatComplex d12 = h_target_params[6];
    magmaFloatComplex d13 = h_target_params[7];
    magmaFloatComplex d14 = h_target_params[8];
    magmaFloatComplex d15 = h_target_params[9];
    magmaFloatComplex d16 = h_target_params[10];
    magmaFloatComplex d17 = h_target_params[11];
    magmaFloatComplex d18 = h_target_params[12];
    magmaFloatComplex d19 = h_target_params[13];
    magmaFloatComplex d20 = h_target_params[14];
    magmaFloatComplex d21 = h_target_params[15];
    magmaFloatComplex d22 = h_target_params[16];
    magmaFloatComplex d23 = h_target_params[17];
    magmaFloatComplex d24 = h_target_params[18];
    magmaFloatComplex d25 = h_target_params[19];

    h_target_coeffs[0] = (-d21*d21 + MAGMA_C_ONE);
    h_target_coeffs[1] = (-d21*d21);
    h_target_coeffs[2] = MAGMA_C_ONE;
    h_target_coeffs[3] = (d6*d21*d21 - d6);
    h_target_coeffs[4] = (-d7);
    h_target_coeffs[5] = (-d8);
    h_target_coeffs[6] = (d6*d21*d21);
    h_target_coeffs[7] = (d7*d21*d21);
    h_target_coeffs[8] = (-d6);
    h_target_coeffs[9] = (d7*d21*d21 - d7);
    h_target_coeffs[10] = (d8*d21*d21);
    h_target_coeffs[11] = (d8*d21*d21 - d8);
    h_target_coeffs[12] = (-d6*d6*d21*d21 + d6*d6 - d7*d7*d21*d21 - d8*d8*d21*d21);
    h_target_coeffs[13] = (d6*d7);
    h_target_coeffs[14] = (d6*d8);
    h_target_coeffs[15] = (-d6*d6*d21*d21 - d7*d7*d21*d21 + d7*d7 - d8*d8*d21*d21);
    h_target_coeffs[16] = (d7*d8);
    h_target_coeffs[17] = (-d6*d6*d21*d21 - d7*d7*d21*d21 - d8*d8*d21*d21 + d8*d8);
    h_target_coeffs[18] = (-d22*d22 + MAGMA_C_ONE);
    h_target_coeffs[19] = (-d22*d22);
    h_target_coeffs[20] = (d9*d22*d22 - d9);
    h_target_coeffs[21] = (-d10);
    h_target_coeffs[22] = (-d11);
    h_target_coeffs[23] = (d9*d22*d22);
    h_target_coeffs[24] = (d10*d22*d22);
    h_target_coeffs[25] = (-d9);
    h_target_coeffs[26] = (d10*d22*d22 - d10);
    h_target_coeffs[27] = (d11*d22*d22);
    h_target_coeffs[28] = (d11*d22*d22 - d11);
    h_target_coeffs[29] = (-d9*d9*d22*d22 + d9*d9 - d10*d10*d22*d22 - d11*d11*d22*d22);
    h_target_coeffs[30] = (d9*d10);
    h_target_coeffs[31] = (d9*d11);
    h_target_coeffs[32] = (-d9*d9*d22*d22 - d10*d10*d22*d22 + d10*d10 - d11*d11*d22*d22);
    h_target_coeffs[33] = (-d9*d9*d22*d22 - d10*d10*d22*d22 - d11*d11*d22*d22 + d11*d11);
    h_target_coeffs[34] = (-d23*d23 + MAGMA_C_ONE);
    h_target_coeffs[35] = (-d23*d23);
    h_target_coeffs[36] = (d12*d23*d23 - d12);
    h_target_coeffs[37] = (-d13);
    h_target_coeffs[38] = (-d14);
    h_target_coeffs[39] = (d12*d23*d23);
    h_target_coeffs[40] = (-d23*d23 + MAGMA_C_ONE);
    h_target_coeffs[41] = (d13*d23*d23);
    h_target_coeffs[42] = (-d12);
    h_target_coeffs[43] = (d13*d23*d23 - d13);
    h_target_coeffs[44] = (d14*d23*d23);
    h_target_coeffs[45] = (d14*d23*d23 - d14);
    h_target_coeffs[46] = (-d12*d12*d23*d23 + d12*d12 - d13*d13*d23*d23 - d14*d14*d23*d23);
    h_target_coeffs[47] = (d12*d13);
    h_target_coeffs[48] = (d12*d14);
    h_target_coeffs[49] = (-d12*d12*d23*d23 - d13*d13*d23*d23 + d13*d13 - d14*d14*d23*d23);
    h_target_coeffs[50] = (d13*d14);
    h_target_coeffs[51] = (-d12*d12*d23*d23 - d13*d13*d23*d23 - d14*d14*d23*d23 + d14*d14);
    h_target_coeffs[52] = (-d24*d24 + MAGMA_C_ONE);
    h_target_coeffs[53] = (-d24*d24);
    h_target_coeffs[54] = (d15*d24*d24 - d15);
    h_target_coeffs[55] = (-d16);
    h_target_coeffs[56] = (-d17);
    h_target_coeffs[57] = (d15*d24*d24);
    h_target_coeffs[58] = (d16*d24*d24);
    h_target_coeffs[59] = (-d15);
    h_target_coeffs[60] = (d16*d24*d24 - d16);
    h_target_coeffs[61] = (-d16*d24*d24);
    h_target_coeffs[62] = (d17*d24*d24);
    h_target_coeffs[63] = (d17*d24*d24 - d17);
    h_target_coeffs[64] = (-d15*d15*d24*d24 + d15*d15 - d16*d16*d24*d24 - d17*d17*d24*d24);
    h_target_coeffs[65] = (d15*d16);
    h_target_coeffs[66] = (d15*d17);
    h_target_coeffs[67] = (-d15*d15*d24*d24 - d16*d16*d24*d24 + d16*d16 - d17*d17*d24*d24);
    h_target_coeffs[68] = (d16*d17);
    h_target_coeffs[69] = (-d15*d15*d24*d24 - d16*d16*d24*d24 - d17*d17*d24*d24 + d17*d17);
    h_target_coeffs[70] = (-d25*d25 + MAGMA_C_ONE);
    h_target_coeffs[71] = (-d25*d25);
    h_target_coeffs[72] = (d18*d25*d25 - d18);
    h_target_coeffs[73] = (-d19);
    h_target_coeffs[74] = (-d20);
    h_target_coeffs[75] = (d18*d25*d25);
    h_target_coeffs[76] = (d19*d25*d25);
    h_target_coeffs[77] = (-d18);
    h_target_coeffs[78] = (d19*d25*d25 - d19);
    h_target_coeffs[79] = (d20*d25*d25);
    h_target_coeffs[80] = (d20*d25*d25 - d20);
    h_target_coeffs[81] = (-d18*d18*d25*d25 + d18*d18 - d19*d19*d25*d25 - d20*d20*d25*d25);
    h_target_coeffs[82] = (d18*d19);
    h_target_coeffs[83] = (d18*d20);
    h_target_coeffs[84] = (-d18*d18*d25*d25 - d19*d19*d25*d25 + d19*d19 - d20*d20*d25*d25);
    h_target_coeffs[85] = (d19*d20);
    h_target_coeffs[86] = (-d18*d18*d25*d25 - d19*d19*d25*d25 - d20*d20*d25*d25 + d20*d20);
    h_target_coeffs[87] = (d10*d11);
  }
} // end of namespace

#endif

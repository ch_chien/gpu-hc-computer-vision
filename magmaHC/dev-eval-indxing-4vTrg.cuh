#ifndef dev_eval_indxing_4vTrg_cuh_
#define dev_eval_indxing_4vTrg_cuh_
// ============================================================================
// Device function for evaluating the parallel indexing for Hx, Ht, and H of
// 4vTrg problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// -- cuda included --
#include <cuda_runtime.h>

// -- magma included --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

namespace magmaHCWrapper {

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_cdt_4vTrg(
        const int tx, float t, magmaFloatComplex s_vector_cdt[coefsCount],
        magmaFloatComplex r_startCoefs[coefsCount], magmaFloatComplex r_targetCoefs[coefsCount])
    {
        #pragma unroll
        for (int i = 0; i < 4; i++) {
            s_vector_cdt[ tx + i * N ] = r_targetCoefs[tx + i * N] * t - r_startCoefs[tx + i * N] * (t-1);
        }
        if (tx < 8) {
          s_vector_cdt[ tx + 4 * N ] = r_targetCoefs[tx + 4 * N] * t - r_startCoefs[tx + 4 * N] * (t-1);
        }
    }

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_Jacobian_4vTrg(
        const int tx, magmaFloatComplex *s_track, magmaFloatComplex r_cgesvA[N], const magma_int_t* __restrict__ d_const_mat_s,
        const magma_int_t* __restrict__ d_const_mat_X, const magma_int_t* __restrict__ d_const_mat_Y,
        magmaFloatComplex s_cdt[coefsCount])
    {
        // -- the maximal terms of Hx in 4vTrg problem is 6 --
        #pragma unroll
        for(int i = 0; i < N; i++) {
          r_cgesvA[i] = d_const_mat_s[tx + i*14] * s_track[ d_const_mat_X[tx + i*14] ] * s_cdt[ d_const_mat_Y[tx + i*14] ]
                      + d_const_mat_s[tx + i*14 + 196] * s_track[ d_const_mat_X[tx + i*14 + 196] ] * s_cdt[ d_const_mat_Y[tx + i*14 + 196] ]
                      + d_const_mat_s[tx + i*14 + 392] * s_track[ d_const_mat_X[tx + i*14 + 392] ] * s_cdt[ d_const_mat_Y[tx + i*14 + 392] ];
        }
    }

    __device__ __inline__ void
    eval_Ht_4vTrg(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_Ht_const_mat_X,
      const magma_int_t* __restrict__ d_Ht_const_mat_Y, const magmaFloatComplex* __restrict__ d_const_cd)
    {
      // -- the maximal terms of Ht in 4vTrg problem is 11 --
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_Ht_const_mat_X[tx] ] * s_track[ d_Ht_const_mat_X[tx + 154] ] * d_const_cd[ d_Ht_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + 14] * s_track[ d_Ht_const_mat_X[tx + 14] ] * s_track[ d_Ht_const_mat_X[tx + 14 + 154] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 14] ]
               + d_Ht_const_mat_s[tx + 28] * s_track[ d_Ht_const_mat_X[tx + 28] ] * s_track[ d_Ht_const_mat_X[tx + 28 + 154] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 28] ]
               + d_Ht_const_mat_s[tx + 42] * s_track[ d_Ht_const_mat_X[tx + 42] ] * s_track[ d_Ht_const_mat_X[tx + 42 + 154] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 42] ]
               + d_Ht_const_mat_s[tx + 56] * s_track[ d_Ht_const_mat_X[tx + 56] ] * s_track[ d_Ht_const_mat_X[tx + 56 + 154] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 56] ]
               + d_Ht_const_mat_s[tx + 70] * s_track[ d_Ht_const_mat_X[tx + 70] ] * s_track[ d_Ht_const_mat_X[tx + 70 + 154] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 70] ]
               + d_Ht_const_mat_s[tx + 84] * s_track[ d_Ht_const_mat_X[tx + 84] ] * s_track[ d_Ht_const_mat_X[tx + 84 + 154] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 84] ]
               + d_Ht_const_mat_s[tx + 98] * s_track[ d_Ht_const_mat_X[tx + 98] ] * s_track[ d_Ht_const_mat_X[tx + 98 + 154] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 98] ]
               + d_Ht_const_mat_s[tx + 112] * s_track[ d_Ht_const_mat_X[tx + 112] ] * s_track[ d_Ht_const_mat_X[tx + 112 + 154] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 112] ]
               + d_Ht_const_mat_s[tx + 126] * s_track[ d_Ht_const_mat_X[tx + 126] ] * s_track[ d_Ht_const_mat_X[tx + 126 + 154] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 126] ]
               + d_Ht_const_mat_s[tx + 140] * s_track[ d_Ht_const_mat_X[tx + 140] ] * s_track[ d_Ht_const_mat_X[tx + 140 + 154] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 140] ];
    }

    template<int coefsCount>
    __device__ __inline__ void
    eval_H_4vTrg(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_H_const_mat_X,
      const magma_int_t* __restrict__ d_H_const_mat_Y, magmaFloatComplex s_cdt[coefsCount])
    {
      // -- same as Ht, except replacing cd by cdt --
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_H_const_mat_X[tx] ] * s_track[ d_H_const_mat_X[tx + 154] ] * s_cdt[ d_H_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + 14] * s_track[ d_H_const_mat_X[tx + 14] ] * s_track[ d_H_const_mat_X[tx + 14 + 154] ] * s_cdt[ d_H_const_mat_Y[tx + 14] ]
               + d_Ht_const_mat_s[tx + 28] * s_track[ d_H_const_mat_X[tx + 28] ] * s_track[ d_H_const_mat_X[tx + 28 + 154] ] * s_cdt[ d_H_const_mat_Y[tx + 28] ]
               + d_Ht_const_mat_s[tx + 42] * s_track[ d_H_const_mat_X[tx + 42] ] * s_track[ d_H_const_mat_X[tx + 42 + 154] ] * s_cdt[ d_H_const_mat_Y[tx + 42] ]
               + d_Ht_const_mat_s[tx + 56] * s_track[ d_H_const_mat_X[tx + 56] ] * s_track[ d_H_const_mat_X[tx + 56 + 154] ] * s_cdt[ d_H_const_mat_Y[tx + 56] ]
               + d_Ht_const_mat_s[tx + 70] * s_track[ d_H_const_mat_X[tx + 70] ] * s_track[ d_H_const_mat_X[tx + 70 + 154] ] * s_cdt[ d_H_const_mat_Y[tx + 70] ]
               + d_Ht_const_mat_s[tx + 84] * s_track[ d_H_const_mat_X[tx + 84] ] * s_track[ d_H_const_mat_X[tx + 84 + 154] ] * s_cdt[ d_H_const_mat_Y[tx + 84] ]
               + d_Ht_const_mat_s[tx + 98] * s_track[ d_H_const_mat_X[tx + 98] ] * s_track[ d_H_const_mat_X[tx + 98 + 154] ] * s_cdt[ d_H_const_mat_Y[tx + 98] ]
               + d_Ht_const_mat_s[tx + 112] * s_track[ d_H_const_mat_X[tx + 112] ] * s_track[ d_H_const_mat_X[tx + 112 + 154] ] * s_cdt[ d_H_const_mat_Y[tx + 112] ]
               + d_Ht_const_mat_s[tx + 126] * s_track[ d_H_const_mat_X[tx + 126] ] * s_track[ d_H_const_mat_X[tx + 126 + 154] ] * s_cdt[ d_H_const_mat_Y[tx + 126] ]
               + d_Ht_const_mat_s[tx + 140] * s_track[ d_H_const_mat_X[tx + 140] ] * s_track[ d_H_const_mat_X[tx + 140 + 154] ] * s_cdt[ d_H_const_mat_Y[tx + 140] ];
    }
}

#endif

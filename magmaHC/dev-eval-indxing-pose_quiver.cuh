#ifndef dev_eval_indxing_pose_quiver_cuh_
#define dev_eval_indxing_pose_quiver_cuh_
// ============================================================================
// Device function for evaluating the parallel indexing for Hx, Ht, and H of
// pose_quiver problem
//
// Modifications
//    Chien  21-10-26:   Originally created
//
// ============================================================================
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// -- cuda included --
#include <cuda_runtime.h>

// -- magma included --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

namespace magmaHCWrapper {

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_cdt_pose_quiver(
        const int tx, float t, magmaFloatComplex s_vector_cdt[coefsCount],
        magmaFloatComplex r_startCoefs[coefsCount], magmaFloatComplex r_targetCoefs[coefsCount])
    {
        #pragma unroll
        for (int i = 0; i < 14; i++) {
            s_vector_cdt[ tx + i * N ] = r_targetCoefs[tx + i * N] * t - r_startCoefs[tx + i * N] * (t-1);
        }
    }

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_Jacobian_pose_quiver(
        const int tx, magmaFloatComplex *s_track, magmaFloatComplex r_cgesvA[N], const magma_int_t* __restrict__ d_const_mat_s,
        const magma_int_t* __restrict__ d_const_mat_X, const magma_int_t* __restrict__ d_const_mat_Y,
        magmaFloatComplex s_cdt[coefsCount])
    {
        // -- the maximal terms of Hx in pose_quiver problem is 8 --
        // -- 128 = 4(vars) * 4(vars) * 8(terms) --
        #pragma unroll
        for(int i = 0; i < N; i++) {
          r_cgesvA[i] = d_const_mat_s[tx + i*N] * s_track[ d_const_mat_X[tx + i*N] ] * s_track[ d_const_mat_X[tx + i*N + 128] ] * s_cdt[ d_const_mat_Y[tx + i*N] ]
                      + d_const_mat_s[tx + i*N + N*N] * s_track[ d_const_mat_X[tx + i*N + N*N] ] * s_track[ d_const_mat_X[tx + i*N + N*N + 128] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N] ]
                      + d_const_mat_s[tx + i*N + N*N*2] * s_track[ d_const_mat_X[tx + i*N + N*N*2] ] * s_track[ d_const_mat_X[tx + i*N + N*N*2 + 128] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*2] ]
                      + d_const_mat_s[tx + i*N + N*N*3] * s_track[ d_const_mat_X[tx + i*N + N*N*3] ] * s_track[ d_const_mat_X[tx + i*N + N*N*3 + 128] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*3] ]
                      + d_const_mat_s[tx + i*N + N*N*4] * s_track[ d_const_mat_X[tx + i*N + N*N*4] ] * s_track[ d_const_mat_X[tx + i*N + N*N*4 + 128] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*4] ]
                      + d_const_mat_s[tx + i*N + N*N*5] * s_track[ d_const_mat_X[tx + i*N + N*N*5] ] * s_track[ d_const_mat_X[tx + i*N + N*N*5 + 128] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*5] ]
                      + d_const_mat_s[tx + i*N + N*N*6] * s_track[ d_const_mat_X[tx + i*N + N*N*6] ] * s_track[ d_const_mat_X[tx + i*N + N*N*6 + 128] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*6] ]
                      + d_const_mat_s[tx + i*N + N*N*7] * s_track[ d_const_mat_X[tx + i*N + N*N*7] ] * s_track[ d_const_mat_X[tx + i*N + N*N*7 + 128] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*7] ];
        }
    }

    template<int N>
    __device__ __inline__ void
    eval_Ht_pose_quiver(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_Ht_const_mat_X,
      const magma_int_t* __restrict__ d_Ht_const_mat_Y, const magmaFloatComplex* __restrict__ d_const_cd)
    {
      // -- the maximal terms of Ht in pose_quiver problem is 18 --
      // -- the maximal parts for pose_quiver problem is 3 --
      // -- N*maximal terms of Ht = 4*18 = 72
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_Ht_const_mat_X[tx] ] * s_track[ d_Ht_const_mat_X[tx + 72] ] * s_track[ d_Ht_const_mat_X[tx + 144] ] * d_const_cd[ d_Ht_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + N] * s_track[ d_Ht_const_mat_X[tx + N] ] * s_track[ d_Ht_const_mat_X[tx + N + 72] ] * s_track[ d_Ht_const_mat_X[tx + N + 144] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N] ]
               + d_Ht_const_mat_s[tx + N*2] * s_track[ d_Ht_const_mat_X[tx + N*2] ] * s_track[ d_Ht_const_mat_X[tx + N*2 + 72] ] * s_track[ d_Ht_const_mat_X[tx + N*2 + 144] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*2] ]
               + d_Ht_const_mat_s[tx + N*3] * s_track[ d_Ht_const_mat_X[tx + N*3] ] * s_track[ d_Ht_const_mat_X[tx + N*3 + 72] ] * s_track[ d_Ht_const_mat_X[tx + N*3 + 144] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*3] ]
               + d_Ht_const_mat_s[tx + N*4] * s_track[ d_Ht_const_mat_X[tx + N*4] ] * s_track[ d_Ht_const_mat_X[tx + N*4 + 72] ] * s_track[ d_Ht_const_mat_X[tx + N*4 + 144] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*4] ]
               + d_Ht_const_mat_s[tx + N*5] * s_track[ d_Ht_const_mat_X[tx + N*5] ] * s_track[ d_Ht_const_mat_X[tx + N*5 + 72] ] * s_track[ d_Ht_const_mat_X[tx + N*5 + 144] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*5] ]
               + d_Ht_const_mat_s[tx + N*6] * s_track[ d_Ht_const_mat_X[tx + N*6] ] * s_track[ d_Ht_const_mat_X[tx + N*6 + 72] ] * s_track[ d_Ht_const_mat_X[tx + N*6 + 144] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*6] ]
               + d_Ht_const_mat_s[tx + N*7] * s_track[ d_Ht_const_mat_X[tx + N*7] ] * s_track[ d_Ht_const_mat_X[tx + N*7 + 72] ] * s_track[ d_Ht_const_mat_X[tx + N*7 + 144] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*7] ]
               + d_Ht_const_mat_s[tx + N*8] * s_track[ d_Ht_const_mat_X[tx + N*8] ] * s_track[ d_Ht_const_mat_X[tx + N*8 + 72] ] * s_track[ d_Ht_const_mat_X[tx + N*8 + 144] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*8] ]
               + d_Ht_const_mat_s[tx + N*9] * s_track[ d_Ht_const_mat_X[tx + N*9] ] * s_track[ d_Ht_const_mat_X[tx + N*9 + 72] ] * s_track[ d_Ht_const_mat_X[tx + N*9 + 144] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*9] ]
               + d_Ht_const_mat_s[tx + N*10] * s_track[ d_Ht_const_mat_X[tx + N*10] ] * s_track[ d_Ht_const_mat_X[tx + N*10 + 72] ] * s_track[ d_Ht_const_mat_X[tx + N*10 + 144] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*10] ]
               + d_Ht_const_mat_s[tx + N*11] * s_track[ d_Ht_const_mat_X[tx + N*11] ] * s_track[ d_Ht_const_mat_X[tx + N*11 + 72] ] * s_track[ d_Ht_const_mat_X[tx + N*11 + 144] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*11] ]
               + d_Ht_const_mat_s[tx + N*12] * s_track[ d_Ht_const_mat_X[tx + N*12] ] * s_track[ d_Ht_const_mat_X[tx + N*12 + 72] ] * s_track[ d_Ht_const_mat_X[tx + N*12 + 144] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*12] ]
               + d_Ht_const_mat_s[tx + N*13] * s_track[ d_Ht_const_mat_X[tx + N*13] ] * s_track[ d_Ht_const_mat_X[tx + N*13 + 72] ] * s_track[ d_Ht_const_mat_X[tx + N*13 + 144] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*13] ]
               + d_Ht_const_mat_s[tx + N*14] * s_track[ d_Ht_const_mat_X[tx + N*14] ] * s_track[ d_Ht_const_mat_X[tx + N*14 + 72] ] * s_track[ d_Ht_const_mat_X[tx + N*14 + 144] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*14] ]
               + d_Ht_const_mat_s[tx + N*15] * s_track[ d_Ht_const_mat_X[tx + N*15] ] * s_track[ d_Ht_const_mat_X[tx + N*15 + 72] ] * s_track[ d_Ht_const_mat_X[tx + N*15 + 144] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*15] ]
               + d_Ht_const_mat_s[tx + N*16] * s_track[ d_Ht_const_mat_X[tx + N*16] ] * s_track[ d_Ht_const_mat_X[tx + N*16 + 72] ] * s_track[ d_Ht_const_mat_X[tx + N*16 + 144] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*16] ]
               + d_Ht_const_mat_s[tx + N*17] * s_track[ d_Ht_const_mat_X[tx + N*17] ] * s_track[ d_Ht_const_mat_X[tx + N*17 + 72] ] * s_track[ d_Ht_const_mat_X[tx + N*17 + 144] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*17] ];
    }

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_H_pose_quiver(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_H_const_mat_X,
      const magma_int_t* __restrict__ d_H_const_mat_Y, magmaFloatComplex s_cdt[coefsCount])
    {
      // -- same as Ht, except replacing cd by cdt --
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_H_const_mat_X[tx] ] * s_track[ d_H_const_mat_X[tx + 72] ] * s_track[ d_H_const_mat_X[tx + 144] ] * s_cdt[ d_H_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + N] * s_track[ d_H_const_mat_X[tx + N] ] * s_track[ d_H_const_mat_X[tx + N + 72] ] * s_track[ d_H_const_mat_X[tx + N + 144] ] * s_cdt[ d_H_const_mat_Y[tx + N] ]
               + d_Ht_const_mat_s[tx + N*2] * s_track[ d_H_const_mat_X[tx + N*2] ] * s_track[ d_H_const_mat_X[tx + N*2 + 72] ] * s_track[ d_H_const_mat_X[tx + N*2 + 144] ] * s_cdt[ d_H_const_mat_Y[tx + N*2] ]
               + d_Ht_const_mat_s[tx + N*3] * s_track[ d_H_const_mat_X[tx + N*3] ] * s_track[ d_H_const_mat_X[tx + N*3 + 72] ] * s_track[ d_H_const_mat_X[tx + N*3 + 144] ] * s_cdt[ d_H_const_mat_Y[tx + N*3] ]
               + d_Ht_const_mat_s[tx + N*4] * s_track[ d_H_const_mat_X[tx + N*4] ] * s_track[ d_H_const_mat_X[tx + N*4 + 72] ] * s_track[ d_H_const_mat_X[tx + N*4 + 144] ] * s_cdt[ d_H_const_mat_Y[tx + N*4] ]
               + d_Ht_const_mat_s[tx + N*5] * s_track[ d_H_const_mat_X[tx + N*5] ] * s_track[ d_H_const_mat_X[tx + N*5 + 72] ] * s_track[ d_H_const_mat_X[tx + N*5 + 144] ] * s_cdt[ d_H_const_mat_Y[tx + N*5] ]
               + d_Ht_const_mat_s[tx + N*6] * s_track[ d_H_const_mat_X[tx + N*6] ] * s_track[ d_H_const_mat_X[tx + N*6 + 72] ] * s_track[ d_H_const_mat_X[tx + N*6 + 144] ] * s_cdt[ d_H_const_mat_Y[tx + N*6] ]
               + d_Ht_const_mat_s[tx + N*7] * s_track[ d_H_const_mat_X[tx + N*7] ] * s_track[ d_H_const_mat_X[tx + N*7 + 72] ] * s_track[ d_H_const_mat_X[tx + N*7 + 144] ] * s_cdt[ d_H_const_mat_Y[tx + N*7] ]
               + d_Ht_const_mat_s[tx + N*8] * s_track[ d_H_const_mat_X[tx + N*8] ] * s_track[ d_H_const_mat_X[tx + N*8 + 72] ] * s_track[ d_H_const_mat_X[tx + N*8 + 144] ] * s_cdt[ d_H_const_mat_Y[tx + N*8] ]
               + d_Ht_const_mat_s[tx + N*9] * s_track[ d_H_const_mat_X[tx + N*9] ] * s_track[ d_H_const_mat_X[tx + N*9 + 72] ] * s_track[ d_H_const_mat_X[tx + N*9 + 144] ] * s_cdt[ d_H_const_mat_Y[tx + N*9] ]
               + d_Ht_const_mat_s[tx + N*10] * s_track[ d_H_const_mat_X[tx + N*10] ] * s_track[ d_H_const_mat_X[tx + N*10 + 72] ] * s_track[ d_H_const_mat_X[tx + N*10 + 144] ] * s_cdt[ d_H_const_mat_Y[tx + N*10] ]
               + d_Ht_const_mat_s[tx + N*11] * s_track[ d_H_const_mat_X[tx + N*11] ] * s_track[ d_H_const_mat_X[tx + N*11 + 72] ] * s_track[ d_H_const_mat_X[tx + N*11 + 144] ] * s_cdt[ d_H_const_mat_Y[tx + N*11] ]
               + d_Ht_const_mat_s[tx + N*12] * s_track[ d_H_const_mat_X[tx + N*12] ] * s_track[ d_H_const_mat_X[tx + N*12 + 72] ] * s_track[ d_H_const_mat_X[tx + N*12 + 144] ] * s_cdt[ d_H_const_mat_Y[tx + N*12] ]
               + d_Ht_const_mat_s[tx + N*13] * s_track[ d_H_const_mat_X[tx + N*13] ] * s_track[ d_H_const_mat_X[tx + N*13 + 72] ] * s_track[ d_H_const_mat_X[tx + N*13 + 144] ] * s_cdt[ d_H_const_mat_Y[tx + N*13] ]
               + d_Ht_const_mat_s[tx + N*14] * s_track[ d_H_const_mat_X[tx + N*14] ] * s_track[ d_H_const_mat_X[tx + N*14 + 72] ] * s_track[ d_H_const_mat_X[tx + N*14 + 144] ] * s_cdt[ d_H_const_mat_Y[tx + N*14] ]
               + d_Ht_const_mat_s[tx + N*15] * s_track[ d_H_const_mat_X[tx + N*15] ] * s_track[ d_H_const_mat_X[tx + N*15 + 72] ] * s_track[ d_H_const_mat_X[tx + N*15 + 144] ] * s_cdt[ d_H_const_mat_Y[tx + N*15] ]
               + d_Ht_const_mat_s[tx + N*16] * s_track[ d_H_const_mat_X[tx + N*16] ] * s_track[ d_H_const_mat_X[tx + N*16 + 72] ] * s_track[ d_H_const_mat_X[tx + N*16 + 144] ] * s_cdt[ d_H_const_mat_Y[tx + N*16] ]
               + d_Ht_const_mat_s[tx + N*17] * s_track[ d_H_const_mat_X[tx + N*17] ] * s_track[ d_H_const_mat_X[tx + N*17 + 72] ] * s_track[ d_H_const_mat_X[tx + N*17 + 144] ] * s_cdt[ d_H_const_mat_Y[tx + N*17] ];
    }
}

#endif

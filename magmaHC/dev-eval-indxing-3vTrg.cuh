#ifndef dev_eval_indxing_3vTrg_cuh_
#define dev_eval_indxing_3vTrg_cuh_
// ============================================================================
// Device function for evaluating the parallel indexing for Hx, Ht, and H of
// 3vTrg problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// -- cuda included --
#include <cuda_runtime.h>

// -- magma included --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

namespace magmaHCWrapper {

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_cdt_3vTrg(
        const int tx, float t, magmaFloatComplex s_vector_cdt[coefsCount],
        magmaFloatComplex r_startCoefs[coefsCount], magmaFloatComplex r_targetCoefs[coefsCount])
    {
        #pragma unroll
        for (int i = 0; i < 3; i++) {
            s_vector_cdt[ tx + i * N ] = r_targetCoefs[tx + i * N] * t - r_startCoefs[tx + i * N] * (t-1);
        }
        if (tx < 7) {
          s_vector_cdt[ tx + 3 * N ] = r_targetCoefs[tx + 3 * N] * t - r_startCoefs[tx + 3 * N] * (t-1);
        }
    }

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_Jacobian_3vTrg(
        const int tx, magmaFloatComplex *s_track, magmaFloatComplex r_cgesvA[N], const magma_int_t* __restrict__ d_const_mat_s,
        const magma_int_t* __restrict__ d_const_mat_X, const magma_int_t* __restrict__ d_const_mat_Y,
        magmaFloatComplex s_cdt[coefsCount])
    {
        // -- the maximal terms of Hx in 3vTrg problem is 6 --
        #pragma unroll
        for(int i = 0; i < N; i++) {
          r_cgesvA[i] = d_const_mat_s[tx + i*9] * s_track[ d_const_mat_X[tx + i*9] ] * s_cdt[ d_const_mat_Y[tx + i*9] ]
                      + d_const_mat_s[tx + i*9 + 81] * s_track[ d_const_mat_X[tx + i*9 + 81] ] * s_cdt[ d_const_mat_Y[tx + i*9 + 81] ]
                      + d_const_mat_s[tx + i*9 + 162] * s_track[ d_const_mat_X[tx + i*9 + 162] ] * s_cdt[ d_const_mat_Y[tx + i*9 + 162] ];
        }
    }

    __device__ __inline__ void
    eval_Ht_3vTrg(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_Ht_const_mat_X,
      const magma_int_t* __restrict__ d_Ht_const_mat_Y, const magmaFloatComplex* __restrict__ d_const_cd)
    {
      // -- the maximal terms of Ht in 3vTrg problem is 9 --
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_Ht_const_mat_X[tx] ] * s_track[ d_Ht_const_mat_X[tx + 81] ] * d_const_cd[ d_Ht_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + 9] * s_track[ d_Ht_const_mat_X[tx + 9] ] * s_track[ d_Ht_const_mat_X[tx + 9 + 81] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 9] ]
               + d_Ht_const_mat_s[tx + 18] * s_track[ d_Ht_const_mat_X[tx + 18] ] * s_track[ d_Ht_const_mat_X[tx + 18 + 81] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 18] ]
               + d_Ht_const_mat_s[tx + 27] * s_track[ d_Ht_const_mat_X[tx + 27] ] * s_track[ d_Ht_const_mat_X[tx + 27 + 81] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 27] ]
               + d_Ht_const_mat_s[tx + 36] * s_track[ d_Ht_const_mat_X[tx + 36] ] * s_track[ d_Ht_const_mat_X[tx + 36 + 81] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 36] ]
               + d_Ht_const_mat_s[tx + 45] * s_track[ d_Ht_const_mat_X[tx + 45] ] * s_track[ d_Ht_const_mat_X[tx + 45 + 81] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 45] ]
               + d_Ht_const_mat_s[tx + 54] * s_track[ d_Ht_const_mat_X[tx + 54] ] * s_track[ d_Ht_const_mat_X[tx + 54 + 81] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 54] ]
               + d_Ht_const_mat_s[tx + 63] * s_track[ d_Ht_const_mat_X[tx + 63] ] * s_track[ d_Ht_const_mat_X[tx + 63 + 81] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 63] ]
               + d_Ht_const_mat_s[tx + 72] * s_track[ d_Ht_const_mat_X[tx + 72] ] * s_track[ d_Ht_const_mat_X[tx + 72 + 81] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 72] ];
    }

    template<int coefsCount>
    __device__ __inline__ void
    eval_H_3vTrg(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_H_const_mat_X,
      const magma_int_t* __restrict__ d_H_const_mat_Y, magmaFloatComplex s_cdt[coefsCount])
    {
      // -- same as Ht, except replacing cd by cdt --
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_H_const_mat_X[tx] ] * s_track[ d_H_const_mat_X[tx + 81] ] * s_cdt[ d_H_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + 9] * s_track[ d_H_const_mat_X[tx + 9] ] * s_track[ d_H_const_mat_X[tx + 9 + 81] ] * s_cdt[ d_H_const_mat_Y[tx + 9] ]
               + d_Ht_const_mat_s[tx + 18] * s_track[ d_H_const_mat_X[tx + 18] ] * s_track[ d_H_const_mat_X[tx + 18 + 81] ] * s_cdt[ d_H_const_mat_Y[tx + 18] ]
               + d_Ht_const_mat_s[tx + 27] * s_track[ d_H_const_mat_X[tx + 27] ] * s_track[ d_H_const_mat_X[tx + 27 + 81] ] * s_cdt[ d_H_const_mat_Y[tx + 27] ]
               + d_Ht_const_mat_s[tx + 36] * s_track[ d_H_const_mat_X[tx + 36] ] * s_track[ d_H_const_mat_X[tx + 36 + 81] ] * s_cdt[ d_H_const_mat_Y[tx + 36] ]
               + d_Ht_const_mat_s[tx + 45] * s_track[ d_H_const_mat_X[tx + 45] ] * s_track[ d_H_const_mat_X[tx + 45 + 81] ] * s_cdt[ d_H_const_mat_Y[tx + 45] ]
               + d_Ht_const_mat_s[tx + 54] * s_track[ d_H_const_mat_X[tx + 54] ] * s_track[ d_H_const_mat_X[tx + 54 + 81] ] * s_cdt[ d_H_const_mat_Y[tx + 54] ]
               + d_Ht_const_mat_s[tx + 63] * s_track[ d_H_const_mat_X[tx + 63] ] * s_track[ d_H_const_mat_X[tx + 63 + 81] ] * s_cdt[ d_H_const_mat_Y[tx + 63] ]
               + d_Ht_const_mat_s[tx + 72] * s_track[ d_H_const_mat_X[tx + 72] ] * s_track[ d_H_const_mat_X[tx + 72 + 81] ] * s_cdt[ d_H_const_mat_Y[tx + 72] ];
    }
}

#endif

#ifndef dev_eval_indxing_5ptRelativePose_v2_cuh_
#define dev_eval_indxing_5ptRelativePose_v2_cuh_
// ============================================================================
// Device function for evaluating the parallel indexing for Hx, Ht, and H of
// 5ptRelativePose_v2 problem
//
// Modifications
//    Chien  21-11-07:   Originally created
//
// ============================================================================
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// -- cuda included --
#include <cuda_runtime.h>

// -- magma included --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

namespace magmaHCWrapper {

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_cdt_5ptRelativePose_v2(
        const int tx, float t, magmaFloatComplex s_vector_cdt[coefsCount],
        magmaFloatComplex r_startCoefs[coefsCount], magmaFloatComplex r_targetCoefs[coefsCount])
    {
        #pragma unroll
        for (int i = 0; i < 20; i++) {
            s_vector_cdt[ tx + i * N ] = r_targetCoefs[tx + i * N] * t - r_startCoefs[tx + i * N] * (t-1);
        }
        /*if (tx < 12) {
          s_vector_cdt[ tx + 32 ] = r_targetCoefs[tx + 32] * t - r_startCoefs[tx + 32] * (t-1);
        }*/
    }

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_Jacobian_5ptRelativePose_v2(
        const int tx, magmaFloatComplex *s_track, magmaFloatComplex r_cgesvA[N], const magma_int_t* __restrict__ d_const_mat_s,
        const magma_int_t* __restrict__ d_const_mat_X, const magma_int_t* __restrict__ d_const_mat_Y,
        magmaFloatComplex s_cdt[coefsCount])
    {
        // -- the maximal terms of Hx in 5ptRelativePose_v2 problem is 10 --
        // -- 90 = 3(vars) * 3(vars) * 10(terms) --
        #pragma unroll
        for(int i = 0; i < N; i++) {
          r_cgesvA[i] = d_const_mat_s[tx + i*N] * s_track[ d_const_mat_X[tx + i*N] ] * s_track[ d_const_mat_X[tx + i*N + 90] ] * s_cdt[ d_const_mat_Y[tx + i*N] ]
                      + d_const_mat_s[tx + i*N + N*N] * s_track[ d_const_mat_X[tx + i*N + N*N] ] * s_track[ d_const_mat_X[tx + i*N + N*N + 90] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N] ]
                      + d_const_mat_s[tx + i*N + N*N*2] * s_track[ d_const_mat_X[tx + i*N + N*N*2] ] * s_track[ d_const_mat_X[tx + i*N + N*N*2 + 90] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*2] ]
                      + d_const_mat_s[tx + i*N + N*N*3] * s_track[ d_const_mat_X[tx + i*N + N*N*3] ] * s_track[ d_const_mat_X[tx + i*N + N*N*3 + 90] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*3] ]
                      + d_const_mat_s[tx + i*N + N*N*4] * s_track[ d_const_mat_X[tx + i*N + N*N*4] ] * s_track[ d_const_mat_X[tx + i*N + N*N*4 + 90] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*4] ]
                      + d_const_mat_s[tx + i*N + N*N*5] * s_track[ d_const_mat_X[tx + i*N + N*N*5] ] * s_track[ d_const_mat_X[tx + i*N + N*N*5 + 90] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*5] ]
                      + d_const_mat_s[tx + i*N + N*N*6] * s_track[ d_const_mat_X[tx + i*N + N*N*6] ] * s_track[ d_const_mat_X[tx + i*N + N*N*6 + 90] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*6] ]
                      + d_const_mat_s[tx + i*N + N*N*7] * s_track[ d_const_mat_X[tx + i*N + N*N*7] ] * s_track[ d_const_mat_X[tx + i*N + N*N*7 + 90] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*7] ]
                      + d_const_mat_s[tx + i*N + N*N*8] * s_track[ d_const_mat_X[tx + i*N + N*N*8] ] * s_track[ d_const_mat_X[tx + i*N + N*N*8 + 90] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*8] ]
                      + d_const_mat_s[tx + i*N + N*N*9] * s_track[ d_const_mat_X[tx + i*N + N*N*9] ] * s_track[ d_const_mat_X[tx + i*N + N*N*9 + 90] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*9] ];
        }
    }

    template<int N>
    __device__ __inline__ void
    eval_Ht_5ptRelativePose_v2(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_Ht_const_mat_X,
      const magma_int_t* __restrict__ d_Ht_const_mat_Y, const magmaFloatComplex* __restrict__ d_const_cd)
    {
      // -- the maximal terms of Ht in 5ptRelativePose_v2 problem is 20 --
      // -- the maximal parts for 5ptRelativePose_v2 problem is 3 --
      // -- N*maximal terms of Ht = 3*20 = 60
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_Ht_const_mat_X[tx] ] * s_track[ d_Ht_const_mat_X[tx + 60] ] * s_track[ d_Ht_const_mat_X[tx + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + N] * s_track[ d_Ht_const_mat_X[tx + N] ] * s_track[ d_Ht_const_mat_X[tx + N + 60] ] * s_track[ d_Ht_const_mat_X[tx + N + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N] ]
               + d_Ht_const_mat_s[tx + N*2] * s_track[ d_Ht_const_mat_X[tx + N*2] ] * s_track[ d_Ht_const_mat_X[tx + N*2 + 60] ] * s_track[ d_Ht_const_mat_X[tx + N*2 + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*2] ]
               + d_Ht_const_mat_s[tx + N*3] * s_track[ d_Ht_const_mat_X[tx + N*3] ] * s_track[ d_Ht_const_mat_X[tx + N*3 + 60] ] * s_track[ d_Ht_const_mat_X[tx + N*3 + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*3] ]
               + d_Ht_const_mat_s[tx + N*4] * s_track[ d_Ht_const_mat_X[tx + N*4] ] * s_track[ d_Ht_const_mat_X[tx + N*4 + 60] ] * s_track[ d_Ht_const_mat_X[tx + N*4 + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*4] ]
               + d_Ht_const_mat_s[tx + N*5] * s_track[ d_Ht_const_mat_X[tx + N*5] ] * s_track[ d_Ht_const_mat_X[tx + N*5 + 60] ] * s_track[ d_Ht_const_mat_X[tx + N*5 + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*5] ]
               + d_Ht_const_mat_s[tx + N*6] * s_track[ d_Ht_const_mat_X[tx + N*6] ] * s_track[ d_Ht_const_mat_X[tx + N*6 + 60] ] * s_track[ d_Ht_const_mat_X[tx + N*6 + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*6] ]
               + d_Ht_const_mat_s[tx + N*7] * s_track[ d_Ht_const_mat_X[tx + N*7] ] * s_track[ d_Ht_const_mat_X[tx + N*7 + 60] ] * s_track[ d_Ht_const_mat_X[tx + N*7 + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*7] ]
               + d_Ht_const_mat_s[tx + N*8] * s_track[ d_Ht_const_mat_X[tx + N*8] ] * s_track[ d_Ht_const_mat_X[tx + N*8 + 60] ] * s_track[ d_Ht_const_mat_X[tx + N*8 + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*8] ]
               + d_Ht_const_mat_s[tx + N*9] * s_track[ d_Ht_const_mat_X[tx + N*9] ] * s_track[ d_Ht_const_mat_X[tx + N*9 + 60] ] * s_track[ d_Ht_const_mat_X[tx + N*9 + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*9] ]
               + d_Ht_const_mat_s[tx + N*10] * s_track[ d_Ht_const_mat_X[tx + N*10] ] * s_track[ d_Ht_const_mat_X[tx + N*10 + 60] ] * s_track[ d_Ht_const_mat_X[tx + N*10 + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*10] ]
               + d_Ht_const_mat_s[tx + N*11] * s_track[ d_Ht_const_mat_X[tx + N*11] ] * s_track[ d_Ht_const_mat_X[tx + N*11 + 60] ] * s_track[ d_Ht_const_mat_X[tx + N*11 + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*11] ]
               + d_Ht_const_mat_s[tx + N*12] * s_track[ d_Ht_const_mat_X[tx + N*12] ] * s_track[ d_Ht_const_mat_X[tx + N*12 + 60] ] * s_track[ d_Ht_const_mat_X[tx + N*12 + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*12] ]
               + d_Ht_const_mat_s[tx + N*13] * s_track[ d_Ht_const_mat_X[tx + N*13] ] * s_track[ d_Ht_const_mat_X[tx + N*13 + 60] ] * s_track[ d_Ht_const_mat_X[tx + N*13 + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*13] ]
               + d_Ht_const_mat_s[tx + N*14] * s_track[ d_Ht_const_mat_X[tx + N*14] ] * s_track[ d_Ht_const_mat_X[tx + N*14 + 60] ] * s_track[ d_Ht_const_mat_X[tx + N*14 + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*14] ]
               + d_Ht_const_mat_s[tx + N*15] * s_track[ d_Ht_const_mat_X[tx + N*15] ] * s_track[ d_Ht_const_mat_X[tx + N*15 + 60] ] * s_track[ d_Ht_const_mat_X[tx + N*15 + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*15] ]
               + d_Ht_const_mat_s[tx + N*16] * s_track[ d_Ht_const_mat_X[tx + N*16] ] * s_track[ d_Ht_const_mat_X[tx + N*16 + 60] ] * s_track[ d_Ht_const_mat_X[tx + N*16 + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*16] ]
               + d_Ht_const_mat_s[tx + N*17] * s_track[ d_Ht_const_mat_X[tx + N*17] ] * s_track[ d_Ht_const_mat_X[tx + N*17 + 60] ] * s_track[ d_Ht_const_mat_X[tx + N*17 + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*17] ]
               + d_Ht_const_mat_s[tx + N*18] * s_track[ d_Ht_const_mat_X[tx + N*18] ] * s_track[ d_Ht_const_mat_X[tx + N*18 + 60] ] * s_track[ d_Ht_const_mat_X[tx + N*18 + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*18] ]
               + d_Ht_const_mat_s[tx + N*19] * s_track[ d_Ht_const_mat_X[tx + N*19] ] * s_track[ d_Ht_const_mat_X[tx + N*19 + 60] ] * s_track[ d_Ht_const_mat_X[tx + N*19 + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*19] ];
    }

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_H_5ptRelativePose_v2(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_H_const_mat_X,
      const magma_int_t* __restrict__ d_H_const_mat_Y, magmaFloatComplex s_cdt[coefsCount])
    {
      // -- same as Ht, except replacing cd by cdt --
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_H_const_mat_X[tx] ] * s_track[ d_H_const_mat_X[tx + 60] ] * s_track[ d_H_const_mat_X[tx + 120] ] * s_cdt[ d_H_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + N] * s_track[ d_H_const_mat_X[tx + N] ] * s_track[ d_H_const_mat_X[tx + N + 60] ] * s_track[ d_H_const_mat_X[tx + N + 120] ] * s_cdt[ d_H_const_mat_Y[tx + N] ]
               + d_Ht_const_mat_s[tx + N*2] * s_track[ d_H_const_mat_X[tx + N*2] ] * s_track[ d_H_const_mat_X[tx + N*2 + 60] ] * s_track[ d_H_const_mat_X[tx + N*2 + 120] ] * s_cdt[ d_H_const_mat_Y[tx + N*2] ]
               + d_Ht_const_mat_s[tx + N*3] * s_track[ d_H_const_mat_X[tx + N*3] ] * s_track[ d_H_const_mat_X[tx + N*3 + 60] ] * s_track[ d_H_const_mat_X[tx + N*3 + 120] ] * s_cdt[ d_H_const_mat_Y[tx + N*3] ]
               + d_Ht_const_mat_s[tx + N*4] * s_track[ d_H_const_mat_X[tx + N*4] ] * s_track[ d_H_const_mat_X[tx + N*4 + 60] ] * s_track[ d_H_const_mat_X[tx + N*4 + 120] ] * s_cdt[ d_H_const_mat_Y[tx + N*4] ]
               + d_Ht_const_mat_s[tx + N*5] * s_track[ d_H_const_mat_X[tx + N*5] ] * s_track[ d_H_const_mat_X[tx + N*5 + 60] ] * s_track[ d_H_const_mat_X[tx + N*5 + 120] ] * s_cdt[ d_H_const_mat_Y[tx + N*5] ]
               + d_Ht_const_mat_s[tx + N*6] * s_track[ d_H_const_mat_X[tx + N*6] ] * s_track[ d_H_const_mat_X[tx + N*6 + 60] ] * s_track[ d_H_const_mat_X[tx + N*6 + 120] ] * s_cdt[ d_H_const_mat_Y[tx + N*6] ]
               + d_Ht_const_mat_s[tx + N*7] * s_track[ d_H_const_mat_X[tx + N*7] ] * s_track[ d_H_const_mat_X[tx + N*7 + 60] ] * s_track[ d_H_const_mat_X[tx + N*7 + 120] ] * s_cdt[ d_H_const_mat_Y[tx + N*7] ]
               + d_Ht_const_mat_s[tx + N*8] * s_track[ d_H_const_mat_X[tx + N*8] ] * s_track[ d_H_const_mat_X[tx + N*8 + 60] ] * s_track[ d_H_const_mat_X[tx + N*8 + 120] ] * s_cdt[ d_H_const_mat_Y[tx + N*8] ]
               + d_Ht_const_mat_s[tx + N*9] * s_track[ d_H_const_mat_X[tx + N*9] ] * s_track[ d_H_const_mat_X[tx + N*9 + 60] ] * s_track[ d_H_const_mat_X[tx + N*9 + 120] ] * s_cdt[ d_H_const_mat_Y[tx + N*9] ]
               + d_Ht_const_mat_s[tx + N*10] * s_track[ d_H_const_mat_X[tx + N*10] ] * s_track[ d_H_const_mat_X[tx + N*10 + 60] ] * s_track[ d_H_const_mat_X[tx + N*10 + 120] ] * s_cdt[ d_H_const_mat_Y[tx + N*10] ]
               + d_Ht_const_mat_s[tx + N*11] * s_track[ d_H_const_mat_X[tx + N*11] ] * s_track[ d_H_const_mat_X[tx + N*11 + 60] ] * s_track[ d_H_const_mat_X[tx + N*11 + 120] ] * s_cdt[ d_H_const_mat_Y[tx + N*11] ]
               + d_Ht_const_mat_s[tx + N*12] * s_track[ d_H_const_mat_X[tx + N*12] ] * s_track[ d_H_const_mat_X[tx + N*12 + 60] ] * s_track[ d_H_const_mat_X[tx + N*12 + 120] ] * s_cdt[ d_H_const_mat_Y[tx + N*12] ]
               + d_Ht_const_mat_s[tx + N*13] * s_track[ d_H_const_mat_X[tx + N*13] ] * s_track[ d_H_const_mat_X[tx + N*13 + 60] ] * s_track[ d_H_const_mat_X[tx + N*13 + 120] ] * s_cdt[ d_H_const_mat_Y[tx + N*13] ]
               + d_Ht_const_mat_s[tx + N*14] * s_track[ d_H_const_mat_X[tx + N*14] ] * s_track[ d_H_const_mat_X[tx + N*14 + 60] ] * s_track[ d_H_const_mat_X[tx + N*14 + 120] ] * s_cdt[ d_H_const_mat_Y[tx + N*14] ]
               + d_Ht_const_mat_s[tx + N*15] * s_track[ d_H_const_mat_X[tx + N*15] ] * s_track[ d_H_const_mat_X[tx + N*15 + 60] ] * s_track[ d_H_const_mat_X[tx + N*15 + 120] ] * s_cdt[ d_H_const_mat_Y[tx + N*15] ]
               + d_Ht_const_mat_s[tx + N*16] * s_track[ d_H_const_mat_X[tx + N*16] ] * s_track[ d_H_const_mat_X[tx + N*16 + 60] ] * s_track[ d_H_const_mat_X[tx + N*16 + 120] ] * s_cdt[ d_H_const_mat_Y[tx + N*16] ]
               + d_Ht_const_mat_s[tx + N*17] * s_track[ d_H_const_mat_X[tx + N*17] ] * s_track[ d_H_const_mat_X[tx + N*17 + 60] ] * s_track[ d_H_const_mat_X[tx + N*17 + 120] ] * s_cdt[ d_H_const_mat_Y[tx + N*17] ]
               + d_Ht_const_mat_s[tx + N*18] * s_track[ d_H_const_mat_X[tx + N*18] ] * s_track[ d_H_const_mat_X[tx + N*18 + 60] ] * s_track[ d_H_const_mat_X[tx + N*18 + 120] ] * s_cdt[ d_H_const_mat_Y[tx + N*18] ]
               + d_Ht_const_mat_s[tx + N*19] * s_track[ d_H_const_mat_X[tx + N*19] ] * s_track[ d_H_const_mat_X[tx + N*19 + 60] ] * s_track[ d_H_const_mat_X[tx + N*19 + 120] ] * s_cdt[ d_H_const_mat_Y[tx + N*19] ];
    }
}

#endif

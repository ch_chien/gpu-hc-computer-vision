#ifndef dev_eval_indxing_dualTDOA_cuh_
#define dev_eval_indxing_dualTDOA_cuh_
// ============================================================================
// Device function for evaluating the parallel indexing for Hx, Ht, and H of
// dualTDOA problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// -- cuda included --
#include <cuda_runtime.h>

// -- magma included --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

namespace magmaHCWrapper {

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_cdt_dualTDOA(
        const int tx, float t, magmaFloatComplex s_vector_cdt[coefsCount],
        magmaFloatComplex r_startCoefs[coefsCount], magmaFloatComplex r_targetCoefs[coefsCount])
    {
        #pragma unroll
        for (int i = 0; i < 17; i++) {
            s_vector_cdt[ tx + i * N ] = r_targetCoefs[tx + i * N] * t - r_startCoefs[tx + i * N] * (t-1);
        }
        if (tx < 3) {
          s_vector_cdt[ tx + 85 ] = r_targetCoefs[tx + 85] * t - r_startCoefs[tx + 85] * (t-1);
        }
    }

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_Jacobian_dualTDOA(
        const int tx, magmaFloatComplex *s_track, magmaFloatComplex r_cgesvA[N], const magma_int_t* __restrict__ d_const_mat_s,
        const magma_int_t* __restrict__ d_const_mat_X, const magma_int_t* __restrict__ d_const_mat_Y,
        magmaFloatComplex s_cdt[coefsCount])
    {
        // -- the maximal terms of Hx in dualTDOA problem is 15 --
        // -- 375 = 5(vars) * 5(vars) * 15(terms) --
        #pragma unroll
        for(int i = 0; i < N; i++) {
          r_cgesvA[i] = d_const_mat_s[tx + i*5] * s_track[ d_const_mat_X[tx + i*5] ] * s_track[ d_const_mat_X[tx + i*5 + 375] ] * s_track[ d_const_mat_X[tx + i*5 + 750] ] * s_cdt[ d_const_mat_Y[tx + i*5] ]
                      + d_const_mat_s[tx + i*5 + 25] * s_track[ d_const_mat_X[tx + i*5 + 25] ] * s_track[ d_const_mat_X[tx + i*5 + 25 + 375] ] * s_track[ d_const_mat_X[tx + i*5 + 25 + 750] ] * s_cdt[ d_const_mat_Y[tx + i*5 + 25] ]
                      + d_const_mat_s[tx + i*5 + 50] * s_track[ d_const_mat_X[tx + i*5 + 50] ] * s_track[ d_const_mat_X[tx + i*5 + 50 + 375] ] * s_track[ d_const_mat_X[tx + i*5 + 50 + 750] ] * s_cdt[ d_const_mat_Y[tx + i*5 + 50] ]
                      + d_const_mat_s[tx + i*5 + 75] * s_track[ d_const_mat_X[tx + i*5 + 75] ] * s_track[ d_const_mat_X[tx + i*5 + 75 + 375] ] * s_track[ d_const_mat_X[tx + i*5 + 75 + 750] ] * s_cdt[ d_const_mat_Y[tx + i*5 + 75] ]
                      + d_const_mat_s[tx + i*5 + 100] * s_track[ d_const_mat_X[tx + i*5 + 100] ] * s_track[ d_const_mat_X[tx + i*5 + 100 + 375] ] * s_track[ d_const_mat_X[tx + i*5 + 100 + 750] ] * s_cdt[ d_const_mat_Y[tx + i*5 + 100] ]
                      + d_const_mat_s[tx + i*5 + 125] * s_track[ d_const_mat_X[tx + i*5 + 125] ] * s_track[ d_const_mat_X[tx + i*5 + 125 + 375] ] * s_track[ d_const_mat_X[tx + i*5 + 125 + 750] ] * s_cdt[ d_const_mat_Y[tx + i*5 + 125] ]
                      + d_const_mat_s[tx + i*5 + 150] * s_track[ d_const_mat_X[tx + i*5 + 150] ] * s_track[ d_const_mat_X[tx + i*5 + 150 + 375] ] * s_track[ d_const_mat_X[tx + i*5 + 150 + 750] ] * s_cdt[ d_const_mat_Y[tx + i*5 + 150] ]
                      + d_const_mat_s[tx + i*5 + 175] * s_track[ d_const_mat_X[tx + i*5 + 175] ] * s_track[ d_const_mat_X[tx + i*5 + 175 + 375] ] * s_track[ d_const_mat_X[tx + i*5 + 175 + 750] ] * s_cdt[ d_const_mat_Y[tx + i*5 + 175] ]
                      + d_const_mat_s[tx + i*5 + 200] * s_track[ d_const_mat_X[tx + i*5 + 200] ] * s_track[ d_const_mat_X[tx + i*5 + 200 + 375] ] * s_track[ d_const_mat_X[tx + i*5 + 200 + 750] ] * s_cdt[ d_const_mat_Y[tx + i*5 + 200] ]
                      + d_const_mat_s[tx + i*5 + 225] * s_track[ d_const_mat_X[tx + i*5 + 225] ] * s_track[ d_const_mat_X[tx + i*5 + 225 + 375] ] * s_track[ d_const_mat_X[tx + i*5 + 225 + 750] ] * s_cdt[ d_const_mat_Y[tx + i*5 + 225] ]
                      + d_const_mat_s[tx + i*5 + 250] * s_track[ d_const_mat_X[tx + i*5 + 250] ] * s_track[ d_const_mat_X[tx + i*5 + 250 + 375] ] * s_track[ d_const_mat_X[tx + i*5 + 250 + 750] ] * s_cdt[ d_const_mat_Y[tx + i*5 + 250] ]
                      + d_const_mat_s[tx + i*5 + 275] * s_track[ d_const_mat_X[tx + i*5 + 275] ] * s_track[ d_const_mat_X[tx + i*5 + 275 + 375] ] * s_track[ d_const_mat_X[tx + i*5 + 275 + 750] ] * s_cdt[ d_const_mat_Y[tx + i*5 + 275] ]
                      + d_const_mat_s[tx + i*5 + 300] * s_track[ d_const_mat_X[tx + i*5 + 300] ] * s_track[ d_const_mat_X[tx + i*5 + 300 + 375] ] * s_track[ d_const_mat_X[tx + i*5 + 300 + 750] ] * s_cdt[ d_const_mat_Y[tx + i*5 + 300] ]
                      + d_const_mat_s[tx + i*5 + 325] * s_track[ d_const_mat_X[tx + i*5 + 325] ] * s_track[ d_const_mat_X[tx + i*5 + 325 + 375] ] * s_track[ d_const_mat_X[tx + i*5 + 325 + 750] ] * s_cdt[ d_const_mat_Y[tx + i*5 + 325] ]
                      + d_const_mat_s[tx + i*5 + 350] * s_track[ d_const_mat_X[tx + i*5 + 350] ] * s_track[ d_const_mat_X[tx + i*5 + 350 + 375] ] * s_track[ d_const_mat_X[tx + i*5 + 350 + 750] ] * s_cdt[ d_const_mat_Y[tx + i*5 + 350] ];
        }
    }

    __device__ __inline__ void
    eval_Ht_dualTDOA(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_Ht_const_mat_X,
      const magma_int_t* __restrict__ d_Ht_const_mat_Y, const magmaFloatComplex* __restrict__ d_const_cd)
    {
      // -- the maximal terms of Ht in dualTDOA problem is 33 --
      // -- N*maximal terms of Ht = 5*33 = 165
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_Ht_const_mat_X[tx] ] * s_track[ d_Ht_const_mat_X[tx + 165] ] * s_track[ d_Ht_const_mat_X[tx + 330] ] * s_track[ d_Ht_const_mat_X[tx + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + 5] * s_track[ d_Ht_const_mat_X[tx + 5] ] * s_track[ d_Ht_const_mat_X[tx + 5 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 5 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 5 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 5] ]
               + d_Ht_const_mat_s[tx + 10] * s_track[ d_Ht_const_mat_X[tx + 10] ] * s_track[ d_Ht_const_mat_X[tx + 10 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 10 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 10 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 10] ]
               + d_Ht_const_mat_s[tx + 15] * s_track[ d_Ht_const_mat_X[tx + 15] ] * s_track[ d_Ht_const_mat_X[tx + 15 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 15 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 15 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 15] ]
               + d_Ht_const_mat_s[tx + 20] * s_track[ d_Ht_const_mat_X[tx + 20] ] * s_track[ d_Ht_const_mat_X[tx + 20 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 20 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 20 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 20] ]
               + d_Ht_const_mat_s[tx + 25] * s_track[ d_Ht_const_mat_X[tx + 25] ] * s_track[ d_Ht_const_mat_X[tx + 25 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 25 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 25 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 25] ]
               + d_Ht_const_mat_s[tx + 30] * s_track[ d_Ht_const_mat_X[tx + 30] ] * s_track[ d_Ht_const_mat_X[tx + 30 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 30 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 30 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 30] ]
               + d_Ht_const_mat_s[tx + 35] * s_track[ d_Ht_const_mat_X[tx + 35] ] * s_track[ d_Ht_const_mat_X[tx + 35 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 35 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 35 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 35] ]
               + d_Ht_const_mat_s[tx + 40] * s_track[ d_Ht_const_mat_X[tx + 40] ] * s_track[ d_Ht_const_mat_X[tx + 40 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 40 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 40 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 40] ]
               + d_Ht_const_mat_s[tx + 45] * s_track[ d_Ht_const_mat_X[tx + 45] ] * s_track[ d_Ht_const_mat_X[tx + 45 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 45 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 45 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 45] ]
               + d_Ht_const_mat_s[tx + 50] * s_track[ d_Ht_const_mat_X[tx + 50] ] * s_track[ d_Ht_const_mat_X[tx + 50 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 50 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 50 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 50] ]
               + d_Ht_const_mat_s[tx + 55] * s_track[ d_Ht_const_mat_X[tx + 55] ] * s_track[ d_Ht_const_mat_X[tx + 55 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 55 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 55 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 55] ]
               + d_Ht_const_mat_s[tx + 60] * s_track[ d_Ht_const_mat_X[tx + 60] ] * s_track[ d_Ht_const_mat_X[tx + 60 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 60 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 60 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 60] ]
               + d_Ht_const_mat_s[tx + 65] * s_track[ d_Ht_const_mat_X[tx + 65] ] * s_track[ d_Ht_const_mat_X[tx + 65 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 65 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 65 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 65] ]
               + d_Ht_const_mat_s[tx + 70] * s_track[ d_Ht_const_mat_X[tx + 70] ] * s_track[ d_Ht_const_mat_X[tx + 70 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 70 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 70 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 70] ]
               + d_Ht_const_mat_s[tx + 75] * s_track[ d_Ht_const_mat_X[tx + 75] ] * s_track[ d_Ht_const_mat_X[tx + 75 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 75 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 75 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 75] ]
               + d_Ht_const_mat_s[tx + 80] * s_track[ d_Ht_const_mat_X[tx + 80] ] * s_track[ d_Ht_const_mat_X[tx + 80 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 80 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 80 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 80] ]
               + d_Ht_const_mat_s[tx + 85] * s_track[ d_Ht_const_mat_X[tx + 85] ] * s_track[ d_Ht_const_mat_X[tx + 85 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 85 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 85 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 85] ]
               + d_Ht_const_mat_s[tx + 90] * s_track[ d_Ht_const_mat_X[tx + 90] ] * s_track[ d_Ht_const_mat_X[tx + 90 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 90 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 90 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 90] ]
               + d_Ht_const_mat_s[tx + 95] * s_track[ d_Ht_const_mat_X[tx + 95] ] * s_track[ d_Ht_const_mat_X[tx + 95 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 95 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 95 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 95] ]
               + d_Ht_const_mat_s[tx + 100] * s_track[ d_Ht_const_mat_X[tx + 100] ] * s_track[ d_Ht_const_mat_X[tx + 100 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 100 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 100 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 100] ]
               + d_Ht_const_mat_s[tx + 105] * s_track[ d_Ht_const_mat_X[tx + 105] ] * s_track[ d_Ht_const_mat_X[tx + 105 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 105 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 105 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 105] ]
               + d_Ht_const_mat_s[tx + 110] * s_track[ d_Ht_const_mat_X[tx + 110] ] * s_track[ d_Ht_const_mat_X[tx + 110 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 110 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 110 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 110] ]
               + d_Ht_const_mat_s[tx + 115] * s_track[ d_Ht_const_mat_X[tx + 115] ] * s_track[ d_Ht_const_mat_X[tx + 115 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 115 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 115 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 115] ]
               + d_Ht_const_mat_s[tx + 120] * s_track[ d_Ht_const_mat_X[tx + 120] ] * s_track[ d_Ht_const_mat_X[tx + 120 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 120 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 120 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 120] ]
               + d_Ht_const_mat_s[tx + 125] * s_track[ d_Ht_const_mat_X[tx + 125] ] * s_track[ d_Ht_const_mat_X[tx + 125 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 125 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 125 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 125] ]
               + d_Ht_const_mat_s[tx + 130] * s_track[ d_Ht_const_mat_X[tx + 130] ] * s_track[ d_Ht_const_mat_X[tx + 130 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 130 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 130 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 130] ]
               + d_Ht_const_mat_s[tx + 135] * s_track[ d_Ht_const_mat_X[tx + 135] ] * s_track[ d_Ht_const_mat_X[tx + 135 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 135 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 135 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 135] ]
               + d_Ht_const_mat_s[tx + 140] * s_track[ d_Ht_const_mat_X[tx + 140] ] * s_track[ d_Ht_const_mat_X[tx + 140 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 140 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 140 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 140] ]
               + d_Ht_const_mat_s[tx + 145] * s_track[ d_Ht_const_mat_X[tx + 145] ] * s_track[ d_Ht_const_mat_X[tx + 145 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 145 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 145 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 145] ]
               + d_Ht_const_mat_s[tx + 150] * s_track[ d_Ht_const_mat_X[tx + 150] ] * s_track[ d_Ht_const_mat_X[tx + 150 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 150 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 150 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 150] ]
               + d_Ht_const_mat_s[tx + 155] * s_track[ d_Ht_const_mat_X[tx + 155] ] * s_track[ d_Ht_const_mat_X[tx + 155 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 155 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 155 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 155] ]
               + d_Ht_const_mat_s[tx + 160] * s_track[ d_Ht_const_mat_X[tx + 160] ] * s_track[ d_Ht_const_mat_X[tx + 160 + 165] ] * s_track[ d_Ht_const_mat_X[tx + 160 + 330] ] * s_track[ d_Ht_const_mat_X[tx + 160 + 495] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 160] ];
    }

    template<int coefsCount>
    __device__ __inline__ void
    eval_H_dualTDOA(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_H_const_mat_X,
      const magma_int_t* __restrict__ d_H_const_mat_Y, magmaFloatComplex s_cdt[coefsCount])
    {
      // -- same as Ht, except replacing cd by cdt --
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_H_const_mat_X[tx] ] * s_track[ d_H_const_mat_X[tx + 165] ] * s_track[ d_H_const_mat_X[tx + 330] ] * s_track[ d_H_const_mat_X[tx + 495] ] * s_cdt[ d_H_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + 5] * s_track[ d_H_const_mat_X[tx + 5] ] * s_track[ d_H_const_mat_X[tx + 5 + 165] ] * s_track[ d_H_const_mat_X[tx + 5 + 330] ] * s_track[ d_H_const_mat_X[tx + 5 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 5] ]
               + d_Ht_const_mat_s[tx + 10] * s_track[ d_H_const_mat_X[tx + 10] ] * s_track[ d_H_const_mat_X[tx + 10 + 165] ] * s_track[ d_H_const_mat_X[tx + 10 + 330] ] * s_track[ d_H_const_mat_X[tx + 10 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 10] ]
               + d_Ht_const_mat_s[tx + 15] * s_track[ d_H_const_mat_X[tx + 15] ] * s_track[ d_H_const_mat_X[tx + 15 + 165] ] * s_track[ d_H_const_mat_X[tx + 15 + 330] ] * s_track[ d_H_const_mat_X[tx + 15 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 15] ]
               + d_Ht_const_mat_s[tx + 20] * s_track[ d_H_const_mat_X[tx + 20] ] * s_track[ d_H_const_mat_X[tx + 20 + 165] ] * s_track[ d_H_const_mat_X[tx + 20 + 330] ] * s_track[ d_H_const_mat_X[tx + 20 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 20] ]
               + d_Ht_const_mat_s[tx + 25] * s_track[ d_H_const_mat_X[tx + 25] ] * s_track[ d_H_const_mat_X[tx + 25 + 165] ] * s_track[ d_H_const_mat_X[tx + 25 + 330] ] * s_track[ d_H_const_mat_X[tx + 25 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 25] ]
               + d_Ht_const_mat_s[tx + 30] * s_track[ d_H_const_mat_X[tx + 30] ] * s_track[ d_H_const_mat_X[tx + 30 + 165] ] * s_track[ d_H_const_mat_X[tx + 30 + 330] ] * s_track[ d_H_const_mat_X[tx + 30 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 30] ]
               + d_Ht_const_mat_s[tx + 35] * s_track[ d_H_const_mat_X[tx + 35] ] * s_track[ d_H_const_mat_X[tx + 35 + 165] ] * s_track[ d_H_const_mat_X[tx + 35 + 330] ] * s_track[ d_H_const_mat_X[tx + 35 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 35] ]
               + d_Ht_const_mat_s[tx + 40] * s_track[ d_H_const_mat_X[tx + 40] ] * s_track[ d_H_const_mat_X[tx + 40 + 165] ] * s_track[ d_H_const_mat_X[tx + 40 + 330] ] * s_track[ d_H_const_mat_X[tx + 40 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 40] ]
               + d_Ht_const_mat_s[tx + 45] * s_track[ d_H_const_mat_X[tx + 45] ] * s_track[ d_H_const_mat_X[tx + 45 + 165] ] * s_track[ d_H_const_mat_X[tx + 45 + 330] ] * s_track[ d_H_const_mat_X[tx + 45 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 45] ]
               + d_Ht_const_mat_s[tx + 50] * s_track[ d_H_const_mat_X[tx + 50] ] * s_track[ d_H_const_mat_X[tx + 50 + 165] ] * s_track[ d_H_const_mat_X[tx + 50 + 330] ] * s_track[ d_H_const_mat_X[tx + 50 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 50] ]
               + d_Ht_const_mat_s[tx + 55] * s_track[ d_H_const_mat_X[tx + 55] ] * s_track[ d_H_const_mat_X[tx + 55 + 165] ] * s_track[ d_H_const_mat_X[tx + 55 + 330] ] * s_track[ d_H_const_mat_X[tx + 55 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 55] ]
               + d_Ht_const_mat_s[tx + 60] * s_track[ d_H_const_mat_X[tx + 60] ] * s_track[ d_H_const_mat_X[tx + 60 + 165] ] * s_track[ d_H_const_mat_X[tx + 60 + 330] ] * s_track[ d_H_const_mat_X[tx + 60 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 60] ]
               + d_Ht_const_mat_s[tx + 65] * s_track[ d_H_const_mat_X[tx + 65] ] * s_track[ d_H_const_mat_X[tx + 65 + 165] ] * s_track[ d_H_const_mat_X[tx + 65 + 330] ] * s_track[ d_H_const_mat_X[tx + 65 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 65] ]
               + d_Ht_const_mat_s[tx + 70] * s_track[ d_H_const_mat_X[tx + 70] ] * s_track[ d_H_const_mat_X[tx + 70 + 165] ] * s_track[ d_H_const_mat_X[tx + 70 + 330] ] * s_track[ d_H_const_mat_X[tx + 70 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 70] ]
               + d_Ht_const_mat_s[tx + 75] * s_track[ d_H_const_mat_X[tx + 75] ] * s_track[ d_H_const_mat_X[tx + 75 + 165] ] * s_track[ d_H_const_mat_X[tx + 75 + 330] ] * s_track[ d_H_const_mat_X[tx + 75 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 75] ]
               + d_Ht_const_mat_s[tx + 80] * s_track[ d_H_const_mat_X[tx + 80] ] * s_track[ d_H_const_mat_X[tx + 80 + 165] ] * s_track[ d_H_const_mat_X[tx + 80 + 330] ] * s_track[ d_H_const_mat_X[tx + 80 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 80] ]
               + d_Ht_const_mat_s[tx + 85] * s_track[ d_H_const_mat_X[tx + 85] ] * s_track[ d_H_const_mat_X[tx + 85 + 165] ] * s_track[ d_H_const_mat_X[tx + 85 + 330] ] * s_track[ d_H_const_mat_X[tx + 85 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 85] ]
               + d_Ht_const_mat_s[tx + 90] * s_track[ d_H_const_mat_X[tx + 90] ] * s_track[ d_H_const_mat_X[tx + 90 + 165] ] * s_track[ d_H_const_mat_X[tx + 90 + 330] ] * s_track[ d_H_const_mat_X[tx + 90 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 90] ]
               + d_Ht_const_mat_s[tx + 95] * s_track[ d_H_const_mat_X[tx + 95] ] * s_track[ d_H_const_mat_X[tx + 95 + 165] ] * s_track[ d_H_const_mat_X[tx + 95 + 330] ] * s_track[ d_H_const_mat_X[tx + 95 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 95] ]
               + d_Ht_const_mat_s[tx + 100] * s_track[ d_H_const_mat_X[tx + 100] ] * s_track[ d_H_const_mat_X[tx + 100 + 165] ] * s_track[ d_H_const_mat_X[tx + 100 + 330] ] * s_track[ d_H_const_mat_X[tx + 100 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 100] ]
               + d_Ht_const_mat_s[tx + 105] * s_track[ d_H_const_mat_X[tx + 105] ] * s_track[ d_H_const_mat_X[tx + 105 + 165] ] * s_track[ d_H_const_mat_X[tx + 105 + 330] ] * s_track[ d_H_const_mat_X[tx + 105 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 105] ]
               + d_Ht_const_mat_s[tx + 110] * s_track[ d_H_const_mat_X[tx + 110] ] * s_track[ d_H_const_mat_X[tx + 110 + 165] ] * s_track[ d_H_const_mat_X[tx + 110 + 330] ] * s_track[ d_H_const_mat_X[tx + 110 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 110] ]
               + d_Ht_const_mat_s[tx + 115] * s_track[ d_H_const_mat_X[tx + 115] ] * s_track[ d_H_const_mat_X[tx + 115 + 165] ] * s_track[ d_H_const_mat_X[tx + 115 + 330] ] * s_track[ d_H_const_mat_X[tx + 115 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 115] ]
               + d_Ht_const_mat_s[tx + 120] * s_track[ d_H_const_mat_X[tx + 120] ] * s_track[ d_H_const_mat_X[tx + 120 + 165] ] * s_track[ d_H_const_mat_X[tx + 120 + 330] ] * s_track[ d_H_const_mat_X[tx + 120 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 120] ]
               + d_Ht_const_mat_s[tx + 125] * s_track[ d_H_const_mat_X[tx + 125] ] * s_track[ d_H_const_mat_X[tx + 125 + 165] ] * s_track[ d_H_const_mat_X[tx + 125 + 330] ] * s_track[ d_H_const_mat_X[tx + 125 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 125] ]
               + d_Ht_const_mat_s[tx + 130] * s_track[ d_H_const_mat_X[tx + 130] ] * s_track[ d_H_const_mat_X[tx + 130 + 165] ] * s_track[ d_H_const_mat_X[tx + 130 + 330] ] * s_track[ d_H_const_mat_X[tx + 130 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 130] ]
               + d_Ht_const_mat_s[tx + 135] * s_track[ d_H_const_mat_X[tx + 135] ] * s_track[ d_H_const_mat_X[tx + 135 + 165] ] * s_track[ d_H_const_mat_X[tx + 135 + 330] ] * s_track[ d_H_const_mat_X[tx + 135 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 135] ]
               + d_Ht_const_mat_s[tx + 140] * s_track[ d_H_const_mat_X[tx + 140] ] * s_track[ d_H_const_mat_X[tx + 140 + 165] ] * s_track[ d_H_const_mat_X[tx + 140 + 330] ] * s_track[ d_H_const_mat_X[tx + 140 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 140] ]
               + d_Ht_const_mat_s[tx + 145] * s_track[ d_H_const_mat_X[tx + 145] ] * s_track[ d_H_const_mat_X[tx + 145 + 165] ] * s_track[ d_H_const_mat_X[tx + 145 + 330] ] * s_track[ d_H_const_mat_X[tx + 145 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 145] ]
               + d_Ht_const_mat_s[tx + 150] * s_track[ d_H_const_mat_X[tx + 150] ] * s_track[ d_H_const_mat_X[tx + 150 + 165] ] * s_track[ d_H_const_mat_X[tx + 150 + 330] ] * s_track[ d_H_const_mat_X[tx + 150 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 150] ]
               + d_Ht_const_mat_s[tx + 155] * s_track[ d_H_const_mat_X[tx + 155] ] * s_track[ d_H_const_mat_X[tx + 155 + 165] ] * s_track[ d_H_const_mat_X[tx + 155 + 330] ] * s_track[ d_H_const_mat_X[tx + 155 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 155] ]
               + d_Ht_const_mat_s[tx + 160] * s_track[ d_H_const_mat_X[tx + 160] ] * s_track[ d_H_const_mat_X[tx + 160 + 165] ] * s_track[ d_H_const_mat_X[tx + 160 + 330] ] * s_track[ d_H_const_mat_X[tx + 160 + 495] ] * s_cdt[ d_H_const_mat_Y[tx + 160] ];
    }
}

#endif

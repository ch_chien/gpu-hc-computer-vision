#ifndef dev_eval_indxing_5ptRelativePose_v4_cuh_
#define dev_eval_indxing_5ptRelativePose_v4_cuh_
// ============================================================================
// Device function for evaluating the parallel indexing for Hx, Ht, and H of
// 5ptRelativePose_v4 problem
//
// Modifications
//    Chien  21-11-16:   Originally created
//
// ============================================================================
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// -- cuda included --
#include <cuda_runtime.h>

// -- magma included --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

namespace magmaHCWrapper {

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_cdt_5ptRelativePose_v4(
        const int tx, float t, magmaFloatComplex s_vector_cdt[coefsCount],
        magmaFloatComplex r_startCoefs[coefsCount], magmaFloatComplex r_targetCoefs[coefsCount])
    {
        #pragma unroll
        for (int i = 0; i < 22; i++) {
            s_vector_cdt[ tx + i * N ] = r_targetCoefs[tx + i * N] * t - r_startCoefs[tx + i * N] * (t-1);
        }
        if (tx < 5) {
          s_vector_cdt[ tx + 132 ] = r_targetCoefs[tx + 132] * t - r_startCoefs[tx + 132] * (t-1);
        }
    }

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_Jacobian_5ptRelativePose_v4(
        const int tx, magmaFloatComplex *s_track, magmaFloatComplex r_cgesvA[N], const magma_int_t* __restrict__ d_const_mat_s,
        const magma_int_t* __restrict__ d_const_mat_X, const magma_int_t* __restrict__ d_const_mat_Y,
        magmaFloatComplex s_cdt[coefsCount])
    {
        // -- the maximal terms of Hx in 5ptRelativePose_v4 problem is 12 --
        // -- 432 = 6(vars) * 6(vars) * 12(terms) --
        #pragma unroll
        for(int i = 0; i < N; i++) {
          r_cgesvA[i] = d_const_mat_s[tx + i*N] * s_track[ d_const_mat_X[tx + i*N] ] * s_track[ d_const_mat_X[tx + i*N + 432] ] * s_cdt[ d_const_mat_Y[tx + i*N] ]
                      + d_const_mat_s[tx + i*N + N*N] * s_track[ d_const_mat_X[tx + i*N + N*N] ] * s_track[ d_const_mat_X[tx + i*N + N*N + 432] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N] ]
                      + d_const_mat_s[tx + i*N + N*N*2] * s_track[ d_const_mat_X[tx + i*N + N*N*2] ] * s_track[ d_const_mat_X[tx + i*N + N*N*2 + 432] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*2] ]
                      + d_const_mat_s[tx + i*N + N*N*3] * s_track[ d_const_mat_X[tx + i*N + N*N*3] ] * s_track[ d_const_mat_X[tx + i*N + N*N*3 + 432] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*3] ]
                      + d_const_mat_s[tx + i*N + N*N*4] * s_track[ d_const_mat_X[tx + i*N + N*N*4] ] * s_track[ d_const_mat_X[tx + i*N + N*N*4 + 432] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*4] ]
                      + d_const_mat_s[tx + i*N + N*N*5] * s_track[ d_const_mat_X[tx + i*N + N*N*5] ] * s_track[ d_const_mat_X[tx + i*N + N*N*5 + 432] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*5] ]
                      + d_const_mat_s[tx + i*N + N*N*6] * s_track[ d_const_mat_X[tx + i*N + N*N*6] ] * s_track[ d_const_mat_X[tx + i*N + N*N*6 + 432] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*6] ]
                      + d_const_mat_s[tx + i*N + N*N*7] * s_track[ d_const_mat_X[tx + i*N + N*N*7] ] * s_track[ d_const_mat_X[tx + i*N + N*N*7 + 432] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*7] ]
                      + d_const_mat_s[tx + i*N + N*N*8] * s_track[ d_const_mat_X[tx + i*N + N*N*8] ] * s_track[ d_const_mat_X[tx + i*N + N*N*8 + 432] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*8] ]
                      + d_const_mat_s[tx + i*N + N*N*9] * s_track[ d_const_mat_X[tx + i*N + N*N*9] ] * s_track[ d_const_mat_X[tx + i*N + N*N*9 + 432] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*9] ]
                      + d_const_mat_s[tx + i*N + N*N*10] * s_track[ d_const_mat_X[tx + i*N + N*N*10] ] * s_track[ d_const_mat_X[tx + i*N + N*N*10 + 432] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*10] ]
                      + d_const_mat_s[tx + i*N + N*N*11] * s_track[ d_const_mat_X[tx + i*N + N*N*11] ] * s_track[ d_const_mat_X[tx + i*N + N*N*11 + 432] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*11] ];
        }
    }

    template<int N>
    __device__ __inline__ void
    eval_Ht_5ptRelativePose_v4(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_Ht_const_mat_X,
      const magma_int_t* __restrict__ d_Ht_const_mat_Y, const magmaFloatComplex* __restrict__ d_const_cd)
    {
      // -- the maximal terms of Ht in 5ptRelativePose_v4 problem is 30 --
      // -- the maximal parts for 5ptRelativePose_v4 problem is 3 --
      // -- N*maximal terms of Ht = 6*30 = 360
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_Ht_const_mat_X[tx] ] * s_track[ d_Ht_const_mat_X[tx + 180] ] * s_track[ d_Ht_const_mat_X[tx + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + N] * s_track[ d_Ht_const_mat_X[tx + N] ] * s_track[ d_Ht_const_mat_X[tx + N + 180] ] * s_track[ d_Ht_const_mat_X[tx + N + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N] ]
               + d_Ht_const_mat_s[tx + N*2] * s_track[ d_Ht_const_mat_X[tx + N*2] ] * s_track[ d_Ht_const_mat_X[tx + N*2 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*2 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*2] ]
               + d_Ht_const_mat_s[tx + N*3] * s_track[ d_Ht_const_mat_X[tx + N*3] ] * s_track[ d_Ht_const_mat_X[tx + N*3 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*3 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*3] ]
               + d_Ht_const_mat_s[tx + N*4] * s_track[ d_Ht_const_mat_X[tx + N*4] ] * s_track[ d_Ht_const_mat_X[tx + N*4 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*4 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*4] ]
               + d_Ht_const_mat_s[tx + N*5] * s_track[ d_Ht_const_mat_X[tx + N*5] ] * s_track[ d_Ht_const_mat_X[tx + N*5 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*5 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*5] ]
               + d_Ht_const_mat_s[tx + N*6] * s_track[ d_Ht_const_mat_X[tx + N*6] ] * s_track[ d_Ht_const_mat_X[tx + N*6 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*6 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*6] ]
               + d_Ht_const_mat_s[tx + N*7] * s_track[ d_Ht_const_mat_X[tx + N*7] ] * s_track[ d_Ht_const_mat_X[tx + N*7 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*7 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*7] ]
               + d_Ht_const_mat_s[tx + N*8] * s_track[ d_Ht_const_mat_X[tx + N*8] ] * s_track[ d_Ht_const_mat_X[tx + N*8 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*8 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*8] ]
               + d_Ht_const_mat_s[tx + N*9] * s_track[ d_Ht_const_mat_X[tx + N*9] ] * s_track[ d_Ht_const_mat_X[tx + N*9 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*9 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*9] ]
               + d_Ht_const_mat_s[tx + N*10] * s_track[ d_Ht_const_mat_X[tx + N*10] ] * s_track[ d_Ht_const_mat_X[tx + N*10 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*10 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*10] ]
               + d_Ht_const_mat_s[tx + N*11] * s_track[ d_Ht_const_mat_X[tx + N*11] ] * s_track[ d_Ht_const_mat_X[tx + N*11 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*11 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*11] ]
               + d_Ht_const_mat_s[tx + N*12] * s_track[ d_Ht_const_mat_X[tx + N*12] ] * s_track[ d_Ht_const_mat_X[tx + N*12 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*12 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*12] ]
               + d_Ht_const_mat_s[tx + N*13] * s_track[ d_Ht_const_mat_X[tx + N*13] ] * s_track[ d_Ht_const_mat_X[tx + N*13 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*13 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*13] ]
               + d_Ht_const_mat_s[tx + N*14] * s_track[ d_Ht_const_mat_X[tx + N*14] ] * s_track[ d_Ht_const_mat_X[tx + N*14 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*14 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*14] ]
               + d_Ht_const_mat_s[tx + N*15] * s_track[ d_Ht_const_mat_X[tx + N*15] ] * s_track[ d_Ht_const_mat_X[tx + N*15 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*15 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*15] ]
               + d_Ht_const_mat_s[tx + N*16] * s_track[ d_Ht_const_mat_X[tx + N*16] ] * s_track[ d_Ht_const_mat_X[tx + N*16 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*16 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*16] ]
               + d_Ht_const_mat_s[tx + N*17] * s_track[ d_Ht_const_mat_X[tx + N*17] ] * s_track[ d_Ht_const_mat_X[tx + N*17 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*17 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*17] ]
               + d_Ht_const_mat_s[tx + N*18] * s_track[ d_Ht_const_mat_X[tx + N*18] ] * s_track[ d_Ht_const_mat_X[tx + N*18 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*18 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*18] ]
               + d_Ht_const_mat_s[tx + N*19] * s_track[ d_Ht_const_mat_X[tx + N*19] ] * s_track[ d_Ht_const_mat_X[tx + N*19 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*19 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*19] ]
               + d_Ht_const_mat_s[tx + N*20] * s_track[ d_Ht_const_mat_X[tx + N*20] ] * s_track[ d_Ht_const_mat_X[tx + N*20 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*20 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*20] ]
               + d_Ht_const_mat_s[tx + N*21] * s_track[ d_Ht_const_mat_X[tx + N*21] ] * s_track[ d_Ht_const_mat_X[tx + N*21 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*21 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*21] ]
               + d_Ht_const_mat_s[tx + N*22] * s_track[ d_Ht_const_mat_X[tx + N*22] ] * s_track[ d_Ht_const_mat_X[tx + N*22 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*22 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*22] ]
               + d_Ht_const_mat_s[tx + N*23] * s_track[ d_Ht_const_mat_X[tx + N*23] ] * s_track[ d_Ht_const_mat_X[tx + N*23 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*23 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*23] ]
               + d_Ht_const_mat_s[tx + N*24] * s_track[ d_Ht_const_mat_X[tx + N*24] ] * s_track[ d_Ht_const_mat_X[tx + N*24 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*24 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*24] ]
               + d_Ht_const_mat_s[tx + N*25] * s_track[ d_Ht_const_mat_X[tx + N*25] ] * s_track[ d_Ht_const_mat_X[tx + N*25 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*25 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*25] ]
               + d_Ht_const_mat_s[tx + N*26] * s_track[ d_Ht_const_mat_X[tx + N*26] ] * s_track[ d_Ht_const_mat_X[tx + N*26 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*26 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*26] ]
               + d_Ht_const_mat_s[tx + N*27] * s_track[ d_Ht_const_mat_X[tx + N*27] ] * s_track[ d_Ht_const_mat_X[tx + N*27 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*27 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*27] ]
               + d_Ht_const_mat_s[tx + N*28] * s_track[ d_Ht_const_mat_X[tx + N*28] ] * s_track[ d_Ht_const_mat_X[tx + N*28 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*28 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*28] ]
               + d_Ht_const_mat_s[tx + N*29] * s_track[ d_Ht_const_mat_X[tx + N*29] ] * s_track[ d_Ht_const_mat_X[tx + N*29 + 180] ] * s_track[ d_Ht_const_mat_X[tx + N*29 + 360] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*29] ];
    }

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_H_5ptRelativePose_v4(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_H_const_mat_X,
      const magma_int_t* __restrict__ d_H_const_mat_Y, magmaFloatComplex s_cdt[coefsCount])
    {
      // -- same as Ht, except replacing cd by cdt --
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_H_const_mat_X[tx] ] * s_track[ d_H_const_mat_X[tx + 180] ] * s_track[ d_H_const_mat_X[tx + 360] ] * s_cdt[ d_H_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + N] * s_track[ d_H_const_mat_X[tx + N] ] * s_track[ d_H_const_mat_X[tx + N + 180] ] * s_track[ d_H_const_mat_X[tx + N + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N] ]
               + d_Ht_const_mat_s[tx + N*2] * s_track[ d_H_const_mat_X[tx + N*2] ] * s_track[ d_H_const_mat_X[tx + N*2 + 180] ] * s_track[ d_H_const_mat_X[tx + N*2 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*2] ]
               + d_Ht_const_mat_s[tx + N*3] * s_track[ d_H_const_mat_X[tx + N*3] ] * s_track[ d_H_const_mat_X[tx + N*3 + 180] ] * s_track[ d_H_const_mat_X[tx + N*3 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*3] ]
               + d_Ht_const_mat_s[tx + N*4] * s_track[ d_H_const_mat_X[tx + N*4] ] * s_track[ d_H_const_mat_X[tx + N*4 + 180] ] * s_track[ d_H_const_mat_X[tx + N*4 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*4] ]
               + d_Ht_const_mat_s[tx + N*5] * s_track[ d_H_const_mat_X[tx + N*5] ] * s_track[ d_H_const_mat_X[tx + N*5 + 180] ] * s_track[ d_H_const_mat_X[tx + N*5 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*5] ]
               + d_Ht_const_mat_s[tx + N*6] * s_track[ d_H_const_mat_X[tx + N*6] ] * s_track[ d_H_const_mat_X[tx + N*6 + 180] ] * s_track[ d_H_const_mat_X[tx + N*6 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*6] ]
               + d_Ht_const_mat_s[tx + N*7] * s_track[ d_H_const_mat_X[tx + N*7] ] * s_track[ d_H_const_mat_X[tx + N*7 + 180] ] * s_track[ d_H_const_mat_X[tx + N*7 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*7] ]
               + d_Ht_const_mat_s[tx + N*8] * s_track[ d_H_const_mat_X[tx + N*8] ] * s_track[ d_H_const_mat_X[tx + N*8 + 180] ] * s_track[ d_H_const_mat_X[tx + N*8 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*8] ]
               + d_Ht_const_mat_s[tx + N*9] * s_track[ d_H_const_mat_X[tx + N*9] ] * s_track[ d_H_const_mat_X[tx + N*9 + 180] ] * s_track[ d_H_const_mat_X[tx + N*9 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*9] ]
               + d_Ht_const_mat_s[tx + N*10] * s_track[ d_H_const_mat_X[tx + N*10] ] * s_track[ d_H_const_mat_X[tx + N*10 + 180] ] * s_track[ d_H_const_mat_X[tx + N*10 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*10] ]
               + d_Ht_const_mat_s[tx + N*11] * s_track[ d_H_const_mat_X[tx + N*11] ] * s_track[ d_H_const_mat_X[tx + N*11 + 180] ] * s_track[ d_H_const_mat_X[tx + N*11 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*11] ]
               + d_Ht_const_mat_s[tx + N*12] * s_track[ d_H_const_mat_X[tx + N*12] ] * s_track[ d_H_const_mat_X[tx + N*12 + 180] ] * s_track[ d_H_const_mat_X[tx + N*12 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*12] ]
               + d_Ht_const_mat_s[tx + N*13] * s_track[ d_H_const_mat_X[tx + N*13] ] * s_track[ d_H_const_mat_X[tx + N*13 + 180] ] * s_track[ d_H_const_mat_X[tx + N*13 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*13] ]
               + d_Ht_const_mat_s[tx + N*14] * s_track[ d_H_const_mat_X[tx + N*14] ] * s_track[ d_H_const_mat_X[tx + N*14 + 180] ] * s_track[ d_H_const_mat_X[tx + N*14 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*14] ]
               + d_Ht_const_mat_s[tx + N*15] * s_track[ d_H_const_mat_X[tx + N*15] ] * s_track[ d_H_const_mat_X[tx + N*15 + 180] ] * s_track[ d_H_const_mat_X[tx + N*15 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*15] ]
               + d_Ht_const_mat_s[tx + N*16] * s_track[ d_H_const_mat_X[tx + N*16] ] * s_track[ d_H_const_mat_X[tx + N*16 + 180] ] * s_track[ d_H_const_mat_X[tx + N*16 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*16] ]
               + d_Ht_const_mat_s[tx + N*17] * s_track[ d_H_const_mat_X[tx + N*17] ] * s_track[ d_H_const_mat_X[tx + N*17 + 180] ] * s_track[ d_H_const_mat_X[tx + N*17 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*17] ]
               + d_Ht_const_mat_s[tx + N*18] * s_track[ d_H_const_mat_X[tx + N*18] ] * s_track[ d_H_const_mat_X[tx + N*18 + 180] ] * s_track[ d_H_const_mat_X[tx + N*18 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*18] ]
               + d_Ht_const_mat_s[tx + N*19] * s_track[ d_H_const_mat_X[tx + N*19] ] * s_track[ d_H_const_mat_X[tx + N*19 + 180] ] * s_track[ d_H_const_mat_X[tx + N*19 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*19] ]
               + d_Ht_const_mat_s[tx + N*20] * s_track[ d_H_const_mat_X[tx + N*20] ] * s_track[ d_H_const_mat_X[tx + N*20 + 180] ] * s_track[ d_H_const_mat_X[tx + N*20 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*20] ]
               + d_Ht_const_mat_s[tx + N*21] * s_track[ d_H_const_mat_X[tx + N*21] ] * s_track[ d_H_const_mat_X[tx + N*21 + 180] ] * s_track[ d_H_const_mat_X[tx + N*21 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*21] ]
               + d_Ht_const_mat_s[tx + N*22] * s_track[ d_H_const_mat_X[tx + N*22] ] * s_track[ d_H_const_mat_X[tx + N*22 + 180] ] * s_track[ d_H_const_mat_X[tx + N*22 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*22] ]
               + d_Ht_const_mat_s[tx + N*23] * s_track[ d_H_const_mat_X[tx + N*23] ] * s_track[ d_H_const_mat_X[tx + N*23 + 180] ] * s_track[ d_H_const_mat_X[tx + N*23 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*23] ]
               + d_Ht_const_mat_s[tx + N*24] * s_track[ d_H_const_mat_X[tx + N*24] ] * s_track[ d_H_const_mat_X[tx + N*24 + 180] ] * s_track[ d_H_const_mat_X[tx + N*24 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*24] ]
               + d_Ht_const_mat_s[tx + N*25] * s_track[ d_H_const_mat_X[tx + N*25] ] * s_track[ d_H_const_mat_X[tx + N*25 + 180] ] * s_track[ d_H_const_mat_X[tx + N*25 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*25] ]
               + d_Ht_const_mat_s[tx + N*26] * s_track[ d_H_const_mat_X[tx + N*26] ] * s_track[ d_H_const_mat_X[tx + N*26 + 180] ] * s_track[ d_H_const_mat_X[tx + N*26 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*26] ]
               + d_Ht_const_mat_s[tx + N*27] * s_track[ d_H_const_mat_X[tx + N*27] ] * s_track[ d_H_const_mat_X[tx + N*27 + 180] ] * s_track[ d_H_const_mat_X[tx + N*27 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*27] ]
               + d_Ht_const_mat_s[tx + N*28] * s_track[ d_H_const_mat_X[tx + N*28] ] * s_track[ d_H_const_mat_X[tx + N*28 + 180] ] * s_track[ d_H_const_mat_X[tx + N*28 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*28] ]
               + d_Ht_const_mat_s[tx + N*29] * s_track[ d_H_const_mat_X[tx + N*29] ] * s_track[ d_H_const_mat_X[tx + N*29 + 180] ] * s_track[ d_H_const_mat_X[tx + N*29 + 360] ] * s_cdt[ d_H_const_mat_Y[tx + N*29] ];
    }
}

#endif

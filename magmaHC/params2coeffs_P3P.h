#ifndef params2coeffs_P3P_h
#define params2coeffs_P3P_h
// =======================================================================
//
// Modifications
//    Chien  21-11-03:   Initially Created
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min

namespace magmaHCWrapper {

  void params2coeffs_P3P(magmaFloatComplex *h_target_params, magmaFloatComplex *h_target_coeffs)
  {
    magmaFloatComplex x11 = h_target_params[0];
    magmaFloatComplex x12 = h_target_params[1];
    magmaFloatComplex x13 = h_target_params[2];
    magmaFloatComplex x14 = h_target_params[3];
    magmaFloatComplex x15 = h_target_params[4];
    magmaFloatComplex x16 = h_target_params[5];
    magmaFloatComplex x17 = h_target_params[6];
    magmaFloatComplex x18 = h_target_params[7];
    magmaFloatComplex x19 = h_target_params[8];
    magmaFloatComplex x20 = h_target_params[9];
    magmaFloatComplex x21 = h_target_params[10];
    magmaFloatComplex x22 = h_target_params[11];
    magmaFloatComplex x23 = h_target_params[12];
    magmaFloatComplex x24 = h_target_params[13];
    magmaFloatComplex x25 = h_target_params[14];

    h_target_coeffs[0] = MAGMA_C_MAKE(-2.0, 0.0);
    h_target_coeffs[1] = (2*x12);
    h_target_coeffs[2] = (-2*x12);
    h_target_coeffs[3] = (2*x11);
    h_target_coeffs[4] = MAGMA_C_NEG_ONE;
    h_target_coeffs[5] = (-x11);
    h_target_coeffs[6] = (x17);
    h_target_coeffs[7] = MAGMA_C_ONE+MAGMA_C_ONE;
    h_target_coeffs[8] = (-2*x11);
    h_target_coeffs[9] = (-x12);
    h_target_coeffs[10] = (x18);
    h_target_coeffs[11] = (x19);
    h_target_coeffs[12] = (2*x14);
    h_target_coeffs[13] = (-2*x14);
    h_target_coeffs[14] = (2*x13);
    h_target_coeffs[15] = (-x13);
    h_target_coeffs[16] = (x20);
    h_target_coeffs[17] = (-2*x13);
    h_target_coeffs[18] = (-x14);
    h_target_coeffs[19] = (x21);
    h_target_coeffs[20] = (x22);
    h_target_coeffs[21] = (2*x16);
    h_target_coeffs[22] = (-2*x16);
    h_target_coeffs[23] = (2*x15);
    h_target_coeffs[24] = (-x15);
    h_target_coeffs[25] = (x23);
    h_target_coeffs[26] = (-2*x15);
    h_target_coeffs[27] = (-x16);
    h_target_coeffs[28] = (x24);
    h_target_coeffs[29] = (x25);
    h_target_coeffs[30] = MAGMA_C_ONE;
  }
} // end of namespace

#endif


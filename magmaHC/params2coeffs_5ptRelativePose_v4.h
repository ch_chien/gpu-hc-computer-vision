#ifndef params2coeffs_5ptRelativePose_v4_h
#define params2coeffs_5ptRelativePose_v4_h
// =======================================================================
//
// Modifications
//    Chien  21-10-12:   Initially Created
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min

namespace magmaHCWrapper {

  void params2coeffs_5ptRelativePose_v4(magmaFloatComplex *h_target_params, magmaFloatComplex *h_target_coeffs)
  {
    magmaFloatComplex x7 = h_target_params[0];
    magmaFloatComplex x8 = h_target_params[1];
    magmaFloatComplex x9 = h_target_params[2];
    magmaFloatComplex x10 = h_target_params[3];
    magmaFloatComplex x11 = h_target_params[4];
    magmaFloatComplex x12 = h_target_params[5];
    magmaFloatComplex x13 = h_target_params[6];
    magmaFloatComplex x14 = h_target_params[7];
    magmaFloatComplex x15 = h_target_params[8];
    magmaFloatComplex x16 = h_target_params[9];
    magmaFloatComplex x17 = h_target_params[10];
    magmaFloatComplex x18 = h_target_params[11];
    magmaFloatComplex x19 = h_target_params[12];
    magmaFloatComplex x20 = h_target_params[13];
    magmaFloatComplex x21 = h_target_params[14];
    magmaFloatComplex x22 = h_target_params[15];
    magmaFloatComplex x23 = h_target_params[16];
    magmaFloatComplex x24 = h_target_params[17];
    magmaFloatComplex x25 = h_target_params[18];
    magmaFloatComplex x26 = h_target_params[19];

    magmaFloatComplex magmaTwo = MAGMA_C_ONE + MAGMA_C_ONE;
    h_target_coeffs[0] = (x8 - x18);
    h_target_coeffs[1] = (x17 - x7);
    h_target_coeffs[2] = (x7*x18 - x8*x17);
    h_target_coeffs[3] = (- 2*x8*x18 - magmaTwo);
    h_target_coeffs[4] = (2*x8*x17);
    h_target_coeffs[5] = (2*x17);
    h_target_coeffs[6] = (2*x7*x18);
    h_target_coeffs[7] = (- 2*x7*x17 - magmaTwo);
    h_target_coeffs[8] = (2*x18);
    h_target_coeffs[9] = (2*x7);
    h_target_coeffs[10] = (2*x8);
    h_target_coeffs[11] = (- 2*x7*x17 - magmaTwo*x8*x18);
    h_target_coeffs[12] = (x18 - x8);
    h_target_coeffs[13] = (- x7 - x17);
    h_target_coeffs[14] = (x7*x18 + x8*x17);
    h_target_coeffs[15] = (-2*x8);
    h_target_coeffs[16] = (2*x8*x18 - 2*x7*x17);
    h_target_coeffs[17] = (-2*x7*x18);
    h_target_coeffs[18] = (2*x7*x17 - magmaTwo);
    h_target_coeffs[19] = (x8 + x18);
    h_target_coeffs[20] = (x7 - x17);
    h_target_coeffs[21] = (- x7*x18 - x8*x17);
    h_target_coeffs[22] = (magmaTwo - 2*x8*x18);
    h_target_coeffs[23] = (-2*x17);
    h_target_coeffs[24] = (- x8 - x18);
    h_target_coeffs[25] = (x7 + x17);
    h_target_coeffs[26] = (x8*x17 - x7*x18);
    h_target_coeffs[27] = (x10 - x20);
    h_target_coeffs[28] = (x19 - x9);
    h_target_coeffs[29] = (x9*x20 - x10*x19);
    h_target_coeffs[30] = (- 2*x10*x20 - magmaTwo);
    h_target_coeffs[31] = (2*x10*x19);
    h_target_coeffs[32] = (2*x19);
    h_target_coeffs[33] = (2*x9*x20);
    h_target_coeffs[34] = (- 2*x9*x19 - magmaTwo);
    h_target_coeffs[35] = (2*x20);
    h_target_coeffs[36] = (2*x9);
    h_target_coeffs[37] = (2*x10);
    h_target_coeffs[38] = (- 2*x9*x19 - 2*x10*x20);
    h_target_coeffs[39] = (x20 - x10);
    h_target_coeffs[40] = (- x9 - x19);
    h_target_coeffs[41] = (x9*x20 + x10*x19);
    h_target_coeffs[42] = (-2*x10);
    h_target_coeffs[43] = (2*x10*x20 - 2*x9*x19);
    h_target_coeffs[44] = (-2*x9*x20);
    h_target_coeffs[45] = (2*x9*x19 - magmaTwo);
    h_target_coeffs[46] = (x10 + x20);
    h_target_coeffs[47] = (x9 - x19);
    h_target_coeffs[48] = (- x9*x20 - x10*x19);
    h_target_coeffs[49] = (magmaTwo - 2*x10*x20);
    h_target_coeffs[50] = (-2*x19);
    h_target_coeffs[51] = (- x10 - x20);
    h_target_coeffs[52] = (x9 + x19);
    h_target_coeffs[53] = (x10*x19 - x9*x20);
    h_target_coeffs[54] = (x12 - x22);
    h_target_coeffs[55] = (x21 - x11);
    h_target_coeffs[56] = (x11*x22 - x12*x21);
    h_target_coeffs[57] = (- 2*x12*x22 - magmaTwo);
    h_target_coeffs[58] = (2*x12*x21);
    h_target_coeffs[59] = (2*x21);
    h_target_coeffs[60] = (2*x11*x22);
    h_target_coeffs[61] = (- 2*x11*x21 - magmaTwo);
    h_target_coeffs[62] = (2*x22);
    h_target_coeffs[63] = (2*x11);
    h_target_coeffs[64] = (2*x12);
    h_target_coeffs[65] = (- 2*x11*x21 - 2*x12*x22);
    h_target_coeffs[66] = (x22 - x12);
    h_target_coeffs[67] = (- x11 - x21);
    h_target_coeffs[68] = (x11*x22 + x12*x21);
    h_target_coeffs[69] = (-2*x12);
    h_target_coeffs[70] = (2*x12*x22 - 2*x11*x21);
    h_target_coeffs[71] = (-2*x11*x22);
    h_target_coeffs[72] = (2*x11*x21 - magmaTwo);
    h_target_coeffs[73] = (x12 + x22);
    h_target_coeffs[74] = (x11 - x21);
    h_target_coeffs[75] = (- x11*x22 - x12*x21);
    h_target_coeffs[76] = (magmaTwo - 2*x12*x22);
    h_target_coeffs[77] = (-2*x21);
    h_target_coeffs[78] = (- x12 - x22);
    h_target_coeffs[79] = (x11 + x21);
    h_target_coeffs[80] = (x12*x21 - x11*x22);
    h_target_coeffs[81] = (x14 - x24);
    h_target_coeffs[82] = (x23 - x13);
    h_target_coeffs[83] = (x13*x24 - x14*x23);
    h_target_coeffs[84] = (- 2*x14*x24 - magmaTwo);
    h_target_coeffs[85] = (2*x14*x23);
    h_target_coeffs[86] = (2*x23);
    h_target_coeffs[87] = (2*x13*x24);
    h_target_coeffs[88] = (- 2*x13*x23 - magmaTwo);
    h_target_coeffs[89] = (2*x24);
    h_target_coeffs[90] = (2*x13);
    h_target_coeffs[91] = (2*x14);
    h_target_coeffs[92] = (- 2*x13*x23 - 2*x14*x24);
    h_target_coeffs[93] = (x24 - x14);
    h_target_coeffs[94] = (- x13 - x23);
    h_target_coeffs[95] = (x13*x24 + x14*x23);
    h_target_coeffs[96] = (-2*x14);
    h_target_coeffs[97] = (2*x14*x24 - 2*x13*x23);
    h_target_coeffs[98] = (-2*x13*x24);
    h_target_coeffs[99] = (2*x13*x23 - magmaTwo);
    h_target_coeffs[100] = (x14 + x24);
    h_target_coeffs[101] = (x13 - x23);
    h_target_coeffs[102] = (- x13*x24 - x14*x23);
    h_target_coeffs[103] = (magmaTwo - 2*x14*x24);
    h_target_coeffs[104] = (-2*x23);
    h_target_coeffs[105] = (- x14 - x24);
    h_target_coeffs[106] = (x13 + x23);
    h_target_coeffs[107] = (x14*x23 - x13*x24);
    h_target_coeffs[108] = (x16 - x26);
    h_target_coeffs[109] = (x25 - x15);
    h_target_coeffs[110] = (x15*x26 - x16*x25);
    h_target_coeffs[111] = (- 2*x16*x26 - magmaTwo);
    h_target_coeffs[112] = (2*x16*x25);
    h_target_coeffs[113] = (2*x25);
    h_target_coeffs[114] = (2*x15*x26);
    h_target_coeffs[115] = (- 2*x15*x25 - magmaTwo);
    h_target_coeffs[116] = (2*x26);
    h_target_coeffs[117] = (2*x15);
    h_target_coeffs[118] = (2*x16);
    h_target_coeffs[119] = (- 2*x15*x25 - 2*x16*x26);
    h_target_coeffs[120] = (x26 - x16);
    h_target_coeffs[121] = (- x15 - x25);
    h_target_coeffs[122] = (x15*x26 + x16*x25);
    h_target_coeffs[123] = (-2*x16);
    h_target_coeffs[124] = (2*x16*x26 - 2*x15*x25);
    h_target_coeffs[125] = (-2*x15*x26);
    h_target_coeffs[126] = (2*x15*x25 - magmaTwo);
    h_target_coeffs[127] = (x16 + x26);
    h_target_coeffs[128] = (x15 - x25);
    h_target_coeffs[129] = (- x15*x26 - x16*x25);
    h_target_coeffs[130] = (magmaTwo - 2*x16*x26);
    h_target_coeffs[131] = (-2*x25);
    h_target_coeffs[132] = (- x16 - x26);
    h_target_coeffs[133] = (x15 + x25);
    h_target_coeffs[134] = (x16*x25 - x15*x26);
    h_target_coeffs[135] = MAGMA_C_ONE;
    h_target_coeffs[136] = MAGMA_C_NEG_ONE;
  }
} // end of namespace

#endif


#ifndef dev_eval_indxing_5ptRelativePose_cuh_
#define dev_eval_indxing_5ptRelativePose_cuh_
// ============================================================================
// Device function for evaluating the parallel indexing for Hx, Ht, and H of
// 5ptRelativePose problem
//
// Modifications
//    Chien  21-10-26:   Originally created
//
// ============================================================================
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// -- cuda included --
#include <cuda_runtime.h>

// -- magma included --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

namespace magmaHCWrapper {

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_cdt_5ptRelativePose(
        const int tx, float t, magmaFloatComplex s_vector_cdt[coefsCount],
        magmaFloatComplex r_startCoefs[coefsCount], magmaFloatComplex r_targetCoefs[coefsCount])
    {
        #pragma unroll
        for (int i = 0; i < 2; i++) {
            s_vector_cdt[ tx + i * N ] = r_targetCoefs[tx + i * N] * t - r_startCoefs[tx + i * N] * (t-1);
        }
        if (tx < 12) {
          s_vector_cdt[ tx + 32 ] = r_targetCoefs[tx + 32] * t - r_startCoefs[tx + 32] * (t-1);
        }
    }

    // -- Improved Hx parallel indexing --
    template<int N, int coefsCount, int size_Hx, int max_terms, int max_parts>
    __device__ __inline__ void
    eval_Jacobian_5ptRelativePose_improve(
        magmaFloatComplex *s_track, magmaFloatComplex r_cgesvA[N], 
        magmaFloatComplex s_cdt[coefsCount], magma_int_t r_Hx_idx[size_Hx])
    {
      #pragma unroll
      for(int i = 0; i < N; i++) {
        r_cgesvA[i] = MAGMA_C_ZERO;

        #pragma unroll
        for(int j = 0; j < max_terms; j++) {
          r_cgesvA[i] += r_Hx_idx[j*max_parts + i*max_terms*max_parts] 
                       * s_track[ r_Hx_idx[j*max_parts + 1 + i*max_terms*max_parts] ]
                       * s_track[ r_Hx_idx[j*max_parts + 2 + i*max_terms*max_parts] ] 
                       * s_cdt[ r_Hx_idx[j*max_parts + 3 + i*max_terms*max_parts] ];
        }
      }
    }

    // -- Improved Ht parallel indexing --
    template<int N, int size_Ht, int max_terms, int max_parts>
    __device__ __inline__ void
    eval_Ht_5ptRelativePose_improve(
        magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
        const magmaFloatComplex* __restrict__ d_const_cd, magma_int_t r_Ht_idx[size_Ht])
    {
      r_cgesvB = MAGMA_C_ZERO;
      #pragma unroll
      for (int i = 0; i < max_terms; i++) {
        r_cgesvB += r_Ht_idx[i*max_parts] * s_track[ r_Ht_idx[i*max_parts+1] ] * s_track[ r_Ht_idx[i*max_parts+2] ] * s_track[ r_Ht_idx[i*max_parts+3] ] * d_const_cd[ r_Ht_idx[i*max_parts+4] ];
      }
    }

    // -- Improved H parallel indexing --
    template<int N, int coefsCount, int size_Ht, int max_terms, int max_parts>
    __device__ __inline__ void
    eval_H_5ptRelativePose_improve(
        magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
        magmaFloatComplex s_cdt[coefsCount], magma_int_t r_Ht_idx[size_Ht])
    {
      r_cgesvB = MAGMA_C_ZERO;
      #pragma unroll
      for (int i = 0; i < max_terms; i++) {
        r_cgesvB += r_Ht_idx[i*max_parts] * s_track[ r_Ht_idx[i*max_parts+1] ] * s_track[ r_Ht_idx[i*max_parts+2] ] * s_track[ r_Ht_idx[i*max_parts+3] ] * s_cdt[ r_Ht_idx[i*max_parts+4] ];
      }
    }


    // ============================================================================================================================
    /*template<int N, int coefsCount>
    __device__ __inline__ void
    eval_Jacobian_5ptRelativePose(
        const int tx, magmaFloatComplex *s_track, magmaFloatComplex r_cgesvA[N], const magma_int_t* __restrict__ d_const_mat_s,
        const magma_int_t* __restrict__ d_const_mat_X, const magma_int_t* __restrict__ d_const_mat_Y,
        magmaFloatComplex s_cdt[coefsCount])
    {
        // -- the maximal terms of Hx in 5ptRelativePose problem is 7 --
        // -- 1792 = 16(vars) * 16(vars) * 7(terms) --
        #pragma unroll
        for(int i = 0; i < N; i++) {
          r_cgesvA[i] = d_const_mat_s[tx + i*N] * s_track[ d_const_mat_X[tx + i*N] ] * s_track[ d_const_mat_X[tx + i*N + 1792] ] * s_cdt[ d_const_mat_Y[tx + i*N] ]
                      + d_const_mat_s[tx + i*N + N*N] * s_track[ d_const_mat_X[tx + i*N + N*N] ] * s_track[ d_const_mat_X[tx + i*N + N*N + 1792] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N] ]
                      + d_const_mat_s[tx + i*N + N*N*2] * s_track[ d_const_mat_X[tx + i*N + N*N*2] ] * s_track[ d_const_mat_X[tx + i*N + N*N*2 + 1792] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*2] ]
                      + d_const_mat_s[tx + i*N + N*N*3] * s_track[ d_const_mat_X[tx + i*N + N*N*3] ] * s_track[ d_const_mat_X[tx + i*N + N*N*3 + 1792] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*3] ]
                      + d_const_mat_s[tx + i*N + N*N*4] * s_track[ d_const_mat_X[tx + i*N + N*N*4] ] * s_track[ d_const_mat_X[tx + i*N + N*N*4 + 1792] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*4] ]
                      + d_const_mat_s[tx + i*N + N*N*5] * s_track[ d_const_mat_X[tx + i*N + N*N*5] ] * s_track[ d_const_mat_X[tx + i*N + N*N*5 + 1792] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*5] ]
                      + d_const_mat_s[tx + i*N + N*N*6] * s_track[ d_const_mat_X[tx + i*N + N*N*6] ] * s_track[ d_const_mat_X[tx + i*N + N*N*6 + 1792] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*6] ];
        }
    }

    template<int N>
    __device__ __inline__ void
    eval_Ht_5ptRelativePose(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_Ht_const_mat_X,
      const magma_int_t* __restrict__ d_Ht_const_mat_Y, const magmaFloatComplex* __restrict__ d_const_cd)
    {
      // -- the maximal terms of Ht in 5ptRelativePose problem is 9 --
      // -- the maximal parts for 5ptRelativePose problem is 3 --
      // -- N*maximal terms of Ht = 16*9 = 144
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_Ht_const_mat_X[tx] ] * s_track[ d_Ht_const_mat_X[tx + 144] ] * s_track[ d_Ht_const_mat_X[tx + 288] ] * d_const_cd[ d_Ht_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + N] * s_track[ d_Ht_const_mat_X[tx + N] ] * s_track[ d_Ht_const_mat_X[tx + N + 144] ] * s_track[ d_Ht_const_mat_X[tx + N + 288] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N] ]
               + d_Ht_const_mat_s[tx + N*2] * s_track[ d_Ht_const_mat_X[tx + N*2] ] * s_track[ d_Ht_const_mat_X[tx + N*2 + 144] ] * s_track[ d_Ht_const_mat_X[tx + N*2 + 288] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*2] ]
               + d_Ht_const_mat_s[tx + N*3] * s_track[ d_Ht_const_mat_X[tx + N*3] ] * s_track[ d_Ht_const_mat_X[tx + N*3 + 144] ] * s_track[ d_Ht_const_mat_X[tx + N*3 + 288] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*3] ]
               + d_Ht_const_mat_s[tx + N*4] * s_track[ d_Ht_const_mat_X[tx + N*4] ] * s_track[ d_Ht_const_mat_X[tx + N*4 + 144] ] * s_track[ d_Ht_const_mat_X[tx + N*4 + 288] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*4] ]
               + d_Ht_const_mat_s[tx + N*5] * s_track[ d_Ht_const_mat_X[tx + N*5] ] * s_track[ d_Ht_const_mat_X[tx + N*5 + 144] ] * s_track[ d_Ht_const_mat_X[tx + N*5 + 288] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*5] ]
               + d_Ht_const_mat_s[tx + N*6] * s_track[ d_Ht_const_mat_X[tx + N*6] ] * s_track[ d_Ht_const_mat_X[tx + N*6 + 144] ] * s_track[ d_Ht_const_mat_X[tx + N*6 + 288] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*6] ]
               + d_Ht_const_mat_s[tx + N*7] * s_track[ d_Ht_const_mat_X[tx + N*7] ] * s_track[ d_Ht_const_mat_X[tx + N*7 + 144] ] * s_track[ d_Ht_const_mat_X[tx + N*7 + 288] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*7] ]
               + d_Ht_const_mat_s[tx + N*8] * s_track[ d_Ht_const_mat_X[tx + N*8] ] * s_track[ d_Ht_const_mat_X[tx + N*8 + 144] ] * s_track[ d_Ht_const_mat_X[tx + N*8 + 288] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*8] ];
    }

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_H_5ptRelativePose(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_H_const_mat_X,
      const magma_int_t* __restrict__ d_H_const_mat_Y, magmaFloatComplex s_cdt[coefsCount])
    {
      // -- same as Ht, except replacing cd by cdt --
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_H_const_mat_X[tx] ] * s_track[ d_H_const_mat_X[tx + 144] ] * s_track[ d_H_const_mat_X[tx + 288] ] * s_cdt[ d_H_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + N] * s_track[ d_H_const_mat_X[tx + N] ] * s_track[ d_H_const_mat_X[tx + N + 144] ] * s_track[ d_H_const_mat_X[tx + N + 288] ] * s_cdt[ d_H_const_mat_Y[tx + N] ]
               + d_Ht_const_mat_s[tx + N*2] * s_track[ d_H_const_mat_X[tx + N*2] ] * s_track[ d_H_const_mat_X[tx + N*2 + 144] ] * s_track[ d_H_const_mat_X[tx + N*2 + 288] ] * s_cdt[ d_H_const_mat_Y[tx + N*2] ]
               + d_Ht_const_mat_s[tx + N*3] * s_track[ d_H_const_mat_X[tx + N*3] ] * s_track[ d_H_const_mat_X[tx + N*3 + 144] ] * s_track[ d_H_const_mat_X[tx + N*3 + 288] ] * s_cdt[ d_H_const_mat_Y[tx + N*3] ]
               + d_Ht_const_mat_s[tx + N*4] * s_track[ d_H_const_mat_X[tx + N*4] ] * s_track[ d_H_const_mat_X[tx + N*4 + 144] ] * s_track[ d_H_const_mat_X[tx + N*4 + 288] ] * s_cdt[ d_H_const_mat_Y[tx + N*4] ]
               + d_Ht_const_mat_s[tx + N*5] * s_track[ d_H_const_mat_X[tx + N*5] ] * s_track[ d_H_const_mat_X[tx + N*5 + 144] ] * s_track[ d_H_const_mat_X[tx + N*5 + 288] ] * s_cdt[ d_H_const_mat_Y[tx + N*5] ]
               + d_Ht_const_mat_s[tx + N*6] * s_track[ d_H_const_mat_X[tx + N*6] ] * s_track[ d_H_const_mat_X[tx + N*6 + 144] ] * s_track[ d_H_const_mat_X[tx + N*6 + 288] ] * s_cdt[ d_H_const_mat_Y[tx + N*6] ]
               + d_Ht_const_mat_s[tx + N*7] * s_track[ d_H_const_mat_X[tx + N*7] ] * s_track[ d_H_const_mat_X[tx + N*7 + 144] ] * s_track[ d_H_const_mat_X[tx + N*7 + 288] ] * s_cdt[ d_H_const_mat_Y[tx + N*7] ]
               + d_Ht_const_mat_s[tx + N*8] * s_track[ d_H_const_mat_X[tx + N*8] ] * s_track[ d_H_const_mat_X[tx + N*8 + 144] ] * s_track[ d_H_const_mat_X[tx + N*8 + 288] ] * s_cdt[ d_H_const_mat_Y[tx + N*8] ];
    }*/
}

#endif

#ifndef params2coeffs_Eq17_h
#define params2coeffs_Eq17_h
// =======================================================================
//
// Modifications
//    Chien  21-10-12:   Initially Created
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min

namespace magmaHCWrapper {

  void params2coeffs_Eq17(magmaFloatComplex *h_target_params, magmaFloatComplex *h_target_coeffs)
  {
    magmaFloatComplex x11 = h_target_params[0];
    magmaFloatComplex x12 = h_target_params[1];
    magmaFloatComplex x13 = h_target_params[2];
    magmaFloatComplex x14 = h_target_params[3];
    magmaFloatComplex x15 = h_target_params[4];
    magmaFloatComplex x16 = h_target_params[5];
    magmaFloatComplex x17 = h_target_params[6];
    magmaFloatComplex x18 = h_target_params[7];
    magmaFloatComplex x19 = h_target_params[8];
    magmaFloatComplex x20 = h_target_params[9];
    magmaFloatComplex x21 = h_target_params[10];
    magmaFloatComplex x22 = h_target_params[11];
    magmaFloatComplex x23 = h_target_params[12];
    magmaFloatComplex x24 = h_target_params[13];
    magmaFloatComplex x25 = h_target_params[14];
    magmaFloatComplex x26 = h_target_params[15];
    magmaFloatComplex x27 = h_target_params[16];
    magmaFloatComplex x28 = h_target_params[17];
    magmaFloatComplex x29 = h_target_params[18];
    magmaFloatComplex x30 = h_target_params[19];
    magmaFloatComplex x31 = h_target_params[20];
    magmaFloatComplex x32 = h_target_params[21];
    magmaFloatComplex x33 = h_target_params[22];
    magmaFloatComplex x34 = h_target_params[23];
    magmaFloatComplex x35 = h_target_params[24];
    magmaFloatComplex x36 = h_target_params[25];
    magmaFloatComplex x37 = h_target_params[26];
    magmaFloatComplex x38 = h_target_params[27];
    magmaFloatComplex x39 = h_target_params[28];
    magmaFloatComplex x40 = h_target_params[29];
    magmaFloatComplex x41 = h_target_params[30];
    magmaFloatComplex x42 = h_target_params[31];
    magmaFloatComplex x43 = h_target_params[32];
    magmaFloatComplex x44 = h_target_params[33];

    h_target_coeffs[0] = (x11 - x14);
    h_target_coeffs[1] = (x11 - x17);
    h_target_coeffs[2] = (x11 - x20);
    h_target_coeffs[3] = (x11 - x23);
    h_target_coeffs[4] = (x12 - x15);
    h_target_coeffs[5] = (x12 - x18);
    h_target_coeffs[6] = (x12 - x21);
    h_target_coeffs[7] = (x12 - x24);
    h_target_coeffs[8] = (x29 - x26);
    h_target_coeffs[9] = (x32 - x26);
    h_target_coeffs[10] = (x35 - x26);
    h_target_coeffs[11] = (x38 - x26);
    h_target_coeffs[12] = (x30 - x27);
    h_target_coeffs[13] = (x33 - x27);
    h_target_coeffs[14] = (x36 - x27);
    h_target_coeffs[15] = (x39 - x27);
    h_target_coeffs[16] = (-x11*x41 - x12*x42 - x13 + x14*x41 + x15*x42 + x16);
    h_target_coeffs[17] = (-x11*x41 - x12*x42 - x13 + x17*x41 + x18*x42 + x19);
    h_target_coeffs[18] = (-x11*x41 - x12*x42 - x13 + x20*x41 + x21*x42 + x22);
    h_target_coeffs[19] = (-x11*x41 - x12*x42 - x13 + x23*x41 + x24*x42 + x25);
    h_target_coeffs[20] = (x39 + x27);
    h_target_coeffs[21] = (x26*x43 + x27*x44 + x28 - x29*x43 - x30*x44 - x31);
    h_target_coeffs[22] = (x26*x43 + x27*x44 + x28 - x32*x43 - x33*x44 - x34);
    h_target_coeffs[23] = (x26*x43 + x27*x44 + x28 - x35*x43 - x36*x44 - x37);
    h_target_coeffs[24] = (x26*x43 + x27*x44 + x28 - x38*x43 - x39*x44 - x40);
    h_target_coeffs[25] = (x11*x11 + x12*x12 + x13*x13 - x14*x14 - x15*x15 - x16*x16 - x26*x26 - x27*x27 - x28*x28 + x29*x29 + x30*x30 + x31*x31);
    h_target_coeffs[26] = (x11*x11 + x12*x12 + x13*x13 - x17*x17 - x18*x18 - x19*x19 - x26*x26 - x27*x27 - x28*x28 + x32*x32 + x33*x33 + x34*x34);
    h_target_coeffs[27] = (x11*x11 + x12*x12 + x13*x13 - x20*x20 - x21*x21 - x22*x22 - x26*x26 - x27*x27 - x28*x28 + x35*x35 + x36*x36 + x37*x37);
    h_target_coeffs[28] = (x11*x11 + x12*x12 + x13*x13 - x23*x23 - x24*x24 - x25*x25 - x26*x26 - x27*x27 - x28*x28 + x38*x38 + x39*x39 + x40*x40);
    h_target_coeffs[29] = (x38 + x26);
    h_target_coeffs[30] = MAGMA_C_ONE;
    h_target_coeffs[31] = MAGMA_C_ONE;
    h_target_coeffs[32] = MAGMA_C_ONE;
    h_target_coeffs[33] = MAGMA_C_ONE;
  }
} // end of namespace

#endif

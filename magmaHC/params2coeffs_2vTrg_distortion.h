#ifndef params2coeffs_2vTrg_distortion_h
#define params2coeffs_2vTrg_distortion_h
// =======================================================================
//
// Modifications
//    Chien  21-11-22:   Initially Created
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min

namespace magmaHCWrapper {

  void params2coeffs_2vTrg_distortion(magmaFloatComplex *h_target_params, magmaFloatComplex *h_target_coeffs)
  {
    magmaFloatComplex x6 = h_target_params[0];
    magmaFloatComplex x7 = h_target_params[1];
    magmaFloatComplex x8 = h_target_params[2];
    magmaFloatComplex x9 = h_target_params[3];
    magmaFloatComplex x10 = h_target_params[4];
    magmaFloatComplex x11 = h_target_params[5];
    magmaFloatComplex x12 = h_target_params[6];
    magmaFloatComplex x13 = h_target_params[7];
    magmaFloatComplex x14 = h_target_params[8];
    magmaFloatComplex x15 = h_target_params[9];
    magmaFloatComplex x16 = h_target_params[10];
    magmaFloatComplex x17 = h_target_params[11];
    magmaFloatComplex x18 = h_target_params[12];
    magmaFloatComplex x19 = h_target_params[13];
    magmaFloatComplex x20 = h_target_params[14];

    h_target_coeffs[0] = (x18*x19*x20);
    h_target_coeffs[1] = (x16*x19);
    h_target_coeffs[2] = (x17*x19);
    h_target_coeffs[3] = (x18*x19);
    h_target_coeffs[4] = (x12*x20);
    h_target_coeffs[5] = (x10);
    h_target_coeffs[6] = (x11);
    h_target_coeffs[7] = (x12);
    h_target_coeffs[8] = (x15*x20);
    h_target_coeffs[9] = (x13);
    h_target_coeffs[10] = (x14);
    h_target_coeffs[11] = (x15);
    h_target_coeffs[12] = (x18*x20);
    h_target_coeffs[13] = (x16);
    h_target_coeffs[14] = (x17);
    h_target_coeffs[15] = (x18);
    h_target_coeffs[16] = (4*x18*x19*x20);
    h_target_coeffs[17] = (4*x16*x19);
    h_target_coeffs[18] = (4*x17*x19);
    h_target_coeffs[19] = (4*x18*x19);
    h_target_coeffs[20] = MAGMA_C_ONE + MAGMA_C_ONE;
    h_target_coeffs[21] = (2*x12*x20);
    h_target_coeffs[22] = (2*x10);
    h_target_coeffs[23] = (2*x11);
    h_target_coeffs[24] = (2*x12);
    h_target_coeffs[25] = (-2*x6);
    h_target_coeffs[26] = (2*x15*x20);
    h_target_coeffs[27] = (2*x13);
    h_target_coeffs[28] = (2*x14);
    h_target_coeffs[29] = (2*x15);
    h_target_coeffs[30] = (-2*x7);
    h_target_coeffs[31] = (2*x12*x19);
    h_target_coeffs[32] = (4*x16*x20);
    h_target_coeffs[33] = (4*x17*x20);
    h_target_coeffs[34] = (4*x18*x20);
    h_target_coeffs[35] = (-2*x8);
    h_target_coeffs[36] = (2*x15*x19);
    h_target_coeffs[37] = (-2*x9);
  }
} // end of namespace

#endif


#ifndef dev_eval_indxing_optpose4pt_cuh_
#define dev_eval_indxing_optpose4pt_cuh_
// ============================================================================
// Device function for evaluating the parallel indexing for Hx, Ht, and H of
// optpose4pt problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// -- cuda included --
#include <cuda_runtime.h>

// -- magma included --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

namespace magmaHCWrapper {

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_cdt_optpose4pt(
        const int tx, float t, magmaFloatComplex s_vector_cdt[coefsCount],
        magmaFloatComplex r_startCoefs[coefsCount], magmaFloatComplex r_targetCoefs[coefsCount])
    {
        #pragma unroll
        for (int i = 0; i < 17; i++) {
            s_vector_cdt[ tx + i * N ] = r_targetCoefs[tx + i * N] * t - r_startCoefs[tx + i * N] * (t-1);
        }
        if (tx < 2) {
          s_vector_cdt[ tx + 85 ] = r_targetCoefs[tx + 85] * t - r_startCoefs[tx + 85] * (t-1);
        }
    }

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_Jacobian_optpose4pt(
        const int tx, magmaFloatComplex *s_track, magmaFloatComplex r_cgesvA[N], const magma_int_t* __restrict__ d_const_mat_s,
        const magma_int_t* __restrict__ d_const_mat_X, const magma_int_t* __restrict__ d_const_mat_Y,
        magmaFloatComplex s_cdt[coefsCount])
    {
        // -- the maximal terms of Hx in optpose4pt problem is 6 --
        #pragma unroll
        for(int i = 0; i < N; i++) {
          r_cgesvA[i] = d_const_mat_s[tx + i*5] * s_track[ d_const_mat_X[tx + i*5] ] * s_cdt[ d_const_mat_Y[tx + i*5] ]
                      + d_const_mat_s[tx + i*5 + 25] * s_track[ d_const_mat_X[tx + i*5 + 25] ] * s_cdt[ d_const_mat_Y[tx + i*5 + 25] ]
                      + d_const_mat_s[tx + i*5 + 50] * s_track[ d_const_mat_X[tx + i*5 + 50] ] * s_cdt[ d_const_mat_Y[tx + i*5 + 50] ]
                      + d_const_mat_s[tx + i*5 + 75] * s_track[ d_const_mat_X[tx + i*5 + 75] ] * s_cdt[ d_const_mat_Y[tx + i*5 + 75] ]
                      + d_const_mat_s[tx + i*5 + 100] * s_track[ d_const_mat_X[tx + i*5 + 100] ] * s_cdt[ d_const_mat_Y[tx + i*5 + 100] ]
                      + d_const_mat_s[tx + i*5 + 125] * s_track[ d_const_mat_X[tx + i*5 + 125] ] * s_cdt[ d_const_mat_Y[tx + i*5 + 125] ];
        }
    }

    __device__ __inline__ void
    eval_Ht_optpose4pt(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_Ht_const_mat_X,
      const magma_int_t* __restrict__ d_Ht_const_mat_Y, const magmaFloatComplex* __restrict__ d_const_cd)
    {
      // -- the maximal terms of Ht in optpose4pt problem is 21 --
      // -- N*maximal terms of Ht = 5*21 = 105
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_Ht_const_mat_X[tx] ] * s_track[ d_Ht_const_mat_X[tx + 105] ] * d_const_cd[ d_Ht_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + 5] * s_track[ d_Ht_const_mat_X[tx + 5] ] * s_track[ d_Ht_const_mat_X[tx + 5 + 105] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 5] ]
               + d_Ht_const_mat_s[tx + 10] * s_track[ d_Ht_const_mat_X[tx + 10] ] * s_track[ d_Ht_const_mat_X[tx + 10 + 105] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 10] ]
               + d_Ht_const_mat_s[tx + 15] * s_track[ d_Ht_const_mat_X[tx + 15] ] * s_track[ d_Ht_const_mat_X[tx + 15 + 105] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 15] ]
               + d_Ht_const_mat_s[tx + 20] * s_track[ d_Ht_const_mat_X[tx + 20] ] * s_track[ d_Ht_const_mat_X[tx + 20 + 105] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 20] ]
               + d_Ht_const_mat_s[tx + 25] * s_track[ d_Ht_const_mat_X[tx + 25] ] * s_track[ d_Ht_const_mat_X[tx + 25 + 105] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 25] ]
               + d_Ht_const_mat_s[tx + 30] * s_track[ d_Ht_const_mat_X[tx + 30] ] * s_track[ d_Ht_const_mat_X[tx + 30 + 105] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 30] ]
               + d_Ht_const_mat_s[tx + 35] * s_track[ d_Ht_const_mat_X[tx + 35] ] * s_track[ d_Ht_const_mat_X[tx + 35 + 105] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 35] ]
               + d_Ht_const_mat_s[tx + 40] * s_track[ d_Ht_const_mat_X[tx + 40] ] * s_track[ d_Ht_const_mat_X[tx + 40 + 105] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 40] ]
               + d_Ht_const_mat_s[tx + 45] * s_track[ d_Ht_const_mat_X[tx + 45] ] * s_track[ d_Ht_const_mat_X[tx + 45 + 105] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 45] ]
               + d_Ht_const_mat_s[tx + 50] * s_track[ d_Ht_const_mat_X[tx + 50] ] * s_track[ d_Ht_const_mat_X[tx + 50 + 105] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 50] ]
               + d_Ht_const_mat_s[tx + 55] * s_track[ d_Ht_const_mat_X[tx + 55] ] * s_track[ d_Ht_const_mat_X[tx + 55 + 105] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 55] ]
               + d_Ht_const_mat_s[tx + 60] * s_track[ d_Ht_const_mat_X[tx + 60] ] * s_track[ d_Ht_const_mat_X[tx + 60 + 105] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 60] ]
               + d_Ht_const_mat_s[tx + 65] * s_track[ d_Ht_const_mat_X[tx + 65] ] * s_track[ d_Ht_const_mat_X[tx + 65 + 105] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 65] ]
               + d_Ht_const_mat_s[tx + 70] * s_track[ d_Ht_const_mat_X[tx + 70] ] * s_track[ d_Ht_const_mat_X[tx + 70 + 105] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 70] ]
               + d_Ht_const_mat_s[tx + 75] * s_track[ d_Ht_const_mat_X[tx + 75] ] * s_track[ d_Ht_const_mat_X[tx + 75 + 105] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 75] ]
               + d_Ht_const_mat_s[tx + 80] * s_track[ d_Ht_const_mat_X[tx + 80] ] * s_track[ d_Ht_const_mat_X[tx + 80 + 105] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 80] ]
               + d_Ht_const_mat_s[tx + 85] * s_track[ d_Ht_const_mat_X[tx + 85] ] * s_track[ d_Ht_const_mat_X[tx + 85 + 105] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 85] ]
               + d_Ht_const_mat_s[tx + 90] * s_track[ d_Ht_const_mat_X[tx + 90] ] * s_track[ d_Ht_const_mat_X[tx + 90 + 105] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 90] ]
               + d_Ht_const_mat_s[tx + 95] * s_track[ d_Ht_const_mat_X[tx + 95] ] * s_track[ d_Ht_const_mat_X[tx + 95 + 105] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 95] ]
               + d_Ht_const_mat_s[tx + 100] * s_track[ d_Ht_const_mat_X[tx + 100] ] * s_track[ d_Ht_const_mat_X[tx + 100 + 105] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 100] ];
    }

    template<int coefsCount>
    __device__ __inline__ void
    eval_H_optpose4pt(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_H_const_mat_X,
      const magma_int_t* __restrict__ d_H_const_mat_Y, magmaFloatComplex s_cdt[coefsCount])
    {
      // -- same as Ht, except replacing cd by cdt --
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_H_const_mat_X[tx] ] * s_track[ d_H_const_mat_X[tx + 105] ] * s_cdt[ d_H_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + 5] * s_track[ d_H_const_mat_X[tx + 5] ] * s_track[ d_H_const_mat_X[tx + 5 + 105] ] * s_cdt[ d_H_const_mat_Y[tx + 5] ]
               + d_Ht_const_mat_s[tx + 10] * s_track[ d_H_const_mat_X[tx + 10] ] * s_track[ d_H_const_mat_X[tx + 10 + 105] ] * s_cdt[ d_H_const_mat_Y[tx + 10] ]
               + d_Ht_const_mat_s[tx + 15] * s_track[ d_H_const_mat_X[tx + 15] ] * s_track[ d_H_const_mat_X[tx + 15 + 105] ] * s_cdt[ d_H_const_mat_Y[tx + 15] ]
               + d_Ht_const_mat_s[tx + 20] * s_track[ d_H_const_mat_X[tx + 20] ] * s_track[ d_H_const_mat_X[tx + 20 + 105] ] * s_cdt[ d_H_const_mat_Y[tx + 20] ]
               + d_Ht_const_mat_s[tx + 25] * s_track[ d_H_const_mat_X[tx + 25] ] * s_track[ d_H_const_mat_X[tx + 25 + 105] ] * s_cdt[ d_H_const_mat_Y[tx + 25] ]
               + d_Ht_const_mat_s[tx + 30] * s_track[ d_H_const_mat_X[tx + 30] ] * s_track[ d_H_const_mat_X[tx + 30 + 105] ] * s_cdt[ d_H_const_mat_Y[tx + 30] ]
               + d_Ht_const_mat_s[tx + 35] * s_track[ d_H_const_mat_X[tx + 35] ] * s_track[ d_H_const_mat_X[tx + 35 + 105] ] * s_cdt[ d_H_const_mat_Y[tx + 35] ]
               + d_Ht_const_mat_s[tx + 40] * s_track[ d_H_const_mat_X[tx + 40] ] * s_track[ d_H_const_mat_X[tx + 40 + 105] ] * s_cdt[ d_H_const_mat_Y[tx + 40] ]
               + d_Ht_const_mat_s[tx + 45] * s_track[ d_H_const_mat_X[tx + 45] ] * s_track[ d_H_const_mat_X[tx + 45 + 105] ] * s_cdt[ d_H_const_mat_Y[tx + 45] ]
               + d_Ht_const_mat_s[tx + 50] * s_track[ d_H_const_mat_X[tx + 50] ] * s_track[ d_H_const_mat_X[tx + 50 + 105] ] * s_cdt[ d_H_const_mat_Y[tx + 50] ]
               + d_Ht_const_mat_s[tx + 55] * s_track[ d_H_const_mat_X[tx + 55] ] * s_track[ d_H_const_mat_X[tx + 55 + 105] ] * s_cdt[ d_H_const_mat_Y[tx + 55] ]
               + d_Ht_const_mat_s[tx + 60] * s_track[ d_H_const_mat_X[tx + 60] ] * s_track[ d_H_const_mat_X[tx + 60 + 105] ] * s_cdt[ d_H_const_mat_Y[tx + 60] ]
               + d_Ht_const_mat_s[tx + 65] * s_track[ d_H_const_mat_X[tx + 65] ] * s_track[ d_H_const_mat_X[tx + 65 + 105] ] * s_cdt[ d_H_const_mat_Y[tx + 65] ]
               + d_Ht_const_mat_s[tx + 70] * s_track[ d_H_const_mat_X[tx + 70] ] * s_track[ d_H_const_mat_X[tx + 70 + 105] ] * s_cdt[ d_H_const_mat_Y[tx + 70] ]
               + d_Ht_const_mat_s[tx + 75] * s_track[ d_H_const_mat_X[tx + 75] ] * s_track[ d_H_const_mat_X[tx + 75 + 105] ] * s_cdt[ d_H_const_mat_Y[tx + 75] ]
               + d_Ht_const_mat_s[tx + 80] * s_track[ d_H_const_mat_X[tx + 80] ] * s_track[ d_H_const_mat_X[tx + 80 + 105] ] * s_cdt[ d_H_const_mat_Y[tx + 80] ]
               + d_Ht_const_mat_s[tx + 85] * s_track[ d_H_const_mat_X[tx + 85] ] * s_track[ d_H_const_mat_X[tx + 85 + 105] ] * s_cdt[ d_H_const_mat_Y[tx + 85] ]
               + d_Ht_const_mat_s[tx + 90] * s_track[ d_H_const_mat_X[tx + 90] ] * s_track[ d_H_const_mat_X[tx + 90 + 105] ] * s_cdt[ d_H_const_mat_Y[tx + 90] ]
               + d_Ht_const_mat_s[tx + 95] * s_track[ d_H_const_mat_X[tx + 95] ] * s_track[ d_H_const_mat_X[tx + 95 + 105] ] * s_cdt[ d_H_const_mat_Y[tx + 95] ]
               + d_Ht_const_mat_s[tx + 100] * s_track[ d_H_const_mat_X[tx + 100] ] * s_track[ d_H_const_mat_X[tx + 100 + 105] ] * s_cdt[ d_H_const_mat_Y[tx + 100] ];
    }
}

#endif

#ifndef params2coeffs_4vTrg_h
#define params2coeffs_4vTrg_h
// =======================================================================
//
// Modifications
//    Chien  21-10-12:   Initially Created
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min

namespace magmaHCWrapper {

  void params2coeffs_4vTrg(magmaFloatComplex *h_target_params, magmaFloatComplex *h_target_coeffs)
  {
    for (int i = 0; i < 62; i++) {
      h_target_coeffs[i] = h_target_params[i];
    }

    h_target_coeffs[62] = MAGMA_C_MAKE(2.0, 0.0);
  }
} // end of namespace

#endif

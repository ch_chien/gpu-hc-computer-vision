#ifndef dev_eval_indxing_optimalPnPQ_cuh_
#define dev_eval_indxing_optimalPnPQ_cuh_
// ============================================================================
// Device function for evaluating the parallel indexing for Hx, Ht, and H of
// optimalPnPQ problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// -- cuda included --
#include <cuda_runtime.h>

// -- magma included --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

namespace magmaHCWrapper {

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_cdt_optimalPnPQ(
        const int tx, float t, magmaFloatComplex s_vector_cdt[coefsCount],
        magmaFloatComplex r_startCoefs[coefsCount], magmaFloatComplex r_targetCoefs[coefsCount])
    {
        #pragma unroll
        for (int i = 0; i < 26; i++) {
            s_vector_cdt[ tx + i * N ] = r_targetCoefs[tx + i * N] * t - r_startCoefs[tx + i * N] * (t-1);
        }
        if (tx < 3) {
          s_vector_cdt[ tx + 104 ] = r_targetCoefs[tx + 104] * t - r_startCoefs[tx + 104] * (t-1);
        }
    }

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_Jacobian_optimalPnPQ(
        const int tx, magmaFloatComplex *s_track, magmaFloatComplex r_cgesvA[N], const magma_int_t* __restrict__ d_const_mat_s,
        const magma_int_t* __restrict__ d_const_mat_X, const magma_int_t* __restrict__ d_const_mat_Y,
        magmaFloatComplex s_cdt[coefsCount])
    {
        // -- the maximal terms of Hx in optimalPnPQ problem is 20 --
        // -- 320 = 4(vars) * 4(vars) * 20(terms) --
        #pragma unroll
        for(int i = 0; i < N; i++) {
          r_cgesvA[i] = d_const_mat_s[tx + i*N] * s_track[ d_const_mat_X[tx + i*N] ] * s_track[ d_const_mat_X[tx + i*N + 320] ] * s_track[ d_const_mat_X[tx + i*N + 640] ] * s_cdt[ d_const_mat_Y[tx + i*N] ]
                      + d_const_mat_s[tx + i*N + N*N] * s_track[ d_const_mat_X[tx + i*N + N*N] ] * s_track[ d_const_mat_X[tx + i*N + N*N + 320] ] * s_track[ d_const_mat_X[tx + i*N + N*N + 640] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N] ]
                      + d_const_mat_s[tx + i*N + 2*N*N] * s_track[ d_const_mat_X[tx + i*N + 2*N*N] ] * s_track[ d_const_mat_X[tx + i*N + 2*N*N + 320] ] * s_track[ d_const_mat_X[tx + i*N + 2*N*N + 640] ] * s_cdt[ d_const_mat_Y[tx + i*N + 2*N*N] ]
                      + d_const_mat_s[tx + i*N + 3*N*N] * s_track[ d_const_mat_X[tx + i*N + 3*N*N] ] * s_track[ d_const_mat_X[tx + i*N + 3*N*N + 320] ] * s_track[ d_const_mat_X[tx + i*N + 3*N*N + 640] ] * s_cdt[ d_const_mat_Y[tx + i*N + 3*N*N] ]
                      + d_const_mat_s[tx + i*N + 4*N*N] * s_track[ d_const_mat_X[tx + i*N + 4*N*N] ] * s_track[ d_const_mat_X[tx + i*N + 4*N*N + 320] ] * s_track[ d_const_mat_X[tx + i*N + 4*N*N + 640] ] * s_cdt[ d_const_mat_Y[tx + i*N + 4*N*N] ]
                      + d_const_mat_s[tx + i*N + 5*N*N] * s_track[ d_const_mat_X[tx + i*N + 5*N*N] ] * s_track[ d_const_mat_X[tx + i*N + 5*N*N + 320] ] * s_track[ d_const_mat_X[tx + i*N + 5*N*N + 640] ] * s_cdt[ d_const_mat_Y[tx + i*N + 5*N*N] ]
                      + d_const_mat_s[tx + i*N + 6*N*N] * s_track[ d_const_mat_X[tx + i*N + 6*N*N] ] * s_track[ d_const_mat_X[tx + i*N + 6*N*N + 320] ] * s_track[ d_const_mat_X[tx + i*N + 6*N*N + 640] ] * s_cdt[ d_const_mat_Y[tx + i*N + 6*N*N] ]
                      + d_const_mat_s[tx + i*N + 7*N*N] * s_track[ d_const_mat_X[tx + i*N + 7*N*N] ] * s_track[ d_const_mat_X[tx + i*N + 7*N*N + 320] ] * s_track[ d_const_mat_X[tx + i*N + 7*N*N + 640] ] * s_cdt[ d_const_mat_Y[tx + i*N + 7*N*N] ]
                      + d_const_mat_s[tx + i*N + 8*N*N] * s_track[ d_const_mat_X[tx + i*N + 8*N*N] ] * s_track[ d_const_mat_X[tx + i*N + 8*N*N + 320] ] * s_track[ d_const_mat_X[tx + i*N + 8*N*N + 640] ] * s_cdt[ d_const_mat_Y[tx + i*N + 8*N*N] ]
                      + d_const_mat_s[tx + i*N + 9*N*N] * s_track[ d_const_mat_X[tx + i*N + 9*N*N] ] * s_track[ d_const_mat_X[tx + i*N + 9*N*N + 320] ] * s_track[ d_const_mat_X[tx + i*N + 9*N*N + 640] ] * s_cdt[ d_const_mat_Y[tx + i*N + 9*N*N] ]
                      + d_const_mat_s[tx + i*N + 10*N*N] * s_track[ d_const_mat_X[tx + i*N + 10*N*N] ] * s_track[ d_const_mat_X[tx + i*N + 10*N*N + 320] ] * s_track[ d_const_mat_X[tx + i*N + 10*N*N + 640] ] * s_cdt[ d_const_mat_Y[tx + i*N + 10*N*N] ]
                      + d_const_mat_s[tx + i*N + 11*N*N] * s_track[ d_const_mat_X[tx + i*N + 11*N*N] ] * s_track[ d_const_mat_X[tx + i*N + 11*N*N + 320] ] * s_track[ d_const_mat_X[tx + i*N + 11*N*N + 640] ] * s_cdt[ d_const_mat_Y[tx + i*N + 11*N*N] ]
                      + d_const_mat_s[tx + i*N + 12*N*N] * s_track[ d_const_mat_X[tx + i*N + 12*N*N] ] * s_track[ d_const_mat_X[tx + i*N + 12*N*N + 320] ] * s_track[ d_const_mat_X[tx + i*N + 12*N*N + 640] ] * s_cdt[ d_const_mat_Y[tx + i*N + 12*N*N] ]
                      + d_const_mat_s[tx + i*N + 13*N*N] * s_track[ d_const_mat_X[tx + i*N + 13*N*N] ] * s_track[ d_const_mat_X[tx + i*N + 13*N*N + 320] ] * s_track[ d_const_mat_X[tx + i*N + 13*N*N + 640] ] * s_cdt[ d_const_mat_Y[tx + i*N + 13*N*N] ]
                      + d_const_mat_s[tx + i*N + 14*N*N] * s_track[ d_const_mat_X[tx + i*N + 14*N*N] ] * s_track[ d_const_mat_X[tx + i*N + 14*N*N + 320] ] * s_track[ d_const_mat_X[tx + i*N + 14*N*N + 640] ] * s_cdt[ d_const_mat_Y[tx + i*N + 14*N*N] ]
                      + d_const_mat_s[tx + i*N + 15*N*N] * s_track[ d_const_mat_X[tx + i*N + 15*N*N] ] * s_track[ d_const_mat_X[tx + i*N + 15*N*N + 320] ] * s_track[ d_const_mat_X[tx + i*N + 15*N*N + 640] ] * s_cdt[ d_const_mat_Y[tx + i*N + 15*N*N] ]
                      + d_const_mat_s[tx + i*N + 16*N*N] * s_track[ d_const_mat_X[tx + i*N + 16*N*N] ] * s_track[ d_const_mat_X[tx + i*N + 16*N*N + 320] ] * s_track[ d_const_mat_X[tx + i*N + 16*N*N + 640] ] * s_cdt[ d_const_mat_Y[tx + i*N + 16*N*N] ]
                      + d_const_mat_s[tx + i*N + 17*N*N] * s_track[ d_const_mat_X[tx + i*N + 17*N*N] ] * s_track[ d_const_mat_X[tx + i*N + 17*N*N + 320] ] * s_track[ d_const_mat_X[tx + i*N + 17*N*N + 640] ] * s_cdt[ d_const_mat_Y[tx + i*N + 17*N*N] ]
                      + d_const_mat_s[tx + i*N + 18*N*N] * s_track[ d_const_mat_X[tx + i*N + 18*N*N] ] * s_track[ d_const_mat_X[tx + i*N + 18*N*N + 320] ] * s_track[ d_const_mat_X[tx + i*N + 18*N*N + 640] ] * s_cdt[ d_const_mat_Y[tx + i*N + 18*N*N] ]
                      + d_const_mat_s[tx + i*N + 19*N*N] * s_track[ d_const_mat_X[tx + i*N + 19*N*N] ] * s_track[ d_const_mat_X[tx + i*N + 19*N*N + 320] ] * s_track[ d_const_mat_X[tx + i*N + 19*N*N + 640] ] * s_cdt[ d_const_mat_Y[tx + i*N + 19*N*N] ];
        }
    }

    template<int N>
    __device__ __inline__ void
    eval_Ht_optimalPnPQ(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_Ht_const_mat_X,
      const magma_int_t* __restrict__ d_Ht_const_mat_Y, const magmaFloatComplex* __restrict__ d_const_cd)
    {
      // -- the maximal terms of Ht in optimalPnPQ problem is 35 --
      // -- N*maximal terms of Ht = 4*35 = 140
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_Ht_const_mat_X[tx] ] * s_track[ d_Ht_const_mat_X[tx + 140] ] * s_track[ d_Ht_const_mat_X[tx + 280] ] * s_track[ d_Ht_const_mat_X[tx + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx] ]
              + d_Ht_const_mat_s[tx + N] * s_track[ d_Ht_const_mat_X[tx + N] ] * s_track[ d_Ht_const_mat_X[tx + N + 140] ] * s_track[ d_Ht_const_mat_X[tx + N + 280] ] * s_track[ d_Ht_const_mat_X[tx + N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N] ]
              + d_Ht_const_mat_s[tx +2*N] * s_track[ d_Ht_const_mat_X[tx +2*N] ] * s_track[ d_Ht_const_mat_X[tx +2*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +2*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +2*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +2*N] ]
              + d_Ht_const_mat_s[tx +3*N] * s_track[ d_Ht_const_mat_X[tx +3*N] ] * s_track[ d_Ht_const_mat_X[tx +3*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +3*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +3*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +3*N] ]
              + d_Ht_const_mat_s[tx +4*N] * s_track[ d_Ht_const_mat_X[tx +4*N] ] * s_track[ d_Ht_const_mat_X[tx +4*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +4*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +4*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +4*N] ]
              + d_Ht_const_mat_s[tx +5*N] * s_track[ d_Ht_const_mat_X[tx +5*N] ] * s_track[ d_Ht_const_mat_X[tx +5*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +5*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +5*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +5*N] ]
              + d_Ht_const_mat_s[tx +6*N] * s_track[ d_Ht_const_mat_X[tx +6*N] ] * s_track[ d_Ht_const_mat_X[tx +6*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +6*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +6*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +6*N] ]
              + d_Ht_const_mat_s[tx +7*N] * s_track[ d_Ht_const_mat_X[tx +7*N] ] * s_track[ d_Ht_const_mat_X[tx +7*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +7*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +7*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +7*N] ]
              + d_Ht_const_mat_s[tx +8*N] * s_track[ d_Ht_const_mat_X[tx +8*N] ] * s_track[ d_Ht_const_mat_X[tx +8*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +8*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +8*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +8*N] ]
              + d_Ht_const_mat_s[tx +9*N] * s_track[ d_Ht_const_mat_X[tx +9*N] ] * s_track[ d_Ht_const_mat_X[tx +9*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +9*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +9*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +9*N] ]
              + d_Ht_const_mat_s[tx +10*N] * s_track[ d_Ht_const_mat_X[tx +10*N] ] * s_track[ d_Ht_const_mat_X[tx +10*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +10*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +10*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +10*N] ]
              + d_Ht_const_mat_s[tx +11*N] * s_track[ d_Ht_const_mat_X[tx +11*N] ] * s_track[ d_Ht_const_mat_X[tx +11*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +11*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +11*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +11*N] ]
              + d_Ht_const_mat_s[tx +12*N] * s_track[ d_Ht_const_mat_X[tx +12*N] ] * s_track[ d_Ht_const_mat_X[tx +12*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +12*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +12*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +12*N] ]
              + d_Ht_const_mat_s[tx +13*N] * s_track[ d_Ht_const_mat_X[tx +13*N] ] * s_track[ d_Ht_const_mat_X[tx +13*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +13*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +13*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +13*N] ]
              + d_Ht_const_mat_s[tx +14*N] * s_track[ d_Ht_const_mat_X[tx +14*N] ] * s_track[ d_Ht_const_mat_X[tx +14*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +14*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +14*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +14*N] ]
              + d_Ht_const_mat_s[tx +15*N] * s_track[ d_Ht_const_mat_X[tx +15*N] ] * s_track[ d_Ht_const_mat_X[tx +15*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +15*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +15*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +15*N] ]
              + d_Ht_const_mat_s[tx +16*N] * s_track[ d_Ht_const_mat_X[tx +16*N] ] * s_track[ d_Ht_const_mat_X[tx +16*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +16*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +16*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +16*N] ]
              + d_Ht_const_mat_s[tx +17*N] * s_track[ d_Ht_const_mat_X[tx +17*N] ] * s_track[ d_Ht_const_mat_X[tx +17*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +17*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +17*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +17*N] ]
              + d_Ht_const_mat_s[tx +18*N] * s_track[ d_Ht_const_mat_X[tx +18*N] ] * s_track[ d_Ht_const_mat_X[tx +18*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +18*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +18*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +18*N] ]
              + d_Ht_const_mat_s[tx +19*N] * s_track[ d_Ht_const_mat_X[tx +19*N] ] * s_track[ d_Ht_const_mat_X[tx +19*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +19*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +19*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +19*N] ]
              + d_Ht_const_mat_s[tx +20*N] * s_track[ d_Ht_const_mat_X[tx +20*N] ] * s_track[ d_Ht_const_mat_X[tx +20*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +20*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +20*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +20*N] ]
              + d_Ht_const_mat_s[tx +21*N] * s_track[ d_Ht_const_mat_X[tx +21*N] ] * s_track[ d_Ht_const_mat_X[tx +21*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +21*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +21*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +21*N] ]
              + d_Ht_const_mat_s[tx +22*N] * s_track[ d_Ht_const_mat_X[tx +22*N] ] * s_track[ d_Ht_const_mat_X[tx +22*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +22*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +22*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +22*N] ]
              + d_Ht_const_mat_s[tx +23*N] * s_track[ d_Ht_const_mat_X[tx +23*N] ] * s_track[ d_Ht_const_mat_X[tx +23*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +23*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +23*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +23*N] ]
              + d_Ht_const_mat_s[tx +24*N] * s_track[ d_Ht_const_mat_X[tx +24*N] ] * s_track[ d_Ht_const_mat_X[tx +24*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +24*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +24*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +24*N] ]
              + d_Ht_const_mat_s[tx +25*N] * s_track[ d_Ht_const_mat_X[tx +25*N] ] * s_track[ d_Ht_const_mat_X[tx +25*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +25*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +25*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +25*N] ]
              + d_Ht_const_mat_s[tx +26*N] * s_track[ d_Ht_const_mat_X[tx +26*N] ] * s_track[ d_Ht_const_mat_X[tx +26*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +26*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +26*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +26*N] ]
              + d_Ht_const_mat_s[tx +27*N] * s_track[ d_Ht_const_mat_X[tx +27*N] ] * s_track[ d_Ht_const_mat_X[tx +27*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +27*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +27*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +27*N] ]
              + d_Ht_const_mat_s[tx +28*N] * s_track[ d_Ht_const_mat_X[tx +28*N] ] * s_track[ d_Ht_const_mat_X[tx +28*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +28*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +28*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +28*N] ]
              + d_Ht_const_mat_s[tx +29*N] * s_track[ d_Ht_const_mat_X[tx +29*N] ] * s_track[ d_Ht_const_mat_X[tx +29*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +29*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +29*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +29*N] ]
              + d_Ht_const_mat_s[tx +30*N] * s_track[ d_Ht_const_mat_X[tx +30*N] ] * s_track[ d_Ht_const_mat_X[tx +30*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +30*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +30*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +30*N] ]
              + d_Ht_const_mat_s[tx +31*N] * s_track[ d_Ht_const_mat_X[tx +31*N] ] * s_track[ d_Ht_const_mat_X[tx +31*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +31*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +31*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +31*N] ]
              + d_Ht_const_mat_s[tx +32*N] * s_track[ d_Ht_const_mat_X[tx +32*N] ] * s_track[ d_Ht_const_mat_X[tx +32*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +32*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +32*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +32*N] ]
              + d_Ht_const_mat_s[tx +33*N] * s_track[ d_Ht_const_mat_X[tx +33*N] ] * s_track[ d_Ht_const_mat_X[tx +33*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +33*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +33*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +33*N] ]
              + d_Ht_const_mat_s[tx +34*N] * s_track[ d_Ht_const_mat_X[tx +34*N] ] * s_track[ d_Ht_const_mat_X[tx +34*N + 140] ] * s_track[ d_Ht_const_mat_X[tx +34*N + 280] ] * s_track[ d_Ht_const_mat_X[tx +34*N + 420] ] * d_const_cd[ d_Ht_const_mat_Y[tx +34*N] ];
    }

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_H_optimalPnPQ(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_H_const_mat_X,
      const magma_int_t* __restrict__ d_H_const_mat_Y, magmaFloatComplex s_cdt[coefsCount])
    {
      // -- same as Ht, except replacing cd by cdt --
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_H_const_mat_X[tx] ] * s_track[ d_H_const_mat_X[tx + 140] ] * s_track[ d_H_const_mat_X[tx + 280] ] * s_track[ d_H_const_mat_X[tx + 420] ] * s_cdt[ d_H_const_mat_Y[tx] ]
              + d_Ht_const_mat_s[tx + N] * s_track[ d_H_const_mat_X[tx + N] ] * s_track[ d_H_const_mat_X[tx + N + 140] ] * s_track[ d_H_const_mat_X[tx + N + 280] ] * s_track[ d_H_const_mat_X[tx + N + 420] ] * s_cdt[ d_H_const_mat_Y[tx + N] ]
              + d_Ht_const_mat_s[tx +2*N] * s_track[ d_H_const_mat_X[tx +2*N] ] * s_track[ d_H_const_mat_X[tx +2*N + 140] ] * s_track[ d_H_const_mat_X[tx +2*N + 280] ] * s_track[ d_H_const_mat_X[tx +2*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +2*N] ]
              + d_Ht_const_mat_s[tx +3*N] * s_track[ d_H_const_mat_X[tx +3*N] ] * s_track[ d_H_const_mat_X[tx +3*N + 140] ] * s_track[ d_H_const_mat_X[tx +3*N + 280] ] * s_track[ d_H_const_mat_X[tx +3*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +3*N] ]
              + d_Ht_const_mat_s[tx +4*N] * s_track[ d_H_const_mat_X[tx +4*N] ] * s_track[ d_H_const_mat_X[tx +4*N + 140] ] * s_track[ d_H_const_mat_X[tx +4*N + 280] ] * s_track[ d_H_const_mat_X[tx +4*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +4*N] ]
              + d_Ht_const_mat_s[tx +5*N] * s_track[ d_H_const_mat_X[tx +5*N] ] * s_track[ d_H_const_mat_X[tx +5*N + 140] ] * s_track[ d_H_const_mat_X[tx +5*N + 280] ] * s_track[ d_H_const_mat_X[tx +5*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +5*N] ]
              + d_Ht_const_mat_s[tx +6*N] * s_track[ d_H_const_mat_X[tx +6*N] ] * s_track[ d_H_const_mat_X[tx +6*N + 140] ] * s_track[ d_H_const_mat_X[tx +6*N + 280] ] * s_track[ d_H_const_mat_X[tx +6*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +6*N] ]
              + d_Ht_const_mat_s[tx +7*N] * s_track[ d_H_const_mat_X[tx +7*N] ] * s_track[ d_H_const_mat_X[tx +7*N + 140] ] * s_track[ d_H_const_mat_X[tx +7*N + 280] ] * s_track[ d_H_const_mat_X[tx +7*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +7*N] ]
              + d_Ht_const_mat_s[tx +8*N] * s_track[ d_H_const_mat_X[tx +8*N] ] * s_track[ d_H_const_mat_X[tx +8*N + 140] ] * s_track[ d_H_const_mat_X[tx +8*N + 280] ] * s_track[ d_H_const_mat_X[tx +8*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +8*N] ]
              + d_Ht_const_mat_s[tx +9*N] * s_track[ d_H_const_mat_X[tx +9*N] ] * s_track[ d_H_const_mat_X[tx +9*N + 140] ] * s_track[ d_H_const_mat_X[tx +9*N + 280] ] * s_track[ d_H_const_mat_X[tx +9*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +9*N] ]
              + d_Ht_const_mat_s[tx +10*N] * s_track[ d_H_const_mat_X[tx +10*N] ] * s_track[ d_H_const_mat_X[tx +10*N + 140] ] * s_track[ d_H_const_mat_X[tx +10*N + 280] ] * s_track[ d_H_const_mat_X[tx +10*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +10*N] ]
              + d_Ht_const_mat_s[tx +11*N] * s_track[ d_H_const_mat_X[tx +11*N] ] * s_track[ d_H_const_mat_X[tx +11*N + 140] ] * s_track[ d_H_const_mat_X[tx +11*N + 280] ] * s_track[ d_H_const_mat_X[tx +11*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +11*N] ]
              + d_Ht_const_mat_s[tx +12*N] * s_track[ d_H_const_mat_X[tx +12*N] ] * s_track[ d_H_const_mat_X[tx +12*N + 140] ] * s_track[ d_H_const_mat_X[tx +12*N + 280] ] * s_track[ d_H_const_mat_X[tx +12*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +12*N] ]
              + d_Ht_const_mat_s[tx +13*N] * s_track[ d_H_const_mat_X[tx +13*N] ] * s_track[ d_H_const_mat_X[tx +13*N + 140] ] * s_track[ d_H_const_mat_X[tx +13*N + 280] ] * s_track[ d_H_const_mat_X[tx +13*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +13*N] ]
              + d_Ht_const_mat_s[tx +14*N] * s_track[ d_H_const_mat_X[tx +14*N] ] * s_track[ d_H_const_mat_X[tx +14*N + 140] ] * s_track[ d_H_const_mat_X[tx +14*N + 280] ] * s_track[ d_H_const_mat_X[tx +14*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +14*N] ]
              + d_Ht_const_mat_s[tx +15*N] * s_track[ d_H_const_mat_X[tx +15*N] ] * s_track[ d_H_const_mat_X[tx +15*N + 140] ] * s_track[ d_H_const_mat_X[tx +15*N + 280] ] * s_track[ d_H_const_mat_X[tx +15*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +15*N] ]
              + d_Ht_const_mat_s[tx +16*N] * s_track[ d_H_const_mat_X[tx +16*N] ] * s_track[ d_H_const_mat_X[tx +16*N + 140] ] * s_track[ d_H_const_mat_X[tx +16*N + 280] ] * s_track[ d_H_const_mat_X[tx +16*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +16*N] ]
              + d_Ht_const_mat_s[tx +17*N] * s_track[ d_H_const_mat_X[tx +17*N] ] * s_track[ d_H_const_mat_X[tx +17*N + 140] ] * s_track[ d_H_const_mat_X[tx +17*N + 280] ] * s_track[ d_H_const_mat_X[tx +17*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +17*N] ]
              + d_Ht_const_mat_s[tx +18*N] * s_track[ d_H_const_mat_X[tx +18*N] ] * s_track[ d_H_const_mat_X[tx +18*N + 140] ] * s_track[ d_H_const_mat_X[tx +18*N + 280] ] * s_track[ d_H_const_mat_X[tx +18*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +18*N] ]
              + d_Ht_const_mat_s[tx +19*N] * s_track[ d_H_const_mat_X[tx +19*N] ] * s_track[ d_H_const_mat_X[tx +19*N + 140] ] * s_track[ d_H_const_mat_X[tx +19*N + 280] ] * s_track[ d_H_const_mat_X[tx +19*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +19*N] ]
              + d_Ht_const_mat_s[tx +20*N] * s_track[ d_H_const_mat_X[tx +20*N] ] * s_track[ d_H_const_mat_X[tx +20*N + 140] ] * s_track[ d_H_const_mat_X[tx +20*N + 280] ] * s_track[ d_H_const_mat_X[tx +20*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +20*N] ]
              + d_Ht_const_mat_s[tx +21*N] * s_track[ d_H_const_mat_X[tx +21*N] ] * s_track[ d_H_const_mat_X[tx +21*N + 140] ] * s_track[ d_H_const_mat_X[tx +21*N + 280] ] * s_track[ d_H_const_mat_X[tx +21*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +21*N] ]
              + d_Ht_const_mat_s[tx +22*N] * s_track[ d_H_const_mat_X[tx +22*N] ] * s_track[ d_H_const_mat_X[tx +22*N + 140] ] * s_track[ d_H_const_mat_X[tx +22*N + 280] ] * s_track[ d_H_const_mat_X[tx +22*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +22*N] ]
              + d_Ht_const_mat_s[tx +23*N] * s_track[ d_H_const_mat_X[tx +23*N] ] * s_track[ d_H_const_mat_X[tx +23*N + 140] ] * s_track[ d_H_const_mat_X[tx +23*N + 280] ] * s_track[ d_H_const_mat_X[tx +23*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +23*N] ]
              + d_Ht_const_mat_s[tx +24*N] * s_track[ d_H_const_mat_X[tx +24*N] ] * s_track[ d_H_const_mat_X[tx +24*N + 140] ] * s_track[ d_H_const_mat_X[tx +24*N + 280] ] * s_track[ d_H_const_mat_X[tx +24*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +24*N] ]
              + d_Ht_const_mat_s[tx +25*N] * s_track[ d_H_const_mat_X[tx +25*N] ] * s_track[ d_H_const_mat_X[tx +25*N + 140] ] * s_track[ d_H_const_mat_X[tx +25*N + 280] ] * s_track[ d_H_const_mat_X[tx +25*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +25*N] ]
              + d_Ht_const_mat_s[tx +26*N] * s_track[ d_H_const_mat_X[tx +26*N] ] * s_track[ d_H_const_mat_X[tx +26*N + 140] ] * s_track[ d_H_const_mat_X[tx +26*N + 280] ] * s_track[ d_H_const_mat_X[tx +26*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +26*N] ]
              + d_Ht_const_mat_s[tx +27*N] * s_track[ d_H_const_mat_X[tx +27*N] ] * s_track[ d_H_const_mat_X[tx +27*N + 140] ] * s_track[ d_H_const_mat_X[tx +27*N + 280] ] * s_track[ d_H_const_mat_X[tx +27*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +27*N] ]
              + d_Ht_const_mat_s[tx +28*N] * s_track[ d_H_const_mat_X[tx +28*N] ] * s_track[ d_H_const_mat_X[tx +28*N + 140] ] * s_track[ d_H_const_mat_X[tx +28*N + 280] ] * s_track[ d_H_const_mat_X[tx +28*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +28*N] ]
              + d_Ht_const_mat_s[tx +29*N] * s_track[ d_H_const_mat_X[tx +29*N] ] * s_track[ d_H_const_mat_X[tx +29*N + 140] ] * s_track[ d_H_const_mat_X[tx +29*N + 280] ] * s_track[ d_H_const_mat_X[tx +29*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +29*N] ]
              + d_Ht_const_mat_s[tx +30*N] * s_track[ d_H_const_mat_X[tx +30*N] ] * s_track[ d_H_const_mat_X[tx +30*N + 140] ] * s_track[ d_H_const_mat_X[tx +30*N + 280] ] * s_track[ d_H_const_mat_X[tx +30*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +30*N] ]
              + d_Ht_const_mat_s[tx +31*N] * s_track[ d_H_const_mat_X[tx +31*N] ] * s_track[ d_H_const_mat_X[tx +31*N + 140] ] * s_track[ d_H_const_mat_X[tx +31*N + 280] ] * s_track[ d_H_const_mat_X[tx +31*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +31*N] ]
              + d_Ht_const_mat_s[tx +32*N] * s_track[ d_H_const_mat_X[tx +32*N] ] * s_track[ d_H_const_mat_X[tx +32*N + 140] ] * s_track[ d_H_const_mat_X[tx +32*N + 280] ] * s_track[ d_H_const_mat_X[tx +32*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +32*N] ]
              + d_Ht_const_mat_s[tx +33*N] * s_track[ d_H_const_mat_X[tx +33*N] ] * s_track[ d_H_const_mat_X[tx +33*N + 140] ] * s_track[ d_H_const_mat_X[tx +33*N + 280] ] * s_track[ d_H_const_mat_X[tx +33*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +33*N] ]
              + d_Ht_const_mat_s[tx +34*N] * s_track[ d_H_const_mat_X[tx +34*N] ] * s_track[ d_H_const_mat_X[tx +34*N + 140] ] * s_track[ d_H_const_mat_X[tx +34*N + 280] ] * s_track[ d_H_const_mat_X[tx +34*N + 420] ] * s_cdt[ d_H_const_mat_Y[tx +34*N] ];
    }
}

#endif

#ifndef dev_eval_indxing_2vTrg_distortion_cuh_
#define dev_eval_indxing_2vTrg_distortion_cuh_
// ============================================================================
// Device function for evaluating the parallel indexing for Hx, Ht, and H of
// 2vTrg_distortion problem
//
// Modifications
//    Chien  21-11-22:   Originally created
//
// ============================================================================
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// -- cuda included --
#include <cuda_runtime.h>

// -- magma included --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

namespace magmaHCWrapper {

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_cdt_2vTrg_distortion(
        const int tx, float t, magmaFloatComplex s_vector_cdt[coefsCount],
        magmaFloatComplex r_startCoefs[coefsCount], magmaFloatComplex r_targetCoefs[coefsCount])
    {
        #pragma unroll
        for (int i = 0; i < 7; i++) {
            s_vector_cdt[ tx + i * N ] = r_targetCoefs[tx + i * N] * t - r_startCoefs[tx + i * N] * (t-1);
        }
        if (tx < 3) {
          s_vector_cdt[ tx + 35 ] = r_targetCoefs[tx + 35] * t - r_startCoefs[tx + 35] * (t-1);
        }
    }

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_Jacobian_2vTrg_distortion(
        const int tx, magmaFloatComplex *s_track, magmaFloatComplex r_cgesvA[N], const magma_int_t* __restrict__ d_const_mat_s,
        const magma_int_t* __restrict__ d_const_mat_X, const magma_int_t* __restrict__ d_const_mat_Y,
        magmaFloatComplex s_cdt[coefsCount])
    {
        // -- the maximal terms of Hx in 2vTrg_distortion problem is 10 --
        // -- 250 = 5(vars) * 5(vars) * 10(terms) --
        #pragma unroll
        for(int i = 0; i < N; i++) {
          r_cgesvA[i] = d_const_mat_s[tx + i*N] * s_track[ d_const_mat_X[tx + i*N] ] * s_track[ d_const_mat_X[tx + i*N + 250] ] * s_track[ d_const_mat_X[tx + i*N + 500] ] * s_cdt[ d_const_mat_Y[tx + i*N] ]
                      + d_const_mat_s[tx + i*N + N*N] * s_track[ d_const_mat_X[tx + i*N + N*N] ] * s_track[ d_const_mat_X[tx + i*N + N*N + 250] ] * s_track[ d_const_mat_X[tx + i*N + N*N + 500] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N] ]
                      + d_const_mat_s[tx + i*N + N*N*2] * s_track[ d_const_mat_X[tx + i*N + N*N*2] ] * s_track[ d_const_mat_X[tx + i*N + N*N*2 + 250] ] * s_track[ d_const_mat_X[tx + i*N + N*N*2 + 500] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*2] ]
                      + d_const_mat_s[tx + i*N + N*N*3] * s_track[ d_const_mat_X[tx + i*N + N*N*3] ] * s_track[ d_const_mat_X[tx + i*N + N*N*3 + 250] ] * s_track[ d_const_mat_X[tx + i*N + N*N*3 + 500] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*3] ]
                      + d_const_mat_s[tx + i*N + N*N*4] * s_track[ d_const_mat_X[tx + i*N + N*N*4] ] * s_track[ d_const_mat_X[tx + i*N + N*N*4 + 250] ] * s_track[ d_const_mat_X[tx + i*N + N*N*4 + 500] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*4] ]
                      + d_const_mat_s[tx + i*N + N*N*5] * s_track[ d_const_mat_X[tx + i*N + N*N*5] ] * s_track[ d_const_mat_X[tx + i*N + N*N*5 + 250] ] * s_track[ d_const_mat_X[tx + i*N + N*N*5 + 500] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*5] ]
                      + d_const_mat_s[tx + i*N + N*N*6] * s_track[ d_const_mat_X[tx + i*N + N*N*6] ] * s_track[ d_const_mat_X[tx + i*N + N*N*6 + 250] ] * s_track[ d_const_mat_X[tx + i*N + N*N*6 + 500] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*6] ]
                      + d_const_mat_s[tx + i*N + N*N*7] * s_track[ d_const_mat_X[tx + i*N + N*N*7] ] * s_track[ d_const_mat_X[tx + i*N + N*N*7 + 250] ] * s_track[ d_const_mat_X[tx + i*N + N*N*7 + 500] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*7] ]
                      + d_const_mat_s[tx + i*N + N*N*8] * s_track[ d_const_mat_X[tx + i*N + N*N*8] ] * s_track[ d_const_mat_X[tx + i*N + N*N*8 + 250] ] * s_track[ d_const_mat_X[tx + i*N + N*N*8 + 500] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*8] ]
                      + d_const_mat_s[tx + i*N + N*N*9] * s_track[ d_const_mat_X[tx + i*N + N*N*9] ] * s_track[ d_const_mat_X[tx + i*N + N*N*9 + 250] ] * s_track[ d_const_mat_X[tx + i*N + N*N*9 + 500] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N*9] ];
        }
    }

    template<int N>
    __device__ __inline__ void
    eval_Ht_2vTrg_distortion(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_Ht_const_mat_X,
      const magma_int_t* __restrict__ d_Ht_const_mat_Y, const magmaFloatComplex* __restrict__ d_const_cd)
    {
      // -- the maximal terms of Ht in 2vTrg_distortion problem is 25 --
      // -- the maximal parts for 2vTrg_distortion problem is 4 --
      // -- N*maximal terms of Ht = 5*25 = 125
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_Ht_const_mat_X[tx] ] * s_track[ d_Ht_const_mat_X[tx + 125] ] * s_track[ d_Ht_const_mat_X[tx + 250] ] * s_track[ d_Ht_const_mat_X[tx + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + N] * s_track[ d_Ht_const_mat_X[tx + N] ] * s_track[ d_Ht_const_mat_X[tx + N + 125] ] * s_track[ d_Ht_const_mat_X[tx + N + 250] ] * s_track[ d_Ht_const_mat_X[tx + N + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N] ]
               + d_Ht_const_mat_s[tx + N*2] * s_track[ d_Ht_const_mat_X[tx + N*2] ] * s_track[ d_Ht_const_mat_X[tx + N*2 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*2 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*2 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*2] ]
               + d_Ht_const_mat_s[tx + N*3] * s_track[ d_Ht_const_mat_X[tx + N*3] ] * s_track[ d_Ht_const_mat_X[tx + N*3 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*3 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*3 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*3] ]
               + d_Ht_const_mat_s[tx + N*4] * s_track[ d_Ht_const_mat_X[tx + N*4] ] * s_track[ d_Ht_const_mat_X[tx + N*4 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*4 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*4 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*4] ]
               + d_Ht_const_mat_s[tx + N*5] * s_track[ d_Ht_const_mat_X[tx + N*5] ] * s_track[ d_Ht_const_mat_X[tx + N*5 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*5 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*5 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*5] ]
               + d_Ht_const_mat_s[tx + N*6] * s_track[ d_Ht_const_mat_X[tx + N*6] ] * s_track[ d_Ht_const_mat_X[tx + N*6 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*6 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*6 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*6] ]
               + d_Ht_const_mat_s[tx + N*7] * s_track[ d_Ht_const_mat_X[tx + N*7] ] * s_track[ d_Ht_const_mat_X[tx + N*7 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*7 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*7 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*7] ]
               + d_Ht_const_mat_s[tx + N*8] * s_track[ d_Ht_const_mat_X[tx + N*8] ] * s_track[ d_Ht_const_mat_X[tx + N*8 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*8 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*8 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*8] ]
               + d_Ht_const_mat_s[tx + N*9] * s_track[ d_Ht_const_mat_X[tx + N*9] ] * s_track[ d_Ht_const_mat_X[tx + N*9 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*9 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*9 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*9] ]
               + d_Ht_const_mat_s[tx + N*10] * s_track[ d_Ht_const_mat_X[tx + N*10] ] * s_track[ d_Ht_const_mat_X[tx + N*10 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*10 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*10 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*10] ]
               + d_Ht_const_mat_s[tx + N*11] * s_track[ d_Ht_const_mat_X[tx + N*11] ] * s_track[ d_Ht_const_mat_X[tx + N*11 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*11 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*11 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*11] ]
               + d_Ht_const_mat_s[tx + N*12] * s_track[ d_Ht_const_mat_X[tx + N*12] ] * s_track[ d_Ht_const_mat_X[tx + N*12 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*12 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*12 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*12] ]
               + d_Ht_const_mat_s[tx + N*13] * s_track[ d_Ht_const_mat_X[tx + N*13] ] * s_track[ d_Ht_const_mat_X[tx + N*13 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*13 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*13 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*13] ]
               + d_Ht_const_mat_s[tx + N*14] * s_track[ d_Ht_const_mat_X[tx + N*14] ] * s_track[ d_Ht_const_mat_X[tx + N*14 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*14 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*14 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*14] ]
               + d_Ht_const_mat_s[tx + N*15] * s_track[ d_Ht_const_mat_X[tx + N*15] ] * s_track[ d_Ht_const_mat_X[tx + N*15 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*15 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*15 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*15] ]
               + d_Ht_const_mat_s[tx + N*16] * s_track[ d_Ht_const_mat_X[tx + N*16] ] * s_track[ d_Ht_const_mat_X[tx + N*16 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*16 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*16 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*16] ]
               + d_Ht_const_mat_s[tx + N*17] * s_track[ d_Ht_const_mat_X[tx + N*17] ] * s_track[ d_Ht_const_mat_X[tx + N*17 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*17 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*17 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*17] ]
               + d_Ht_const_mat_s[tx + N*18] * s_track[ d_Ht_const_mat_X[tx + N*18] ] * s_track[ d_Ht_const_mat_X[tx + N*18 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*18 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*18 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*18] ]
               + d_Ht_const_mat_s[tx + N*19] * s_track[ d_Ht_const_mat_X[tx + N*19] ] * s_track[ d_Ht_const_mat_X[tx + N*19 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*19 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*19 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*19] ]
               + d_Ht_const_mat_s[tx + N*20] * s_track[ d_Ht_const_mat_X[tx + N*20] ] * s_track[ d_Ht_const_mat_X[tx + N*20 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*20 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*20 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*20] ]
               + d_Ht_const_mat_s[tx + N*21] * s_track[ d_Ht_const_mat_X[tx + N*21] ] * s_track[ d_Ht_const_mat_X[tx + N*21 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*21 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*21 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*21] ]
               + d_Ht_const_mat_s[tx + N*22] * s_track[ d_Ht_const_mat_X[tx + N*22] ] * s_track[ d_Ht_const_mat_X[tx + N*22 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*22 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*22 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*22] ]
               + d_Ht_const_mat_s[tx + N*23] * s_track[ d_Ht_const_mat_X[tx + N*23] ] * s_track[ d_Ht_const_mat_X[tx + N*23 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*23 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*23 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*23] ]
               + d_Ht_const_mat_s[tx + N*24] * s_track[ d_Ht_const_mat_X[tx + N*24] ] * s_track[ d_Ht_const_mat_X[tx + N*24 + 125] ] * s_track[ d_Ht_const_mat_X[tx + N*24 + 250] ] * s_track[ d_Ht_const_mat_X[tx + N*24 + 375] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N*24] ];
    }

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_H_2vTrg_distortion(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_H_const_mat_X,
      const magma_int_t* __restrict__ d_H_const_mat_Y, magmaFloatComplex s_cdt[coefsCount])
    {
      // -- same as Ht, except replacing cd by cdt --
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_H_const_mat_X[tx] ] * s_track[ d_H_const_mat_X[tx + 125] ] * s_track[ d_H_const_mat_X[tx + 250] ] * s_track[ d_H_const_mat_X[tx + 375] ] * s_cdt[ d_H_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + N] * s_track[ d_H_const_mat_X[tx + N] ] * s_track[ d_H_const_mat_X[tx + N + 125] ] * s_track[ d_H_const_mat_X[tx + N + 250] ] * s_track[ d_H_const_mat_X[tx + N + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N] ]
               + d_Ht_const_mat_s[tx + N*2] * s_track[ d_H_const_mat_X[tx + N*2] ] * s_track[ d_H_const_mat_X[tx + N*2 + 125] ] * s_track[ d_H_const_mat_X[tx + N*2 + 250] ] * s_track[ d_H_const_mat_X[tx + N*2 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*2] ]
               + d_Ht_const_mat_s[tx + N*3] * s_track[ d_H_const_mat_X[tx + N*3] ] * s_track[ d_H_const_mat_X[tx + N*3 + 125] ] * s_track[ d_H_const_mat_X[tx + N*3 + 250] ] * s_track[ d_H_const_mat_X[tx + N*3 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*3] ]
               + d_Ht_const_mat_s[tx + N*4] * s_track[ d_H_const_mat_X[tx + N*4] ] * s_track[ d_H_const_mat_X[tx + N*4 + 125] ] * s_track[ d_H_const_mat_X[tx + N*4 + 250] ] * s_track[ d_H_const_mat_X[tx + N*4 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*4] ]
               + d_Ht_const_mat_s[tx + N*5] * s_track[ d_H_const_mat_X[tx + N*5] ] * s_track[ d_H_const_mat_X[tx + N*5 + 125] ] * s_track[ d_H_const_mat_X[tx + N*5 + 250] ] * s_track[ d_H_const_mat_X[tx + N*5 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*5] ]
               + d_Ht_const_mat_s[tx + N*6] * s_track[ d_H_const_mat_X[tx + N*6] ] * s_track[ d_H_const_mat_X[tx + N*6 + 125] ] * s_track[ d_H_const_mat_X[tx + N*6 + 250] ] * s_track[ d_H_const_mat_X[tx + N*6 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*6] ]
               + d_Ht_const_mat_s[tx + N*7] * s_track[ d_H_const_mat_X[tx + N*7] ] * s_track[ d_H_const_mat_X[tx + N*7 + 125] ] * s_track[ d_H_const_mat_X[tx + N*7 + 250] ] * s_track[ d_H_const_mat_X[tx + N*7 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*7] ]
               + d_Ht_const_mat_s[tx + N*8] * s_track[ d_H_const_mat_X[tx + N*8] ] * s_track[ d_H_const_mat_X[tx + N*8 + 125] ] * s_track[ d_H_const_mat_X[tx + N*8 + 250] ] * s_track[ d_H_const_mat_X[tx + N*8 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*8] ]
               + d_Ht_const_mat_s[tx + N*9] * s_track[ d_H_const_mat_X[tx + N*9] ] * s_track[ d_H_const_mat_X[tx + N*9 + 125] ] * s_track[ d_H_const_mat_X[tx + N*9 + 250] ] * s_track[ d_H_const_mat_X[tx + N*9 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*9] ]
               + d_Ht_const_mat_s[tx + N*10] * s_track[ d_H_const_mat_X[tx + N*10] ] * s_track[ d_H_const_mat_X[tx + N*10 + 125] ] * s_track[ d_H_const_mat_X[tx + N*10 + 250] ] * s_track[ d_H_const_mat_X[tx + N*10 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*10] ]
               + d_Ht_const_mat_s[tx + N*11] * s_track[ d_H_const_mat_X[tx + N*11] ] * s_track[ d_H_const_mat_X[tx + N*11 + 125] ] * s_track[ d_H_const_mat_X[tx + N*11 + 250] ] * s_track[ d_H_const_mat_X[tx + N*11 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*11] ]
               + d_Ht_const_mat_s[tx + N*12] * s_track[ d_H_const_mat_X[tx + N*12] ] * s_track[ d_H_const_mat_X[tx + N*12 + 125] ] * s_track[ d_H_const_mat_X[tx + N*12 + 250] ] * s_track[ d_H_const_mat_X[tx + N*12 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*12] ]
               + d_Ht_const_mat_s[tx + N*13] * s_track[ d_H_const_mat_X[tx + N*13] ] * s_track[ d_H_const_mat_X[tx + N*13 + 125] ] * s_track[ d_H_const_mat_X[tx + N*13 + 250] ] * s_track[ d_H_const_mat_X[tx + N*13 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*13] ]
               + d_Ht_const_mat_s[tx + N*14] * s_track[ d_H_const_mat_X[tx + N*14] ] * s_track[ d_H_const_mat_X[tx + N*14 + 125] ] * s_track[ d_H_const_mat_X[tx + N*14 + 250] ] * s_track[ d_H_const_mat_X[tx + N*14 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*14] ]
               + d_Ht_const_mat_s[tx + N*15] * s_track[ d_H_const_mat_X[tx + N*15] ] * s_track[ d_H_const_mat_X[tx + N*15 + 125] ] * s_track[ d_H_const_mat_X[tx + N*15 + 250] ] * s_track[ d_H_const_mat_X[tx + N*15 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*15] ]
               + d_Ht_const_mat_s[tx + N*16] * s_track[ d_H_const_mat_X[tx + N*16] ] * s_track[ d_H_const_mat_X[tx + N*16 + 125] ] * s_track[ d_H_const_mat_X[tx + N*16 + 250] ] * s_track[ d_H_const_mat_X[tx + N*16 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*16] ]
               + d_Ht_const_mat_s[tx + N*17] * s_track[ d_H_const_mat_X[tx + N*17] ] * s_track[ d_H_const_mat_X[tx + N*17 + 125] ] * s_track[ d_H_const_mat_X[tx + N*17 + 250] ] * s_track[ d_H_const_mat_X[tx + N*17 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*17] ]
               + d_Ht_const_mat_s[tx + N*18] * s_track[ d_H_const_mat_X[tx + N*18] ] * s_track[ d_H_const_mat_X[tx + N*18 + 125] ] * s_track[ d_H_const_mat_X[tx + N*18 + 250] ] * s_track[ d_H_const_mat_X[tx + N*18 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*18] ]
               + d_Ht_const_mat_s[tx + N*19] * s_track[ d_H_const_mat_X[tx + N*19] ] * s_track[ d_H_const_mat_X[tx + N*19 + 125] ] * s_track[ d_H_const_mat_X[tx + N*19 + 250] ] * s_track[ d_H_const_mat_X[tx + N*19 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*19] ]
               + d_Ht_const_mat_s[tx + N*20] * s_track[ d_H_const_mat_X[tx + N*20] ] * s_track[ d_H_const_mat_X[tx + N*20 + 125] ] * s_track[ d_H_const_mat_X[tx + N*20 + 250] ] * s_track[ d_H_const_mat_X[tx + N*20 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*20] ]
               + d_Ht_const_mat_s[tx + N*21] * s_track[ d_H_const_mat_X[tx + N*21] ] * s_track[ d_H_const_mat_X[tx + N*21 + 125] ] * s_track[ d_H_const_mat_X[tx + N*21 + 250] ] * s_track[ d_H_const_mat_X[tx + N*21 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*21] ]
               + d_Ht_const_mat_s[tx + N*22] * s_track[ d_H_const_mat_X[tx + N*22] ] * s_track[ d_H_const_mat_X[tx + N*22 + 125] ] * s_track[ d_H_const_mat_X[tx + N*22 + 250] ] * s_track[ d_H_const_mat_X[tx + N*22 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*22] ]
               + d_Ht_const_mat_s[tx + N*23] * s_track[ d_H_const_mat_X[tx + N*23] ] * s_track[ d_H_const_mat_X[tx + N*23 + 125] ] * s_track[ d_H_const_mat_X[tx + N*23 + 250] ] * s_track[ d_H_const_mat_X[tx + N*23 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*23] ]
               + d_Ht_const_mat_s[tx + N*24] * s_track[ d_H_const_mat_X[tx + N*24] ] * s_track[ d_H_const_mat_X[tx + N*24 + 125] ] * s_track[ d_H_const_mat_X[tx + N*24 + 250] ] * s_track[ d_H_const_mat_X[tx + N*24 + 375] ] * s_cdt[ d_H_const_mat_Y[tx + N*24] ];
    }
}

#endif

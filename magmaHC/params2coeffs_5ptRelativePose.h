#ifndef params2coeffs_5ptRelativePose_h
#define params2coeffs_5ptRelativePose_h
// =======================================================================
//
// Modifications
//    Chien  21-10-12:   Initially Created
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min

namespace magmaHCWrapper {

  void params2coeffs_5ptRelativePose(magmaFloatComplex *h_target_params, magmaFloatComplex *h_target_coeffs)
  {
    magmaFloatComplex x17 = h_target_params[0];
    magmaFloatComplex x18 = h_target_params[1];
    magmaFloatComplex x19 = h_target_params[2];
    magmaFloatComplex x20 = h_target_params[3];
    magmaFloatComplex x21 = h_target_params[4];
    magmaFloatComplex x22 = h_target_params[5];
    magmaFloatComplex x23 = h_target_params[6];
    magmaFloatComplex x24 = h_target_params[7];
    magmaFloatComplex x25 = h_target_params[8];
    magmaFloatComplex x26 = h_target_params[9];
    magmaFloatComplex x27 = h_target_params[10];
    magmaFloatComplex x28 = h_target_params[11];
    magmaFloatComplex x29 = h_target_params[12];
    magmaFloatComplex x30 = h_target_params[13];
    magmaFloatComplex x31 = h_target_params[14];
    magmaFloatComplex x32 = h_target_params[15];
    magmaFloatComplex x33 = h_target_params[16];
    magmaFloatComplex x34 = h_target_params[17];
    magmaFloatComplex x35 = h_target_params[18];
    magmaFloatComplex x36 = h_target_params[19];

    h_target_coeffs[0] = MAGMA_C_MAKE(-2.0, 0.0);
    h_target_coeffs[1] = (2*x18);
    h_target_coeffs[2] = (-2*x18);
    h_target_coeffs[3] = (2*x17);
    h_target_coeffs[4] = MAGMA_C_NEG_ONE;
    h_target_coeffs[5] = (-x17);
    h_target_coeffs[6] = (x19);
    h_target_coeffs[7] = MAGMA_C_ONE + MAGMA_C_ONE;
    h_target_coeffs[8] = (-2*x17);
    h_target_coeffs[9] = (-x18);
    h_target_coeffs[10] = (x20);
    h_target_coeffs[11] = MAGMA_C_ONE;
    h_target_coeffs[12] = (2*x22);
    h_target_coeffs[13] = (-2*x22);
    h_target_coeffs[14] = (2*x21);
    h_target_coeffs[15] = (-x21);
    h_target_coeffs[16] = (x23);
    h_target_coeffs[17] = (-2*x21);
    h_target_coeffs[18] = (-x22);
    h_target_coeffs[19] = (x24);
    h_target_coeffs[20] = (2*x26);
    h_target_coeffs[21] = (-2*x26);
    h_target_coeffs[22] = (2*x25);
    h_target_coeffs[23] = (-x25);
    h_target_coeffs[24] = (x27);
    h_target_coeffs[25] = (-2*x25);
    h_target_coeffs[26] = (-x26);
    h_target_coeffs[27] = (x28);
    h_target_coeffs[28] = (2*x30);
    h_target_coeffs[29] = (-2*x30);
    h_target_coeffs[30] = (2*x29);
    h_target_coeffs[31] = (-x29);
    h_target_coeffs[32] = (x31);
    h_target_coeffs[33] = (-2*x29);
    h_target_coeffs[34] = (-x30);
    h_target_coeffs[35] = (x32);
    h_target_coeffs[36] = (2*x34);
    h_target_coeffs[37] = (-2*x34);
    h_target_coeffs[38] = (2*x33);
    h_target_coeffs[39] = (-x33);
    h_target_coeffs[40] = (x35);
    h_target_coeffs[41] = (-2*x33);
    h_target_coeffs[42] = (-x34);
    h_target_coeffs[43] = (x36);

  }
} // end of namespace

#endif


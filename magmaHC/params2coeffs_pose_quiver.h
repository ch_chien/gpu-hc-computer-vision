#ifndef params2coeffs_pose_quiver_h
#define params2coeffs_pose_quiver_h
// =======================================================================
//
// Modifications
//    Chien  21-10-12:   Initially Created
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min

namespace magmaHCWrapper {

  void params2coeffs_pose_quiver(magmaFloatComplex *h_target_params, magmaFloatComplex *h_target_coeffs)
  {
    magmaFloatComplex x5 = h_target_params[0];
    magmaFloatComplex x6 = h_target_params[1];
    magmaFloatComplex x7 = h_target_params[2];
    magmaFloatComplex x8 = h_target_params[3];
    magmaFloatComplex x9 = h_target_params[4];
    magmaFloatComplex x10 = h_target_params[5];
    magmaFloatComplex x11 = h_target_params[6];
    magmaFloatComplex x12 = h_target_params[7];
    magmaFloatComplex x13 = h_target_params[8];
    magmaFloatComplex x14 = h_target_params[9];
    magmaFloatComplex x15 = h_target_params[10];
    magmaFloatComplex x16 = h_target_params[11];
    magmaFloatComplex x17 = h_target_params[12];
    magmaFloatComplex x18 = h_target_params[13];
    magmaFloatComplex x19 = h_target_params[14];
    magmaFloatComplex x20 = h_target_params[15];
    magmaFloatComplex x21 = h_target_params[16];
    magmaFloatComplex x22 = h_target_params[17];
    magmaFloatComplex x23 = h_target_params[18];
    magmaFloatComplex x24 = h_target_params[19];
    magmaFloatComplex x25 = h_target_params[20];
    magmaFloatComplex x26 = h_target_params[21];
    magmaFloatComplex x27 = h_target_params[22];
    magmaFloatComplex x28 = h_target_params[23];
    magmaFloatComplex x29 = h_target_params[24];
    magmaFloatComplex x30 = h_target_params[25];
    magmaFloatComplex x31 = h_target_params[26];
    magmaFloatComplex x32 = h_target_params[27];
    magmaFloatComplex x33 = h_target_params[28];
    magmaFloatComplex x34 = h_target_params[29];
    magmaFloatComplex x35 = h_target_params[30];
    magmaFloatComplex x36 = h_target_params[31];
    magmaFloatComplex x37 = h_target_params[32];
    magmaFloatComplex x38 = h_target_params[33];
    magmaFloatComplex x39 = h_target_params[34];
    magmaFloatComplex x40 = h_target_params[35];

    h_target_coeffs[0] = (-x37);
    h_target_coeffs[1] = (x5 - x21);
    h_target_coeffs[2] = (2*x9 + 2*x17);
    h_target_coeffs[3] = (2*x13);
    h_target_coeffs[4] = (2*x29);
    h_target_coeffs[5] = (2*x25);
    h_target_coeffs[6] = (-2*x33);
    h_target_coeffs[7] = (x21 - x5);
    h_target_coeffs[8] = (2*x33);
    h_target_coeffs[9] = (-2*x13);
    h_target_coeffs[10] = (x37);
    h_target_coeffs[11] = (-x5 - x21);
    h_target_coeffs[12] = (2*x9 - 2*x17);
    h_target_coeffs[13] = (x5 + x21);
    h_target_coeffs[14] = (-x38);
    h_target_coeffs[15] = (x6 - x22);
    h_target_coeffs[16] = (2*x10 + 2*x18);
    h_target_coeffs[17] = (2*x14);
    h_target_coeffs[18] = (2*x30);
    h_target_coeffs[19] = (2*x26);
    h_target_coeffs[20] = (-2*x34);
    h_target_coeffs[21] = (x22 - x6);
    h_target_coeffs[22] = (2*x34);
    h_target_coeffs[23] = (-2*x14);
    h_target_coeffs[24] = (x38);
    h_target_coeffs[25] = (- x6 - x22);
    h_target_coeffs[26] = (2*x10 - 2*x18);
    h_target_coeffs[27] = (x6 + x22);
    h_target_coeffs[28] = (-x39);
    h_target_coeffs[29] = (x7 - x23);
    h_target_coeffs[30] = (2*x11 + 2*x19);
    h_target_coeffs[31] = (2*x15);
    h_target_coeffs[32] = (2*x31);
    h_target_coeffs[33] = (2*x27);
    h_target_coeffs[34] = (-2*x35);
    h_target_coeffs[35] = (x23 - x7);
    h_target_coeffs[36] = (2*x35);
    h_target_coeffs[37] = (-2*x15);
    h_target_coeffs[38] = (x39);
    h_target_coeffs[39] = (- x7 - x23);
    h_target_coeffs[40] = (2*x11 - 2*x19);
    h_target_coeffs[41] = (x7 + x23);
    h_target_coeffs[42] = (-x40);
    h_target_coeffs[43] = (x8 - x24);
    h_target_coeffs[44] = (2*x12 + 2*x20);
    h_target_coeffs[45] = (2*x16);
    h_target_coeffs[46] = (2*x32);
    h_target_coeffs[47] = (2*x28);
    h_target_coeffs[48] = (-2*x36);
    h_target_coeffs[49] = (x24 - x8);
    h_target_coeffs[50] = (2*x36);
    h_target_coeffs[51] = (-2*x16);
    h_target_coeffs[52] = (x40);
    h_target_coeffs[53] = (- x8 - x24);
    h_target_coeffs[54] = (2*x12 - 2*x20);
    h_target_coeffs[55] = (x8 + x24);
  }
} // end of namespace

#endif

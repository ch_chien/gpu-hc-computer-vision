#ifndef params2coeffs_9pt2radial_h
#define params2coeffs_9pt2radial_h
// =======================================================================
//
// Modifications
//    Chien  21-10-28:   Initially Created
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min

namespace magmaHCWrapper {

  void params2coeffs_9pt2radial(magmaFloatComplex *h_target_params, magmaFloatComplex *h_target_coeffs)
  {
    magmaFloatComplex x5 = h_target_params[0];
    magmaFloatComplex x6 = h_target_params[1];
    magmaFloatComplex x7 = h_target_params[2];
    magmaFloatComplex x8 = h_target_params[3];
    magmaFloatComplex x9 = h_target_params[4];
    magmaFloatComplex x10 = h_target_params[5];
    magmaFloatComplex x11 = h_target_params[6];
    magmaFloatComplex x12 = h_target_params[7];
    magmaFloatComplex x13 = h_target_params[8];
    magmaFloatComplex x14 = h_target_params[9];
    magmaFloatComplex x15 = h_target_params[10];
    magmaFloatComplex x16 = h_target_params[11];
    magmaFloatComplex x17 = h_target_params[12];
    magmaFloatComplex x18 = h_target_params[13];
    magmaFloatComplex x19 = h_target_params[14];
    magmaFloatComplex x20 = h_target_params[15];
    magmaFloatComplex x21 = h_target_params[16];
    magmaFloatComplex x22 = h_target_params[17];
    magmaFloatComplex x23 = h_target_params[18];
    magmaFloatComplex x24 = h_target_params[19];
    magmaFloatComplex x25 = h_target_params[20];
    magmaFloatComplex x26 = h_target_params[21];
    magmaFloatComplex x27 = h_target_params[22];
    magmaFloatComplex x28 = h_target_params[23];
    magmaFloatComplex x29 = h_target_params[24];
    magmaFloatComplex x30 = h_target_params[25];
    magmaFloatComplex x31 = h_target_params[26];
    magmaFloatComplex x32 = h_target_params[27];
    magmaFloatComplex x33 = h_target_params[28];
    magmaFloatComplex x34 = h_target_params[29];
    magmaFloatComplex x35 = h_target_params[30];
    magmaFloatComplex x36 = h_target_params[31];
    magmaFloatComplex x37 = h_target_params[32];
    magmaFloatComplex x38 = h_target_params[33];
    magmaFloatComplex x39 = h_target_params[34];
    magmaFloatComplex x40 = h_target_params[35];
    magmaFloatComplex x41 = h_target_params[36];
    magmaFloatComplex x42 = h_target_params[37];
    magmaFloatComplex x43 = h_target_params[38];
    magmaFloatComplex x44 = h_target_params[39];
    magmaFloatComplex x45 = h_target_params[40];
    magmaFloatComplex x46 = h_target_params[41];
    magmaFloatComplex x47 = h_target_params[42];
    magmaFloatComplex x48 = h_target_params[43];
    magmaFloatComplex x49 = h_target_params[44];
    magmaFloatComplex x50 = h_target_params[45];
    magmaFloatComplex x51 = h_target_params[46];
    magmaFloatComplex x52 = h_target_params[47];
    magmaFloatComplex x53 = h_target_params[48];
    magmaFloatComplex x54 = h_target_params[49];
    magmaFloatComplex x55 = h_target_params[50];
    magmaFloatComplex x56 = h_target_params[51];
    magmaFloatComplex x57 = h_target_params[52];
    magmaFloatComplex x58 = h_target_params[53];
    magmaFloatComplex x59 = h_target_params[54];
    magmaFloatComplex x60 = h_target_params[55];
    magmaFloatComplex x61 = h_target_params[56];
    magmaFloatComplex x62 = h_target_params[57];
    magmaFloatComplex x63 = h_target_params[58];
    magmaFloatComplex x64 = h_target_params[59];
    magmaFloatComplex x65 = h_target_params[60];
    magmaFloatComplex x66 = h_target_params[61];
    magmaFloatComplex x67 = h_target_params[62];
    magmaFloatComplex x68 = h_target_params[63];
    magmaFloatComplex x69 = h_target_params[64];
    magmaFloatComplex x70 = h_target_params[65];
    magmaFloatComplex x71 = h_target_params[66];
    magmaFloatComplex x72 = h_target_params[67];
    magmaFloatComplex x73 = h_target_params[68];
    magmaFloatComplex x74 = h_target_params[69];
    magmaFloatComplex x75 = h_target_params[70];
    magmaFloatComplex x76 = h_target_params[71];

    h_target_coeffs[0] = (x21);
    h_target_coeffs[1] = (x53);
    h_target_coeffs[2] = (x24);
    h_target_coeffs[3] = (x56);
    h_target_coeffs[4] = (x22);
    h_target_coeffs[5] = (x54);
    h_target_coeffs[6] = (x25);
    h_target_coeffs[7] = (x57);
    h_target_coeffs[8] = (x23);
    h_target_coeffs[9] = (x26 + x55);
    h_target_coeffs[10] = (x58);
    h_target_coeffs[11] = (x27);
    h_target_coeffs[12] = (x28 + x59);
    h_target_coeffs[13] = (x60);
    h_target_coeffs[14] = (x45);
    h_target_coeffs[15] = (x61);
    h_target_coeffs[16] = (x48);
    h_target_coeffs[17] = (x64);
    h_target_coeffs[18] = (x46);
    h_target_coeffs[19] = (x62);
    h_target_coeffs[20] = (x49);
    h_target_coeffs[21] = (x65);
    h_target_coeffs[22] = (x47);
    h_target_coeffs[23] = (x50 + x63);
    h_target_coeffs[24] = (x66);
    h_target_coeffs[25] = (x51);
    h_target_coeffs[26] = (x52 + x67);
    h_target_coeffs[27] = (x68);
    h_target_coeffs[28] = (x69 + 1);
    h_target_coeffs[29] = (x72);
    h_target_coeffs[30] = (x70);
    h_target_coeffs[31] = (x73);
    h_target_coeffs[32] = (x71);
    h_target_coeffs[33] = (x74);
    h_target_coeffs[34] = (x75);
    h_target_coeffs[35] = (x76);
    h_target_coeffs[36] = (x13*x45 - x21*x37);
    h_target_coeffs[37] = (x13*x48 + x16*x45 - x21*x40 - x24*x37);
    h_target_coeffs[38] = (x16*x48 - x24*x40);
    h_target_coeffs[39] = (x21*x29 - x5*x45 + x13*x46 + x14*x45 - x21*x38 - x22*x37);
    h_target_coeffs[40] = (x21*x32 - x8*x45 - x5*x48 + x24*x29 + x13*x49 + x14*x48 + x16*x46 + x17*x45 - x21*x41 - x22*x40 - x24*x38 - x25*x37);
    h_target_coeffs[41] = (x24*x32 - x8*x48 + x16*x49 + x17*x48 - x24*x41 - x25*x40);
    h_target_coeffs[42] = (x13*x47 + x15*x45 - x21*x39 - x23*x37);
    h_target_coeffs[43] = (x5*x37 - x13*x29 + x13*x50 + x18*x45 - x21*x42 - x26*x37);
    h_target_coeffs[44] = (x15*x48 + x16*x47 - x23*x40 - x24*x39 + x13*x51 + x19*x45 - x21*x43 - x27*x37);
    h_target_coeffs[45] = (x5*x40 + x8*x37 - x13*x32 - x16*x29 + x13*x52 + x20*x45 - x21*x44 - x28*x37 + x16*x50 + x18*x48 - x24*x42 - x26*x40);
    h_target_coeffs[46] = (x16*x51 + x19*x48 - x24*x43 - x27*x40);
    h_target_coeffs[47] = (x8*x40 - x16*x32 + x16*x52 + x20*x48 - x24*x44 - x28*x40);
    h_target_coeffs[48] = (x21*x30 - x6*x45 - x5*x46 + x22*x29 + x14*x46 - x22*x38);
    h_target_coeffs[49] = (x21*x33 - x6*x48 - x8*x46 - x9*x45 - x5*x49 + x22*x32 + x24*x30 + x25*x29 + x14*x49 + x17*x46 - x22*x41 - x25*x38);
    h_target_coeffs[50] = (x24*x33 - x9*x48 - x8*x49 + x25*x32 + x17*x49 - x25*x41);
    h_target_coeffs[51] = (x21*x31 - x7*x45 - x5*x47 + x23*x29 + x14*x47 + x15*x46 - x22*x39 - x23*x38);
    h_target_coeffs[52] = (x5*x38 + x6*x37 - x13*x30 - x14*x29 - x5*x50 - x10*x45 + x21*x34 + x26*x29 + x14*x50 + x18*x46 - x22*x42 - x26*x38);
    h_target_coeffs[53] = (x23*x32 - x8*x47 - x7*x48 + x24*x31 - x5*x51 - x11*x45 + x21*x35 + x27*x29 + x15*x49 + x17*x47 - x23*x41 - x25*x39 + x14*x51 + x19*x46 - x22*x43 - x27*x38);
    h_target_coeffs[54] = (x5*x41 + x6*x40 + x8*x38 + x9*x37 - x13*x33 - x14*x32 - x16*x30 - x17*x29 - x5*x52 - x12*x45 + x21*x36 + x28*x29 - x8*x50 - x10*x48 + x24*x34 + x26*x32 + x14*x52 + x20*x46 - x22*x44 - x28*x38 + x17*x50 + x18*x49 - x25*x42 - x26*x41);
    h_target_coeffs[55] = (x24*x35 - x11*x48 - x8*x51 + x27*x32 + x17*x51 + x19*x49 - x25*x43 - x27*x41);
    h_target_coeffs[56] = (x8*x41 + x9*x40 - x16*x33 - x17*x32 - x8*x52 - x12*x48 + x24*x36 + x28*x32 + x17*x52 + x20*x49 - x25*x44 - x28*x41);
    h_target_coeffs[57] = (x15*x47 - x23*x39);
    h_target_coeffs[58] = (x5*x39 + x7*x37 - x13*x31 - x15*x29 + x15*x50 + x18*x47 - x23*x42 - x26*x39);
    h_target_coeffs[59] = (x5*x42 + x10*x37 - x13*x34 - x18*x29 + x18*x50 - x26*x42);
    h_target_coeffs[60] = (x15*x51 + x19*x47 - x23*x43 - x27*x39);
    h_target_coeffs[61] = (x7*x40 + x8*x39 - x15*x32 - x16*x31 + x5*x43 + x11*x37 - x13*x35 - x19*x29 + x15*x52 + x20*x47 - x23*x44 - x28*x39 + x18*x51 + x19*x50 - x26*x43 - x27*x42);
    h_target_coeffs[62] = (x5*x44 + x12*x37 - x13*x36 - x20*x29 + x8*x42 + x10*x40 - x16*x34 - x18*x32 + x18*x52 + x20*x50 - x26*x44 - x28*x42);
    h_target_coeffs[63] = (x19*x51 - x27*x43);
    h_target_coeffs[64] = (x8*x43 + x11*x40 - x16*x35 - x19*x32 + x19*x52 + x20*x51 - x27*x44 - x28*x43);
    h_target_coeffs[65] = (x8*x44 + x12*x40 - x16*x36 - x20*x32 + x20*x52 - x28*x44);
    h_target_coeffs[66] = (x22*x30 - x6*x46);
    h_target_coeffs[67] = (x22*x33 - x9*x46 - x6*x49 + x25*x30);
    h_target_coeffs[68] = (x25*x33 - x9*x49);
    h_target_coeffs[69] = (x22*x31 - x7*x46 - x6*x47 + x23*x30);
    h_target_coeffs[70] = (x6*x38 - x14*x30 - x6*x50 - x10*x46 + x22*x34 + x26*x30);
    h_target_coeffs[71] = (x23*x33 - x9*x47 - x7*x49 + x25*x31 - x6*x51 - x11*x46 + x22*x35 + x27*x30);
    h_target_coeffs[72] = (x6*x41 + x9*x38 - x14*x33 - x17*x30 - x6*x52 - x12*x46 + x22*x36 + x28*x30 - x9*x50 - x10*x49 + x25*x34 + x26*x33);
    h_target_coeffs[73] = (x25*x35 - x11*x49 - x9*x51 + x27*x33);
    h_target_coeffs[74] = (x9*x41 - x17*x33 - x9*x52 - x12*x49 + x25*x36 + x28*x33);
    h_target_coeffs[75] = (x23*x31 - x7*x47);
    h_target_coeffs[76] = (x6*x39 + x7*x38 - x14*x31 - x15*x30 - x7*x50 - x10*x47 + x23*x34 + x26*x31);
    h_target_coeffs[77] = (x6*x42 + x10*x38 - x14*x34 - x18*x30 - x10*x50 + x26*x34);
    h_target_coeffs[78] = (x23*x35 - x11*x47 - x7*x51 + x27*x31);
    h_target_coeffs[79] = (x7*x41 + x9*x39 - x15*x33 - x17*x31 + x6*x43 + x11*x38 - x14*x35 - x19*x30 - x7*x52 - x12*x47 + x23*x36 + x28*x31 - x10*x51 - x11*x50 + x26*x35 + x27*x34);
    h_target_coeffs[80] = (x6*x44 + x12*x38 - x14*x36 - x20*x30 + x9*x42 + x10*x41 - x17*x34 - x18*x33 - x10*x52 - x12*x50 + x26*x36 + x28*x34);
    h_target_coeffs[81] = (x27*x35 - x11*x51);
    h_target_coeffs[82] = (x9*x43 + x11*x41 - x17*x35 - x19*x33 - x11*x52 - x12*x51 + x27*x36 + x28*x35);
    h_target_coeffs[83] = (x9*x44 + x12*x41 - x17*x36 - x20*x33 - x12*x52 + x28*x36);
    h_target_coeffs[84] = (x7*x39 - x15*x31);
    h_target_coeffs[85] = (x7*x42 + x10*x39 - x15*x34 - x18*x31);
    h_target_coeffs[86] = (x10*x42 - x18*x34);
    h_target_coeffs[87] = (x7*x43 + x11*x39 - x15*x35 - x19*x31);
    h_target_coeffs[88] = (x7*x44 + x12*x39 - x15*x36 - x20*x31 + x10*x43 + x11*x42 - x18*x35 - x19*x34);
    h_target_coeffs[89] = (x10*x44 + x12*x42 - x18*x36 - x20*x34);
    h_target_coeffs[90] = (x11*x43 - x19*x35);
    h_target_coeffs[91] = (x11*x44 + x12*x43 - x19*x36 - x20*x35);
    h_target_coeffs[92] = (x12*x44 - x20*x36);
  }
} // end of namespace

#endif

#ifndef magmaHC_kernels_h
#define magmaHC_kernels_h
// ============================================================================
// Header file declaring all kernels
//
// Modifications
//    Chien  21-05-04:
//
// ============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "magmaHC-problems.cuh"

// -- magma --
#include "flops.h"
#include "magma_v2.h"

extern "C" {
namespace magmaHCWrapper {

  // -- homotopy continuation solver - kernel_HC_Solver_3vTrg kernel --
  real_Double_t kernel_HC_Solver_3vTrg(
    magma_int_t N, magma_int_t batchCount, magma_int_t coefsCount, magma_int_t ldda,
    magma_queue_t my_queue,
    magmaFloatComplex** d_startSols_array, magmaFloatComplex** d_Track_array,
    magmaFloatComplex** d_startCoefs_array, magmaFloatComplex** d_targetCoefs_array,
    magmaFloatComplex** d_cgesvA_array, magmaFloatComplex** d_cgesvB_array,
    const_mats *cm
  );

  // -- homotopy continuation solver - kernel_HC_Solver_4vTrg kernel --
  real_Double_t kernel_HC_Solver_4vTrg(
    magma_int_t N, magma_int_t batchCount, magma_int_t coefsCount, magma_int_t ldda,
    magma_queue_t my_queue,
    magmaFloatComplex** d_startSols_array, magmaFloatComplex** d_Track_array,
    magmaFloatComplex** d_startCoefs_array, magmaFloatComplex** d_targetCoefs_array,
    magmaFloatComplex** d_cgesvA_array, magmaFloatComplex** d_cgesvB_array,
    const_mats *cm
  );

  // -- homotopy continuation solver - kernel_HC_Solver_Eq17 kernel --
  real_Double_t kernel_HC_Solver_Eq17(
    magma_int_t N, magma_int_t batchCount, magma_int_t coefsCount, magma_int_t ldda,
    magma_queue_t my_queue,
    magmaFloatComplex** d_startSols_array, magmaFloatComplex** d_Track_array,
    magmaFloatComplex** d_startCoefs_array, magmaFloatComplex** d_targetCoefs_array,
    magmaFloatComplex** d_cgesvA_array, magmaFloatComplex** d_cgesvB_array,
    const_mats *cm
  );

  // -- homotopy continuation solver - kernel_HC_Solver_optpose4pt kernel --
  real_Double_t kernel_HC_Solver_optpose4pt(
    magma_int_t N, magma_int_t batchCount, magma_int_t coefsCount, magma_int_t ldda,
    magma_queue_t my_queue,
    magmaFloatComplex** d_startSols_array, magmaFloatComplex** d_Track_array,
    magmaFloatComplex** d_startCoefs_array, magmaFloatComplex** d_targetCoefs_array,
    magmaFloatComplex** d_cgesvA_array, magmaFloatComplex** d_cgesvB_array,
    const_mats *cm
  );

  // -- homotopy continuation solver - kernel_HC_Solver_homo3pt kernel --
  real_Double_t kernel_HC_Solver_homo3pt(
    magma_int_t N, magma_int_t batchCount, magma_int_t coefsCount, magma_int_t ldda,
    magma_queue_t my_queue,
    magmaFloatComplex** d_startSols_array, magmaFloatComplex** d_Track_array,
    magmaFloatComplex** d_startCoefs_array, magmaFloatComplex** d_targetCoefs_array,
    magmaFloatComplex** d_cgesvA_array, magmaFloatComplex** d_cgesvB_array,
    const_mats *cm, magma_int_t** d_Hx_idx_array, magma_int_t** d_Ht_idx_array,
    magma_int_t* h_Hx_idx, magma_int_t* h_Ht_idx
  );

  // -- homotopy continuation solver - kernel_HC_Solver_dualTDOA kernel --
  real_Double_t kernel_HC_Solver_dualTDOA(
    magma_int_t N, magma_int_t batchCount, magma_int_t coefsCount, magma_int_t ldda,
    magma_queue_t my_queue,
    magmaFloatComplex** d_startSols_array, magmaFloatComplex** d_Track_array,
    magmaFloatComplex** d_startCoefs_array, magmaFloatComplex** d_targetCoefs_array,
    magmaFloatComplex** d_cgesvA_array, magmaFloatComplex** d_cgesvB_array,
    const_mats *cm
  );

  // -- homotopy continuation solver - kernel_HC_Solver_optimalPnPQ kernel --
  real_Double_t kernel_HC_Solver_optimalPnPQ(
    magma_int_t N, magma_int_t batchCount, magma_int_t coefsCount, magma_int_t ldda,
    magma_queue_t my_queue,
    magmaFloatComplex** d_startSols_array, magmaFloatComplex** d_Track_array,
    magmaFloatComplex** d_startCoefs_array, magmaFloatComplex** d_targetCoefs_array,
    magmaFloatComplex** d_cgesvA_array, magmaFloatComplex** d_cgesvB_array,
    const_mats *cm
  );

  // -- homotopy continuation solver - kernel_HC_Solver_9pt2radial kernel --
  real_Double_t kernel_HC_Solver_9pt2radial(
    magma_int_t N, magma_int_t batchCount, magma_int_t coefsCount, magma_int_t ldda,
    magma_queue_t my_queue,
    magmaFloatComplex** d_startSols_array, magmaFloatComplex** d_Track_array,
    magmaFloatComplex** d_startCoefs_array, magmaFloatComplex** d_targetCoefs_array,
    magmaFloatComplex** d_cgesvA_array, magmaFloatComplex** d_cgesvB_array,
    const_mats *cm
  );

  // -- homotopy continuation solver - kernel_HC_Solver_pose_quiver kernel --
  real_Double_t kernel_HC_Solver_pose_quiver(
    magma_int_t N, magma_int_t batchCount, magma_int_t coefsCount, magma_int_t ldda,
    magma_queue_t my_queue,
    magmaFloatComplex** d_startSols_array, magmaFloatComplex** d_Track_array,
    magmaFloatComplex** d_startCoefs_array, magmaFloatComplex** d_targetCoefs_array,
    magmaFloatComplex** d_cgesvA_array, magmaFloatComplex** d_cgesvB_array,
    const_mats *cm
  );

  // -- homotopy continuation solver - kernel_HC_Solver_5 points relative pose estimation kernel --
  real_Double_t kernel_HC_Solver_5ptRelativePose(
    magma_int_t N, magma_int_t batchCount, magma_int_t coefsCount, magma_int_t ldda,
    magma_queue_t my_queue,
    magmaFloatComplex** d_startSols_array, magmaFloatComplex** d_Track_array,
    magmaFloatComplex** d_startCoefs_array, magmaFloatComplex** d_targetCoefs_array,
    magmaFloatComplex** d_cgesvA_array, magmaFloatComplex** d_cgesvB_array, const_mats *cm,
    magma_int_t** d_Hx_idx_array, magma_int_t** d_Ht_idx_array,
    magma_int_t* h_Hx_idx, magma_int_t* h_Ht_idx
  );

  // -- homotopy continuation solver - kernel_HC_Solver_5 points version 2 relative pose estimation kernel --
  real_Double_t kernel_HC_Solver_5ptRelativePose_v2(
    magma_int_t N, magma_int_t batchCount, magma_int_t coefsCount, magma_int_t ldda,
    magma_queue_t my_queue,
    magmaFloatComplex** d_startSols_array, magmaFloatComplex** d_Track_array,
    magmaFloatComplex** d_startCoefs_array, magmaFloatComplex** d_targetCoefs_array,
    magmaFloatComplex** d_cgesvA_array, magmaFloatComplex** d_cgesvB_array,
    const_mats *cm
  );

  // -- homotopy continuation solver - kernel_HC_Solver_5 points version 4 relative pose estimation kernel --
  real_Double_t kernel_HC_Solver_5ptRelativePose_v4(
    magma_int_t N, magma_int_t batchCount, magma_int_t coefsCount, magma_int_t ldda,
    magma_queue_t my_queue,
    magmaFloatComplex** d_startSols_array, magmaFloatComplex** d_Track_array,
    magmaFloatComplex** d_startCoefs_array, magmaFloatComplex** d_targetCoefs_array,
    magmaFloatComplex** d_cgesvA_array, magmaFloatComplex** d_cgesvB_array,
    const_mats *cm
  );

  // -- homotopy continuation solver - kernel_HC_Solver_P3P kernel --
  real_Double_t kernel_HC_Solver_P3P(
    magma_int_t N, magma_int_t batchCount, magma_int_t coefsCount, magma_int_t ldda,
    magma_queue_t my_queue,
    magmaFloatComplex** d_startSols_array, magmaFloatComplex** d_Track_array,
    magmaFloatComplex** d_startCoefs_array, magmaFloatComplex** d_targetCoefs_array,
    magmaFloatComplex** d_cgesvA_array, magmaFloatComplex** d_cgesvB_array,
    const_mats *cm
  );

  // -- homotopy continuation solver - kernel_HC_Solver_P3P_v2 kernel --
  real_Double_t kernel_HC_Solver_P3P_v2(
    magma_int_t N, magma_int_t batchCount, magma_int_t coefsCount, magma_int_t ldda,
    magma_queue_t my_queue,
    magmaFloatComplex** d_startSols_array, magmaFloatComplex** d_Track_array,
    magmaFloatComplex** d_startCoefs_array, magmaFloatComplex** d_targetCoefs_array,
    magmaFloatComplex** d_cgesvA_array, magmaFloatComplex** d_cgesvB_array,
    const_mats *cm
  );

  // -- homotopy continuation solver - kernel_HC_Solver_2vTrg_distortion kernel --
  real_Double_t kernel_HC_Solver_2vTrg_distortion(
    magma_int_t N, magma_int_t batchCount, magma_int_t coefsCount, magma_int_t ldda,
    magma_queue_t my_queue,
    magmaFloatComplex** d_startSols_array, magmaFloatComplex** d_Track_array,
    magmaFloatComplex** d_startCoefs_array, magmaFloatComplex** d_targetCoefs_array,
    magmaFloatComplex** d_cgesvA_array, magmaFloatComplex** d_cgesvB_array,
    const_mats *cm
  );

  // -- homotopy continuation solver - kernel_HC_Solver_R6P1lin kernel --
  real_Double_t kernel_HC_Solver_R6P1lin(
    magma_int_t N, magma_int_t batchCount, magma_int_t coefsCount, magma_int_t ldda,
    magma_queue_t my_queue,
    magmaFloatComplex** d_startSols_array, magmaFloatComplex** d_Track_array,
    magmaFloatComplex** d_startCoefs_array, magmaFloatComplex** d_targetCoefs_array,
    magmaFloatComplex** d_cgesvA_array, magmaFloatComplex** d_cgesvB_array,
    const_mats *cm
  );
}
}

#endif

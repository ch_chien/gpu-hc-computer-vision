#ifndef params2coeffs_3vTrg_h
#define params2coeffs_3vTrg_h
// =======================================================================
//
// Modifications
//    Chien  21-10-12:   Initially Created
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min

namespace magmaHCWrapper {

  void params2coeffs_3vTrg(magmaFloatComplex *h_target_params, magmaFloatComplex *h_target_coeffs)
  {
    for (int i = 0; i < 27; i++) {
      h_target_coeffs[i] = h_target_params[i];
    }

    magmaFloatComplex NEG2 = MAGMA_C_MAKE(-2.0, 0.0);
    h_target_coeffs[27] = NEG2 * h_target_params[27];
    h_target_coeffs[28] = NEG2 * h_target_params[28];
    h_target_coeffs[29] = NEG2 * h_target_params[29];
    h_target_coeffs[30] = NEG2 * h_target_params[30];
    h_target_coeffs[31] = NEG2 * h_target_params[31];
    h_target_coeffs[32] = NEG2 * h_target_params[32];
    h_target_coeffs[33] = MAGMA_C_ONE + MAGMA_C_ONE;

  }
} // end of namespace

#endif

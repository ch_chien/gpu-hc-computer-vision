#ifndef homotopy_continuation_solver_cu
#define homotopy_continuation_solver_cu
// =======================================================================
//
// Modifications
//    Chien  21-10-18:   Originally Created
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// cuda included
#include <cuda.h>
#include <cuda_runtime.h>

// magma
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

#include "magmaHC-problems.cuh"
#include "magmaHC-kernels.h"
#include "cpu-compute/cpu-compute.h"
#include "const-matrices/define-const-matrices.h"
#include "const-matrices/const-matrices-allocations.h"

namespace magmaHCWrapper {

  void homotopy_continuation_solver(
    magmaFloatComplex *h_startSols, magmaFloatComplex *h_Track,
    magmaFloatComplex *h_startCoefs, magmaFloatComplex *h_targetCoefs,
    magma_int_t *h_Hx_idx, magma_int_t *h_Ht_idx,
    problem_params* pp, const_mats *cm, std::string hc_problem, 
    std::ofstream &track_sols_file, std::ofstream &tracks_success_file)
  {
    magma_init();
    magma_print_environment();

    magma_int_t batchCount = pp->numOfTracks;
    magma_int_t coefsCount = pp->numOfCoeffs;
    magma_int_t N = pp->numOfVars;

    real_Double_t     gpu_time;
    real_Double_t     cpu_time;
    real_Double_t     data_h2d_time, data_d2h_time;
    magmaFloatComplex *h_cgesvA, *h_cgesvB;
    magmaFloatComplex *h_cgesvA_verify, *h_cgesvB_verify;
    magmaFloatComplex *h_track_sols, *h_track_numOfPred_s;
    magmaFloatComplex_ptr d_startSols, d_Track;
    magmaFloatComplex_ptr d_startCoefs, d_targetCoefs;
    magmaFloatComplex_ptr d_cgesvA, d_cgesvB;
    magma_int_t lda, ldb, ldda, lddb, ldd_coefs, sizeA, sizeB;
    magma_int_t ldd_const_matrices_Hx, ldd_const_matrices_Ht;
    magma_int_t ldd_const_matrices_Hx_collection, ldd_const_matrices_Ht_collection;
    magma_int_t ione = 1;
    magma_int_t ISEED[4] = {0,0,0,1};

    magmaFloatComplex **d_startSols_array = NULL;
    magmaFloatComplex **d_Track_array = NULL;
    magmaFloatComplex **d_startCoefs_array = NULL;
    magmaFloatComplex **d_targetCoefs_array = NULL;
    magmaFloatComplex **d_cgesvA_array = NULL;
    magmaFloatComplex **d_cgesvB_array = NULL;

    magma_device_t cdev;       // variable to indicate current gpu id
    magma_queue_t my_queue;    // magma queue variable, internally holds a cuda stream and a cublas handle
    magma_getdevice( &cdev );
    magma_queue_create( cdev, &my_queue );     // create a queue on this cdev

    lda    = N;
    ldb    = lda;
    ldda   = magma_roundup( N, 32 );  // multiple of 32 by default
    lddb   = ldda;
    ldd_coefs = magma_roundup( coefsCount, 32 );  // multiple of 32 by default
    ldd_const_matrices_Hx = magma_roundup( N*N*pp->Hx_maximal_terms, 32 );  // multiple of 32 by default
    ldd_const_matrices_Ht = magma_roundup( N*pp->Ht_maximal_terms, 32 );  // multiple of 32 by default
    ldd_const_matrices_Hx_collection = magma_roundup( N*N*pp->Hx_maximal_terms*pp->Hx_maximal_parts_x, 32 );  // multiple of 32 by default
    ldd_const_matrices_Ht_collection = magma_roundup( N*pp->Ht_maximal_terms*pp->Ht_H_maximal_terms_x, 32 );  // multiple of 32 by default
    sizeA = lda*N*batchCount;
    sizeB = ldb*batchCount;

    // ===================================== test =========================================
    magmaInt_ptr d_Hx_idx;
    magmaInt_ptr d_Ht_idx;
    magma_int_t **d_Hx_idx_array = NULL;
    magma_int_t **d_Ht_idx_array = NULL;
    magma_int_t size_Hx = N*N*pp->Hx_maximal_terms*(pp->Hx_maximal_parts_x+2);
    magma_int_t size_Ht = N*pp->Ht_maximal_terms*(pp->Ht_H_maximal_terms_x+2);
    magma_int_t rnded_size_Hx = magma_roundup( size_Hx, 32 );
    magma_int_t rnded_size_Ht = magma_roundup( size_Ht, 32 );

    // -- allocate gpu memories --
    magma_imalloc( &d_Hx_idx, size_Hx );
    magma_imalloc( &d_Ht_idx, size_Ht );
    magma_malloc( (void**) &d_Hx_idx_array, sizeof(magma_int_t*) );
    magma_malloc( (void**) &d_Ht_idx_array, sizeof(magma_int_t*) );

    // -- transfer data from cpu to gpu --
    magma_isetmatrix( size_Hx, 1, h_Hx_idx, size_Hx, d_Hx_idx, rnded_size_Hx, my_queue );
    magma_isetmatrix( size_Ht, 1, h_Ht_idx, size_Ht, d_Ht_idx, rnded_size_Ht, my_queue );
    magma_iset_pointer( d_Hx_idx_array, d_Hx_idx, rnded_size_Hx, 0, 0, rnded_size_Hx, 1, my_queue );
    magma_iset_pointer( d_Ht_idx_array, d_Ht_idx, rnded_size_Ht, 0, 0, rnded_size_Ht, 1, my_queue );
    // ======================================================================================

    // -- allocate CPU memory --
    magma_cmalloc_cpu( &h_cgesvA, N*N*batchCount );
    magma_cmalloc_cpu( &h_cgesvB, N*batchCount );
    magma_cmalloc_cpu( &h_cgesvA_verify, N*N*batchCount );
    magma_cmalloc_cpu( &h_cgesvB_verify, N*batchCount );
    magma_cmalloc_cpu( &h_track_sols, (N+1)*batchCount );
    magma_cmalloc_cpu( &h_track_numOfPred_s, (N+1)*batchCount );

    std::cout<<"check point 3"<<std::endl;

    // -- allocate constant matrcies in CPU and GPU memories and define values of constant matrices --
    cm->const_matrices_allocations(pp, ldd_const_matrices_Hx_collection, ldd_const_matrices_Ht_collection, ldd_const_matrices_Hx, ldd_const_matrices_Ht, h_startCoefs, h_targetCoefs, ldd_coefs, my_queue, hc_problem);

    std::cout<<"check point 4"<<std::endl;

    // -- allocate GPU gm --
    magma_cmalloc( &d_startSols, (N+1)*batchCount );
    magma_cmalloc( &d_Track, (N+1)*batchCount );
    magma_cmalloc( &d_startCoefs, ldd_coefs );
    magma_cmalloc( &d_targetCoefs, ldd_coefs );
    magma_cmalloc( &d_cgesvA, ldda*N*batchCount );
    magma_cmalloc( &d_cgesvB, ldda*batchCount );

    // -- allocate 2d arrays in GPU gm --
    magma_malloc( (void**) &d_startSols_array,  batchCount * sizeof(magmaFloatComplex*) );
    magma_malloc( (void**) &d_Track_array,    batchCount * sizeof(magmaFloatComplex*) );
    magma_malloc( (void**) &d_startCoefs_array,    sizeof(magmaFloatComplex*) );
    magma_malloc( (void**) &d_targetCoefs_array, sizeof(magmaFloatComplex*) );
    magma_malloc( (void**) &d_cgesvA_array,    batchCount * sizeof(magmaFloatComplex*) );
    magma_malloc( (void**) &d_cgesvB_array,    batchCount * sizeof(magmaFloatComplex*) );

    // -- random initialization for h_cgesvA and h_cgesvB (doesn't matter the value) --
    lapackf77_clarnv( &ione, ISEED, &sizeA, h_cgesvA );
    lapackf77_clarnv( &ione, ISEED, &sizeB, h_cgesvB );

    // -- transfer data from CPU memory to GPU memory --
    data_h2d_time = magma_sync_wtime( my_queue );
    magma_csetmatrix( N+1, batchCount, h_startSols, (N+1), d_startSols, (N+1), my_queue );
    magma_csetmatrix( N+1, batchCount, h_Track, (N+1), d_Track, (N+1), my_queue );
    magma_csetmatrix( coefsCount, 1, h_startCoefs, coefsCount, d_startCoefs, ldd_coefs, my_queue );
    magma_csetmatrix( coefsCount, 1, h_targetCoefs, coefsCount, d_targetCoefs, ldd_coefs, my_queue );
    magma_csetmatrix( N, N*batchCount, h_cgesvA, lda, d_cgesvA, ldda, my_queue );
    magma_csetmatrix( N, batchCount,   h_cgesvB, ldb, d_cgesvB, lddb, my_queue );

    // -- connect pointer to 2d arrays --
    magma_cset_pointer( d_startSols_array, d_startSols, (N+1), 0, 0, (N+1), batchCount, my_queue );
    magma_cset_pointer( d_Track_array, d_Track, (N+1), 0, 0, (N+1), batchCount, my_queue );
    magma_cset_pointer( d_startCoefs_array, d_startCoefs, ldd_coefs, 0, 0, ldd_coefs, 1, my_queue );
    magma_cset_pointer( d_targetCoefs_array, d_targetCoefs, ldd_coefs, 0, 0, ldd_coefs, 1, my_queue );
    magma_cset_pointer( d_cgesvA_array, d_cgesvA, ldda, 0, 0, ldda*N, batchCount, my_queue );
    magma_cset_pointer( d_cgesvB_array, d_cgesvB, lddb, 0, 0, ldda, batchCount, my_queue );

    data_h2d_time = magma_sync_wtime( my_queue ) - data_h2d_time;
    std::cout<<"Host to device data transfer time: "<<data_h2d_time*1000<<std::endl;

    int s = 0;
    //std::cout<<"origins"<<std::endl;
    //magma_cprint(N+1, 1, h_startSols + s * (N+1), (N+1));
    //magma_cprint(coefsCount, 1, h_targetCoefs, ldd_coefs);
    //magma_cprint(coefsCount, 1, h_startCoefs, ldd_coefs);
    //magma_cprint(N, 1, h_cgesvB + s * ldb, ldb);

    // ===================================================================
    // GPU solver for Homotopy Continuation
    // ===================================================================
    std::cout<<"GPU computing ..."<<std::endl;

    if (hc_problem == "3vTrg") {
      std::cout<<"Solving 3vTrg problem ..."<<std::endl<<std::endl;
      gpu_time = kernel_HC_Solver_3vTrg(N, batchCount, coefsCount, ldda, my_queue, d_startSols_array, d_Track_array,
                                          d_startCoefs_array, d_targetCoefs_array, d_cgesvA_array, d_cgesvB_array, cm);
    }
    else if (hc_problem == "4vTrg") {
      std::cout<<"Solving 4vTrg problem ..."<<std::endl<<std::endl;
      gpu_time = kernel_HC_Solver_4vTrg(N, batchCount, coefsCount, ldda, my_queue, d_startSols_array, d_Track_array,
                                          d_startCoefs_array, d_targetCoefs_array, d_cgesvA_array, d_cgesvB_array, cm);
    }
    else if (hc_problem == "Eq17") {
      std::cout<<"Solving Eq17 problem ..."<<std::endl<<std::endl;
      gpu_time = kernel_HC_Solver_Eq17(N, batchCount, coefsCount, ldda, my_queue, d_startSols_array, d_Track_array,
                                          d_startCoefs_array, d_targetCoefs_array, d_cgesvA_array, d_cgesvB_array, cm);
    }
    else if (hc_problem == "optpose4pt") {
      std::cout<<"Solving optpose4pt problem ..."<<std::endl<<std::endl;
      gpu_time = kernel_HC_Solver_optpose4pt(N, batchCount, coefsCount, ldda, my_queue, d_startSols_array, d_Track_array,
                                             d_startCoefs_array, d_targetCoefs_array, d_cgesvA_array, d_cgesvB_array, cm);
    }
    else if (hc_problem == "homo3pt") {
      std::cout<<"Solving homo3pt problem ..."<<std::endl<<std::endl;
      gpu_time = kernel_HC_Solver_homo3pt(N, batchCount, coefsCount, ldda, my_queue, d_startSols_array, d_Track_array,
                                          d_startCoefs_array, d_targetCoefs_array, d_cgesvA_array, d_cgesvB_array, cm,
                                          d_Hx_idx_array, d_Ht_idx_array, h_Hx_idx, h_Ht_idx
                                         );
    }
    else if (hc_problem == "dualTDOA") {
      std::cout<<"Solving dualTDOA problem ..."<<std::endl<<std::endl;
      gpu_time = kernel_HC_Solver_dualTDOA(N, batchCount, coefsCount, ldda, my_queue, d_startSols_array, d_Track_array,
                                             d_startCoefs_array, d_targetCoefs_array, d_cgesvA_array, d_cgesvB_array, cm);
    }
    else if (hc_problem == "optimalPnPQ") {
      std::cout<<"Solving optimalPnPQ problem ..."<<std::endl<<std::endl;
      gpu_time = kernel_HC_Solver_optimalPnPQ(N, batchCount, coefsCount, ldda, my_queue, d_startSols_array, d_Track_array,
                                             d_startCoefs_array, d_targetCoefs_array, d_cgesvA_array, d_cgesvB_array, cm);
    }
    else if (hc_problem == "9pt2radial") {
      std::cout<<"Solving 9pt2radial problem ..."<<std::endl<<std::endl;
      gpu_time = kernel_HC_Solver_9pt2radial(N, batchCount, coefsCount, ldda, my_queue, d_startSols_array, d_Track_array,
                                             d_startCoefs_array, d_targetCoefs_array, d_cgesvA_array, d_cgesvB_array, cm);
    }
    else if (hc_problem == "pose_quiver") {
      std::cout<<"Solving pose_quiver problem ..."<<std::endl<<std::endl;
      gpu_time = kernel_HC_Solver_pose_quiver(N, batchCount, coefsCount, ldda, my_queue, d_startSols_array, d_Track_array,
                                             d_startCoefs_array, d_targetCoefs_array, d_cgesvA_array, d_cgesvB_array, cm);
    }
    else if (hc_problem == "5ptRelativePose") {
      std::cout<<"Solving 5ptRelativePose problem ..."<<std::endl<<std::endl;
      gpu_time = kernel_HC_Solver_5ptRelativePose(N, batchCount, coefsCount, ldda, my_queue, d_startSols_array, d_Track_array,
                                                  d_startCoefs_array, d_targetCoefs_array, d_cgesvA_array, d_cgesvB_array, cm,
                                                  d_Hx_idx_array, d_Ht_idx_array, h_Hx_idx, h_Ht_idx);
    }
    else if (hc_problem == "5ptRelativePose_v2") {
      std::cout<<"Solving 5ptRelativePose_v2 problem ..."<<std::endl<<std::endl;
      gpu_time = kernel_HC_Solver_5ptRelativePose_v2(N, batchCount, coefsCount, ldda, my_queue, d_startSols_array, d_Track_array,
                                                    d_startCoefs_array, d_targetCoefs_array, d_cgesvA_array, d_cgesvB_array, cm);
    }
    else if (hc_problem == "5ptRelativePose_v4") {
      std::cout<<"Solving 5ptRelativePose_v4 problem ..."<<std::endl<<std::endl;
      gpu_time = kernel_HC_Solver_5ptRelativePose_v4(N, batchCount, coefsCount, ldda, my_queue, d_startSols_array, d_Track_array,
                                                    d_startCoefs_array, d_targetCoefs_array, d_cgesvA_array, d_cgesvB_array, cm);
    }
    else if (hc_problem == "P3P") {
      std::cout<<"Solving P3P problem ..."<<std::endl<<std::endl;
      gpu_time = kernel_HC_Solver_P3P(N, batchCount, coefsCount, ldda, my_queue, d_startSols_array, d_Track_array,
                                             d_startCoefs_array, d_targetCoefs_array, d_cgesvA_array, d_cgesvB_array, cm);
    }
    else if (hc_problem == "P3P_v2") {
      std::cout<<"Solving P3P_v2 problem ..."<<std::endl<<std::endl;
      gpu_time = kernel_HC_Solver_P3P_v2(N, batchCount, coefsCount, ldda, my_queue, d_startSols_array, d_Track_array,
                                             d_startCoefs_array, d_targetCoefs_array, d_cgesvA_array, d_cgesvB_array, cm);
    }
    else if (hc_problem == "2vTrg_distortion") {
      std::cout<<"Solving 2vTrg_distortion problem ..."<<std::endl<<std::endl;
      gpu_time = kernel_HC_Solver_2vTrg_distortion(N, batchCount, coefsCount, ldda, my_queue, d_startSols_array, d_Track_array,
                                             d_startCoefs_array, d_targetCoefs_array, d_cgesvA_array, d_cgesvB_array, cm);
    }
    else if (hc_problem == "R6P1lin") {
      std::cout<<"Solving R6P1lin problem ..."<<std::endl<<std::endl;
      gpu_time = kernel_HC_Solver_R6P1lin(N, batchCount, coefsCount, ldda, my_queue, d_startSols_array, d_Track_array,
                                             d_startCoefs_array, d_targetCoefs_array, d_cgesvA_array, d_cgesvB_array, cm);
    }
    else if (hc_problem == "trifocal_rel_pose_f") {
      std::cout<<"Solving trifocal_rel_pose_f problem ..."<<std::endl<<std::endl;
      gpu_time = kernel_HC_Solver_P3P(N, batchCount, coefsCount, ldda, my_queue, d_startSols_array, d_Track_array,
                                             d_startCoefs_array, d_targetCoefs_array, d_cgesvA_array, d_cgesvB_array, cm);
    }

    // -- check returns from the kernel --
    data_d2h_time = magma_sync_wtime( my_queue );
    magma_cgetmatrix( (N+1), batchCount, d_Track, (N+1), h_track_sols, (N+1), my_queue );
    magma_cgetmatrix( (N+1), batchCount, d_startSols, (N+1), h_track_numOfPred_s, (N+1), my_queue );
    magma_cgetmatrix( N, batchCount, d_cgesvB, lddb, h_cgesvB_verify, ldb, my_queue );
    magma_cgetmatrix( N, N*batchCount, d_cgesvA, ldda, h_cgesvA_verify, lda, my_queue );
    data_d2h_time = magma_sync_wtime( my_queue ) - data_d2h_time;
    std::cout<<"results:"<<std::endl;
    magma_cprint(N+1, 1, h_track_sols + s * (N+1), N+1);
    //magma_cprint(N+1, 1, h_track_numOfPred_s + s * (N+1), N+1);
    magma_cprint(N, 1, h_cgesvB_verify + s * ldb, ldb);
    //magma_cprint(N, N, h_cgesvA_verify + s * lda, lda);


    // ===================================================================
    // CPU solver for Homotopy Continuation
    // ===================================================================
    magmaFloatComplex *h_Track_cpu;
    magma_int_t *h_Track_Success;
    magma_cmalloc_cpu( &h_Track_cpu, batchCount*N );
    magma_imalloc_cpu( &h_Track_Success, batchCount );
    for (int i = 0; i < batchCount; i++) {
      h_Track_Success[i] = 0;
    }

    // -- cpu computation on the HC problem --
    std::cout<<"CPU-only computing ..."<<std::endl;
    if (hc_problem == "3vTrg") {
      cpu_time = cpu_hc_solver_3vTrg(h_Track_cpu, h_Track_Success, h_startSols, h_Track, h_startCoefs, h_targetCoefs, h_cgesvA, h_cgesvB, batchCount, coefsCount, N, my_queue, 70, tracks_success_file);
    }
    else if (hc_problem == "4vTrg") {
      cpu_time = cpu_hc_solver_4vTrg(h_Track_cpu, h_Track_Success, h_startSols, h_Track, h_startCoefs, h_targetCoefs, h_cgesvA, h_cgesvB, batchCount, coefsCount, N, my_queue, 120, tracks_success_file);
    }
    else if (hc_problem == "Eq17") {
      cpu_time = cpu_hc_solver_Eq17(h_Track_cpu, h_Track_Success, h_startSols, h_Track, h_startCoefs, h_targetCoefs, h_cgesvA, h_cgesvB, batchCount, coefsCount, N, my_queue, 50, tracks_success_file);
    }
    else if (hc_problem == "optpose4pt") {
      cpu_time = cpu_hc_solver_optpose4pt(h_Track_cpu, h_Track_Success, h_startSols, h_Track, h_startCoefs, h_targetCoefs, h_cgesvA, h_cgesvB, batchCount, coefsCount, N, my_queue, 30, tracks_success_file);
    }
    else if (hc_problem == "homo3pt") {
      cpu_time = cpu_hc_solver_homo3pt(h_Track_cpu, h_Track_Success, h_startSols, h_Track, h_startCoefs, h_targetCoefs, h_cgesvA, h_cgesvB, batchCount, coefsCount, N, my_queue, 30, tracks_success_file);
    }
    else if (hc_problem == "dualTDOA") {
      cpu_time = cpu_hc_solver_dualTDOA(h_Track_cpu, h_Track_Success, h_startSols, h_Track, h_startCoefs, h_targetCoefs, h_cgesvA, h_cgesvB, batchCount, coefsCount, N, my_queue, 50, tracks_success_file);
    }
    else if (hc_problem == "optimalPnPQ") {
      cpu_time = cpu_hc_solver_optimalPnPQ(h_Track_cpu, h_Track_Success, h_startSols, h_Track, h_startCoefs, h_targetCoefs, h_cgesvA, h_cgesvB, batchCount, coefsCount, N, my_queue, 50, tracks_success_file);
    }
    else if (hc_problem == "9pt2radial") {
      cpu_time = cpu_hc_solver_9pt2radial(h_Track_cpu, h_Track_Success, h_startSols, h_Track, h_startCoefs, h_targetCoefs, h_cgesvA, h_cgesvB, batchCount, coefsCount, N, my_queue, 1000, tracks_success_file);
    }
    else if (hc_problem == "pose_quiver") {
      cpu_time = cpu_hc_solver_pose_quiver(h_Track_cpu, h_Track_Success, h_startSols, h_Track, h_startCoefs, h_targetCoefs, h_cgesvA, h_cgesvB, batchCount, coefsCount, N, my_queue, 40, tracks_success_file);
    }
    else if (hc_problem == "5ptRelativePose") {
      cpu_time = cpu_hc_solver_5ptRelativePose(h_Track_cpu, h_Track_Success, h_startSols, h_Track, h_startCoefs, h_targetCoefs, h_cgesvA, h_cgesvB, batchCount, coefsCount, N, my_queue, 100, tracks_success_file);
    }
    else if (hc_problem == "5ptRelativePose_v2") {
      cpu_time = cpu_hc_solver_5ptRelativePose_v2(h_Track_cpu, h_Track_Success, h_startSols, h_Track, h_startCoefs, h_targetCoefs, h_cgesvA, h_cgesvB, batchCount, coefsCount, N, my_queue, 1000, tracks_success_file);
    }
    else if (hc_problem == "5ptRelativePose_v4") {
      cpu_time = cpu_hc_solver_5ptRelativePose_v4(h_Track_cpu, h_Track_Success, h_startSols, h_Track, h_startCoefs, h_targetCoefs, h_cgesvA, h_cgesvB, batchCount, coefsCount, N, my_queue, 1000, tracks_success_file);
    }
    else if (hc_problem == "P3P") {
      cpu_time = cpu_hc_solver_P3P(h_Track_cpu, h_Track_Success, h_startSols, h_Track, h_startCoefs, h_targetCoefs, h_cgesvA, h_cgesvB, batchCount, coefsCount, N, my_queue, 1000, tracks_success_file);
    }
    else if (hc_problem == "P3P_v2") {
      cpu_time = cpu_hc_solver_P3P_v2(h_Track_cpu, h_Track_Success, h_startSols, h_Track, h_startCoefs, h_targetCoefs, h_cgesvA, h_cgesvB, batchCount, coefsCount, N, my_queue, 20, tracks_success_file);
    }
    else if (hc_problem == "2vTrg_distortion") {
      cpu_time = cpu_hc_solver_2vTrg_distortion(h_Track_cpu, h_Track_Success, h_startSols, h_Track, h_startCoefs, h_targetCoefs, h_cgesvA, h_cgesvB, batchCount, coefsCount, N, my_queue, 30, tracks_success_file);
    }
    else if (hc_problem == "R6P1lin") {
      cpu_time = cpu_hc_solver_R6P1lin(h_Track_cpu, h_Track_Success, h_startSols, h_Track, h_startCoefs, h_targetCoefs, h_cgesvA, h_cgesvB, batchCount, coefsCount, N, my_queue, 100, tracks_success_file);
    }
    else if (hc_problem == "trifocal_rel_pose_f") {
      cpu_time = cpu_hc_solver_trifocal_rel_pose_f(h_Track_cpu, h_Track_Success, h_startSols, h_Track, h_startCoefs, h_targetCoefs, h_cgesvA, h_cgesvB, batchCount, coefsCount, N, my_queue, 10000, tracks_success_file);
    }

    printf("%% Problem   # of eqs   # of sols    GPU+CPU time (ms)    CPU time (ms)\n");
    printf("%%===========================================================================\n");
    printf(" %s     %5lld   %10lld           %7.2f            %7.2f\n",
            hc_problem.c_str(), (long long) N, (long long) batchCount, (gpu_time)*1000, cpu_time*1000);

    // -- write converged HC tracks to a file --
    for (int sol_idx = 0; sol_idx < batchCount; sol_idx++) {
      if (h_Track_Success[sol_idx] == 1) {
          for(int i = 0; i < N; i++) {
            track_sols_file << MAGMA_C_REAL((h_Track_cpu + sol_idx * N)[i]) << "+" << MAGMA_C_IMAG((h_Track_cpu + sol_idx * N)[i]) << "i"<< "\n";
        }
      }
    }

    magma_queue_destroy( my_queue );

    magma_free_cpu( h_cgesvA );
    magma_free_cpu( h_cgesvB );
    magma_free_cpu( h_cgesvA_verify );
    magma_free_cpu( h_cgesvB_verify );
    magma_free_cpu( h_track_sols );
    magma_free_cpu( h_track_numOfPred_s );

    magma_free_cpu( h_Track_cpu );
    magma_free_cpu( h_Track_Success );

    magma_free( d_startSols );
    magma_free( d_Track );
    magma_free( d_startCoefs );
    magma_free( d_targetCoefs );
    magma_free( d_cgesvA );
    magma_free( d_cgesvB );
    magma_free( d_startSols_array );
    magma_free( d_Track_array );
    magma_free( d_startCoefs_array );
    magma_free( d_targetCoefs_array );
    magma_free( d_cgesvA_array );
    magma_free( d_cgesvB_array );

    cm->free_const_matrices();

    fflush( stdout );
    printf( "\n" );
    magma_finalize();
  }

} // end of namespace

#endif

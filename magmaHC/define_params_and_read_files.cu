#ifndef define_params_and_read_files_cu
#define define_params_and_read_files_cu
// =======================================================================
//
// Modifications
//    Chien  21-09-13:   Originally Created
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

#include "magmaHC-problems.cuh"

namespace magmaHCWrapper {

  void problem_params::define_problem_params(std::string problem_filename, std::string HC_problem)
  {
    //magmaHCWrapper::problem_params specifications;
    startSols_filename = problem_filename;
    startCoef_filename = problem_filename;
    targetParam_filename = problem_filename;
    targetParam_filename.append("/target_params.txt");
    startSols_filename.append("/start_sols.txt");
    startCoef_filename.append("/start_coeffs.txt");

    if (HC_problem == "3vTrg") {
      // -- problem specifications --
      numOfCoeffs = 34;
      numOfTargetParams = 33;
      numOfTracks = 94;
      numOfVars = 9;

      // -- constant matrices parameters --
      Hx_maximal_terms = 3;
      Hx_maximal_parts_x = 1;
      Ht_maximal_terms = 9;
      Ht_H_maximal_terms_x = 2;
    }
    else if (HC_problem == "4vTrg") {
      // -- problem specifications --
      numOfCoeffs = 63;
      numOfTargetParams = 62;
      numOfTracks = 296;
      numOfVars = 14;

      // -- constant matrices parameters --
      Hx_maximal_terms = 3;
      Hx_maximal_parts_x = 1;
      Ht_maximal_terms = 11;
      Ht_H_maximal_terms_x = 2;
    }
    else if (HC_problem == "5ptRelativePose") {
      // -- problem specifications --
      numOfCoeffs = 44;
      numOfTargetParams = 20;
      numOfTracks = 160;
      numOfVars = 16;

      // -- constant matrices parameters --
      Hx_maximal_terms = 7;
      Hx_maximal_parts_x = 2;
      Ht_maximal_terms = 9;
      Ht_H_maximal_terms_x = 3;
    }
    else if (HC_problem == "5ptRelativePose_v2") {
      // -- problem specifications --
      numOfCoeffs = 60;
      numOfTargetParams = 36;
      numOfTracks = 27;
      numOfVars = 3;

      // -- constant matrices parameters --
      Hx_maximal_terms = 10;
      Hx_maximal_parts_x = 2;
      Ht_maximal_terms = 20;
      Ht_H_maximal_terms_x = 3;
    }
    else if (HC_problem == "5ptRelativePose_v3") {
      // -- problem specifications --
      numOfCoeffs = 43;
      numOfTargetParams = 20;
      numOfTracks = 10;
      numOfVars = 9;

      // -- constant matrices parameters --
      Hx_maximal_terms = 7;
      Hx_maximal_parts_x = 2;
      Ht_maximal_terms = 9;
      Ht_H_maximal_terms_x = 3;
    }
    else if (HC_problem == "5ptRelativePose_v4") {
      // -- problem specifications --
      numOfCoeffs = 137;
      numOfTargetParams = 20;
      numOfTracks = 160;
      numOfVars = 6;

      // -- constant matrices parameters --
      Hx_maximal_terms = 12;
      Hx_maximal_parts_x = 2;
      Ht_maximal_terms = 30;
      Ht_H_maximal_terms_x = 3;
    }
    else if (HC_problem == "Eq17") {
      // -- problem specifications --
      numOfCoeffs = 34;
      numOfTargetParams = 34;
      numOfTracks = 12;
      numOfVars = 10;

      // -- constant matrices parameters --
      Hx_maximal_terms = 4;
      Hx_maximal_parts_x = 1;
      Ht_maximal_terms = 12;
      Ht_H_maximal_terms_x = 2;
    }
    else if (HC_problem == "optpose4pt") {
      // -- problem specifications --
      numOfCoeffs = 87;
      numOfTargetParams = 48;
      numOfTracks = 32;
      numOfVars = 5;

      // -- constant matrices parameters --
      Hx_maximal_terms = 6;
      Hx_maximal_parts_x = 1;
      Ht_maximal_terms = 21;
      Ht_H_maximal_terms_x = 2;
    }
    else if (HC_problem == "homo3pt") {
      // -- problem specifications --
      numOfCoeffs = 44;
      numOfTargetParams = 18;
      numOfTracks = 8;
      numOfVars = 8;

      // -- constant matrices parameters --
      Hx_maximal_terms = 6;
      Hx_maximal_parts_x = 2;
      Ht_maximal_terms = 9;
      Ht_H_maximal_terms_x = 3;
    }
    else if (HC_problem == "dualTDOA") {
      // -- problem specifications --
      numOfCoeffs = 88;
      numOfTargetParams = 20;
      numOfTracks = 192;
      numOfVars = 5;

      // -- constant matrices parameters --
      Hx_maximal_terms = 15;
      Hx_maximal_parts_x = 3;
      Ht_maximal_terms = 33;
      Ht_H_maximal_terms_x = 4;
    }
    else if (HC_problem == "optimalPnPQ") {
      // -- problem specifications --
      numOfCoeffs = 107;
      numOfTargetParams = 81;
      numOfTracks = 128;
      numOfVars = 4;

      // -- constant matrices parameters --
      Hx_maximal_terms = 20;
      Hx_maximal_parts_x = 3;
      Ht_maximal_terms = 35;
      Ht_H_maximal_terms_x = 4;
    }
    else if (HC_problem == "9pt2radial") {
      // -- problem specifications --
      numOfCoeffs = 93;
      numOfTargetParams = 72;
      numOfTracks = 27;
      numOfVars = 4;

      // -- constant matrices parameters --
      Hx_maximal_terms = 38;
      Hx_maximal_parts_x = 4;
      Ht_maximal_terms = 57;
      Ht_H_maximal_terms_x = 5;
    }
    else if (HC_problem == "pose_quiver") {
      // -- problem specifications --
      numOfCoeffs = 56;
      numOfTargetParams = 36;
      numOfTracks = 28;
      numOfVars = 4;

      // -- constant matrices parameters --
      Hx_maximal_terms = 8;
      Hx_maximal_parts_x = 2;
      Ht_maximal_terms = 18;
      Ht_H_maximal_terms_x = 3;
    }
    else if (HC_problem == "P3P") {
      // -- problem specifications --
      numOfCoeffs = 31;
      numOfTargetParams = 15;
      numOfTracks = 112;
      numOfVars = 10;

      // -- constant matrices parameters --
      Hx_maximal_terms = 7;
      Hx_maximal_parts_x = 2;
      Ht_maximal_terms = 9;
      Ht_H_maximal_terms_x = 3;
    }
    else if (HC_problem == "P3P_v2") {
      // -- problem specifications --
      numOfCoeffs = 9;
      numOfTargetParams = 18;
      numOfTracks = 8;
      numOfVars = 3;

      // -- constant matrices parameters --
      Hx_maximal_terms = 2;
      Hx_maximal_parts_x = 1;
      Ht_maximal_terms = 4;
      Ht_H_maximal_terms_x = 2;
    }
    else if (HC_problem == "R6P1lin") {
      // -- problem specifications --
      numOfCoeffs = 164;
      numOfTargetParams = 55;
      numOfTracks = 160;
      numOfVars = 18;

      // -- constant matrices parameters --
      Hx_maximal_terms = 12;
      Hx_maximal_parts_x = 2;
      Ht_maximal_terms = 36;
      Ht_H_maximal_terms_x = 3;
    }
    else if (HC_problem == "2vTrg_distortion") {
      // -- problem specifications --
      numOfCoeffs = 38;
      numOfTargetParams = 15;
      numOfTracks = 28;
      numOfVars = 5;

      // -- constant matrices parameters --
      Hx_maximal_terms = 10;
      Hx_maximal_parts_x = 3;
      Ht_maximal_terms = 25;
      Ht_H_maximal_terms_x = 4;
    }
    else if (HC_problem == "trifocal_rel_pose_f") {
      // -- problem specifications --
      numOfCoeffs = 143;
      numOfTargetParams = 26;
      numOfTracks = 1784;
      numOfVars = 16;

      // -- constant matrices parameters --
      Hx_maximal_terms = 2;
      Hx_maximal_parts_x = 2;
      Ht_maximal_terms = 2;
      Ht_H_maximal_terms_x = 2;
    }
    else {
      std::cout<<"You are entering invalid HC problem in your input argument!"<<std::endl;
      print_usage();
      exit(1);
    }
  }

  void problem_params::print_usage() {
    std::cerr << "===============================================================================================================\n";
    std::cerr << "Usage: ./magmaHC-main <input-argument> <command>\n\n";
    std::cerr << "Choices of input arguments and commands\n";
    std::cerr << "<input-argument>      <command>\n"
                 "       -p             <problem>         # (or --problem)  : minimal problem name \n"
                 "       -h                               # (or --help)     : print this help message\n\n";
    std::cerr << "----------------------- NOTICE -----------------------\n";
    std::cerr << "1. Order matters.\n";
    std::cerr << "2. If <input-argument> and <command> are not specified, the help message will be automatically shown.\n\n";
    std::cerr << "----------------------- Examples -----------------------\n";
    std::cerr << "./magmaHC-main -p homo3pt               # solve homogeneous 3 points problem\n";
    std::cerr << "===============================================================================================================\n";
  }


} // end of namespace

#endif

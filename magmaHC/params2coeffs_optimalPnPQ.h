#ifndef params2coeffs_optimalPnPQ_h
#define params2coeffs_optimalPnPQ_h
// =======================================================================
//
// Modifications
//    Chien  21-10-28:   Initially Created
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min

namespace magmaHCWrapper {

  void params2coeffs_optimalPnPQ(magmaFloatComplex *h_target_params, magmaFloatComplex *h_target_coeffs)
  {
    magmaFloatComplex x5 = h_target_params[0];
    magmaFloatComplex x6 = h_target_params[1];
    magmaFloatComplex x7 = h_target_params[2];
    magmaFloatComplex x8 = h_target_params[3];
    magmaFloatComplex x9 = h_target_params[4];
    magmaFloatComplex x10 = h_target_params[5];
    magmaFloatComplex x11 = h_target_params[6];
    magmaFloatComplex x12 = h_target_params[7];
    magmaFloatComplex x13 = h_target_params[8];
    magmaFloatComplex x14 = h_target_params[9];
    magmaFloatComplex x15 = h_target_params[10];
    magmaFloatComplex x16 = h_target_params[11];
    magmaFloatComplex x17 = h_target_params[12];
    magmaFloatComplex x18 = h_target_params[13];
    magmaFloatComplex x19 = h_target_params[14];
    magmaFloatComplex x20 = h_target_params[15];
    magmaFloatComplex x21 = h_target_params[16];
    magmaFloatComplex x22 = h_target_params[17];
    magmaFloatComplex x23 = h_target_params[18];
    magmaFloatComplex x24 = h_target_params[19];
    magmaFloatComplex x25 = h_target_params[20];
    magmaFloatComplex x26 = h_target_params[21];
    magmaFloatComplex x27 = h_target_params[22];
    magmaFloatComplex x28 = h_target_params[23];
    magmaFloatComplex x29 = h_target_params[24];
    magmaFloatComplex x30 = h_target_params[25];
    magmaFloatComplex x31 = h_target_params[26];
    magmaFloatComplex x32 = h_target_params[27];
    magmaFloatComplex x33 = h_target_params[28];
    magmaFloatComplex x34 = h_target_params[29];
    magmaFloatComplex x35 = h_target_params[30];
    magmaFloatComplex x36 = h_target_params[31];
    magmaFloatComplex x37 = h_target_params[32];
    magmaFloatComplex x38 = h_target_params[33];
    magmaFloatComplex x39 = h_target_params[34];
    magmaFloatComplex x40 = h_target_params[35];
    magmaFloatComplex x41 = h_target_params[36];
    magmaFloatComplex x42 = h_target_params[37];
    magmaFloatComplex x43 = h_target_params[38];
    magmaFloatComplex x44 = h_target_params[39];
    magmaFloatComplex x45 = h_target_params[40];
    magmaFloatComplex x46 = h_target_params[41];
    magmaFloatComplex x47 = h_target_params[42];
    magmaFloatComplex x48 = h_target_params[43];
    magmaFloatComplex x49 = h_target_params[44];
    magmaFloatComplex x50 = h_target_params[45];
    magmaFloatComplex x51 = h_target_params[46];
    magmaFloatComplex x52 = h_target_params[47];
    magmaFloatComplex x53 = h_target_params[48];
    magmaFloatComplex x54 = h_target_params[49];
    magmaFloatComplex x55 = h_target_params[50];
    magmaFloatComplex x56 = h_target_params[51];
    magmaFloatComplex x57 = h_target_params[52];
    magmaFloatComplex x58 = h_target_params[53];
    magmaFloatComplex x59 = h_target_params[54];
    magmaFloatComplex x60 = h_target_params[55];
    magmaFloatComplex x61 = h_target_params[56];
    magmaFloatComplex x62 = h_target_params[57];
    magmaFloatComplex x63 = h_target_params[58];
    magmaFloatComplex x64 = h_target_params[59];
    magmaFloatComplex x65 = h_target_params[60];
    magmaFloatComplex x66 = h_target_params[61];
    magmaFloatComplex x67 = h_target_params[62];
    magmaFloatComplex x68 = h_target_params[63];
    magmaFloatComplex x69 = h_target_params[64];
    magmaFloatComplex x70 = h_target_params[65];
    magmaFloatComplex x71 = h_target_params[66];
    magmaFloatComplex x72 = h_target_params[67];
    magmaFloatComplex x73 = h_target_params[68];
    magmaFloatComplex x74 = h_target_params[69];
    magmaFloatComplex x75 = h_target_params[70];
    magmaFloatComplex x76 = h_target_params[71];
    magmaFloatComplex x77 = h_target_params[72];
    magmaFloatComplex x78 = h_target_params[73];
    magmaFloatComplex x79 = h_target_params[74];
    magmaFloatComplex x80 = h_target_params[75];
    magmaFloatComplex x81 = h_target_params[76];
    magmaFloatComplex x82 = h_target_params[77];
    magmaFloatComplex x83 = h_target_params[78];
    magmaFloatComplex x84 = h_target_params[79];
    magmaFloatComplex x85 = h_target_params[80];

    h_target_coeffs[0] = (x8 - x6 - x42 + x44 - x78 + x80);
    h_target_coeffs[1] = (2*x53 - 2*x47 - 2*x51 - 2*x11 + 2*x69 - 2*x71 - 2*x83);
    h_target_coeffs[2] = (2*x24 - 2*x12 - 2*x26 - 2*x48 - 2*x60 + 2*x62 - 2*x84);
    h_target_coeffs[3] = (2*x5 + 2*x9 - 2*x15 + 2*x17 + 2*x33 - 2*x35 + 2*x41 + 2*x45 + 2*x77 + 2*x81);
    h_target_coeffs[4] = (4*x74 - 2*x44 - 4*x56 - 2*x6 - 2*x80);
    h_target_coeffs[5] = (2*x5 - 2*x9 - 2*x15 + 2*x17 + 4*x29 - 2*x33 + 2*x35 + 2*x41 - 2*x45 - 4*x57 - 4*x65 + 4*x75 + 2*x77 - 2*x81);
    h_target_coeffs[6] = (2*x26 - 4*x20 - 2*x24 - 2*x12 + 4*x38 - 2*x48 + 4*x50 + 4*x54 - 2*x60 + 2*x62 - 4*x68 - 4*x72 - 2*x84);
    h_target_coeffs[7] = (2*x6 + 4*x30 + 2*x44 - 4*x66 + 2*x78);
    h_target_coeffs[8] = (2*x11 - 4*x21 - 4*x23 - 4*x27 + 4*x39 + 2*x47 - 2*x51 + 2*x53 + 4*x59 + 4*x63 - 2*x69 + 2*x71 + 2*x83);
    h_target_coeffs[9] = (2*x6 - 2*x8 + 4*x14 + 4*x18 - 4*x32 - 4*x36 + 2*x42 - 2*x44);
    h_target_coeffs[10] = (2*x47 - 2*x11 - 2*x51 - 2*x53 + 2*x69 + 2*x71 + 2*x83);
    h_target_coeffs[11] = (2*x24 - 4*x20 - 2*x12 + 2*x26 - 4*x38 + 2*x48 + 4*x50 - 4*x54 - 2*x60 - 2*x62 - 4*x68 + 4*x72 + 2*x84);
    h_target_coeffs[12] = (2*x5 + 2*x9 - 2*x15 - 2*x17 - 4*x29 + 2*x33 + 2*x35 - 2*x41 - 2*x45 - 4*x57 - 4*x65 + 4*x75 - 2*x77 - 2*x81);
    h_target_coeffs[13] = (2*x11 - 4*x21 - 4*x23 + 4*x27 - 4*x39 - 2*x47 + 2*x51 + 2*x53 + 4*x59 - 4*x63 - 2*x69 - 2*x71 + 2*x83);
    h_target_coeffs[14] = (8*x14 + 8*x36 - 8*x66 - 8*x74);
    h_target_coeffs[15] = (2*x11 - 4*x21 + 4*x23 + 4*x27 + 4*x39 + 2*x47 + 2*x51 - 2*x53 + 4*x59 + 4*x63 - 2*x69 + 2*x71 - 2*x83);
    h_target_coeffs[16] = (2*x12 - 2*x24 - 2*x26 - 2*x48 + 2*x60 + 2*x62 + 2*x84);
    h_target_coeffs[17] = (2*x15 - 2*x9 - 2*x5 + 2*x17 - 4*x29 - 2*x33 - 2*x35 + 2*x41 + 2*x45 - 4*x57 + 4*x65 - 4*x75 - 2*x77 - 2*x81);
    h_target_coeffs[18] = (2*x12 + 4*x20 - 2*x24 + 2*x26 - 4*x38 + 2*x48 + 4*x50 + 4*x54 + 2*x60 - 2*x62 + 4*x68 + 4*x72 - 2*x84);
    h_target_coeffs[19] = (2*x15 - 2*x9 - 2*x5 - 2*x17 - 2*x33 + 2*x35 - 2*x41 - 2*x45 + 2*x77 + 2*x81);
    h_target_coeffs[20] = (x42 - x8 - x6 + x44 + x78 + x80);
    h_target_coeffs[21] = (2*x5 - 2*x9 - 2*x15 - 2*x17 - 2*x33 - 2*x35 - 2*x41 + 2*x45 - 2*x77 + 2*x81);
    h_target_coeffs[22] = (2*x48 - 2*x24 - 2*x26 - 2*x12 - 2*x60 - 2*x62 + 2*x84);
    h_target_coeffs[23] = (2*x6 + 2*x8 + 4*x14 - 4*x18 + 4*x32 - 4*x36 - 2*x42 - 2*x44);
    h_target_coeffs[24] = (2*x11 - 4*x21 + 4*x23 - 4*x27 - 4*x39 - 2*x47 - 2*x51 - 2*x53 + 4*x59 - 4*x63 - 2*x69 - 2*x71 - 2*x83);
    h_target_coeffs[25] = (2*x6 - 4*x30 + 2*x44 - 4*x66 - 2*x78);
    h_target_coeffs[26] = (2*x9 - 2*x5 + 2*x15 + 2*x17 + 2*x33 + 2*x35 + 2*x41 - 2*x45 - 2*x77 + 2*x81);
    h_target_coeffs[27] = (2*x12 + 4*x20 + 2*x24 + 2*x26 + 4*x38 - 2*x48 + 4*x50 - 4*x54 + 2*x60 + 2*x62 + 4*x68 - 4*x72 + 2*x84);
    h_target_coeffs[28] = (2*x9 - 2*x5 + 2*x15 - 2*x17 + 4*x29 + 2*x33 - 2*x35 - 2*x41 + 2*x45 - 4*x57 + 4*x65 - 4*x75 + 2*x77 - 2*x81);
    h_target_coeffs[29] = (2*x12 + 2*x24 - 2*x26 + 2*x48 + 2*x60 - 2*x62 - 2*x84);
    h_target_coeffs[30] = (x42 - x8 - x6 + x44 - x78 - x80);
    h_target_coeffs[31] = (2*x47 - 2*x11 + 2*x51 + 2*x53 + 2*x69 + 2*x71 - 2*x83);
    h_target_coeffs[32] = (4*x56 - 2*x44 - 2*x6 + 4*x74 + 2*x80);
    h_target_coeffs[33] = (2*x51 - 2*x47 - 2*x11 - 2*x53 + 2*x69 - 2*x71 + 2*x83);
    h_target_coeffs[34] = (x8 - x6 - x42 + x44 + x78 - x80);
    h_target_coeffs[35] = (x11 - x7 - x43 + x47 - x79 + x83);
    h_target_coeffs[36] = (2*x8 + 2*x44 - 2*x52 + 2*x56 + 2*x70 - 2*x74 + 2*x80);
    h_target_coeffs[37] = (2*x25 - 2*x13 - 2*x5 - 2*x29 - 2*x41 - 2*x49 - 2*x61 + 2*x65 - 2*x77 - 2*x85);
    h_target_coeffs[38] = (2*x10 - 2*x16 + 2*x20 + 2*x34 - 2*x38 + 2*x46 + 2*x82);
    h_target_coeffs[39] = (4*x53 - 2*x47 - 2*x7 - 4*x71 - 2*x83);
    h_target_coeffs[40] = (2*x20 - 2*x16 - 2*x10 - 4*x26 - 2*x34 + 2*x38 - 2*x46 - 4*x50 - 4*x58 + 4*x62 + 4*x68 + 4*x76 - 2*x82);
    h_target_coeffs[41] = (2*x5 - 2*x13 + 4*x17 - 2*x25 + 2*x29 - 4*x35 + 2*x41 - 2*x49 + 4*x55 - 2*x61 + 2*x65 - 4*x73 + 2*x77 - 2*x85);
    h_target_coeffs[42] = (2*x7 - 2*x11 + 4*x23 + 4*x31 - 4*x59 - 4*x67 + 2*x79 - 2*x83);
    h_target_coeffs[43] = (2*x8 - 4*x14 - 4*x22 - 4*x28 + 4*x32 + 4*x40 + 2*x44 - 2*x52 + 2*x56 + 4*x64 - 2*x70 + 2*x74 + 2*x80);
    h_target_coeffs[44] = (2*x7 + 4*x19 - 4*x37 + 2*x43 + 2*x83);
    h_target_coeffs[45] = (2*x8 - 2*x44 - 2*x52 - 2*x56 + 2*x70 + 2*x74 - 2*x80);
    h_target_coeffs[46] = (4*x17 - 2*x13 - 2*x5 + 2*x25 + 2*x29 + 4*x35 + 2*x41 + 2*x49 - 4*x55 - 2*x61 - 2*x65 + 4*x73 + 2*x77 + 2*x85);
    h_target_coeffs[47] = (2*x10 - 2*x16 - 2*x20 + 4*x26 + 2*x34 + 2*x38 - 2*x46 + 4*x50 - 4*x58 + 4*x62 - 4*x68 + 4*x76 - 2*x82);
    h_target_coeffs[48] = (4*x28 - 4*x14 - 4*x22 - 2*x8 - 4*x32 - 4*x40 + 2*x44 + 2*x52 - 2*x56 - 4*x64 - 2*x70 + 2*x74 - 2*x80);
    h_target_coeffs[49] = (8*x37 - 8*x23 + 8*x53 - 8*x67);
    h_target_coeffs[50] = (4*x14 - 2*x8 - 4*x22 + 4*x28 - 4*x32 + 4*x40 - 2*x44 + 2*x52 + 2*x56 + 4*x64 - 2*x70 - 2*x74 + 2*x80);
    h_target_coeffs[51] = (2*x5 + 2*x13 - 2*x25 + 2*x29 - 2*x41 - 2*x49 + 2*x61 - 2*x65 + 2*x77 + 2*x85);
    h_target_coeffs[52] = (2*x16 - 2*x10 - 2*x20 - 4*x26 - 2*x34 + 2*x38 + 2*x46 - 4*x50 - 4*x58 + 4*x62 - 4*x68 - 4*x76 - 2*x82);
    h_target_coeffs[53] = (2*x5 + 2*x13 + 4*x17 - 2*x25 - 2*x29 - 4*x35 + 2*x41 + 2*x49 + 4*x55 + 2*x61 + 2*x65 + 4*x73 - 2*x77 - 2*x85);
    h_target_coeffs[54] = (2*x16 - 2*x10 + 2*x20 - 2*x34 - 2*x38 - 2*x46 + 2*x82);
    h_target_coeffs[55] = (x43 - x11 - x7 + x47 + x79 + x83);
    h_target_coeffs[56] = (2*x46 - 2*x16 - 2*x20 - 2*x34 - 2*x38 - 2*x10 + 2*x82);
    h_target_coeffs[57] = (2*x5 - 2*x13 - 2*x25 - 2*x29 - 2*x41 + 2*x49 - 2*x61 - 2*x65 - 2*x77 + 2*x85);
    h_target_coeffs[58] = (2*x7 - 4*x19 - 4*x37 - 2*x43 + 2*x83);
    h_target_coeffs[59] = (2*x8 + 4*x14 - 4*x22 - 4*x28 + 4*x32 - 4*x40 - 2*x44 - 2*x52 - 2*x56 - 4*x64 - 2*x70 - 2*x74 - 2*x80);
    h_target_coeffs[60] = (2*x7 + 2*x11 + 4*x23 - 4*x31 + 4*x59 - 4*x67 - 2*x79 - 2*x83);
    h_target_coeffs[61] = (2*x10 + 2*x16 - 2*x20 + 2*x34 - 2*x38 - 2*x46 + 2*x82);
    h_target_coeffs[62] = (2*x13 - 2*x5 + 4*x17 + 2*x25 - 2*x29 + 4*x35 + 2*x41 - 2*x49 - 4*x55 + 2*x61 - 2*x65 - 4*x73 - 2*x77 + 2*x85);
    h_target_coeffs[63] = (2*x10 + 2*x16 + 2*x20 + 4*x26 + 2*x34 + 2*x38 + 2*x46 + 4*x50 - 4*x58 + 4*x62 + 4*x68 - 4*x76 - 2*x82);
    h_target_coeffs[64] = (2*x13 - 2*x5 + 2*x25 + 2*x29 - 2*x41 + 2*x49 + 2*x61 + 2*x65 + 2*x77 - 2*x85);
    h_target_coeffs[65] = (x11 - x7 + x43 - x47 - x79 + x83);
    h_target_coeffs[66] = (2*x44 - 2*x8 + 2*x52 - 2*x56 + 2*x70 - 2*x74 - 2*x80);
    h_target_coeffs[67] = (2*x47 - 2*x7 + 4*x53 + 4*x71 - 2*x83);
    h_target_coeffs[68] = (2*x52 - 2*x44 - 2*x8 + 2*x56 + 2*x70 + 2*x74 + 2*x80);
    h_target_coeffs[69] = (x79 - x11 - x43 - x47 - x7 + x83);
    h_target_coeffs[70] = (x12 - x10 - x46 + x48 - x82 + x84);
    h_target_coeffs[71] = (2*x9 + 2*x13 + 2*x45 + 2*x49 - 2*x55 + 2*x57 + 2*x73 - 2*x75 + 2*x81 + 2*x85);
    h_target_coeffs[72] = (2*x28 - 2*x6 - 2*x30 - 2*x42 - 2*x64 + 2*x66 - 2*x78);
    h_target_coeffs[73] = (2*x21 - 2*x19 - 2*x7 + 2*x37 - 2*x39 - 2*x43 - 2*x79);
    h_target_coeffs[74] = (2*x46 - 2*x48 + 4*x54 + 4*x58 - 4*x72 - 4*x76 + 2*x82 - 2*x84);
    h_target_coeffs[75] = (2*x21 - 2*x19 - 2*x7 - 4*x27 - 4*x31 - 2*x37 + 2*x39 - 2*x43 - 4*x51 + 4*x63 + 4*x67 + 4*x69 - 2*x79);
    h_target_coeffs[76] = (2*x6 + 4*x18 + 4*x22 - 2*x28 + 2*x30 - 4*x36 - 4*x40 + 2*x42 - 4*x52 - 2*x64 + 2*x66 + 4*x70 + 2*x78);
    h_target_coeffs[77] = (4*x24 - 2*x12 - 2*x46 - 4*x60 - 2*x84);
    h_target_coeffs[78] = (2*x9 - 2*x13 - 4*x15 + 4*x25 + 4*x33 + 2*x45 - 2*x49 - 2*x55 + 2*x57 - 4*x61 - 2*x73 + 2*x75 + 2*x81 - 2*x85);
    h_target_coeffs[79] = (2*x10 - 4*x16 + 4*x34 + 2*x46 + 2*x84);
    h_target_coeffs[80] = (2*x9 + 2*x13 - 2*x45 - 2*x49 + 2*x55 - 2*x57 - 2*x73 + 2*x75 - 2*x81 - 2*x85);
    h_target_coeffs[81] = (4*x18 - 2*x6 + 4*x22 - 2*x28 + 2*x30 + 4*x36 + 4*x40 + 2*x42 - 4*x52 + 2*x64 - 2*x66 + 4*x70 + 2*x78);
    h_target_coeffs[82] = (2*x19 - 2*x7 - 2*x21 + 4*x27 + 4*x31 - 2*x37 + 2*x39 + 2*x43 + 4*x51 + 4*x63 + 4*x67 - 4*x69 + 2*x79);
    h_target_coeffs[83] = (4*x25 - 2*x13 - 4*x15 - 2*x9 - 4*x33 + 2*x45 + 2*x49 - 2*x55 - 2*x57 - 4*x61 + 2*x73 + 2*x75 - 2*x81 - 2*x85);
    h_target_coeffs[84] = (8*x54 - 8*x24 - 8*x16 + 8*x76);
    h_target_coeffs[85] = (4*x15 - 2*x13 - 2*x9 - 4*x25 - 4*x33 - 2*x45 - 2*x49 + 2*x55 + 2*x57 - 4*x61 - 2*x73 - 2*x75 + 2*x81 + 2*x85);
    h_target_coeffs[86] = (2*x6 + 2*x28 + 2*x30 - 2*x42 - 2*x64 - 2*x66 + 2*x78);
    h_target_coeffs[87] = (2*x7 - 2*x19 - 2*x21 - 4*x27 + 4*x31 + 2*x37 + 2*x39 - 2*x43 - 4*x51 + 4*x63 - 4*x67 - 4*x69 + 2*x79);
    h_target_coeffs[88] = (2*x6 + 4*x18 - 4*x22 - 2*x28 - 2*x30 - 4*x36 + 4*x40 + 2*x42 - 4*x52 + 2*x64 + 2*x66 - 4*x70 - 2*x78);
    h_target_coeffs[89] = (2*x7 + 2*x19 + 2*x21 - 2*x37 - 2*x39 + 2*x43 - 2*x79);
    h_target_coeffs[90] = (x10 - x12 - x46 + x48 - x82 + x84);
    h_target_coeffs[91] = (2*x19 - 2*x7 - 2*x21 + 2*x37 - 2*x39 + 2*x43 + 2*x79);
    h_target_coeffs[92] = (2*x6 + 2*x28 - 2*x30 - 2*x42 + 2*x64 - 2*x66 - 2*x78);
    h_target_coeffs[93] = (2*x46 - 4*x16 - 4*x34 - 2*x10 + 2*x84);
    h_target_coeffs[94] = (2*x9 - 2*x13 + 4*x15 - 4*x25 + 4*x33 - 2*x45 + 2*x49 + 2*x55 - 2*x57 - 4*x61 + 2*x73 - 2*x75 - 2*x81 + 2*x85);
    h_target_coeffs[95] = (2*x12 + 4*x24 - 2*x46 + 4*x60 - 2*x84);
    h_target_coeffs[96] = (2*x7 - 2*x19 - 2*x21 - 2*x37 - 2*x39 - 2*x43 + 2*x79);
    h_target_coeffs[97] = (4*x18 - 2*x6 - 4*x22 - 2*x28 - 2*x30 + 4*x36 - 4*x40 + 2*x42 - 4*x52 - 2*x64 - 2*x66 - 4*x70 - 2*x78);
    h_target_coeffs[98] = (2*x7 + 2*x19 + 2*x21 + 4*x27 - 4*x31 + 2*x37 + 2*x39 + 2*x43 + 4*x51 + 4*x63 - 4*x67 + 4*x69 - 2*x79);
    h_target_coeffs[99] = (2*x28 - 2*x6 + 2*x30 - 2*x42 + 2*x64 + 2*x66 + 2*x78);
    h_target_coeffs[100] = (x10 + x12 - x46 - x48 + x82 + x84);
    h_target_coeffs[101] = (2*x13 - 2*x9 + 2*x45 - 2*x49 - 2*x55 - 2*x57 - 2*x73 - 2*x75 - 2*x81 + 2*x85);
    h_target_coeffs[102] = (2*x46 + 2*x48 + 4*x54 - 4*x58 + 4*x72 - 4*x76 - 2*x82 - 2*x84);
    h_target_coeffs[103] = (2*x13 - 2*x9 - 2*x45 + 2*x49 + 2*x55 + 2*x57 + 2*x73 + 2*x75 + 2*x81 - 2*x85);
    h_target_coeffs[104] = (x82 - x12 - x46 - x48 - x10 + x84);
    h_target_coeffs[105] = MAGMA_C_ONE;
    h_target_coeffs[106] = MAGMA_C_NEG_ONE;
  }
} // end of namespace

#endif

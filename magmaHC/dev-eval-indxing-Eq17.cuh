#ifndef dev_eval_indxing_Eq17_cuh_
#define dev_eval_indxing_Eq17_cuh_
// ============================================================================
// Device function for evaluating the parallel indexing for Hx, Ht, and H of
// Eq17 problem
//
// Modifications
//    Chien  21-09-16:   Originally created
//
// ============================================================================
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>

// -- cuda included --
#include <cuda_runtime.h>

// -- magma included --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min
#include "magma_templates.h"
#include "sync.cuh"
#undef max
#undef min
#include "shuffle.cuh"
#undef max
#undef min
#include "batched_kernel_param.h"

namespace magmaHCWrapper {

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_cdt_Eq17(
        const int tx, float t, magmaFloatComplex s_vector_cdt[coefsCount],
        magmaFloatComplex r_startCoefs[coefsCount], magmaFloatComplex r_targetCoefs[coefsCount])
    {
        #pragma unroll
        for (int i = 0; i < 3; i++) {
            s_vector_cdt[ tx + i * N ] = r_targetCoefs[tx + i * N] * t - r_startCoefs[tx + i * N] * (t-1);
        }
        if (tx < 4) {
          s_vector_cdt[ tx + 3 * N ] = r_targetCoefs[tx + 3 * N] * t - r_startCoefs[tx + 3 * N] * (t-1);
        }
    }

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_Jacobian_Eq17(
        const int tx, magmaFloatComplex *s_track, magmaFloatComplex r_cgesvA[N], const magma_int_t* __restrict__ d_const_mat_s,
        const magma_int_t* __restrict__ d_const_mat_X, const magma_int_t* __restrict__ d_const_mat_Y,
        magmaFloatComplex s_cdt[coefsCount])
    {
        // -- the maximal terms of Hx in Eq17 problem is 4 --
        #pragma unroll
        for(int i = 0; i < N; i++) {
          r_cgesvA[i] = d_const_mat_s[tx + i*N] * s_track[ d_const_mat_X[tx + i*N] ] * s_cdt[ d_const_mat_Y[tx + i*N] ]
                      + d_const_mat_s[tx + i*N + N*N] * s_track[ d_const_mat_X[tx + i*N + N*N] ] * s_cdt[ d_const_mat_Y[tx + i*N + N*N] ]
                      + d_const_mat_s[tx + i*N + 2*N*N] * s_track[ d_const_mat_X[tx + i*N + 2*N*N] ] * s_cdt[ d_const_mat_Y[tx + i*N + 2*N*N] ]
                      + d_const_mat_s[tx + i*N + 3*N*N] * s_track[ d_const_mat_X[tx + i*N + 3*N*N] ] * s_cdt[ d_const_mat_Y[tx + i*N + 3*N*N] ];
            //r_cgesvA[i] = s_cdt[ d_const_mat_Y[tx + i*N] ];
        }
    }

    template<int N>
    __device__ __inline__ void
    eval_Ht_Eq17(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_Ht_const_mat_X,
      const magma_int_t* __restrict__ d_Ht_const_mat_Y, const magmaFloatComplex* __restrict__ d_const_cd)
    {
      // -- the maximal terms of Ht in Eq17 problem is 12 --
      // -- 120 = N*maximal terms of Ht
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_Ht_const_mat_X[tx] ] * s_track[ d_Ht_const_mat_X[tx + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + N] * s_track[ d_Ht_const_mat_X[tx + N] ] * s_track[ d_Ht_const_mat_X[tx + N + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + N] ]
               + d_Ht_const_mat_s[tx + 2*N] * s_track[ d_Ht_const_mat_X[tx + 2*N] ] * s_track[ d_Ht_const_mat_X[tx + 2*N + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 2*N] ]
               + d_Ht_const_mat_s[tx + 3*N] * s_track[ d_Ht_const_mat_X[tx + 3*N] ] * s_track[ d_Ht_const_mat_X[tx + 3*N + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 3*N] ]
               + d_Ht_const_mat_s[tx + 4*N] * s_track[ d_Ht_const_mat_X[tx + 4*N] ] * s_track[ d_Ht_const_mat_X[tx + 4*N + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 4*N] ]
               + d_Ht_const_mat_s[tx + 5*N] * s_track[ d_Ht_const_mat_X[tx + 5*N] ] * s_track[ d_Ht_const_mat_X[tx + 5*N + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 5*N] ]
               + d_Ht_const_mat_s[tx + 6*N] * s_track[ d_Ht_const_mat_X[tx + 6*N] ] * s_track[ d_Ht_const_mat_X[tx + 6*N + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 6*N] ]
               + d_Ht_const_mat_s[tx + 7*N] * s_track[ d_Ht_const_mat_X[tx + 7*N] ] * s_track[ d_Ht_const_mat_X[tx + 7*N + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 7*N] ]
               + d_Ht_const_mat_s[tx + 8*N] * s_track[ d_Ht_const_mat_X[tx + 8*N] ] * s_track[ d_Ht_const_mat_X[tx + 8*N + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 8*N] ]
               + d_Ht_const_mat_s[tx + 9*N] * s_track[ d_Ht_const_mat_X[tx + 9*N] ] * s_track[ d_Ht_const_mat_X[tx + 9*N + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 9*N] ]
               + d_Ht_const_mat_s[tx + 10*N] * s_track[ d_Ht_const_mat_X[tx + 10*N] ] * s_track[ d_Ht_const_mat_X[tx + 10*N + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 10*N] ]
               + d_Ht_const_mat_s[tx + 11*N] * s_track[ d_Ht_const_mat_X[tx + 11*N] ] * s_track[ d_Ht_const_mat_X[tx + 11*N + 120] ] * d_const_cd[ d_Ht_const_mat_Y[tx + 11*N] ];
    }

    template<int N, int coefsCount>
    __device__ __inline__ void
    eval_H_Eq17(
      const int tx, magmaFloatComplex *s_track, magmaFloatComplex &r_cgesvB,
      const magma_int_t* __restrict__ d_Ht_const_mat_s, const magma_int_t* __restrict__ d_H_const_mat_X,
      const magma_int_t* __restrict__ d_H_const_mat_Y, magmaFloatComplex s_cdt[coefsCount])
    {
      // -- same as Ht, except replacing cd by cdt --
      // -- 120 = N*maximal terms of Ht
      r_cgesvB = d_Ht_const_mat_s[tx] * s_track[ d_H_const_mat_X[tx] ] * s_track[ d_H_const_mat_X[tx + 120] ] * s_cdt[ d_H_const_mat_Y[tx] ]
               + d_Ht_const_mat_s[tx + N] * s_track[ d_H_const_mat_X[tx + N] ] * s_track[ d_H_const_mat_X[tx + N + 120] ] * s_cdt[ d_H_const_mat_Y[tx + N] ]
               + d_Ht_const_mat_s[tx + 2*N] * s_track[ d_H_const_mat_X[tx + 2*N] ] * s_track[ d_H_const_mat_X[tx + 2*N + 120] ] * s_cdt[ d_H_const_mat_Y[tx + 2*N] ]
               + d_Ht_const_mat_s[tx + 3*N] * s_track[ d_H_const_mat_X[tx + 3*N] ] * s_track[ d_H_const_mat_X[tx + 3*N + 120] ] * s_cdt[ d_H_const_mat_Y[tx + 3*N] ]
               + d_Ht_const_mat_s[tx + 4*N] * s_track[ d_H_const_mat_X[tx + 4*N] ] * s_track[ d_H_const_mat_X[tx + 4*N + 120] ] * s_cdt[ d_H_const_mat_Y[tx + 4*N] ]
               + d_Ht_const_mat_s[tx + 5*N] * s_track[ d_H_const_mat_X[tx + 5*N] ] * s_track[ d_H_const_mat_X[tx + 5*N + 120] ] * s_cdt[ d_H_const_mat_Y[tx + 5*N] ]
               + d_Ht_const_mat_s[tx + 6*N] * s_track[ d_H_const_mat_X[tx + 6*N] ] * s_track[ d_H_const_mat_X[tx + 6*N + 120] ] * s_cdt[ d_H_const_mat_Y[tx + 6*N] ]
               + d_Ht_const_mat_s[tx + 7*N] * s_track[ d_H_const_mat_X[tx + 7*N] ] * s_track[ d_H_const_mat_X[tx + 7*N + 120] ] * s_cdt[ d_H_const_mat_Y[tx + 7*N] ]
               + d_Ht_const_mat_s[tx + 8*N] * s_track[ d_H_const_mat_X[tx + 8*N] ] * s_track[ d_H_const_mat_X[tx + 8*N + 120] ] * s_cdt[ d_H_const_mat_Y[tx + 8*N] ]
               + d_Ht_const_mat_s[tx + 9*N] * s_track[ d_H_const_mat_X[tx + 9*N] ] * s_track[ d_H_const_mat_X[tx + 9*N + 120] ] * s_cdt[ d_H_const_mat_Y[tx + 9*N] ]
               + d_Ht_const_mat_s[tx + 10*N] * s_track[ d_H_const_mat_X[tx + 10*N] ] * s_track[ d_H_const_mat_X[tx + 10*N + 120] ] * s_cdt[ d_H_const_mat_Y[tx + 10*N] ]
               + d_Ht_const_mat_s[tx + 11*N] * s_track[ d_H_const_mat_X[tx + 11*N] ] * s_track[ d_H_const_mat_X[tx + 11*N + 120] ] * s_cdt[ d_H_const_mat_Y[tx + 11*N] ];
    }
}

#endif

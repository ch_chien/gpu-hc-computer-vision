#ifndef params2coeffs_homo3pt_h
#define params2coeffs_homo3pt_h
// =======================================================================
//
// Modifications
//    Chien  21-10-26:   Initially Created
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min

namespace magmaHCWrapper {

  void params2coeffs_homo3pt(magmaFloatComplex *h_target_params, magmaFloatComplex *h_target_coeffs)
  {
    magmaFloatComplex x9 = h_target_params[0];
    magmaFloatComplex x10 = h_target_params[1];
    magmaFloatComplex x11 = h_target_params[2];
    magmaFloatComplex x12 = h_target_params[3];
    magmaFloatComplex x13 = h_target_params[4];
    magmaFloatComplex x14 = h_target_params[5];
    magmaFloatComplex x15 = h_target_params[6];
    magmaFloatComplex x16 = h_target_params[7];
    magmaFloatComplex x17 = h_target_params[8];
    magmaFloatComplex x18 = h_target_params[9];
    magmaFloatComplex x19 = h_target_params[10];
    magmaFloatComplex x20 = h_target_params[11];
    magmaFloatComplex x21 = h_target_params[12];
    magmaFloatComplex x22 = h_target_params[13];
    magmaFloatComplex x23 = h_target_params[14];
    magmaFloatComplex x24 = h_target_params[15];
    magmaFloatComplex x25 = h_target_params[16];
    magmaFloatComplex x26 = h_target_params[17];

    h_target_coeffs[0] = (x9*x24);
    h_target_coeffs[1] = (-x9*x21);
    h_target_coeffs[2] = (x12*x24);
    h_target_coeffs[3] = (-x12*x21);
    h_target_coeffs[4] = (x15*x24);
    h_target_coeffs[5] = (-x15*x21);
    h_target_coeffs[6] = (-x12*x24);
    h_target_coeffs[7] = (-x9*x24);
    h_target_coeffs[8] = (x15*x21);
    h_target_coeffs[9] = (x9*x18);
    h_target_coeffs[10] = (x12*x18);
    h_target_coeffs[11] = (-x15*x24);
    h_target_coeffs[12] = (x15*x18);
    h_target_coeffs[13] = (-x15*x18);
    h_target_coeffs[14] = (x10*x25);
    h_target_coeffs[15] = (-x10*x22);
    h_target_coeffs[16] = (x13*x25);
    h_target_coeffs[17] = (-x13*x22);
    h_target_coeffs[18] = (x16*x25);
    h_target_coeffs[19] = (-x16*x22);
    h_target_coeffs[20] = (-x13*x25);
    h_target_coeffs[21] = (-x10*x25);
    h_target_coeffs[22] = (x16*x22);
    h_target_coeffs[23] = (x10*x19);
    h_target_coeffs[24] = (x13*x19);
    h_target_coeffs[25] = (-x16*x25);
    h_target_coeffs[26] = (x16*x19);
    h_target_coeffs[27] = (-x16*x19);
    h_target_coeffs[28] = (x11*x26);
    h_target_coeffs[29] = (-x11*x23);
    h_target_coeffs[30] = (x14*x26);
    h_target_coeffs[31] = (-x14*x23);
    h_target_coeffs[32] = (x17*x26);
    h_target_coeffs[33] = (-x17*x23);
    h_target_coeffs[34] = (-x14*x26);
    h_target_coeffs[35] = (-x11*x26);
    h_target_coeffs[36] = (x17*x23);
    h_target_coeffs[37] = (x11*x20);
    h_target_coeffs[38] = (x14*x20);
    h_target_coeffs[39] = (-x17*x26);
    h_target_coeffs[40] = (x17*x20);
    h_target_coeffs[41] = (-x17*x20);
    h_target_coeffs[42] = MAGMA_C_NEG_ONE;
    h_target_coeffs[43] = MAGMA_C_ONE;
  }
} // end of namespace

#endif

#ifndef params2coeffs_R6P1lin_h
#define params2coeffs_R6P1lin_h
// =======================================================================
//
// Modifications
//    Chien  21-11-03:   Initially Created
//
// =======================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// -- magma --
#include "flops.h"
#include "magma_v2.h"
#include "magma_lapack.h"
#include "magma_internal.h"
#undef max
#undef min

namespace magmaHCWrapper {

  void params2coeffs_R6P1lin(magmaFloatComplex *h_target_params, magmaFloatComplex *h_target_coeffs)
  {
    magmaFloatComplex x19 = h_target_params[0];
    magmaFloatComplex x20 = h_target_params[1];
    magmaFloatComplex x21 = h_target_params[2];
    magmaFloatComplex x22 = h_target_params[3];
    magmaFloatComplex x23 = h_target_params[4];
    magmaFloatComplex x24 = h_target_params[5];
    magmaFloatComplex x25 = h_target_params[6];
    magmaFloatComplex x26 = h_target_params[7];
    magmaFloatComplex x27 = h_target_params[8];
    magmaFloatComplex x28 = h_target_params[9];
    magmaFloatComplex x29 = h_target_params[10];
    magmaFloatComplex x30 = h_target_params[11];
    magmaFloatComplex x31 = h_target_params[12];
    magmaFloatComplex x32 = h_target_params[13];
    magmaFloatComplex x33 = h_target_params[14];
    magmaFloatComplex x34 = h_target_params[15];
    magmaFloatComplex x35 = h_target_params[16];
    magmaFloatComplex x36 = h_target_params[17];
    magmaFloatComplex x37 = h_target_params[18];
    magmaFloatComplex x38 = h_target_params[19];
    magmaFloatComplex x39 = h_target_params[20];
    magmaFloatComplex x40 = h_target_params[21];
    magmaFloatComplex x41 = h_target_params[22];
    magmaFloatComplex x42 = h_target_params[23];
    magmaFloatComplex x43 = h_target_params[24];
    magmaFloatComplex x44 = h_target_params[25];
    magmaFloatComplex x45 = h_target_params[26];
    magmaFloatComplex x46 = h_target_params[27];
    magmaFloatComplex x47 = h_target_params[28];
    magmaFloatComplex x48 = h_target_params[29];
    magmaFloatComplex x49 = h_target_params[30];
    magmaFloatComplex x50 = h_target_params[31];
    magmaFloatComplex x51 = h_target_params[32];
    magmaFloatComplex x52 = h_target_params[33];
    magmaFloatComplex x53 = h_target_params[34];
    magmaFloatComplex x54 = h_target_params[35];
    magmaFloatComplex x55 = h_target_params[36];

    h_target_coeffs[0] = (x20*x40 - x19*x40);
    h_target_coeffs[1] = (x19*x39 - x20*x39);
    h_target_coeffs[2] = MAGMA_C_NEG_ONE;
    h_target_coeffs[3] = (x19 - x20);
    h_target_coeffs[4] = (x26);
    h_target_coeffs[5] = (-x38);
    h_target_coeffs[6] = (2*x20*x38 - 2*x19*x38);
    h_target_coeffs[7] = (-2*x39);
    h_target_coeffs[8] = (2*x19*x38 - 2*x20*x38);
    h_target_coeffs[9] = (-2*x40);
    h_target_coeffs[10] = (2*x19*x39 - 2*x20*x39);
    h_target_coeffs[11] = (2*x19*x40 - 2*x20*x40);
    h_target_coeffs[12] = (x20*x39 - x19*x39);
    h_target_coeffs[13] = (x38);
    h_target_coeffs[14] = (2*x20*x40 - 2*x19*x40);
    h_target_coeffs[15] = (x19*x40 - x20*x40);
    h_target_coeffs[16] = (2*x39);
    h_target_coeffs[17] = (x19*x38 - x20*x38);
    h_target_coeffs[18] = (x27);
    h_target_coeffs[19] = (x39);
    h_target_coeffs[20] = (-2*x38);
    h_target_coeffs[21] = (2*x20*x39 - 2*x19*x39);
    h_target_coeffs[22] = (2*x40);
    h_target_coeffs[23] = (x20*x38 - x19*x38);
    h_target_coeffs[24] = (-x39);
    h_target_coeffs[25] = MAGMA_C_ONE;
    h_target_coeffs[26] = (x40);
    h_target_coeffs[27] = (2*x38);
    h_target_coeffs[28] = (-x40);
    h_target_coeffs[29] = (x21*x43 - x19*x43);
    h_target_coeffs[30] = (x19*x42 - x21*x42);
    h_target_coeffs[31] = (x19 - x21);
    h_target_coeffs[32] = (x28);
    h_target_coeffs[33] = (-x41);
    h_target_coeffs[34] = (2*x21*x41 - 2*x19*x41);
    h_target_coeffs[35] = (-2*x42);
    h_target_coeffs[36] = (2*x19*x41 - 2*x21*x41);
    h_target_coeffs[37] = (-2*x43);
    h_target_coeffs[38] = (2*x19*x42 - 2*x21*x42);
    h_target_coeffs[39] = (2*x19*x43 - 2*x21*x43);
    h_target_coeffs[40] = (x21*x42 - x19*x42);
    h_target_coeffs[41] = (x41);
    h_target_coeffs[42] = (2*x21*x43 - 2*x19*x43);
    h_target_coeffs[43] = (x19*x43 - x21*x43);
    h_target_coeffs[44] = (2*x42);
    h_target_coeffs[45] = (x19*x41 - x21*x41);
    h_target_coeffs[46] = (x29);
    h_target_coeffs[47] = (x42);
    h_target_coeffs[48] = (-2*x41);
    h_target_coeffs[49] = (2*x21*x42 - 2*x19*x42);
    h_target_coeffs[50] = (2*x43);
    h_target_coeffs[51] = (x21*x41 - x19*x41);
    h_target_coeffs[52] = (-x42);
    h_target_coeffs[53] = (x43);
    h_target_coeffs[54] = (2*x41);
    h_target_coeffs[55] = (-x43);
    h_target_coeffs[56] = (x22*x46 - x19*x46);
    h_target_coeffs[57] = (x19*x45 - x22*x45);
    h_target_coeffs[58] = (x19 - x22);
    h_target_coeffs[59] = (x30);
    h_target_coeffs[60] = (-x44);
    h_target_coeffs[61] = (2*x22*x44 - 2*x19*x44);
    h_target_coeffs[62] = (-2*x45);
    h_target_coeffs[63] = (2*x19*x44 - 2*x22*x44);
    h_target_coeffs[64] = (-2*x46);
    h_target_coeffs[65] = (2*x19*x45 - 2*x22*x45);
    h_target_coeffs[66] = (2*x19*x46 - 2*x22*x46);
    h_target_coeffs[67] = (x22*x45 - x19*x45);
    h_target_coeffs[68] = (x44);
    h_target_coeffs[69] = (2*x22*x46 - 2*x19*x46);
    h_target_coeffs[70] = (x19*x46 - x22*x46);
    h_target_coeffs[71] = (2*x45);
    h_target_coeffs[72] = (x19*x44 - x22*x44);
    h_target_coeffs[73] = (x31);
    h_target_coeffs[74] = (x45);
    h_target_coeffs[75] = (-2*x44);
    h_target_coeffs[76] = (2*x22*x45 - 2*x19*x45);
    h_target_coeffs[77] = (2*x46);
    h_target_coeffs[78] = (x22*x44 - x19*x44);
    h_target_coeffs[79] = (-x45);
    h_target_coeffs[80] = (x46);
    h_target_coeffs[81] = (2*x44);
    h_target_coeffs[82] = (-x46);
    h_target_coeffs[83] = (x23*x49 - x19*x49);
    h_target_coeffs[84] = (x19*x48 - x23*x48);
    h_target_coeffs[85] = (x19 - x23);
    h_target_coeffs[86] = (x32);
    h_target_coeffs[87] = (-x47);
    h_target_coeffs[88] = (2*x23*x47 - 2*x19*x47);
    h_target_coeffs[89] = (-2*x48);
    h_target_coeffs[90] = (2*x19*x47 - 2*x23*x47);
    h_target_coeffs[91] = (-2*x49);
    h_target_coeffs[92] = (2*x19*x48 - 2*x23*x48);
    h_target_coeffs[93] = (2*x19*x49 - 2*x23*x49);
    h_target_coeffs[94] = (x23*x48 - x19*x48);
    h_target_coeffs[95] = (x47);
    h_target_coeffs[96] = (2*x23*x49 - 2*x19*x49);
    h_target_coeffs[97] = (x19*x49 - x23*x49);
    h_target_coeffs[98] = (2*x48);
    h_target_coeffs[99] = (x19*x47 - x23*x47);
    h_target_coeffs[100] = (x33);
    h_target_coeffs[101] = (x48);
    h_target_coeffs[102] = (-2*x47);
    h_target_coeffs[103] = (2*x23*x48 - 2*x19*x48);
    h_target_coeffs[104] = (2*x49);
    h_target_coeffs[105] = (x23*x47 - x19*x47);
    h_target_coeffs[106] = (-x48);
    h_target_coeffs[107] = (x49);
    h_target_coeffs[108] = (2*x47);
    h_target_coeffs[109] = (-x49);
    h_target_coeffs[110] = (x24*x52 - x19*x52);
    h_target_coeffs[111] = (x19*x51 - x24*x51);
    h_target_coeffs[112] = (x19 - x24);
    h_target_coeffs[113] = (x34);
    h_target_coeffs[114] = (-x50);
    h_target_coeffs[115] = (2*x24*x50 - 2*x19*x50);
    h_target_coeffs[116] = (-2*x51);
    h_target_coeffs[117] = (2*x19*x50 - 2*x24*x50);
    h_target_coeffs[118] = (-2*x52);
    h_target_coeffs[119] = (2*x19*x51 - 2*x24*x51);
    h_target_coeffs[120] = (2*x19*x52 - 2*x24*x52);
    h_target_coeffs[121] = (x24*x51 - x19*x51);
    h_target_coeffs[122] = (x50);
    h_target_coeffs[123] = (2*x24*x52 - 2*x19*x52);
    h_target_coeffs[124] = (x19*x52 - x24*x52);
    h_target_coeffs[125] = (2*x51);
    h_target_coeffs[126] = (x19*x50 - x24*x50);
    h_target_coeffs[127] = (x35);
    h_target_coeffs[128] = (x51);
    h_target_coeffs[129] = (-2*x50);
    h_target_coeffs[130] = (2*x24*x51 - 2*x19*x51);
    h_target_coeffs[131] = (2*x52);
    h_target_coeffs[132] = (x24*x50 - x19*x50);
    h_target_coeffs[133] = (-x51);
    h_target_coeffs[134] = (x52);
    h_target_coeffs[135] = (2*x50);
    h_target_coeffs[136] = (-x52);
    h_target_coeffs[137] = (x25*x55 - x19*x55);
    h_target_coeffs[138] = (x19*x54 - x25*x54);
    h_target_coeffs[139] = (x19 - x25);
    h_target_coeffs[140] = (x36);
    h_target_coeffs[141] = (-x53);
    h_target_coeffs[142] = (2*x25*x53 - 2*x19*x53);
    h_target_coeffs[143] = (-2*x54);
    h_target_coeffs[144] = (2*x19*x53 - 2*x25*x53);
    h_target_coeffs[145] = (-2*x55);
    h_target_coeffs[146] = (2*x19*x54 - 2*x25*x54);
    h_target_coeffs[147] = (2*x19*x55 - 2*x25*x55);
    h_target_coeffs[148] = (x25*x54 - x19*x54);
    h_target_coeffs[149] = (x53);
    h_target_coeffs[150] = (2*x25*x55 - 2*x19*x55);
    h_target_coeffs[151] = (x19*x55 - x25*x55);
    h_target_coeffs[152] = (2*x54);
    h_target_coeffs[153] = (x19*x53 - x25*x53);
    h_target_coeffs[154] = (x37);
    h_target_coeffs[155] = (x54);
    h_target_coeffs[156] = (-2*x53);
    h_target_coeffs[157] = (2*x25*x54 - 2*x19*x54);
    h_target_coeffs[158] = (2*x55);
    h_target_coeffs[159] = (x25*x53 - x19*x53);
    h_target_coeffs[160] = (-x54);
    h_target_coeffs[161] = (x55);
    h_target_coeffs[162] = (2*x53);
    h_target_coeffs[163] = (-x55);
  }
} // end of namespace

#endif


# 1. How to use the code
(1) clone the repo
```bash
git clone https://ch_chien@bitbucket.org/ch_chien/gpu-hc-computer-vision.git
```
(2) under the repo folder, create a 'build' directory
```bash
mkdir build
```
(3) enter the build folder
```bash
cd build
```
(4) create a make file
```bash
cmake ..
```
(5) compile the entire code
```bash
make -j
```
(6) enter the bin foler
```bash
cd bin
```
(7) run the code by specifically typing input arguments
```bash
./magmaHC-main <input-argument> <command>
```

## Input arguments and the associated commands are listed below:
```
<input-argument>      <command>
       -p             <problem-name>         # (or --problem) : computer vision minimal problem 
```
      
## Notice:
```
If not specified, the help message will be automatically shown.
```

## Execution examples:
```
./magmaHC-main -p 4vTrg    # solve 4-view triangulation problem
```
  
# 2. Additional notices:
```
(1) You need to change the directories where the updated magma "gesv_batched branch" is installed in
    line 119~122  in CMakeLists.txt
    line 259~262  in magmaHC/CMakeLists.txt
(2) You may also need to change the installed libraries of cuda and magma in
    line 266~269  in magmaHC/CMakeLists.txt
(3) The cblas.h file directory in line 18 of magmaHC/cpu-compute/cpu-compute.h is also needed to be changed
(4) The absolute directories appeared in line 49 in cmd/magmaHC-main.cu should also be changed, depending on the routes of your local machine.
```
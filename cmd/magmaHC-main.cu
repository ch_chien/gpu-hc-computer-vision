#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <chrono>
#include <string>
#include <random>
// ============================================================================
// main function
//
// Modifications
//    Chien  21-06-10    Revised from the original alea6 main code
//    Chien  21-06-14    Add start systems
//    Chien  21-06-24    Edit the files read directories
//
// ============================================================================
// -- nvidia cuda --
#include <cuda.h>
#include <cuda_runtime.h>

// -- magma --
#include "magma_v2.h"

// -- magma --
#include "magmaHC/magmaHC-problems.cuh"

// -- p2c --
#include "../magmaHC/params2coeffs_3vTrg.h"
#include "../magmaHC/params2coeffs_4vTrg.h"
#include "../magmaHC/params2coeffs_Eq17.h"
#include "../magmaHC/params2coeffs_optpose4pt.h"
#include "../magmaHC/params2coeffs_homo3pt.h"
#include "../magmaHC/params2coeffs_dualTDOA.h"
#include "../magmaHC/params2coeffs_optimalPnPQ.h"
#include "../magmaHC/params2coeffs_9pt2radial.h"
#include "../magmaHC/params2coeffs_pose_quiver.h"
#include "../magmaHC/params2coeffs_P3P.h"
#include "../magmaHC/params2coeffs_P3P_v2.h"
#include "../magmaHC/params2coeffs_2vTrg_distortion.h"
#include "../magmaHC/params2coeffs_5ptRelativePose.h"
#include "../magmaHC/params2coeffs_5ptRelativePose_v2.h"
#include "../magmaHC/params2coeffs_5ptRelativePose_v4.h"
#include "../magmaHC/params2coeffs_trifocal_rel_pose_f.h"
#include "../magmaHC/params2coeffs_R6P1lin.h"

// -- global repo directory --
std::string repo_dir = "/users/cchien3/data/cchien3/gpu-hc-computer-vision/";

int main(int argc, char **argv) {
  --argc; ++argv;
  std::string arg;
  int argIndx = 0;
  int argTotal = 4;
  std::string HC_problem = "default";

  // -- declare class objects (put the long lasting object in dynamic memory) --
  magmaHCWrapper::problem_params* pp = new magmaHCWrapper::problem_params;
  magmaHCWrapper::const_mats* cm = new magmaHCWrapper::const_mats;

  if (argc) {
    arg = std::string(*argv);
    if (arg == "-h" || arg == "--help") {
      pp->print_usage();
      exit(1);
    }
    else if (argc <= argTotal) {
      while(argIndx <= argTotal-1) {
        if (arg == "-p" || arg == "--problem") {
          argv++;
          arg = std::string(*argv);
          HC_problem = arg;
          argIndx+=2;
          break;
        }
        else {
          std::cerr<<"invalid input arguments! See examples: \n";
          pp->print_usage();
          exit(1);
        }
        argv++;
      }
    }
    else if (argc > argTotal) {
      std::cerr<<"too many arguments!\n";
      pp->print_usage();
      exit(1);
    }
  }
  else {
    pp->print_usage();
    exit(1);
  }

  magmaFloatComplex *h_startSols;
  magmaFloatComplex *h_Track;
  magmaFloatComplex *h_startCoefs;
  magmaFloatComplex *h_targetCoefs;
  magmaFloatComplex *h_targetParams;

  // -- files to be read --
  std::string problem_filename = repo_dir;
  problem_filename.append("problems/");
  problem_filename.append(HC_problem);

  // -- files to be written --
  std::ofstream track_sols_file;
  std::ofstream target_coeffs_file;
  std::ofstream track_success_file;
  std::string write_sols_file_dir = repo_dir;
  std::string write_coeffs_file_dir = repo_dir;
  std::string write_success_file_dir = repo_dir;
  write_sols_file_dir.append("converged_HC_tracks.txt");
  write_coeffs_file_dir.append("target_coefficients.txt");
  write_success_file_dir.append("HC_number_of_successes.txt");
  track_sols_file.open(write_sols_file_dir);
  target_coeffs_file.open(write_coeffs_file_dir);
  track_success_file.open(write_success_file_dir);
  if ( !track_sols_file.is_open() || !target_coeffs_file.is_open() || !track_success_file.is_open() )
    std::cout<<"files cannot be opened!"<<std::endl;

  pp->define_problem_params(problem_filename, HC_problem);

  //std::cout<<pp->numOfCoeffs<<std::endl;

  // -- allocate tracks and coeffs arrays in cpu --
  magma_cmalloc_cpu( &h_startSols, pp->numOfTracks*(pp->numOfVars+1) );
  magma_cmalloc_cpu( &h_Track, pp->numOfTracks*(pp->numOfVars+1) );
  magma_cmalloc_cpu( &h_startCoefs, pp->numOfCoeffs );
  magma_cmalloc_cpu( &h_targetCoefs, pp->numOfCoeffs );
  magma_cmalloc_cpu( &h_targetParams, pp->numOfTargetParams );

  // =============================================================================
  // -- read files: start solutions, start coefficients, and target parameters --
  // =============================================================================
  std::string targetParam_filename_test = problem_filename;
  std::string startCoef_filename_test = problem_filename;
  std::string startSols_filename_test = problem_filename;
  startSols_filename_test.append("/start_sols.txt");
  targetParam_filename_test.append("/target_params.txt");
  startCoef_filename_test.append("/start_coeffs.txt");
  std::fstream startCoef_file;
  std::fstream targetParams_file;
  std::fstream startSols_file;
  bool read_success = 0;
  bool start_sols_read_success = 0;
  bool start_coeffs_read_success = 0;
  bool targetParams_read_success = 0;

  float s_real, s_imag;
  int d = 0, i = 0; 
  startSols_file.open(startSols_filename_test, std::ios_base::in);
  std::cout<<"============= start solutions h_startSols ============"<<std::endl;
  if (!startSols_file) {
    std::cerr << "problem start solutions file not existed!\n";
  }
  else {
    while (startSols_file >> s_real >> s_imag) {
      (h_startSols + i * (pp->numOfVars+1))[d] = MAGMA_C_MAKE(s_real, s_imag);
      (h_Track + i * (pp->numOfVars+1))[d] = MAGMA_C_MAKE(s_real, s_imag);
      if (d < pp->numOfVars-1) {
        d++;
      }
      else {
        d = 0;
        i++;
      }
    }
    for(int k = 0; k < pp->numOfTracks; k++) {
      (h_startSols + k * (pp->numOfVars+1))[pp->numOfVars] = MAGMA_C_MAKE(1.0, 0.0);
      (h_Track + k * (pp->numOfVars+1))[pp->numOfVars] = MAGMA_C_MAKE(1.0, 0.0);
    }
    start_sols_read_success = 1;
  }

  d = 0;
  startCoef_file.open(startCoef_filename_test, std::ios_base::in);
  if (!startCoef_file) {
    std::cerr << "problem start coefficients file not existed!\n";
  }
  else {
    while (startCoef_file >> s_real >> s_imag) {
      (h_startCoefs)[d] = MAGMA_C_MAKE(s_real, s_imag);
      d++;
    }
    start_coeffs_read_success = 1;
  }

  //magma_cprint(pp->numOfCoeffs, 1, h_startCoefs, pp->numOfCoeffs);

  d = 0;
  // -- read start system coefficients --
  targetParams_file.open(targetParam_filename_test, std::ios_base::in);
  if (!targetParams_file) {
    std::cerr << "problem target parameters file not existed!\n";
  }
  else {
    while (targetParams_file >> s_real >> s_imag) {
      (h_targetParams)[d] = MAGMA_C_MAKE(s_real, s_imag);
      d++;
    }
    targetParams_read_success = 1;
  }

  // ================================= TEST for new idx matrix ======================================
  magma_int_t *h_Hx_idx;
  magma_int_t *h_Ht_idx;
  magma_imalloc_cpu( &h_Hx_idx, pp->numOfVars*pp->numOfVars*pp->Hx_maximal_terms*(pp->Hx_maximal_parts_x+2) );
  magma_imalloc_cpu( &h_Ht_idx, pp->numOfVars*pp->Ht_maximal_terms*(pp->Ht_H_maximal_terms_x+2) );

  std::string filename_Hx = problem_filename;
  std::string filename_Ht = problem_filename;
  filename_Hx.append("/Hx_idx.txt");
  filename_Ht.append("/Ht_idx.txt");
  std::fstream Hx_idx_file;
  std::fstream Ht_idx_file;
  bool Hx_file_read_success = false;
  bool Ht_file_read_success = false;
  int index;
  // ========= 1) Hx read =============
  d = 0;
  Hx_idx_file.open(filename_Hx, std::ios_base::in);
  if (!Hx_idx_file) {
    std::cerr << "problem Hx index matrix file not existed!\n";
  }
  else {
    while (Hx_idx_file >> index) {
      (h_Hx_idx)[d] = index;
      d++;
    }
    Hx_file_read_success = 1;
  }
  // ========= 2) Ht read =============
  d = 0;
  Ht_idx_file.open(filename_Ht, std::ios_base::in);
  if (!Ht_idx_file) {
    std::cerr << "problem Ht index matrix file not existed!\n";
  }
  else {
    while (Ht_idx_file >> index) {
      (h_Ht_idx)[d] = index;
      d++;
    }
    Ht_file_read_success = 1;
  }

/*  if (Hx_file_read_success && Ht_file_read_success) {
    std::cout<<"Hx and Ht files are read successfully!"<<std::endl;
    for(int r = 0; r < pp->numOfVars; r++) {
      for(int c = 0; c < pp->Ht_maximal_terms*(pp->Ht_H_maximal_terms_x+2); c++) {
        std::cout<<h_Ht_idx[c + r*pp->Ht_maximal_terms*(pp->Ht_H_maximal_terms_x+2)]<<"\t";
      }
      std::cout<<std::endl;
    }
  }*/

  // =============================================================================
  // -- run over multiple runs to get converged HC solution tracks --
  // =============================================================================
  for(int runs = 0; runs < 1; runs++) {
    // -- create random complex numbers of target params if needed --
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
    std::normal_distribution<double> distribution(0.0,1.0);
    /*for (int i = 0; i < pp->numOfTargetParams; i++) {
      double number = distribution(generator);
      h_targetParams[i] = MAGMA_C_MAKE(number, 0.0);
    }*/

    //magma_cprint(pp->numOfTargetParams, 1, h_targetParams, pp->numOfTargetParams);
    //magma_cprint(pp->numOfVars+1, 1, h_startSols + 1779*(pp->numOfVars+1), pp->numOfVars+1);

    // -- params2coeffs --
    if (HC_problem == "3vTrg") {
      magmaHCWrapper::params2coeffs_3vTrg(h_targetParams, h_targetCoefs);
    }
    else if (HC_problem == "trifocal_rel_pose_f") {
      magmaHCWrapper::params2coeffs_trifocal_rel_pose_f(h_targetParams, h_targetCoefs);
    }
    else if (HC_problem == "4vTrg") {
      magmaHCWrapper::params2coeffs_4vTrg(h_targetParams, h_targetCoefs);
    }
    else if (HC_problem == "Eq17") {
      magmaHCWrapper::params2coeffs_Eq17(h_targetParams, h_targetCoefs);
    }
    else if (HC_problem == "optpose4pt") {
      magmaHCWrapper::params2coeffs_optpose4pt(h_targetParams, h_targetCoefs);
    }
    else if (HC_problem == "homo3pt") {
      magmaHCWrapper::params2coeffs_homo3pt(h_targetParams, h_targetCoefs);
    }
    else if (HC_problem == "dualTDOA") {
      magmaHCWrapper::params2coeffs_dualTDOA(h_targetParams, h_targetCoefs);
    }
    else if (HC_problem == "optimalPnPQ") {
      magmaHCWrapper::params2coeffs_optimalPnPQ(h_targetParams, h_targetCoefs);
    }
    else if (HC_problem == "9pt2radial") {
      magmaHCWrapper::params2coeffs_9pt2radial(h_targetParams, h_targetCoefs);
    }
    else if (HC_problem == "pose_quiver") {
      magmaHCWrapper::params2coeffs_pose_quiver(h_targetParams, h_targetCoefs);
    }
    else if (HC_problem == "5ptRelativePose") {
      magmaHCWrapper::params2coeffs_5ptRelativePose(h_targetParams, h_targetCoefs);
    }
    else if (HC_problem == "5ptRelativePose_v2") {
      magmaHCWrapper::params2coeffs_5ptRelativePose_v2(h_targetParams, h_targetCoefs);
    }
    else if (HC_problem == "5ptRelativePose_v4") {
      magmaHCWrapper::params2coeffs_5ptRelativePose_v4(h_targetParams, h_targetCoefs);
    }
    else if (HC_problem == "P3P") {
      magmaHCWrapper::params2coeffs_P3P(h_targetParams, h_targetCoefs);
    }
    else if (HC_problem == "P3P_v2") {
      magmaHCWrapper::params2coeffs_P3P_v2(h_targetParams, h_targetCoefs);
    }
    else if (HC_problem == "2vTrg_distortion") {
      magmaHCWrapper::params2coeffs_2vTrg_distortion(h_targetParams, h_targetCoefs);
    }
    else if (HC_problem == "R6P1lin") {
      magmaHCWrapper::params2coeffs_R6P1lin(h_targetParams, h_targetCoefs);
    }

    // -- write target coefficients to the file --
    for (int c = 0; c < pp->numOfCoeffs; c++) {
      target_coeffs_file << MAGMA_C_REAL(h_targetCoefs[c]) << "\n";
    }

    magma_cprint(pp->numOfCoeffs, 1, h_targetCoefs, pp->numOfCoeffs);

    // -- call homotopy continuation solver --
    read_success = (start_sols_read_success && start_coeffs_read_success && targetParams_read_success);
    if (read_success) {
      magmaHCWrapper::homotopy_continuation_solver(h_startSols, h_Track, h_startCoefs, h_targetCoefs, h_Hx_idx, h_Ht_idx, pp, cm, HC_problem, track_sols_file, track_success_file);
    }
    else {
      std::cout<<"read files failed!"<<std::endl;
      exit(1);
    }
  }

  track_sols_file.close();
  target_coeffs_file.close();
  track_success_file.close();

  delete pp;
  delete cm;
  magma_free_cpu( h_startSols );
  magma_free_cpu( h_Track );
  magma_free_cpu( h_startCoefs );
  magma_free_cpu( h_targetCoefs );
  magma_free_cpu( h_targetParams );

  // ========= test =============
  magma_free_cpu( h_Hx_idx );
  magma_free_cpu( h_Ht_idx );

  return 0;
}
